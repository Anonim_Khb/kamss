<?php

return [

    'menu' => 'Group manage',

    'title' => [
        'prepend' => 'Admin',
        'index' => 'Groups',
        'create' => 'Group create',
        'update' => 'Group update',
        'add-clients' => 'Add clients',
    ],

    'index' => [
        'title' => 'Groups manage',
        'create' => 'Create',
        'table' => [
            'name' => 'Name',
            'description' => 'Description',
            'icon' => 'Icon',
            'actions' => 'Actions',
            'update-tooltip' => 'Update',
            'delete-tooltip' => 'Delete',
            'add-clients-tooltip' => 'Add clients in group',
        ],
    ],

    'create' => [
        'title' => 'Group create',
        'btn' => 'Create',
        'placeholder-name' => 'Enter group name',
        'placeholder-description' => 'Enter group description',
        'placeholder-icon' => 'Group icon (fa fa-user fa-fw)',
    ],

    'update' => [
        'title' => 'Group update',
        'btn' => 'Update',
    ],

    'search' => [
        'no-results' => 'No results'
    ],

    'notification' => [
        'group-create-or-update' => 'Group successfully created/updated',
        'group-destroy' => 'Group successfully removed',
    ],

    'add-clients' => [
        'title' => 'Add clients in group <b>:group</b><i class=":icon fa-lg"></i>',
        'add-in-group-tooltip' => 'Add in group',
    ],

];
