<?php

namespace App\Jobs;

use App\Http\DTO\companiesExcelDTO;
use App\Jobs\Job;
use App\Models\Company;
use App\Models\ImportError;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CompaniesImportProcessing extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $companies;
    protected $user_id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($companies, $user_id)
    {
        $this->companies = $companies;
        $this->user_id = $user_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(companiesExcelDTO $dto, ImportError $error)
    {
        foreach ($this->companies as $company) {
            $array = [
                'name' => isset($company[$dto->getCompany('import')]) ? $company[$dto->getCompany('import')] : null,
                'inn' => isset($company[$dto->getInn('import')]) ? $company[$dto->getInn('import')] : null,
                'type' => isset($company[$dto->getType('import')]) ? $company[$dto->getType('import')] : null
            ];

            $v = \Validator::make($array, Company::rulesCreate('name', 'inn'));

            if ($v->fails()) {
                is_null($array['name']) ?: $error::createError($this->user_id, $error::TYPE_COMPANIES, $array, $v->errors()->all());
            } else {
                Company::createCompany($this->user_id, $array['type'], $array['name'], $array['inn']);
            }
        }
    }
}
