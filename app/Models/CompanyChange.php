<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompanyChange extends Model
{
    protected $table = 'company_changes';

    protected $fillable = [
        'author_id', 'company_id', 'name', 'type', 'inn'
    ];

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = e(str_replace(['\'', '"'], '', $value));
    }

    public function setTypeAttribute($value)
    {
        $this->attributes['type'] = e(str_replace(['\'', '"'], '', $value));
    }

    public function author()
    {
        return $this->hasOne('App\Models\User', 'id', 'author_id');
    }

    public function company()
    {
        return $this->hasOne('App\Models\Company', 'id', 'company_id');
    }
}
