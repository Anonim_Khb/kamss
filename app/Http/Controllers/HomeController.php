<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Client;
use Notifications;

class HomeController extends Controller
{
    public function index()
    {
        return \Auth::check() ? redirect()->route('panel-index') : view('simple.welcome');
    }

    public function setLocale($locale)
    {
        in_array($locale, config('site.locales'), true) ? session(['locale' => $locale]) : '';

        return redirect()->back();
    }
}
