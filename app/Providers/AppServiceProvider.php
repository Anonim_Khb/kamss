<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        include_once(app_path('helpers.php'));
        // Cabinet
        view()->composer('partials.left-menu', \App\Http\ViewComposers\LeftMenuComposer::class);
        view()->composer('partials.header', \App\Http\ViewComposers\HeaderComposer::class);
        view()->composer('main', \App\Http\ViewComposers\MainComposer::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
