<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="ru">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="format-detection" content="telephone=no">
    <title>Добро пожаловать, {{ $name or null }}</title>

    <style type="text/css">
        body {
            margin: 0;
            padding: 0;
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
        }

        table {
            border-spacing: 0;
        }

        table td {
            border-collapse: collapse;
        }

        .ExternalClass {
            width: 100%;
        }

        .ExternalClass,
        .ExternalClass p,
        .ExternalClass span,
        .ExternalClass font,
        .ExternalClass td,
        .ExternalClass div {
            line-height: 100%;
        }

        .ReadMsgBody {
            width: 100%;
            background-color: #ebebeb;
        }

        table {
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        }

        img {
            -ms-interpolation-mode: bicubic;
        }

        .yshortcuts a {
            border-bottom: none !important;
        }

        @media screen and (max-width: 599px) {
            .force-row,
            .container {
                width: 100% !important;
                max-width: 100% !important;
            }
        }

        @media screen and (max-width: 400px) {
            .container-padding {
                padding-left: 12px !important;
                padding-right: 12px !important;
            }
        }

        .ios-footer a {
            color: #aaaaaa !important;
            text-decoration: underline;
        }

        .btn-general {
            text-decoration: none;
            padding: 1em 1.3em;
            background: #0087FF;
            color: #f4f4f4;
            font-family: Helvetica, Arial, sans-serif;
            font-size: 14px;
            border-radius: .26em;
        }
        .btn-general:hover {
            background: #1f7de6;
        }
    </style>
</head>
<body style="margin:0; padding:0;" bgcolor="#F0F0F0" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" bgcolor="#F0F0F0">
    <tr>
        <td align="center" valign="top" bgcolor="#F0F0F0" style="background-color: #F0F0F0;">
            <br>
            <table border="0" width="600" cellpadding="0" cellspacing="0" class="container"
                   style="width:600px;max-width:600px">
                <tr>
                    <td class="container-padding header" align="left" style="padding-bottom: 1em">
                        <img style="width: 4.4em;" src="{{ asset(config('site.logo_mini_color_1')) }}" alt="Logo"/>
                    </td>
                </tr>
                <tr>
                    <td class="container-padding content" align="left"
                        style="padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff">
                        <br>
                        <div class="title"
                             style="font-family:Helvetica, Arial, sans-serif;font-size:18px;font-weight:600;color:#374550">
                            Отчет
                        </div>
                        <br>
                        <div class="body-text"
                             style="font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333">
                            Письмо содержит отчет, который был отправлен <strong>{{ $author_name }}</strong> с сайта
                            <a style="color: #868ca5" href="{{ config('site.name_full') }}">{{ config('site.name_full') }}</a>
                            <br><br>
                            @if($letter_text)
                                <strong>Автор оставил Вам сообщение:</strong>
                                <div><i>{{ $letter_text }}</i></div>
                                <br>
                            @endif
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="container-padding content" align="center" style="background-color:#ffffff">
                        <a class="btn-general" href="{{ route('login') }}" style="">Перейти на сайт</a>
                        <br><br><br>
                    </td>
                </tr>
                <tr>
                    <td class="container-padding footer-text" align="left"
                        style="font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px">
                        <br>
                        <strong>
                            <a style="color: #959cb8"
                               href="{{ config('site.name_full') }}">{{ config('site.name_full') }}</a>
                        </strong><br>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>
