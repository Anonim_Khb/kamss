<?php

return [

    'menu' => 'Sending',

    'title' => [
        'index' => 'Sending',
    ],

    'index' => [
        'title' => 'Email sending'
    ],

    'table' => [
        'select-all--tooltip' => 'Select ALL',
        'template-select' => 'Select Email template',
        'template-placeholder' => 'Not selected',
    ],

    'notification' => [
        'send-success' => 'Emails sanding successfully',
        'send-error' => 'Something errors with select users or template',
    ],

];
