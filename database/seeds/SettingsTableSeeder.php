<?php

use Illuminate\Database\Seeder;
use App\Models\Setting;

class SettingsTableSeeder extends Seeder
{

    public function run()
    {
        $settings = [
            [
                'key' => Setting::CAPTCHA_REGISTER,
                'value' => true,
                'description' => 'Каптча при регистрации'
            ],
            [
                'key' => Setting::CAPTCHA_LOGIN,
                'value' => true,
                'description' => 'Каптча при входе'
            ],
            [
                'key' => Setting::CAPTCHA_PASS_RESET,
                'value' => true,
                'description' => 'Каптча при сбросе пароля'
            ],
            [
                'key' => Setting::CHANGE_CLIENT_COMPANY,
                'value' => true,
                'description' => 'Смена компании у контакта'
            ],
            [
                'key' => Setting::TEMPLATES_READY,
                'value' => true,
                'description' => 'Использование готовых шаблонов'
            ],
            [
                'key' => Setting::FILES_WITH_EMAILS,
                'value' => true,
                'description' => 'Отправка файлов заказчикам'
            ],
        ];

        Setting::insert($settings);
    }
}
