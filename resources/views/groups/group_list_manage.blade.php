@extends('main')

@section('body-title')

    {{ trans('group.index.title') }}

    @include('partials.button-create', ['route' => route('group-create-admin'), 'text' => trans('group.index.create')])

@endsection

@section('body')

    <div class="users-index">
        @if(!$clientsGroups->isEmpty())
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>{{ trans('group.index.table.name') }}</th>
                        <th>{{ trans('group.index.table.description') }}</th>
                        <th>{{ trans('group.index.table.icon') }}</th>
                        <th>{{ trans('group.index.table.actions') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($clientsGroups as $clientsGroup)
                        <tr>
                            <th>{{ $clientsGroup->id }}</th>
                            <td>{{ $clientsGroup->name }}</td>
                            <td>{{ $clientsGroup->description }}</td>
                            <td><i class="{{ $clientsGroup->icon }}"></i></td>
                            <td>
                                @if(ur(\App\Models\User::ROLE_SUPERADMIN))
                                    <form class="inline"
                                          action="{{ route('group-edit-admin', $clientsGroup->id) }}"
                                          method="GET">
                                        <button type="submit" class="btn btn-xs btn-warning" data-toggle="tooltip"
                                                data-placement="top"
                                                title="{{ trans('group.index.table.update-tooltip') }}"><i
                                                    class="fa fa-pencil"></i>
                                        </button>
                                    </form>
                                    <form class="inline"
                                          action="{{ route('group-destroy-admin', ['id' => $clientsGroup->id]) }}"
                                          method="POST">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <button type="submit" onclick="return buttonConfirmation(event, 'Delete?')"
                                                class="btn btn-xs btn-danger" data-toggle="tooltip" data-placement="top"
                                                title="{{ trans('group.index.table.delete-tooltip') }}"><i
                                                    class="fa fa-power-off"></i>
                                        </button>
                                    </form>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @else
            <div class="no-results text-center">
                <i class="fa fa-list-alt fa-3x"></i>
                <p>{{ trans('group.search.no-results') }}</p>
            </div>
        @endif
    </div>

@endsection
