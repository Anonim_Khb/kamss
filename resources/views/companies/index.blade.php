@extends('main')

@section('css')
    <link href="/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type='text/css'>
    <link href="/libs/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" type='text/css'>
    <link href="/css/datatable.css" rel="stylesheet" type='text/css'>
@endsection

@section('body-title')

    Компании

@endsection

@section('body')

    <div class="no-background">
        <form class="no-background form-inline" action="{{ route('companies-index')}}"
              method="GET">
            @if(ur(\App\Models\User::ROLE_MODERATOR))
                <br>
                <div class="input-group input-group-sm border-bg-blue">
                        <span class="input-group-addon bg-blue" id="sizing-addon2">
                            <b>Статус:</b>
                        </span>
                    <select name="status" class="form-control form-filter-activation"
                            aria-describedby="sizing-addon2">
                        <option @if(request('status') == \App\Models\Company::STATUS_CHECKED) selected
                                @endif value="{{ \App\Models\Company::STATUS_CHECKED }}">Проверенные
                        </option>
                        <option @if(request('status') == \App\Models\Company::STATUS_UNCHECKED) selected
                                @endif value="{{ \App\Models\Company::STATUS_UNCHECKED }}">Непроверенные
                        </option>
                        <option @if(request('status') == null) selected
                                @endif value="{{ null }}">Все
                        </option>
                    </select>
                </div>
                <a class="btn btn-danger pull-right" href="{{ route('company-changes') }}"
                   @if($companyChangesCount < 1) disabled @endif>
                    Правки &nbsp;<span class="badge"><strong>{{ $companyChangesCount }}</strong></span>
                </a>
                <br><br>
            @endif
        </form>
        <div class="table-responsive">
            <table id="companiesTable" class="table table-hover table-bordered text-center">
                <thead>
                <tr>
                    <th class="col-sm-1">Id</th>
                    <th>Тип</th>
                    <th>Название</th>
                    <th>ИНН</th>
                    <th>Контактов</th>
                    <th>Изменено</th>
                    <th class="no-print action-dropdown-icon"><i class="fa fa-flash fa-lg"></i></th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th class="col-sm-1">Id</th>
                    <th>Тип</th>
                    <th>Название</th>
                    <th>ИНН</th>
                    <th>Контактов</th>
                    <th>Изменено</th>
                    <th></th>
                </tr>
                </tfoot>
                <tbody>
                @foreach($companies as $company)
                    <tr>
                        <td>{{ $company->id }}</td>
                        <td class="for-width-6">{{ $company->type }}</td>
                        <td>{{ $company->name }}</td>
                        <td class="for-width-6">{{ $company->inn }}</td>
                        <td class="for-width-8">{{ count($company->contacts) }}</td>
                        <td class="for-width-8">{{ df($company->updated_at) }}</td>
                        <td class="for-width-6" style="white-space: nowrap;">
                            @if(ur(\App\Models\User::ROLE_MODERATOR))
                                <a onclick="setCheckedStatus(this, '{{ $company->id }}', '{{ route('company-set-checked-ajax') }}');"
                                   class="animated-jqe btn btn-xs @if($company->status == \App\Models\Company::STATUS_CHECKED) btn-success @else btn-warning @endif"
                                   data-toggle="tooltip" data-placement="top"
                                   title="{{ trans('clients.index.table.change-status-tooltip') }}">
                                    <i class="fa fa-check"></i>
                                </a>
                            @endif
                            <a class="animated-jqe btn btn-xs bg-orange-active" data-toggle="tooltip"
                               href="{{ route('companies-edit', $company->id) }}" data-placement="top"
                               title="{{ trans('group.index.table.update-tooltip') }}">
                                <i class="fa fa-pencil"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection

@section('js')
    <script src="/libs/sweetalert2/sweetalert2.min.js"></script>
    <script src="/libs/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="/js/datatable.js"></script>
    <script src="/js/company-actions.js"></script>
    <script>
        $(document).ready(function () {
            myDataTable('#companiesTable', true, 25);
            formSending('.form-filter-activation');
        });
    </script>
@endsection
