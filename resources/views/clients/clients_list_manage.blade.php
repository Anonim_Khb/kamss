@extends('main')

@section('css')
    <link href="/libs/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" type='text/css'>
    <link href="/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type='text/css'>
    <link href="/css/datatable.css" rel="stylesheet" type='text/css'>
    <link href="/libs/bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet" type='text/css'>
@endsection

@section('body-title')
    Список контактов
    @include('partials.button-create', ['route' => route('clients-create-or-update-get'), 'text' => trans('clients.index.create')])
@endsection

@section('body')

    <div class="users-index no-background">

        @include('partials._filter-clients-1', ['route' => route('clients-index')])

        @if(!$clients->isEmpty())

            @include('partials.clients-table', ['clients' => $clients])

        @else
            <div class="no-results text-center">
                <i class="fa fa-group fa-3x"></i>
                <p>{{ trans('group.search.no-results') }}</p>
            </div>
        @endif

    </div>
@endsection

@section('js')
    <script src="/js/client-actions.js"></script>
    <script src="/libs/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="/libs/sweetalert2/sweetalert2.min.js"></script>
    <script src="/js/datatable.js"></script>
    <script src="/js/client-sending.js"></script>
@endsection