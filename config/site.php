<?php

return [

    'captcha' => env('NOCAPTCHA_STATUS', true), //true - Enabled, false - Disabled

    'locales' => ['en', 'ru'], //Available languages

    'name' => env('SITE_NAME', 'site'),
    'name_full' => env('SITE_NAME_FULL', 'site.com'),

//    'logo_full_color_1' => '/images/logo/full_color_1.png',
//    'logo_full_white_1' => '/images/logo/full_white_1.png',
//    'logo_full_black_1' => '/images/logo/full_black_1.png',

    'logo_mini_color_1' => '/images/logo/mini_color_1.png',
    'logo_mini_white_1' => '/images/logo/mini_white_1.png',
    'logo_mini_black_1' => '/images/logo/mini_black_1.png',

    'password_reset_limit' => 10, //Minutes

    'admin_avatar' => '/images/admin/admin_1.png',

    'files_email_path' => storage_path('app/mail_files'),
    'files_excel_report_path' => storage_path('app/public'),
];
