<?php

return [

    'prepend' => [
        'site' => config('site.name'),
        'panel' => 'Panel',
    ],

    'index-panel' => 'Home',
    'login' => 'Login',
    'requests-show-admin' => 'Add Requests',
    'request-add-accepted-admin' => 'Request add accepted',
    'password-reset' => 'Password reset',
    'password-reset-form' => 'Password reset',
    'groups-index' => 'Groups',
    'group-create' => 'Create group',
    'group-update' => 'Update group',
    'templates-ready' => 'Ready templates',
    'templates-own' => 'Own templates',
    'templates-create' => 'Template create',
    'users-index' => 'Users',

];
