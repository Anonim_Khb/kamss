<?php

namespace App\Http\Controllers\Auth;

use App\Events\PasswordReset;
use App\Http\Controllers\Controller;
use App\Http\Requests\PasswordResetSendEmailRequest;
use App\Http\Requests\PasswordResetUpdateDataRequest;
use App\Models\User;
use Jenssegers\Date\Date;
use Notifications;

class PasswordController extends Controller
{
    public function showSendEmailForResetForm()
    {
        \Title::append(trans('title.password-reset'));

        return view('auth.passwords.email');
    }

    public function sendResetLinkEmail(PasswordResetSendEmailRequest $request, User $user)
    {
        $user = $user->where('email', $request->email)->first();

        $token = str_random();

        if ($user->last_restore > Date::now()->subMinutes(config('site.password_reset_limit'))) {
            Notifications::error(trans_choice('notification.password-reset-email-error', config('site.password_reset_limit'), ['minutes' => config('site.password_reset_limit')]), 'top');

            return redirect()->back()->withInput();
        } else {
            $user->update([
                'token' => $token,
                'last_restore' => Date::now()
            ]);

            event(new PasswordReset($user));

            Notifications::success(trans('notification.password-reset-email-success'), 'top');

            return redirect()->route('index');
        }
    }

    public function showResetForm($email, $token, User $user)
    {
        $user = $user->where('email', $email)
            ->where('token', $token)
            ->first();

        if (!is_null($user) && $user->last_restore > Date::now()->subMinutes(config('site.password_reset_limit'))) {
            \Title::append(trans('title.password-reset-form'));

            return view('auth.passwords.reset', ['token' => $token]);
        } else {
            Notifications::error(trans('notification.password-reset-form-error'), 'top');

            return redirect()->route('login');
        }
    }

    public function resetPost(PasswordResetUpdateDataRequest $errors, User $user)
    {
        $user = $user->where('email', request('email'))
            ->where('token', request('token'))
            ->first();

        if (!is_null($user)) {
            $user->update([
                'token' => str_random(),
                'password' => request('password')
            ]);

            Notifications::success(trans('notification.password-reset-form-success'), 'top');

            return redirect()->route('login');
        } else {
            Notifications::error(trans('notification.password-reset-form-unknown-error'), 'top');

            return redirect()->back()->withInput();
        }

    }

}
