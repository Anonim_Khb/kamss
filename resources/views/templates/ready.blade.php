@extends('main')

@section('body-title')
    {{ trans('templates.index.title-ready') }}
@endsection

@section('body')
    <div class="users-index no-background email-templates">
        @include('partials._info-block-1', ['text' => trans('info.ready-templates')])
        @foreach($ready as $ready_tmp)
            <div class="col-sm-4 col-md-3">
                <div class="thumbnail">
                    <img style="background: url('/images/templates/{{ $ready_tmp }}.png') no-repeat; background-size: cover;"
                         src="">
                    <div class="caption">
                        <h3>{{ $ready_tmp }}</h3>
                        <p>
                            <a href="{{ route('choose-template-for-send', [$ready_tmp, \App\Models\UsersTemplate::STATUS_READY]) }}" class="btn btn-warning"
                               role="button">
                                {{ trans('templates.index.choose') }}
                            </a>
                            <a href="{{ route('template-show', [$ready_tmp, \App\Models\UsersTemplate::STATUS_READY]) }}"
                               class="btn btn-primary pull-right" role="button" target="_blank">
                                {{ trans('templates.index.show') }}
                            </a>
                        </p>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection
