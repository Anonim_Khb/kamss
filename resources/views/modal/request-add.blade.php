<div id="membershipApplication" class="modal fade modal-type-1" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" method="POST" action="{{ route('request-add') }}">
                {!! csrf_field() !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">{{ trans('simple.welcome.modal_ma.title') }}</h4>
                </div>
                <div class="modal-body">
                    <div class="modal-body-text">
                        Оставьте заявку, заполнив все поля. Решение по заявке будет отправлено Вам на указазнный в заявке эл.адрес.
                    </div>
                    <div class="form-group has-feedback">
                        <input type="text" name="name" class="form-control"
                               placeholder="{{ trans('simple.welcome.modal_ma.name_placeholder') }}"
                               value="{{ old('name') }}" required>
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="text" name="work_phone" class="form-control"
                               placeholder="{{ trans('simple.welcome.modal_ma.work_phone-placeholder') }}"
                               value="{{ old('work_phone') }}" required>
                        <span class="glyphicon glyphicon-phone-alt form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="text" name="cell_phone" class="form-control"
                               placeholder="{{ trans('simple.welcome.modal_ma.cell_phone-placeholder') }}"
                               value="{{ old('cell_phone') }}" required>
                        <span class="glyphicon glyphicon-phone form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="email" name="email" class="form-control"
                               placeholder="{{ trans('simple.welcome.modal_ma.email_placeholder') }}"
                               value="{{ old('email') }}" required>
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                            <textarea class="form-control" name="text" rows="2" maxlength="{{ \App\Models\MembershipApplication::TEXT_LIMIT }}"
                                      placeholder="{{ trans('simple.welcome.modal_ma.text_placeholder') }}">{{ old('text') }}</textarea>
                        <span class="glyphicon glyphicon-pencil form-control-feedback"></span>
                    </div>
                    @if(conf_site()->getOne(\App\Models\Setting::CAPTCHA_REGISTER))
                        @include('partials.recaptcha')
                    @endif
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        {{ trans('simple.welcome.modal_ma.btn-cancel') }}
                    </button>
                    <button type="submit" class="btn btn-primary">
                        {{ trans('simple.welcome.modal_ma.btn-submit') }}
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
