<?php

return [

    'menu' => 'Memberships',

    'title' => [
        'prepend' => 'Admin',
        'index' => 'Membership requests',
        'accepted' => 'Membership accepted',
    ],

    'index' => [
        'title' => 'Membership requests',
        'table' => [
            'name' => 'Name',
            'email' => 'Email',
            'phone' => 'Phone',
            'text' => 'Comment',
            'action' => 'Actions',
            'status' => 'Status',
            'created' => 'Created',
            'status-input' => 'Status',
            'status-unread' => 'Unread',
            'status-read' => 'Read',
            'status-all' => 'All',
            'rejected-tooltip' => 'Rejected membership',
            'accepted-tooltip' => 'Accepted membership',
            'delete' => [
                'title' => 'Delete',
                'not-selected' => 'Not selected',
            ],
        ],
    ],

    'accepted' => [
        'title' => 'Membership accepted for <b>:name</b>',
        'table' => [
            'role-title' => 'Select role',
            'role-none' => 'Not selected',
            'branch' => 'Company branch',
            'branch-none' => 'Not selected',
            'treatment' => 'Treatment',
            'btn-create' => 'Create',
        ],
    ],

    'notification' => [
        'request-rejected' => 'The request is rejected',
        'user-created' => 'New user successfully created',
        'delete-success' => 'Requests successfully removed',
        'delete-error' => 'Nothing for removed',
    ],

];
