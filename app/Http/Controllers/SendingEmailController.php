<?php

namespace App\Http\Controllers;

use App\Events\Own_1_Email;
use App\Events\Ready_1_Email;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Jobs\ShareContactToEmail;
use App\Models\Client;
use App\Models\SendStatistic;
use App\Models\UsersTemplate;
use Notifications;

class SendingEmailController extends Controller
{
    public function __construct()
    {
        $this->middleware('inactive');
    }

    public function sendingEmail()
    {
        set_time_limit(0);
        $clients = Client::whereIn('id', request('client'))->get();

        if (request('t_type') == UsersTemplate::STATUS_READY) {
            event(new Ready_1_Email($clients, request('t_name')));
        } elseif (request('t_type') == UsersTemplate::STATUS_OWN) {
            event(new Own_1_Email($clients, request('t_name'), request('t_text')));
        } else {
            Notifications::error(trans('sending.notification.send-error'), 'top');

            return redirect()->back()->withInput();
        }

        notify_site()->setSuccessSendClientEmailsNotification(\Auth::id(), count($clients));

        SendStatistic::create([
            'user_id' => \Auth::user()->id,
            'count' => $clients->count()
        ]);

        session()->forget(['t_type', 't_text', 't_name']);

        Notifications::success(trans('sending.notification.send-success'), 'top');
        return redirect()->route('panel-index');
    }

    public function cancelSendingEmail()
    {
        session()->forget(['t_type', 't_text', 't_name']);

        Notifications::success('Отправка была отменена', 'top');
        return redirect()->route('clients-index');
    }

    public function shareContactToEmail(Client $client)
    {
        $v = \Validator::make(request()->all(), [
            'email' => 'required|email',
            'clientId' => 'required|exists:clients,id,author_id,' . \Auth::id()
        ]);
        if ($v->fails()) return 'error';

        $client = $client->where('id', request('clientId'))
            ->with('clientCompany')->first();

        $job = (new ShareContactToEmail(request('email'), $client, \Auth::user()));
        dispatch($job);

        return 'ok';
    }
}
