<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Models\Bid;
use Illuminate\Contracts\Validation\Validator;
use Redirect;
use Notifications;

class BidCreateOrUpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if($this->route('id')) {
            return Bid::where('id', $this->route('id'))
                ->where('author_id', \Auth::id())->exists();
        } else {
            return true;
        }
    }

    public function forbiddenResponse()
    {
        return abort(403);
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Bid $bid)
    {
        $rules = [
            'source' => 'in:' . implode(',', $bid->arraySource()),
            'sector' => 'in:' . implode(',', $bid->arraySector()),
            'currency' => 'in:' . implode(',', $bid->arrayCurrency()),
            'category' => 'in:' . implode(',', $bid->arrayCategory()),
            'first_contact' => 'date|date_format:d.m.Y',
            'last_contact' => 'date|date_format:d.m.Y',
            'next_contact' => 'date|date_format:d.m.Y',
            'current_stage' => 'required|exists:bid_current_stages,id',
            'status' => 'in:on',
        ];

        if (is_null(\Route::current()->getParameter('id'))) {
            $rules = array_add($rules, 'client_id', 'required|exists:clients,id');
        }

        return $rules;
    }

    public function response(Array $errors)
    {
        return redirect()->back()->withInput();
    }


    public function formatErrors(Validator $validator)
    {
        foreach ($validator->errors()->all() as $error) {
            Notifications::danger($error, 'page');
        }

        return $validator->errors()->getMessages();
    }
}
