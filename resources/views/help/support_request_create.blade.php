@extends('main')

@section('body-title')

    Техподдержка

@endsection

@section('body')

    <div class="users-create">
        <div class="no-background">@include('partials._info-block-1', ['text' => trans('info.user-support-request')])</div>
        <div class="register-box">

            @include('basis.notifications-page')

            <div class="login-box-body">
                <form action="{{ route('help-support-post') }}"
                      method="POST">
                    {!! csrf_field() !!}
                    <div class="form-group has-feedback">
                        <label for="title">Заголовок</label>
                        <input id="title" type="text" name="title" class="form-control"
                               placeholder="Заголовок обращения"
                               value="{{ old('title') }}" required>
                        <span class="glyphicon glyphicon-tag form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <label for="text">Текст</label>
                        <textarea id="text" rows="10" type="text" name="text" class="form-control"
                                  placeholder="Текст обращения"
                                  required>{{ old('text') }}</textarea>
                        <span class="glyphicon glyphicon-font form-control-feedback"></span>
                    </div>
                    <div class="row" style="margin: 0">
                        <div class="pull-right">
                            <button type="submit" class="btn btn-success btn-flat">
                                Отправить
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
