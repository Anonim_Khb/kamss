<a href="{{ $route }}" class="btn btn-success pull-right">
    <i class="fa fa-plus"></i>&nbsp;
    <span class="hidden-xs">{{ $text }}</span>
</a>
