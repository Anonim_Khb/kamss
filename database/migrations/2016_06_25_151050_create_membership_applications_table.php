<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\MembershipApplication;

class CreateMembershipApplicationsTable extends Migration
{
    public function up()
    {
        Schema::create('membership_applications', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('work_phone');
            $table->string('cell_phone');
            $table->string('email');
            $table->text('text')->nullable();
            $table->string('status')->default(MembershipApplication::STATUS_UNREAD);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('membership_applications');
    }
}
