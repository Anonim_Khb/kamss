<?php

namespace App\Http\ViewComposers;

use App\Models\Bid;
use App\Models\Company;
use App\Models\CompanyChange;
use App\Models\MembershipApplication;
use App\Models\Support;
use Illuminate\Contracts\View\View;
use Jenssegers\Date\Date;
use App\Models\BidCurrentStage;

class LeftMenuComposer
{

    public function compose(View $view)
    {
        $view->with([
            'user' => auth()->user(),
            'membershipRequests' => MembershipApplication::where('status', MembershipApplication::STATUS_UNREAD)->count(),
            'supportsCount' => Support::where('status', Support::STATUS_UNREAD)->count(),
            'bidsTodayCount' => Bid::where('author_id', \Auth::user()->id)->where('next_contact', df(Date::now(), 'Y-m-d'))->whereHas('current_stages', function ($query) {
                $query->where('status', BidCurrentStage::STATUS_ACTIVE);
            })->count(),
            'companiesUncheckedCount' => Company::where('status', Company::STATUS_UNCHECKED)->count(),
            'companyChangesCount' => CompanyChange::count(),
        ]);
    }
}
