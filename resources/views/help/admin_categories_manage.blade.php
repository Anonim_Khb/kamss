@extends('main')

@section('body-title')

    ВиО категории

    @include('partials.button-create', ['route' => route('help-category-create-admin'), 'text' => 'Создать'])

@endsection

@section('body')

    <div class="users-index">
        @if(!$categories->isEmpty())
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>{{ trans('group.index.table.name') }}</th>
                        <th>{{ trans('group.index.table.actions') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($categories as $category)
                        <tr>
                            <th>{{ $category->id }}</th>
                            <td>{{ $category->category }}</td>
                            <td>
                                <form class="inline"
                                      action="{{ route('help-category-edit-admin', $category->id) }}"
                                      method="GET">
                                    <button type="submit" class="btn btn-xs btn-warning" data-toggle="tooltip"
                                            data-placement="top"
                                            title="{{ trans('group.index.table.update-tooltip') }}"><i
                                                class="fa fa-pencil"></i>
                                    </button>
                                </form>
                                <form class="inline"
                                      action="{{ route('help-category-destroy-admin', ['id' => $category->id]) }}"
                                      method="POST">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                    <button type="submit" onclick="return buttonConfirmation(event, 'Delete?')"
                                            class="btn btn-xs btn-danger" data-toggle="tooltip" data-placement="top"
                                            title="{{ trans('group.index.table.delete-tooltip') }}"><i
                                                class="fa fa-power-off"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endif
    </div>

@endsection
