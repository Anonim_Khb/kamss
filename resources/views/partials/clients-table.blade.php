<div class="row dashboard-general">
    <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="info-box">
            @if(!session()->has('t_type'))
                <span class="info-box-icon bg-red"><i class="fa fa-file-text-o"></i></span>
                <div class="info-box-content">
                    <span class="info-box-number">Шаблон</span>
                    <span class="info-box-text">не выбран</span>
                </div>
            @elseif(session('t_type') == \App\Models\UsersTemplate::STATUS_READY)
                <span class="info-box-icon bg-green-gradient"><i class="fa fa-file-text-o"></i></span>
                <div class="info-box-content">
                    <span class="info-box-number">Шаблон</span>
                    <span class="info-box-text">выбран готовый <br>
                        <a class="info-box-link text-aqua"
                           href="{{ route('template-show', [session('t_name'), session('t_type'), session('t_text')]) }}"
                           target="_blank">просмотр
                        </a>
                    </span>
                </div>
            @elseif(session('t_type') == \App\Models\UsersTemplate::STATUS_OWN)
                <span class="info-box-icon @if(!session()->has('t_name')) bg-red-gradient @else bg-yellow-gradient @endif"><i
                            class="fa fa-file-text-o"></i></span>
                <div class="info-box-content">
                    <span class="info-box-number">Шаблон</span>
                    <span class="info-box-text">выбран свой <br>
                        @if(!session()->has('t_name'))
                            <a class="info-box-link" href="{{ route('choose-carcass-template-for-send') }}">каркас не выбран</a>
                        @else
                            <a class="info-box-link text-aqua"
                               href="{{ route('template-show', [session('t_name'), session('t_type'), session('t_text')]) }}"
                               target="_blank">просмотр
                            </a>
                        @endif
                    </span>
                </div>
            @endif
        </div>
    </div>
    <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon {{ \Auth::user()->signature_status == \App\Models\User::SIGNATURE_ACTIVE ? 'bg-yellow-gradient' : 'bg-green-gradient' }}"><i
                        class="fa fa-pencil"></i></span>
            <div class="info-box-content">
                <span class="info-box-number">Подпись</span>
                <span class="info-box-text">
                    {{ \Auth::user()->signature_status == \App\Models\User::SIGNATURE_ACTIVE ? 'личная' : 'стандартная' }}
                </span>
                <a class="info-box-link text-aqua" href="{{ route('user-edit') . '#signature_status' }}">
                    изменить
                </a>
            </div>
        </div>
    </div>
    <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="info-box" href="{{ route('help-faq') }}">
            <span class="info-box-icon bg-aqua-gradient"><i class="fa fa-clock-o"></i></span>
            <div class="info-box-content">
                <span class="info-box-number">Добавлено за сутки</span>
                <span class="info-box-text"><strong>{{ $clients_created_day }}</strong>
                    {{ trans_choice('clients.clients_create_per-day', $clients_created_day) }}
                </span>
                <a class="info-box-link text-aqua" href="{{ route('clients-create-or-update-get') }}">
                    добавить
                </a>
            </div>
        </div>
    </div>
</div>

<div class="table-responsive">
    <form id="sendEmailsForClients" class="no-background form-inline" action="{{ route('sending-start-admin') }}"
          method="POST">
        {{ csrf_field() }}
        <input type="hidden" name="t_type" value="{{ session('t_type') }}">
        <input type="hidden" name="t_text" value="{{ session('t_text') }}">
        <input type="hidden" name="t_name" value="{{ session('t_name') }}">
        <table id="clientsTable" class="for-print table table-hover table-bordered text-center">
            <thead>
            <tr>
                <th style="width: 1em">Id</th>
                @if(session()->has('t_name'))
                    <th class="no-print" style="width: 1em" data-container="body" data-toggle="popover"
                        data-placement="right"
                        data-content="Выбор заказчиков для отправки писем">
                        <i class="fa fa-hand-pointer-o fa-lg"></i>
                    </th>
                @endif
                <th>{{ trans('clients.index.table.name') }}</th>
                <th>{{ trans('clients.index.table.company') }}</th>
                <th>Email</th>
                <th class="no-print">{{ trans('clients.index.table.client-groups') }}</th>
                <th class="no-print">Отправка</th>
                <th class="no-print action-dropdown-icon"><i class="fa fa-flash fa-lg"></i></th>
            </tr>
            </thead>
            <tfoot class="no-print">
            <tr>
                <th>Id</th>
                @if(session()->has('t_name'))
                    <th>Выбор</th>
                @endif
                <th>{{ trans('clients.index.table.name') }}</th>
                <th>{{ trans('clients.index.table.company') }}</th>
                <th>Email</th>
                <th>{{ trans('clients.index.table.client-groups') }}</th>
                <th>Отправка</th>
                <th></th>
            </tr>
            </tfoot>
            <tbody>
            @foreach($clients as $client)
                <tr>
                    <th>{{ $client->id }}</th>
                    @if(session()->has('t_name'))
                        <td class="text-center no-print">
                            @if($client->email)
                                <input type="checkbox" name="client[]" class="checkbox-clients checkbox-2x"
                                       value="{{ $client->id }}">
                            @else
                                <i class="fa fa-envelope-o fa-lg" style="color:rgba(232, 0, 0, 0.55)"
                                   data-toggle="tooltip"
                                   data-placement="right" title="Эл.адрес не указан"></i>
                            @endif
                        </td>
                    @endif
                    <td class="client-name-td">{{ $client->name }}</td>
                    <td class="client-company-td">
                        <div class="client-company" data-toggle="tooltip" data-placement="top"
                             title="{{ $client->clientCompany->name }}">
                            {{ $client->clientCompany->name }}
                        </div>
                        <div class="client-position" data-toggle="tooltip" data-placement="top"
                             title="{{ $client->position }}">
                            {{ $client->position }}
                        </div>
                    </td>
                    <td class="clients-email-td">
                        <a href="mailto:{{ $client->email }}" data-toggle="tooltip" data-placement="top"
                           title="{{ $client->email }}">
                            {{ $client->email }}
                        </a>
                    </td>
                    <td class="clients-all-groups no-print" data-route="{{ route('change-client-group-ajax') }}">
                        @foreach($groups as $group)
                            <i onclick="changeClientGroup(this, '{{ $client->id }}', '{{ $group->id }}');" class="
                            @foreach($client->groups as $client_group)
                            @if($client_group->id == $group->id)
                                    active-group
                                @endif
                            @endforeach
                            {!! $group->icon !!}"></i>
                        @endforeach
                    </td>
                    <td class="no-print">{{ $client->last_send ? ta($client->last_send) : '----' }}</td>
                    <td class="no-print action-dropdown-btn" style="white-space: nowrap;">
                        <div class="dropdown">
                            <span id="bidDd{{ $client->id }}" class="btn-for-actions" data-toggle="dropdown"
                                  aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-ellipsis-h fa-fw fa-lg"></i>
                            </span>
                            <ul class="list-inline dropdown-menu dropdown-menu-right animated jello"
                                aria-labelledby="bidDd{{ $client->id }}">
                                <li onclick="changeClientStatus(this, '{{ $client->id }}', '{{ route('change-client-status-ajax') }}');"
                                    class="@if($client->status == \App\Models\Client::STATUS_ACTIVE) dmb-success @else dmb-danger @endif"
                                    data-toggle="tooltip" data-placement="top"
                                    title="{{ trans('clients.index.table.change-status-tooltip') }}">
                                    <i class="fa fa-lightbulb-o fa-fw"></i>
                                </li>
                                <li onclick="shareContact('{{ $client->name }}', '{{ $client->id }}', '{{ route('client-share-email') }}')"
                                    class="dmb-info" data-toggle="tooltip" data-placement="top"
                                    title="Поделиться контактом">
                                    <i class="fa fa-microphone fa-fw"></i>
                                </li>
                                <li onclick="redirectToRoute('{{ route('clients-create-or-update-get', $client->id) }}')"
                                    class="dmb-warning" data-toggle="tooltip" data-placement="top"
                                    title="{{ trans('group.index.table.update-tooltip') }}">
                                    <i class="fa fa-pencil fa-fw"></i>
                                </li>
                                <li onclick="deleteClient(this, '{{ $client->id }}', '{{ route('delete-client-ajax') }}')"
                                    class="dmb-danger" data-toggle="tooltip" data-placement="top"
                                    title="{{ trans('group.index.table.delete-tooltip') }}">
                                    <i class="fa fa-power-off fa-fw"></i>
                                </li>
                            </ul>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @if(session()->has('t_name'))
            <button id="sendEmailsButton" type="button" class="text-center" disabled>
                Отправить
            </button>
            <i onclick="cancelSendingEmail('{{ route('sending-cancel') }}')"
               id="removeSendingSessions"
               class="fa fa-remove pull-right"></i>
        @endif
    </form>
</div>
