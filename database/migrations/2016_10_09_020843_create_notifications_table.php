<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->default();
            $table->text('text');
            $table->string('icon');
            $table->string('color')->default(\App\Models\Notification::ICON_COLOR_DEFAULT);
            $table->string('link')->nullable();
            $table->integer('status')->default(\App\Models\Notification::STATUS_ACTIVE);
            $table->timestamps();

            $table->index('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('notifications');
    }
}
