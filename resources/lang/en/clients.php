<?php

return [

    'menu' => 'Clients',

    'title' => [
        'prepend' => 'Admin',
        'index' => 'Clients',
        'create' => 'Client create',
        'update' => 'Client update',
    ],

    'index' => [
        'title' => 'Clients list',
        'create' => 'Create',
        'table' => [
            'count' => 'Total records',
            'name' => 'Name',
            'email' => 'Email',
            'phone' => 'Phone',
            'last-send' => 'Last send',
            'company' => 'Company',
            'position' => 'Position',
            'actions' => 'Actions',
            'status-tooltip' => 'Status',
            'update-tooltip' => 'Update',
            'delete-tooltip' => 'Delete',
            'sort-status' => 'Status: ',
            'sort-group' => 'Group: ',
            'sort-keywords' => 'Keywords: ',
            'sort-keywords-placeholder' => 'Keywords',
            'show-all' => 'All',
            'show-active' => 'Active',
            'show-inactive' => 'Inactive',
            'show-without-group' => 'Without',
            'deactivate-tooltip' => 'Deactivate',
            'activate-tooltip' => 'Activate',
            'sort-date-title' => 'At first',
            'sort-date-new' => 'Old',
            'sort-date-old' => 'New',
            'display-title' => 'Display off',
            'display-all' => 'All',
            'last-send-title' => 'Sending',
            'last-send-not-more' => 'Not more',
            'last-send-more' => 'More',
            'last-send-placeholder' => 'Days',
            'group-member-title' => 'This group',
            'group-member-no' => 'Not group',
            'group-member-yes' => 'In group',
            'client-groups' => 'Groups',
            'change-status-tooltip' => 'Change status',
            'watch-tooltip' => 'Watch',
            'send' => 'Send',
            'created' => 'Created',
            'updated' => 'Updated',
            'treatment' => 'Treatment',
            'status' => 'Status',
            'template' => 'Template',
            'send-btn' => 'Send',
        ],
    ],

    'create' => [
        'title' => 'Client create',
        'btn' => 'Create',
        'placeholder-name' => 'Enter client name',
        'placeholder-email' => 'Enter client email',
        'placeholder-treatment' => 'Enter client treatment',
        'placeholder-company' => 'Enter client company',
        'placeholder-position' => 'Enter client position',
        'placeholder-phone' => 'Enter client phone',
        'return-back' => 'Return back after',
    ],

    'update' => [
        'title' => 'Client update',
        'btn' => 'Update',
    ],

    'search' => [
        'no-results' => 'No results'
    ],

    'form' => [
        'login-title' => 'Sign in to start',
        'login-email-placeholder' => 'Email',
        'login-password-placeholder' => 'Password',
        'login-remember' => 'Remember Me',
    ],

    'notification' => [
        'client-create-or-update' => 'Client successfully created/updated',
        'client-destroy' => 'Client successfully removed',
        'client-change-status' => 'Client status successfully changed',
    ],

    'modal' => [
        'btn-close' => 'Close',
    ],

];
