<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Setting;
use App\Models\User;
use App\Models\UsersTemplate;
use Notifications;
use File;
use App\Http\Requests\OwnTemplateCreateRequest;
use Storage;
use Validator;

class TemplateController extends Controller
{
    public function __construct()
    {
        $this->middleware('inactive')->except(['readyTemplates', 'ownTemplates']);
    }

    public function readyTemplates()
    {
        if (!conf_site()->getOne(\App\Models\Setting::TEMPLATES_READY)) {
            abort(403);
        }

        \Title::append(trans('title.templates-ready'));

        $templates = File::allFiles(resource_path('views/email/ready/html'));

        foreach ($templates as $template) {
            $ready[] = str_replace('.blade.php', '', $template->getFilename());
        }

        view()->share('active_link', 'templates_ready_index');

        return view('templates.ready', ['ready' => $ready]);
    }

    public function ownTemplates(UsersTemplate $own)
    {
        \Title::append(trans('title.templates-own'));

        $owns = $own->where('user_id', \Auth::user()->id)->get();

        view()->share('active_link', 'templates_own_index');

        return view('templates.own', ['owns' => $owns, 'setting_email' => conf_site()->getOne(Setting::FILES_WITH_EMAILS)]);
    }

    public function createOwnTemplate()
    {
        \Title::append(trans('title.templates-create'));

        return view('templates.create-or-update-own', ['setting_email' => conf_site()->getOne(Setting::FILES_WITH_EMAILS)]);
    }

    public function editOwnTemplate($id, UsersTemplate $own)
    {
        $own = $own->where('id', $id);

        if (!ur(\App\Models\User::ROLE_MODERATOR)) {
            $own->where('user_id', \Auth::user()->id);
        }

        $own = $own->with('file')->first();

        \Javascript::put([
            'routeRemove' => route('file-email-remove')
        ]);

        \Title::append(trans('title.templates-create'));

        return $own ? view('templates.create-or-update-own', ['own' => $own, 'setting_email' => conf_site()->getOne(Setting::FILES_WITH_EMAILS)]) : abort(403);
    }

    public function createOrUpdateOwnTemplatePost(OwnTemplateCreateRequest $request, UsersTemplate $usersTemplate, $id = null)
    {
        if ($request->hasFile('file') && conf_site()->getOne(Setting::FILES_WITH_EMAILS)) {

            $fileController = new FileController();
            $check = $fileController->fileValidation($request->file('file'), 'email');

            if ($check == 'error') {
                return redirect()->back()->withInput();
            }

            $userFileId = $fileController->fileSave($request->file('file'));
        }

        $usersTemplate = $usersTemplate->findOrNew($id);
        $usersTemplate->user_id = \Auth::id();
        $usersTemplate->name = e($request->name);
        $usersTemplate->text = e($request->text);
        if(isset($userFileId)) {
            $usersTemplate->file_id = $userFileId;
        }
        $usersTemplate->save();

        Notifications::success(trans('notification.own-template-create-or-update'), 'top');
        return redirect()->route('templates-own');
    }

    public function destroyAjax(UsersTemplate $own)
    {
        $own = $own->where('user_id', \Auth::user()->id)
            ->where('id', request('OwnId'))->first();

        if (!is_null($own->file_id)) {
            $file = new FileController();
            $file->removeFile($own->file_id);
        }

        $own->delete();

        return 'success';
    }

    public function chooseTextForOwnTemplate($text)
    {
        $this->setTemplateSession(null, UsersTemplate::STATUS_OWN, $text);

        return redirect()->route('choose-carcass-template-for-send');
    }

    public function chooseTemplate($template, $type, $text = null)
    {
        $this->setTemplateSession($template, $type, $text);

        if ($type == UsersTemplate::STATUS_READY || $type == UsersTemplate::STATUS_OWN) {
            return redirect('/panel/clients?group=&filter=' . \App\Models\Client::STATUS_ACTIVE);
        } else {
            abort(404);
        }
    }

    public function setTemplateSession($template, $type, $text)
    {
        session()->forget(['t_type', 't_text', 't_name']);

        if ($type == UsersTemplate::STATUS_READY) {
            session([
                't_type' => $type,
                't_text' => null,
                't_name' => $template,
            ]);
        } elseif ($type == UsersTemplate::STATUS_OWN) {
            session([
                't_type' => $type,
                't_text' => $text,
                't_name' => $template ? $template : null,
            ]);
        }
    }

    public function getCarcassTemplates()
    {
        \Title::append('Каркасы писем');

        $templates = File::allFiles(resource_path('views/email/carcasses'));

        foreach ($templates as $template) {
            $carcasses[] = str_replace('.blade.php', '', $template->getFilename());
        }

        view()->share('active_link', 'templates_own_index');

        return view('templates.carcasses', ['carcasses' => $carcasses]);
    }

    public function showEmailTemplate($template, $type, $text = null)
    {
        $user = User::where('id', \Auth::user()->id)->with('companyBranch')->first();

        if ($type == UsersTemplate::STATUS_READY) {
            return view('email.ready.html.' . $template, ['user' => $user]);
        } elseif ($type == UsersTemplate::STATUS_OWN) {
            if ($text) {
                $text = UsersTemplate::where('id', $text)->first();
                $text = html_entity_decode($text->text);
            } else {
                $text = null;
            }
            return view('email.carcasses.' . $template, ['text' => $text, 'user' => $user]);
        } else {
            abort(404);
        }
    }

}
