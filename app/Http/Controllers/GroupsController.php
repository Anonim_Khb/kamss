<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Client;
use App\Models\ClientsGroup;
use App\Models\ClientsGroupsList;
use Notifications;

class GroupsController extends Controller
{
    public function __construct()
    {
        $this->middleware('inactive')->except('index');

        view()->share('active_link', 'groups_admin_index');
    }

    public function index()
    {
        $clientsGroups = ClientsGroup::all();

        \Title::append(trans('title.groups-index'));

        return view('groups.group_list_manage', ['clientsGroups' => $clientsGroups]);
    }

    public function create()
    {
        \Title::append(trans('title.group-create'));

        return view('groups.group_create_update');
    }

    public function edit($id, ClientsGroup $clientsGroup)
    {
        \Title::append(trans('title.group-update'));

        $group = $clientsGroup->find($id);

        return view('groups.group_create_update', ['group' => $group]);
    }

    public function createOrUpdate($id = null)
    {
        $group = ClientsGroup::findOrNew($id);

        $group->name = request('name');
        $group->description = request('description');
        $group->icon = request('icon');
        $group->save();

        Notifications::success(trans('notification.group-create-or-update'), 'top');

        return redirect()->route('group-index-admin');
    }

    public function destroy($id)
    {
        ClientsGroup::find($id)->delete();

        Notifications::success(trans('notification.group-destroy'), 'top');

        return redirect()->route('group-index-admin');
    }
}
