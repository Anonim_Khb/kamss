@extends('auth.main')

@section('css')
    <link href="/libs/bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet" type='text/css'>
@endsection

@section('body')
    <div class="container">
        <div class="row">
            <div class="login-box">
                @include('partials.logo-on-page')
                @include('basis.notifications-page')
                <div class="login-box-body">
                    <p class="login-box-msg">{{ trans('auth.form.login-title') }}</p>
                    <form role="form" method="POST" action="{{ route('login.post') }}">
                        {!! csrf_field() !!}
                        <div class="form-group has-feedback">
                            <input type="email" name="email" class="form-control"
                                   placeholder="{{ trans('auth.form.login-email-placeholder') }}"
                                   value="{{ old('email') }}">
                            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                        </div>
                        <div class="form-group has-feedback">
                            <input type="password" name="password" class="form-control"
                                   placeholder="{{ trans('auth.form.login-password-placeholder') }}">
                            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        </div>
                        @if(conf_site()->getOne(\App\Models\Setting::CAPTCHA_LOGIN))
                            @include('partials.recaptcha')
                        @endif
                        <br>
                        <div class="row">
                            <div class="col-xs-8">
                                <div class="checkbox checkbox-primary">
                                    <input id="remember" type="checkbox" class="styled" name="remember" checked>
                                    <label for="remember">{{ trans('auth.form.login-remember') }}</label>
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <button type="submit" class="btn btn-primary btn-flat pull-right">
                                    {{ trans('auth.button.login-submit') }}
                                </button>
                            </div>
                        </div>
                    </form>
                    <a href="{{ route('password-reset.email') }}">{{ trans('auth.button.login-forgot-password') }}</a>
                </div>
            </div>
        </div>
    </div>
@endsection
