<?php

namespace App\Http\DTO;

class clientsExcelDTO
{
    /**
     * @var array
     */
    private $page = ['name' => 'Контакты', 'number' => 1];
    /**
     * @var array
     */
    private $id = ['name' => 'ID', 'export' => 'A', 'import' => 0, 'width' => 6];
    /**
     * @var array
     */
    private $name = ['name' => 'ФИО', 'export' => 'B', 'import' => 1, 'width' => 39];
    /**
     * @var array
     */
    private $company = ['name' => 'Организация', 'export' => 'C', 'import' => 2, 'width' => 30];
    /**
     * @var array
     */
    private $email = ['name' => 'Эл.адрес', 'export' => 'D', 'import' => 3, 'width' => 26];
    /**
     * @var array
     */
    private $treatment = ['name' => 'Обращение', 'export' => 'E', 'import' => 4, 'width' => 28];
    /**
     * @var array
     */
    private $phone = ['name' => 'Телефон', 'export' => 'F', 'import' => 5, 'width' => 28];
    /**
     * @var array
     */
    private $position = ['name' => 'Должность', 'export' => 'G', 'import' => 6, 'width' => 26];

    public function getPage($key)
    {
        return $this->page[$key];
    }
    /**
     * @return string
     */
    public function getId($key = 'export')
    {
        return $this->id[$key];
    }
    /**
     * @return string
     */
    public function getName($key = 'export')
    {
        return $this->name[$key];
    }
    /**
     * @return string
     */
    public function getCompany($key = 'export')
    {
        return $this->company[$key];
    }
    /**
     * @return string
     */
    public function getEmail($key = 'export')
    {
        return $this->email[$key];
    }
    /**
     * @return string
     */
    public function getTreatment($key = 'export')
    {
        return $this->treatment[$key];
    }
    /**
     * @return string
     */
    public function getPhone($key = 'export')
    {
        return $this->phone[$key];
    }
    /**
     * @return string
     */
    public function getPosition($key = 'export')
    {
        return $this->position[$key];
    }
}