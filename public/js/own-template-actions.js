toastr_options();
function deleteOwnTemplate(element, OwnId, url) {
    swal({
        title: "Вы уверены?",
        text: "Вы хотите удалить шаблон",
        type: "question",
        confirmButtonColor: warningColor,
        confirmButtonText: "Да",
        showCancelButton: true,
        cancelButtonColor: defaultColor,
        cancelButtonText: 'Отмена'
    }).then(function() {
        $.ajax({
            type: "POST",
            dataType: "text",
            url: url,
            data: {OwnId: OwnId, _token: $("meta[name='csrf-token']").attr('content')},
            cache: false,
            success: function (data) {
                if (data == 'success') {
                    toastr["warning"]('Шаблон был удален');
                    $(element).parents('tr').hide('slow');
                }
            }
        });
    }, function(dismiss) {
        // dismiss can be 'cancel', 'overlay', 'close', 'timer'
        if (dismiss === 'cancel') {
            //
        }
    });
}

function redirectToRoute(route) {
    window.location.href = route;
}
