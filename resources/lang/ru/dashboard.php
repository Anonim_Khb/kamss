<?php

return [

    'title' => [
        'prepend' => 'Доска',
        'index' => 'Главная',
    ],

    'header' => [
        'logout' => 'Выйти'
    ],

    'left-menu' => [
        'title' => 'Меню навигации',
        'administrators' => 'Администраторы',
    ],

    'header-notifications' => '{0} оповещений|{1} оповещение|[2,4] оповещения|[5,Inf] оповещений',
    'clients-all' => '{0}Заказчиков в списке|{1}Заказчик в списке|[2,4]Заказчика в списке|[5,Inf]Заказчиков в списке',
    'clients-active' => '{0}Активных заказчиков|{1}Активный заказчик|[2,4]Активных заказчика|[5,Inf]Активных заказчиков',
    'clients-inactive' => '{0}Неактивных заказчиков|{1}Неактивный заказчик|[2,4]Неактивных заказчика|[5,Inf]Неактивных заказчиков',
    'bids-all' => '{0}Запросов создано|{1}Запрос создан|[2,Inf]Запроса созданы|[5,Inf]Запросов созданы',
    'bids-active' => '{0}Активных запросов|{1}Активный запрос|[2,4]Активных запроса|[5,Inf]Активных запросов',
    'bids-for-today' => '{0}Срочных запросов|{1}Срочный запрос|[2,4]Срочных запроса|[5,Inf]Срочных запросов',
    'note-all' => '{0}Заметок сохранено|{1}Заметка сохранена|[2,4]Заметки сохранены|[5,Inf]Заметок сохранены',
    'note-active' => '{0}Активных заметок|{1}Активная заметка|[2,4]Активных заметки|[5,Inf]Активных заметок',
    'note-inactive' => '{0}Неактивных заметок|{1}Неактивная заметка|[2,4]Неактивных заметки|[5,Inf]Неактивных заметок',
    'send-email-statistics' => '{0} писем|{1} письмо|[2,4] письма|[5,Inf] писем',
    'import-errors-info' => '{1}найдена :count ошибка|[2,4]найдены :count ошибки||[5,20]найдены :count ошибок|[21,Inf]найдены :count ошибки',
    'import-errors-page' => '{0}:count ошибок|{1}:count ошибка|[2,4]:count ошибки||[5,20]:count ошибок|[21,Inf]:count ошибки',
];
