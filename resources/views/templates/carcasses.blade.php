@extends('main')

@section('body-title')
    Каркасы писем
@endsection

@section('body')

    <div class="users-index no-background email-templates">
        @include('partials._info-block-1', ['text' => trans('info.carcasses-templates')])
        @if(!session()->has('t_text') || session('t_type') != \App\Models\UsersTemplate::STATUS_OWN)
            <div class="alert alert-danger" role="alert">
                <span style="color: #a94442">
                    Для того чтобы иметь возможность выбрать каркас письма и отправить письмо со своим текстом, вначале
                    необходимо выбрать <a style="color: #00a8d5" href="{{ route('templates-own') }}">Свой шаблон</a>
                </span>
            </div>
        @endif
        @foreach($carcasses as $carcass)
            <div class="col-sm-4 col-md-3">
                <div class="thumbnail">
                    <img style="background: url('/images/templates/{{ $carcass }}.png') no-repeat; background-size: cover;"
                         src="">
                    <div class="caption">
                        <h3>{{ $carcass }}</h3>
                        <p>
                            <a href="{{ route('choose-template-for-send', [$carcass, \App\Models\UsersTemplate::STATUS_OWN, session('t_text')]) }}"
                                    class="btn btn-warning @if(!session()->has('t_text') || session('t_type') != \App\Models\UsersTemplate::STATUS_OWN) not-active-link @endif"
                                    role="button">
                                {{ trans('templates.index.choose') }}
                            </a>
                            <a href="{{ route('template-show', [$carcass, \App\Models\UsersTemplate::STATUS_OWN, session('t_text')]) }}"
                                    class="btn btn-primary pull-right" role="button" target="_blank">
                                {{ trans('templates.index.show') }}
                            </a>
                        </p>
                    </div>
                </div>
            </div>
        @endforeach
    </div>

@endsection
