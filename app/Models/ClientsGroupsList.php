<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClientsGroupsList extends Model
{
    protected $table = 'clients_groups_lists';

    protected $fillable = [
        'client_id', 'group_id'
    ];
}
