@extends('main')

@section('css')
    <link href="/libs/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" type='text/css'>
    <link href="/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type='text/css'>
    <link href="/css/datatable.css" rel="stylesheet" type='text/css'>
@endsection

@section('body-title')

    Запросы

    @include('partials.button-create', ['route' => route('bid-create-or-update-get'), 'text' => 'Создать'])

@endsection

@section('body')

    <div class="users-index no-background">
        <form class="no-background form-inline" action="{{ route('bid-index')}}"
              method="GET">
            <br>
            <div class="input-group input-group-sm border-bg-blue">
                    <span class="input-group-addon bg-orange" id="sizing-addon2">
                        <b>Статус:</b>
                    </span>
                <select name="current_stage" class="form-control form-filter-activation"
                        aria-describedby="sizing-addon2">
                    <option @if(request('current_stage') == 'all') selected
                            @endif value="all">Все
                    </option>
                    <option @if(request('current_stage') == null) selected
                            @endif value="{{ null }}">Активные
                    </option>
                    <option @if(request('current_stage') == \App\Models\BidCurrentStage::STATUS_INACTIVE) selected
                            @endif value="{{ \App\Models\BidCurrentStage::STATUS_INACTIVE }}">Завершенные
                    </option>
                </select>
            </div>
            &nbsp;
            <div class="input-group input-group-sm border-bg-blue">
                    <span class="input-group-addon bg-blue" id="sizing-addon3">
                        <b>Дата создания</b>
                    </span>
                <select name="created" class="form-control form-filter-activation" aria-describedby="sizing-addon3">
                    <option @if(request('created') == null) selected
                            @endif value="{{ null }}">За все время
                    </option>
                    <option @if(request('created') == 1) selected
                            @endif value="1">За сегодня
                    </option>
                    <option @if(request('created') == 7) selected
                            @endif value="7">За неделю
                    </option>
                    <option @if(request('created') == 31) selected
                            @endif value="31">За месяц
                    </option>
                    <option @if(request('created') == 365) selected
                            @endif value="365">За год
                    </option>
                </select>
            </div>
            &nbsp;
            <div class="input-group input-group-sm border-bg-blue">
                    <span class="input-group-addon bg-olive" id="sizing-addon2">
                        <b>След.контакт:</b>
                    </span>
                <select name="next_contact" class="form-control form-filter-activation"
                        aria-describedby="sizing-addon2">
                    <option @if(request('next_contact') == null) selected
                            @endif value="{{ null }}">Не учитывать
                    </option>
                    <option @if(request('next_contact') == 'today') selected
                            @endif value="today">Срочные
                    </option>
                    <option @if(request('next_contact') == 'week') selected
                            @endif value="week">Ближ.неделя
                    </option>
                </select>
            </div>
            <div class="func-btns-prie">
                @include('partials._print_button')
            </div>
            <br><br>
        </form>
        @if(!$bids->isEmpty())
            <div class="table-responsive">
                <table id="BidTable" class="for-print table table-hover table-bordered table-responsive text-center">
                    <thead>
                    <tr>
                        <th class="col-sm-1">Id</th>
                        <th class="no-print action-dropdown-icon"><i class="fa fa-flash fa-lg"></i></th>
                        <th>ФИО</th>
                        <th>Организация</th>
                        <th>След.конт.</th>
                        <th>След.действие</th>
                        <th>ДВС</th>
                        <th>Источник</th>
                        <th>Сектор</th>
                        <th>Объект</th>
                        <th>Категория</th>
                        <th>Цена</th>
                        <th>Условие</th>
                        <th>Перв.конт.</th>
                        <th>Посл.конт.</th>
                        <th>Стадия</th>
                        <th>Комментарий</th>
                        <th>Посл.изм.</th>
                    </tr>
                    </thead>
                    <tfoot class="no-print">
                    <tr>
                        <th class="col-sm-1">Id</th>
                        <th class="no-print"></th>
                        <th>ФИО</th>
                        <th>Организация</th>
                        <th>След.конт.</th>
                        <th>След.действие</th>
                        <th>ДВС</th>
                        <th>Источник</th>
                        <th>Сектор</th>
                        <th>Объект</th>
                        <th>Категория</th>
                        <th>Цена</th>
                        <th>Условие</th>
                        <th>Перв.конт.</th>
                        <th>Посл.конт.</th>
                        <th>Стадия</th>
                        <th>Комментарий</th>
                        <th>Посл.изм.</th>
                    </tr>
                    </tfoot>
                    <tbody>
                    @foreach($bids as $bid)
                        <tr>
                            <td>{{ $bid->id }}</td>
                            <td class="no-print action-dropdown-btn" style="white-space: nowrap;">
                                <div class="dropdown">
                                    <span id="bidDd{{ $bid->id }}" class="btn-for-actions" data-toggle="dropdown"
                                          aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-ellipsis-h fa-fw fa-lg"></i>
                                    </span>
                                    <ul class="list-inline dropdown-menu animated jello" aria-labelledby="bidDd{{ $bid->id }}">
                                        @if($bid->status == \App\Models\Bid::STATUS_PRIVATE)
                                            <li class="dmb-success" data-toggle="tooltip"
                                                data-placement="top"
                                                title="Приватная запись | Изменить"
                                                onclick="changeBidStatus(this, '{{ $bid->id }}', '{{ route('change-status-bid-ajax') }}')">
                                                <i class="fa fa-volume-up"></i>
                                            </li>
                                        @else
                                            <li class="dmb-warning" data-toggle="tooltip"
                                                data-placement="top"
                                                title="Публичная запись | Изменить"
                                                onclick="changeBidStatus(this, '{{ $bid->id }}', '{{ route('change-status-bid-ajax') }}')">
                                                <i class="fa fa-volume-up"></i>
                                            </li>
                                        @endif
                                        <li onclick="redirectToRoute('{{ route('bid-create-or-update-get', $bid->id) }}')"
                                            class="dmb-info" data-toggle="tooltip" data-placement="top"
                                            title="Просмотр | Редактирование">
                                            <i class="fa fa-eye"></i>
                                        </li>
                                        <li onclick="deleteBid(this, '{{ $bid->id }}', '{{ route('delete-bid-ajax') }}')"
                                            class="dmb-danger" data-toggle="tooltip" data-placement="top"
                                            title="{{ trans('group.index.table.delete-tooltip') }}"><i
                                                    class="fa fa-power-off"></i>
                                        </li>
                                    </ul>
                                </div>
                            </td>
                            <td class="bid-name-td">
                                @if($bid->client->status == \App\Models\Client::STATUS_DELETED)
                                    <span class="text-red">Заказчик удален</span>
                                @else
                                    <a target="_blank"
                                       href="{{ route('clients-create-or-update-get', $bid->client->id) }}"
                                       data-toggle="tooltip" data-placement="top" title="{{ $bid->client->name }}">
                                        {{ $bid->client->name }}
                                    </a>
                                @endif
                            </td>
                            <td class="bid-company-td">
                                <span data-toggle="tooltip" data-placement="top"
                                      title="{{ $bid->client->clientCompany->name }}">
                                    {{ $bid->client->status == \App\Models\Client::STATUS_DELETED ? '' : $bid->client->clientCompany->name }}
                                </span>
                            </td>
                            <td>{{ $bid->next_contact }}</td>
                            <td class="bid-next-action-td">
                                <span data-toggle="tooltip" data-placement="left" title="{{ $bid->next_action }}">
                                    {{ $bid->next_action }}
                                </span>
                            </td>
                            <td>{{ $bid->engine }}</td>
                            <td>{{ $bid->source }}</td>
                            <td>{{ $bid->sector }}</td>
                            <td class="bid-objectdeal-td">
                                <span data-toggle="tooltip" data-placement="top" title="{{ $bid->object_deal }}">
                                    {{ $bid->object_deal }}
                                </span>
                            </td>
                            <td>{{ $bid->category }}</td>
                            <td style="white-space: nowrap;">
                                {{ $bid->price }}
                                @if($bid->price && $bid->currency == \App\Models\Bid::CURRENCY_DOLLAR)
                                    <i class="fa fa-dollar"></i>
                                @elseif($bid->price && $bid->currency == \App\Models\Bid::CURRENCY_RUBLE)
                                    <i class="fa fa-ruble"></i>
                                @elseif($bid->price && $bid->currency == \App\Models\Bid::CURRENCY_EURO)
                                    <i class="fa fa-euro"></i>
                                @endif
                            </td>
                            <td>{{ $bid->payment_term }}</td>
                            <td>{{ $bid->first_contact }}</td>
                            <td>{{ $bid->last_contact }}</td>
                            <td>{{ $bid->current_stages->name }}</td>
                            <td class="bid-comment-td">
                                <span data-toggle="tooltip" data-placement="left" title="{{ $bid->comment }}">
                                    {{ $bid->comment }}
                                </span>
                            </td>
                            <td>{{ ta($bid->updated_at) }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @else
            <div class="no-results text-center">
                <i class="fa fa-file-code-o fa-3x"></i>
                <p>Записей пока нет</p>
            </div>
        @endif
    </div>

@endsection

@section('js')
    <script src="/libs/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="/libs/sweetalert2/sweetalert2.min.js"></script>
    <script src="/js/datatable.js"></script>
    <script src="/js/bid-actions.js"></script>
    <script>
        $(document).ready(function () {
            myDataTable('#BidTable', true, 25);
            formSending('.form-filter-activation');
        });
    </script>
@endsection
