<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    public $timestamps = false;

    protected $table = 'settings';

    protected $fillable = [
        'key',
        'value',
        'description',
        'user_id'
    ];

    const SETTING_FOR_ALL = 0;

    const CAPTCHA_REGISTER = 'captcha.register';
    const CAPTCHA_LOGIN = 'captcha.login';
    const CAPTCHA_PASS_RESET = 'captcha.pass_reset';
    const CHANGE_CLIENT_COMPANY = 'client.change_company';
    const TEMPLATES_READY = 'templates.ready';
    const FILES_WITH_EMAILS = 'files.email';

    public function getMany($key = null, $default = null, $user_id = self::SETTING_FOR_ALL)
    {
        $result = $this->where('user_id', $user_id);

        if ($key != null) $result = $result->where('key', $key);

        $result = $result->get();

        return count($result) > 0 ? $result : $default;
    }

    public function getOne($key = null, $default = null, $user_id = self::SETTING_FOR_ALL)
    {
        if($key == null) {
            return $default;
        } else {
            $result = $this->where('user_id', $user_id)
                ->where('key', $key)->value('value');
        }

        return count($result) > 0 ? $result : $default;
    }
}
