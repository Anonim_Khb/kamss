<?php

namespace App\Http\Controllers;

use App\Events\ReportDataChanged;
use App\Events\ReportPasswordChangedAdmin;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Bid;
use App\Models\BidCurrentStage;
use App\Models\Client;
use App\Models\CompanyBranch;
use App\Models\Faq;
use App\Models\Note;
use App\Models\Notification;
use App\Models\SendStatistic;
use App\Models\User;
use Notifications;
use App\Http\Requests\UserCreateOrUpdateRequest;
use App\Events\NewUserCreated;
use Jenssegers\Date\Date;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('inactive');

        view()->share('active_link', 'users_admin_index');
    }

    public function createFromRequest($password, $request)
    {
        $user = new User();
        $user->name = $request->name;
        $user->treatment = $request->treatment;
        $user->work_phone = $request->work_phone;
        $user->cell_phone = $request->cell_phone;
        $user->email = $request->email;
        $user->branch = $request->branch;
        $user->roles = $request->roles;
        $user->password = $password;
        $user->save();

        return $user;
    }

    public function getUsersList()
    {
        $users = User::orderBy('id', 'desc')->with('companyBranch')->get();

        \Title::append(trans('title.users-index'));

        return view('user.users_list_manage', ['users' => $users]);
    }

    public function changeUserStatusAjax(User $user)
    {
        $user = $user->where('id', request('userId'))->first();

        $newStatus = $user->status == User::STATUS_ACTIVE ? User::STATUS_INACTIVE : User::STATUS_ACTIVE;

        $user->update(['status' => $newStatus]);

        return $newStatus;
    }

    public function destroyUserAjax(User $user)
    {
        $user->where('id', request('userId'))
            ->where('id', '!=', 1)
            ->delete();

        return 'success';
    }

    public function create(User $roles, CompanyBranch $branch)
    {
        $roles = $roles->getRoles();
        $branches = $branch->orderBy('city', 'asc')->get();

        \Title::append(trans('title.request-add-accepted-admin'));

        return view('user.user_create_update', ['roles' => $roles, 'branches' => $branches]);
    }

    public function edit(CompanyBranch $branch)
    {
        $user = \Auth::user();

        $roles = $user->getRoles();

        $branches = $branch->orderBy('city', 'asc')->get();

        \Title::append(trans('title.request-add-accepted-admin'));

        return view('user.user_create_update', ['user' => $user, 'roles' => $roles, 'branches' => $branches]);
    }

    public function editForAdmin($id, CompanyBranch $branch, User $user)
    {
        $user = $user->find($id);

        $roles = $user->getRoles();

        $branches = $branch->orderBy('city', 'asc')->get();

        \Title::append(trans('title.request-add-accepted-admin'));

        return view('user.user_create_update', ['user' => $user, 'roles' => $roles, 'branches' => $branches]);
    }

    public function createOrUpdate(UserCreateOrUpdateRequest $request, User $user, $id = null)
    {
        $user = $user::findOrNew($id);
        $user->name = srq(e($request->name));
        $user->treatment = srq(e($request->treatment));
        $user->cell_phone = srq(e($request->cell_phone));
        $user->work_phone = srq(e($request->work_phone));

        $user->signature = e($request->signature);

        if ($request->has('signature')) {
            $user->signature_status = $request->has('signature_status') ? User::SIGNATURE_ACTIVE : User::SIGNATURE_INACTIVE;
        } else {
            $user->signature_status = User::SIGNATURE_INACTIVE;
        }

        if (ur(\App\Models\User::ROLE_MODERATOR)) {
            $user->email = srq($request->email);
            $user->roles = srq($request->roles);
            $user->branch = srq($request->branch);

            if ($request->has('password')) {
                $user->password = $request->password;
            }
        }

        $user->save();

        if ($request->has('notification_edit')) {
            $notification = new Notification();
            $notification->setEditUserNotification($user->id);
        } elseif ($request->has('notification_new_user')) {
            $notification = new Notification();
            $notification->setNewUserNotification($user->id, $notification::ID_FOR_ALL);
        }


        if (ur(\App\Models\User::ROLE_MODERATOR)) {
            if ($request->has('send_welcome')) {
                event(new NewUserCreated($user, $request->password));
            }
            if ($request->has('report_info')) {
                event(new ReportDataChanged($user));
            }
            if ($request->has('report_password')) {
                event(new ReportPasswordChangedAdmin($user, $request->password));
            }
        }

        Notifications::success(trans('notification.user-update-create-success'), 'top');

        if (ur(\App\Models\User::ROLE_MODERATOR)) {
            return redirect()->route('users-list-admin');
        } else {
            return redirect()->route('panel-index');
        }


    }

    public function infoForAdmin($id, User $user, Client $client, Bid $bid, Note $note, SendStatistic $statistic, Faq $faq)
    {
        $user = $user->find($id);

        $clients_all = $client->where('author_id', $user->id)
            ->where('status', '!=', Client::STATUS_DELETED)->count();
        $clients_active = $client->where('author_id', $user->id)->where('status', Client::STATUS_ACTIVE)->count();
        $clients_inactive = $client->where('author_id', $user->id)->where('status', Client::STATUS_INACTIVE)->count();

        $bids_all = $bid->where('author_id', $user->id)->count();
        $bids_active = $bid->where('author_id', $user->id)->whereHas('current_stages', function ($query) {
            $query->where('status', BidCurrentStage::STATUS_ACTIVE);
        })->count();
        $bids_today = $bid->where('author_id', $user->id)->where('next_contact', df(Date::now(), 'Y-m-d'))->whereHas('current_stages', function ($query) {
            $query->where('status', BidCurrentStage::STATUS_ACTIVE);
        })->count();

        $note_all = $note->where('author_id', $user->id)->count();
        $note_active = $note->where('author_id', $user->id)->where('status', Note::STATUS_ACTIVE)->count();
        $note_inactive = $note->where('author_id', $user->id)->where('status', Note::STATUS_INACTIVE)->count();

        $client_last_create = $client->where('author_id', $user->id)
            ->where('status', '!=', Client::STATUS_DELETED)->max('created_at');
        $client_last_send = $client->where('author_id', $user->id)
            ->where('status', '!=', Client::STATUS_DELETED)->max('last_send');
        $bid_last_create = $bid->where('author_id', $user->id)->max('created_at');
        $note_last_create = $note->where('author_id', $user->id)->max('created_at');

        $statistic_day = $statistic->where('user_id', $user->id)->where('created_at', '>', Date::now()->subDay())->sum('count');
        $statistic_month = $statistic->where('user_id', $user->id)->where('created_at', '>', Date::now()->subMonth())->sum('count');
        $statistic_year = $statistic->where('user_id', $user->id)->where('created_at', '>', Date::now()->subYear())->sum('count');
        $statistic_all = $statistic->where('user_id', $user->id)->sum('count');

        \Title::append('Информация');

        return view('user.user_info_admin', [
            'user' => $user,
            'clients_all' => $clients_all,
            'clients_active' => $clients_active,
            'clients_inactive' => $clients_inactive,
            'bids_all' => $bids_all,
            'bids_active' => $bids_active,
            'bids_today' => $bids_today,
            'note_all' => $note_all,
            'note_active' => $note_active,
            'note_inactive' => $note_inactive,
            'client_last_create' => $client_last_create,
            'client_last_send' => $client_last_send,
            'bid_last_create' => $bid_last_create,
            'note_last_create' => $note_last_create,
            'statistic_day' => $statistic_day,
            'statistic_month' => $statistic_month,
            'statistic_year' => $statistic_year,
            'statistic_all' => $statistic_all,
        ]);
    }
}
