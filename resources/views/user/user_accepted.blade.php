@extends('main')

@section('body-title')

    {!! trans('membership.accepted.title', ['name' => $membership->name]) !!}

@endsection

@section('body')
    <div class="no-background">
        @include('basis.notifications-page')
    </div>

    <div class="register-box">
        <form action="{{ route('membership-accepted-admin-post') }}"
              method="POST">
            {{ csrf_field() }}

            <div class="form-group has-feedback">
                <label for="name">{{ trans('user.form.name') }}</label>
                <input id="name" type="text" name="name" class="form-control"
                       placeholder="{{ trans('user.form.name-placeholder') }}"
                       value="{{ old('name', $membership->name) }}">
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <label for="treatment">{{ trans('user.form.treatment') }}</label>
                <input id="treatment" type="text" name="treatment" class="form-control"
                       placeholder="{{ trans('user.form.treatment-placeholder') }}"
                       value="{{ old('treatment') }}">
                <span class="glyphicon glyphicon-volume-up form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <label for="cell_phone">{{ trans('user.form.cell_phone') }}</label>
                <input id="cell_phone" type="text" name="cell_phone" class="form-control"
                       placeholder="{{ trans('user.form.cell_phone-placeholder') }}"
                       value="{{ old('cell_phone', $membership->cell_phone) }}">
                <span class="glyphicon glyphicon-phone form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <label for="work_phone">{{ trans('user.form.work_phone') }}</label>
                <input id="work_phone" type="text" name="work_phone" class="form-control"
                       placeholder="{{ trans('user.form.work_phone-placeholder') }}"
                       value="{{ old('work_phone', $membership->work_phone) }}">
                <span class="glyphicon glyphicon-phone-alt form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <label for="email">{{ trans('user.form.email') }}</label>
                <input id="email" type="email" name="email" class="form-control"
                       placeholder="{{ trans('user.form.email-placeholder') }}"
                       value="{{ $membership->email }}">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <label for="branch">{{ trans('user.form.branch') }}</label>
                <select id="branch" name="branch" class="form-control">
                    <option value="{{ null }}">{{ trans('user.form.branch-none') }}</option>
                    @foreach($branches as $branch)
                        <option value="{{ $branch->id }}" class="text-uppercase">{{ $branch->city }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group has-feedback">
                <label for="roles">{{ trans('user.form.role') }}</label>
                <select id="roles" name="roles" class="form-control">
                    <option value="{{ null }}">{{ trans('user.form.role-none') }}</option>
                    @foreach($roles as $role)
                        <option value="{{ $role }}" class="text-uppercase">{{ $role }}</option>
                    @endforeach
                </select>
            </div>
            <p>
                <strong>{{ trans('user.form.comment') }}: </strong>{{ $membership->text }}
            </p>
            <p>
                <strong>{{ trans('user.form.status') }}: </strong>{{ $membership->status }}
            </p>
            <p>
                <strong>{{ trans('user.form.created') }}: </strong>{{ df($membership->created_at) }}
            </p>
            <input type="hidden" name="id" value="{{ $membership->id }}">
            <button type="submit" class="btn btn-primary">
                {{ trans('membership.accepted.table.btn-create') }}
                <i class="fa fa-check"></i>
            </button>
            <br>
            <br>
        </form>
    </div>
@endsection
