@extends('main')

@section('css')
    <link href="/libs/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" type='text/css'>
    <link href="/css/datatable.css" rel="stylesheet" type='text/css'>
@endsection

@section('body-title')

    Помощь (админ)

    @include('partials.button-create', ['route' => route('help-categories-admin'), 'text' => 'Категории'])

@endsection

@section('body')

    <div class="users-index no-background">

        <form class="no-background form-inline" action="{{ route('help-index-admin') }}"
              method="GET">
            <br>
            <div class="input-group input-group-sm border-bg-blue">
            <span class="input-group-addon bg-blue" id="sizing-addon2">
                <b>{{ trans('clients.index.table.sort-status') }}</b>
            </span>
                <select name="status" class="form-control form-filter-activation" aria-describedby="sizing-addon2">
                    <option @if(request('status') == 'all') selected @endif value="all">Все</option>
                    <option @if(request('status') == null || request('status') == \App\Models\Support::STATUS_UNREAD) selected
                            @endif value="{{ null }}">Непрочитанные
                    </option>
                    <option @if(request('status') == \App\Models\Support::STATUS_READ) selected
                            @endif value="{{ \App\Models\Support::STATUS_READ }}">Прочитанные
                    </option>
                </select>
            </div>
            <br>
            <br>
        </form>


        @if(!$supports->isEmpty())
            <div class="table-responsive">
                <table id="SupportTable" class="table table-hover table-bordered text-center">
                    <thead>
                    <tr>
                        <th class="col-sm-1">Id</th>
                        <th>Автор</th>
                        <th>Название</th>
                        <th>{{ trans('templates.table.created') }}</th>
                        <th>{{ trans('templates.table.actions') }}</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th class="col-sm-1">Id</th>
                        <th>Автор</th>
                        <th>Название</th>
                        <th>{{ trans('templates.table.created') }}</th>
                        <th>{{ trans('templates.table.actions') }}</th>
                    </tr>
                    </tfoot>
                    <tbody>
                    @foreach($supports as $support)
                        <tr>
                            <th>{{ $support->id }}</th>
                            <td>{{ $support->author->name }}</td>
                            <td>{{ str_limit($support->title, 100, '....') }}</td>
                            <th>{{ ta($support->created_at) }}</th>
                            <td style="white-space: nowrap;">
                                <a class="btn btn-xs btn-info" data-toggle="tooltip" data-placement="top"
                                   title="Просмотр" href="{{ route('help-watch-support-admin', $support->id) }}">
                                    <i class="fa fa-eye"></i>
                                </a>
                                <a onclick="changeSupportStatus(this, '{{ $support->id }}', 'Сменить статус?', '{{ route('support-change-status-ajax') }}')"
                                   type="button" class="btn btn-xs
                                   @if($support->status == \App\Models\Support::STATUS_READ) btn-success
                                    @else btn-warning @endif" data-toggle="tooltip"
                                   data-placement="top"
                                   title="Сменить статус"><i class="fa fa-pencil"></i>
                                </a>
                                <a type="submit"
                                   onclick="deleteSupport(this, '{{ $support->id }}', 'Удалить?', '{{ route('support-destroy-ajax') }}')"
                                   class="btn btn-xs btn-danger" data-toggle="tooltip" data-placement="top"
                                   title="{{ trans('group.index.table.delete-tooltip') }}"><i
                                            class="fa fa-power-off"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @else
            <div class="no-results text-center">
                <i class="fa fa-file-code-o fa-3x"></i>
                <p>Обращений нет</p>
            </div>
        @endif
    </div>

@endsection

@section('js')
    <script src="/libs/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="/js/datatable.js"></script>
    <script src="/js/support-actions.js"></script>
    <script>
        $(document).ready(function () {
            myDataTable('#SupportTable', true, 25);
            formSending('.form-filter-activation');
        });
    </script>
@endsection
