<?php

namespace App\Http\Middleware;

use Closure;
use Jenssegers\Date\Date;

class LocaleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        Date::setLocale(config('app.locale'));

//        if ($request->session()->has('locale')) {
//            \App::setLocale($request->session()->get('locale'));
//
//            Date::setLocale($request->session()->get('locale'));
//        } else {
//            session(['locale' => (config('app.locale'))]);
//
//            Date::setLocale(config('app.locale'));
//        }
        return $next($request);
    }
}
