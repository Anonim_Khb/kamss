<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\User;

class CreateBidsTable extends Migration
{
    public function up()
    {
        Schema::create('bids', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('author_id');
            $table->integer('client_id')->nullable();
            $table->date('first_contact')->nullable();
            $table->string('source')->nullable();
            $table->string('region')->nullable();
            $table->string('sector')->nullable();
            $table->string('object_deal')->nullable();
            $table->string('price')->nullable();
            $table->string('currency')->nullable()->default(\App\Models\Bid::CURRENCY_DOLLAR);
            $table->string('payment_term')->nullable();
            $table->date('last_contact')->nullable();
            $table->date('next_contact')->nullable();
            $table->string('next_action')->nullable();
            $table->integer('current_stage');
            $table->string('category')->nullable();
            $table->string('engine')->nullable();
            $table->string('comment')->nullable();
            $table->string('status')->default(\App\Models\Bid::STATUS_PUBLIC);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('bids');
    }
}
