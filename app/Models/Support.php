<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Support extends Model
{
    protected $table = 'supports';

    protected $fillable = [
        'title', 'text', 'author_id', 'status'
    ];

    const STATUS_READ = 'read';
    const STATUS_UNREAD = 'unread';

    public function author()
    {
        return $this->hasOne('App\Models\User', 'id', 'author_id');
    }
}
