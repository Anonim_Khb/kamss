<!DOCTYPE html>

<html lang="ru">

<head>

    @include('basis.meta')

    <title>{{ \Title::render() }}</title>

    @include('basis.css')

    @yield('css')

    <link href='/css/dashboard.css' rel='stylesheet' type='text/css'>

</head>

<body class="hold-transition skin-blue sidebar-mini admin-dashboard
    @if(!conf_site()->getOne('sidebar', 1, \Auth::id()))
        sidebar-collapse
    @endif
">

@include('basis.notifications-top')

<div class="wrapper">

    @include('partials.loading')

    @include('partials.header')

    @include('partials.left-menu')

    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                @yield('body-title')
            </h1>
        </section>
        <section class="content">
            <div class="box">
                @yield('body')
            </div>
        </section>
    </div>

    @include('partials.footer')

    @if($importErrorsCount > 0)
        <div id="importErrorsInfo" class="text-center">
            При импорте {{ trans_choice('dashboard.import-errors-info', $importErrorsCount, ['count' => $importErrorsCount]) }}
            <a href="{{ route('impex-import-errors-index') }}" class="btn btn-sm bg-aqua-active">Исправить</a>
        </div>
    @endif

</div>

@include('basis.js')

@yield('js')

</body>

</html>
