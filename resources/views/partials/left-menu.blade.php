<aside class="main-sidebar">
    <section class="sidebar">
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ config('site.admin_avatar') }}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{ $user->name }}</p>
                <a style="margin-top: 1px" class="lm-user-edit" href="{{ route('user-edit') }}">
                    <i class="fa fa-pencil-square-o fa-lg text-aqua">
                        {{ trans('group.index.table.update-tooltip') }}
                    </i>
                </a>
            </div>
        </div>
        <ul class="sidebar-menu">
            <li class="header text-uppercase">
                {{ trans('menu.left.title') }}
            </li>
            <li @if(isset($active_link) && $active_link == 'dashboard_index') class="active" @endif>
                <a href="{{ route('panel-index') }}">
                    <i class="fa fa-desktop" style="color:#00c974"></i>
                    <span>Главная</span>
                </a>
            </li>
            <li @if(isset($active_link) && $active_link == 'client_index') class="active" @endif>
                <a href="{{ route('clients-index') }}">
                    <i class="fa fa-address-book" style="color: #00c0ef"></i>
                    <span>Контакты</span>
                </a>
            </li>
            <li @if(isset($active_link) && $active_link == 'bid_index') class="active" @endif>
                <a href="{{ route('bid-index') }}">
                    <i class="fa fa-shopping-cart" style="color: #ffa413"></i>
                    <span>Запросы</span>
                    @if($bidsTodayCount > 0)
                        <small class="label pull-right bg-red">{{ $bidsTodayCount }}</small>
                    @endif
                </a>
            </li>
            <li @if(isset($active_link) && $active_link == 'note_index') class="active" @endif>
                <a href="{{ route('note-index') }}">
                    <i class="fa fa-sticky-note-o" style="color: #8f8bf6"></i>
                    <span>Заметки</span>
                </a>
            </li>
            <li @if(isset($active_link) && $active_link == 'company_index') class="active" @endif>
                <a href="{{ route('companies-index') }}">
                    <i class="fa fa-building" style="color: #c666ac;"></i>
                    <span>Компании</span>
                    @if($companiesUncheckedCount > 0 && ur(\App\Models\User::ROLE_MODERATOR))
                        <small class="label pull-right bg-red">{{ $companiesUncheckedCount }}</small>
                    @endif
                    @if($companyChangesCount > 0 && ur(\App\Models\User::ROLE_MODERATOR))
                        <small class="label pull-right bg-orange-active">{{ $companyChangesCount }}</small>
                    @endif
                </a>
            </li>
            @if (conf_site()->getOne(\App\Models\Setting::TEMPLATES_READY))
                <li @if(isset($active_link) && $active_link == 'templates_ready_index') class="active" @endif>
                    <a href="{{ route('templates-ready') }}">
                        <i class="fa fa-file-archive-o" style="color: #39CCCC;"></i>
                        <span>{{ trans('menu.left.templates-ready') }}</span>
                    </a>
                </li>
            @endif
            <li @if(isset($active_link) && $active_link == 'templates_own_index') class="active" @endif>
                <a href="{{ route('templates-own') }}">
                    <i class="fa fa-file-code-o" style="color: #ff2285"></i>
                    <span>{{ trans('menu.left.templates-own') }}</span>
                </a>
            </li>
            <li @if(isset($active_link) && $active_link == 'impex_index') class="active" @endif>
                <a href="{{ route('impex-index') }}">
                    <i class="fa fa-eercast" style="color: #2b8af4;"></i>
                    <span>Импорт / Экспорт</span>
                </a>
            </li>
            <li @if(isset($active_link) && $active_link == 'help_index') class="active" @endif>
                <a href="{{ route('help-index') }}">
                    <i class="fa fa-support" style="color: #E8FF0C;"></i>
                    <span>Помощь</span>
                </a>
            </li>
            @if(ur(\App\Models\User::ROLE_MODERATOR))
                <li @if(isset($active_link) && $active_link == 'users_admin_index') class="active" @endif>
                    <a class="moderator-link" href="{{ route('users-list-admin') }}">
                        <i class="fa fa-user-secret"></i>
                        <span>{{ trans('menu.left.users') }}</span>
                    </a>
                </li>
                <li @if(isset($active_link) && $active_link == 'groups_admin_index') class="active" @endif>
                    <a class="moderator-link" href="{{ route('group-index-admin') }}">
                        <i class="fa fa-list-alt"></i>
                        <span>{{ trans('menu.left.groups') }}</span>
                    </a>
                </li>
                <li @if(isset($active_link) && $active_link == 'request_add_index') class="active" @endif>
                    <a class="moderator-link" href="{{ route('requests-show-admin') }}">
                        <i class="fa fa-user-secret"></i>
                        <span>{{ trans('menu.left.request-add') }}
                            @if($membershipRequests > 0)
                                <small class="label pull-right bg-red">{{ $membershipRequests }}</small>
                            @endif
                        </span>
                    </a>
                </li>
                <li @if(isset($active_link) && $active_link == 'help_admin_index') class="active" @endif>
                    <a class="moderator-link" href="{{ route('help-index-admin') }}">
                        <i class="fa fa-fire-extinguisher"></i>
                        <span>Помощь (админ)
                            @if($supportsCount > 0)
                                <small class="label pull-right bg-red">{{ $supportsCount }}</small>
                            @endif
                        </span>
                    </a>
                </li>
                <li @if(isset($active_link) && $active_link == 'notifications_admin') class="active" @endif>
                    <a class="moderator-link" href="{{ route('notifications-index') }}">
                        <i class="fa fa-bell"></i>
                        <span>Оповещения</span>
                    </a>
                </li>
            @endif
            @if(ur(\App\Models\User::ROLE_SUPERADMIN))
                <li @if(isset($active_link) && $active_link == 'settings_index') class="active" @endif>
                    <a class="moderator-link" href="{{ route('settings-index') }}">
                        <i class="fa fa-cogs"></i>
                        <span>Настройки</span>
                    </a>
                </li>
            @endif
        </ul>
    </section>
</aside>
