<?php

namespace App\Http\Controllers;

use App\Http\Classes\ImportErrorCorrections;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Client;
use App\Models\ClientsGroup;
use App\Models\ClientsGroupsList;
use App\Models\ImportError;
use App\Models\Notification;
use Notifications;
use Jenssegers\Date\Date;
use Validator;

class ClientsController extends Controller
{
    public function __construct()
    {
        $this->middleware('inactive')->except('index');

        view()->share('active_link', 'client_index');
    }

    public function index(Client $client, ClientsGroup $group)
    {
        $clients_created_day = $client->where('author_id', \Auth::user()->id)
            ->where('created_at', '>', Date::now()->subDay())->count();

        $templates = new TemplateController();

        $clients = $this->clientsSearch($client);

        $groups = $group->all();

        \Title::append(trans('clients.title.index'));

        return view('clients.clients_list_manage', [
            'clients' => $clients,
            'groups' => $groups,
            'templates' => $templates,
            'clients_created_day' => $clients_created_day
        ]);
    }

    public function clientsSearch($client)
    {
        $client = $client->where('author_id', auth()->user()->id)
            ->where('status', '!=', Client::STATUS_DELETED);

        if (request()->has('filter')) {
            $client = $client->where('status', request('filter'));
        }

        if (request()->has('group')) {
            if (request('group') == 'without') {
                $lists = ClientsGroupsList::select('client_id')->distinct()->get();

                $client = $client->whereNotIn('id', $lists);
            } else {
                $lists = ClientsGroupsList::where('group_id', request('group'))
                    ->select('client_id')->get();
                $client = $client->whereIn('id', $lists);
            }
        }

        if (request()->has('keywords')) {
            $client = $client->where(function ($query) {
                $query->where('name', 'LIKE', '%' . e(request('keywords')) . '%')
                    ->orWhere('email', 'LIKE', '%' . e(request('keywords')) . '%')
                    ->orWhere('phone', 'LIKE', '%' . e(request('keywords')) . '%')
                    ->orWhere('company', 'LIKE', '%' . e(request('keywords')) . '%');
            });
        }

        if (request()->has('at_first')) {
            $client = $client->orderBy('created_at', 'asc');
        } else {
            $client = $client->orderBy('created_at', 'desc');
        }

        if (request()->has('days')) {
            if (request('last_send_filter') == 'more') {
                $client = $client->where('last_send', '<', Date::now()->subDays(request('days')));
            } else {
                $client = $client->where('last_send', '>', Date::now()->subDays(request('days')));
            }
        }

        return $client->with(['groups', 'clientCompany'])->get();
    }

    public function createOrUpdate(
        Client $client,
        ClientsGroup $groups,
        ImportError $error,
        ImportErrorCorrections $corrections,
        $id = null,
        $iec = null
    ) {
        if ($iec) { //Import Error Client
            $clientError = $error->where('user_id', \Auth::id())->where('type', $error::TYPE_CLIENTS)->first();
            $corrections->showNotificationsErrors($clientError->errors);
            $client = (object)unserialize($clientError->array);
            $client->created_at = $clientError->created_at;
            $client->updated_at = $clientError->updated_at;
            $client->error_id = $clientError->id;
        } else {
            $client = $client->where('id', $id)
                ->where('author_id', \Auth::user()->id)
                ->with('groups')->first();
        }

        \Javascript::put([
            'routeSearch' => route('company-search-ajax'),
            'changeCompanyConfig' => conf_site()->getOne(\App\Models\Setting::CHANGE_CLIENT_COMPANY),
        ]);

        $groups = $groups->all();

        \Title::append($id ? 'Редактирование контакта' : 'Новый контакт');

        return view('clients.client_create_update', ['client' => $client, 'groups' => $groups]);
    }

    public function createOrUpdatePost(Request $request, Client $client, ImportError $error, $id = null)
    {
        $check = $client->validateCreateOrUpdate($id, $request->all(), \Auth::id());

        if (!is_null($check)) {
            foreach ($check as $error) {
                Notifications::danger($error, 'page');
            }
            return redirect()->back()->withInput();
        }

        $client = $client->createOrUpdate($id, $request->all(), \Auth::id());

        !isset($request->error_id) ?: $error::removeError(\Auth::id(), $request->error_id);

        if (!is_numeric($client)) {
            Notifications::error('Непредвиденная ошибка. Попробуйте еще раз.', 'top');
            return redirect()->back()->withInput();
        }

        Notifications::success(is_null($id) ? 'Контакт успешно создан' : 'Контакт успешно изменен', 'top');
        return $request->has('return') ? redirect()->back() : redirect()->route('clients-index');
    }

    public function changeStatusAjax(Client $client)
    {
        $client = $client->where('author_id', \Auth::user()->id)->where('id', request('clientId'))->first();

        $newStatus = $client->status == Client::STATUS_ACTIVE ? Client::STATUS_INACTIVE : Client::STATUS_ACTIVE;

        $client->update(['status' => $newStatus]);

        return $newStatus;
    }

    public function destroyAjax(Client $client)
    {
        $client = $client->where('author_id', \Auth::user()->id)->where('id', request('clientId'))->first();

        $client->update(['status' => Client::STATUS_DELETED]);

        return 'success';
    }

    public function unsubscribeRequest($email, $token, Client $client)
    {
        $query = $client->where('email', $email)
            ->where('token', $token)->with('author');

        $client = $query->first();

        if ($client) {
            $query->update(['status' => Client::STATUS_INACTIVE]);

            $notification = new Notification();
            $notification->setClientUnsubscribeNotification($client->name, $client->author->id);

            return view('unsubscribe.success');
        }

        return view('unsubscribe.error');
    }

    public function changeGroupAjax(Client $client, ClientsGroupsList $groupsList)
    {
        $user_id = \Auth::user()->id;
        $client_id = request('clientId');
        $group_id = request('groupId');

        $client = $client->where('author_id', $user_id)
            ->where('id', $client_id)->first();

        if ($client) {
            $check = $groupsList->where('client_id', $client_id)
                ->where('group_id', $group_id)->first();

            if ($check) {
                $client->groups()->detach($group_id);
                return 'remove';
            } else {
                $client->groups()->attach($group_id);
                return 'add';
            }
        }
    }
}
