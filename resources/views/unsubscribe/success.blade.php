@extends('errors.main')

@section('body')
    <div class="unsubscribe-page text-center">
        <h3 class="unsubscribe-text text-success">
            <i class="fa fa-check-circle-o fa-lg"></i>
            Вы успешно отписались от рассылок писем подобной тематики
        </h3>
        <img src="{{ '/images/error/500.png' }}" alt="Unsubscribe">
    </div>
@endsection
