toastr_options();
function setCheckedStatus(element, companyId, url) {
    swal({
        title: "Вы уверены?",
        text: "Вы хотите изменить статус компании (активировать)",
        type: "warning",
        confirmButtonColor: successColor,
        confirmButtonText: "Да",
        showCancelButton: true,
        cancelButtonColor: defaultColor,
        cancelButtonText: 'Отмена'
    }).then(function() {
        $.ajax({
            type: "POST",
            dataType: "text",
            url: url,
            data: {companyId: companyId, _token: $("meta[name='csrf-token']").attr('content')},
            cache: false,
            success: function (data) {
                if (data == 'ok') {
                    toastr["success"]('Компания прошла проверку');
                    $(element).removeClass('btn-warning').addClass('btn-success');
                } else {
                    swal(
                        'Ошибка!',
                        'Возникла ошибка. Пожалуйста, попробуйте еще раз.',
                        'error'
                    );
                }
            }
        });
    }, function(dismiss) {
        // dismiss can be 'cancel', 'overlay', 'close', 'timer'
        if (dismiss === 'cancel') {
            //
        }
    });
}
