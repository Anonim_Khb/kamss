@extends('main')

@section('body-title')

    {{ isset($group) ? trans('group.update.title') : trans('group.create.title') }}

@endsection

@section('body')

    <div class="users-create">
        <div class="register-box">

            @include('basis.notifications-page')

            <div class="login-box-body">
                <form action="{{ route('group-create-or-update-admin', ['id' => isset($group) ? $group->id : null]) }}"
                      method="POST">
                    {!! csrf_field() !!}
                    <div class="form-group has-feedback">
                        <input type="text" name="name" class="form-control"
                               placeholder="{{ trans('group.create.placeholder-name') }}"
                               @if(isset($group)) value="{{ old('name', $group->name) }}" @endif required>
                        <span class="glyphicon glyphicon-tower form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="text" name="description" class="form-control"
                               placeholder="{{ trans('group.create.placeholder-description') }}"
                               @if(isset($group)) value="{{ old('description', $group->description) }}" @endif required>
                        <span class="glyphicon glyphicon-pencil form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="text" name="icon" class="form-control"
                               placeholder="{{ trans('group.create.placeholder-icon') }}"
                               @if(isset($group)) value="{{ old('icon', $group->icon) }}" @endif required>
                        <span class="glyphicon glyphicon-tag form-control-feedback"></span>
                    </div>
                    <div class="row">
                        <div class="col-xs-4 pull-right">
                            <button type="submit"
                                    class="btn btn-primary btn-flat">
                                    {{ isset($group) ? trans('group.update.btn') : trans('group.create.btn') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
