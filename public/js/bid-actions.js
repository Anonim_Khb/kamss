toastr_options();
function changeBidStatus(element, bidId, url) {
    swal({
        title: "Вы уверены?",
        text: "Вы хотите изменить статус запроса",
        type: "question",
        confirmButtonColor: successColor,
        confirmButtonText: "Да",
        showCancelButton: true,
        cancelButtonColor: defaultColor,
        cancelButtonText: 'Отмена'
    }).then(function() {
        $.ajax({
            type: "POST",
            dataType: "text",
            url: url,
            data: {bidId: bidId, _token: $("meta[name='csrf-token']").attr('content')},
            cache: false,
            success: function (data) {
                if (data == 'private') {
                    toastr["success"]("Установлен приватный статус запроса");
                    $(element).removeClass('dmb-warning').addClass('dmb-success');
                } else {
                    toastr["warning"]("Установлен публичный статус запроса");
                    $(element).removeClass('dmb-success').addClass('dmb-warning');
                }
            }
        });
    }, function(dismiss) {
        // dismiss can be 'cancel', 'overlay', 'close', 'timer'
        if (dismiss === 'cancel') {
            //
        }
    });
}

function deleteBid(element, bidId, url) {
    swal({
        title: "Вы уверены?",
        text: "Вы хотите удалить запрос",
        type: "question",
        confirmButtonColor: warningColor,
        confirmButtonText: "Да",
        showCancelButton: true,
        cancelButtonColor: defaultColor,
        cancelButtonText: 'Отмена'
    }).then(function() {
        $.ajax({
            type: "POST",
            dataType: "text",
            url: url,
            data: {bidId: bidId, _token: $("meta[name='csrf-token']").attr('content')},
            cache: false,
            success: function (data) {
                if (data == 'success') {
                    toastr["warning"]("Запрос был удален");
                    $(element).parents('tr').hide('slow');
                }
            }
        });
    }, function(dismiss) {
        // dismiss can be 'cancel', 'overlay', 'close', 'timer'
        if (dismiss === 'cancel') {
            //
        }
    });
}

function redirectToRoute(route) {
    window.location.href = route;
}
