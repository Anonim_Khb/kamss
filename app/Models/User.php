<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    protected $table = 'users';

    protected $fillable = [
        'name',
        'work_phone',
        'cell_phone',
        'treatment',
        'email',
        'password',
        'branch_id',
        'status',
        'token',
        'last_restore',
        'signature',
        'signature_status',
    ];

    protected $hidden = [
        'password', 'remember_token'
    ];

    const STATUS_ACTIVE = 'active';
    const STATUS_INACTIVE = 'inactive';

    const SIGNATURE_ACTIVE = 'on';
    const SIGNATURE_INACTIVE = 'off';

    const ROLE_SUPERADMIN = 'superadmin';
    const ROLE_MODERATOR = 'moderator';
    const ROLE_CHIEF = 'chief';
    const ROLE_MANAGER = 'manager';
    const ROLE_STOREKEEPER = 'storekeeper';

    public function getRoles()
    {
        return [
            'superadmin', 'moderator', 'chief', 'manager', 'storekeeper'
        ];
    }

    public function companyBranch()
    {
        return $this->hasOne('App\Models\CompanyBranch', 'id', 'branch');
    }

    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }
}
