@extends('main')

@section('css')
    <script src='//cdn.tinymce.com/4/tinymce.min.js'></script>
    <link href="/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type='text/css'>
@endsection

@section('body-title')

    {{ trans('templates.create.title') }}

@endsection

@section('body')
    <div class="users-index">
        <div class="no-background">
            @include('basis.notifications-page')

            @if(!isset($own))
                @include('partials._info-block-1', ['text' => trans('info.create-templates') ])
            @endif
        </div>
        <form class="no-background" role="form" method="POST" enctype="multipart/form-data"
              action="{{ route('create-own-template-post', ['id' => isset($own) ? $own->id : null ]) }}">
            {!! csrf_field() !!}
            <div class="users-index">
                <div class="form-group no-background">
                    <label for="exampleInputEmail1">Название<br><span class="text-danger">Внимание! Указанное название будет отображаться у всех получателей этого письма, как тема/заголовок письма</span></label>
                    <input maxlength="52" type="text" name="name" class="form-control own-create-title"
                           value="{{ old('name', isset($own) ? $own->name : '') }}"
                           placeholder="Название будущего письма" required>
                </div>
                <div class="tiny-mce-creator">
                    <textarea name="text" id="tinymceOwnCreate"
                              placeholder="{{ trans('templates.create.text-placeholder') }}">{!! old('text', isset($own) ? $own->text : null) !!}</textarea>
                </div>
            </div>
            @if($setting_email)
                @if(isset($own->file))
                    <div class="ot-file-exists">
                        <span class="ot-file-logo"><i class="fa fa-file-zip-o fa=fw fa-lg"></i></span>
                        <span class="ot-file-name">{{ fs_conv($own->file->origin_name) }}</span>
                        <span class="ot-file-size"><i>({{ fs_conv($own->file->size) }})</i></span>
                        <span class="ot-file-icons pull-right">
                            <a href="{{ route('file-download', ['id' => $own->file->id]) }}" class="ot-file-download" data-toggle="tooltip" data-placement="left"
                               title="Скачать"><i class="fa fa-download fa=fw fa-lg"></i></a>
                            <i class="ot-file-remove fa fa-trash-o fa=fw fa-lg" data-toggle="tooltip" data-placement="left"
                               title="Удалить" onclick="removeFile('{{ $own->file->id }}')"></i>
                        </span>
                    </div>
                @endif
                <div id="selectNewFile" @if(isset($own->file)) style="display: none;" @endif>
                    <br>
                    @include('partials._file-upload', ['extension' => 'email']) <!--Submit button must be '#submitFormWithFileButton'-->
                </div>
            @endif
            <br>
            <button type="submit" class="btn btn-success btn-lg">
                {{ trans('templates.create.save-btn') }}
            </button>
            <br>
            <br>
        </form>
    </div>
@endsection

@section('js')
    <script src="/libs/sweetalert2/sweetalert2.min.js"></script>
    <script src="/js/file-upload.js"></script>
    <script src="/js/tinymce-verse-1.js"></script>
    <script>
        tynyMceVerse1('#tinymceOwnCreate', '2.3');
    </script>
@endsection