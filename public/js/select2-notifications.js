function formatResult(myResult) {
    return $('<option>' + myResult.text + '</option>');
}

function resultSelection(selectData) {
    return selectData.text;
}

$('.select2-notifications-users').select2({
    placeholder: "Начните вводить свой вопрос....",
    allowClear: false,
    multiple: true,
    maximumSelectionLength: 100,
    language: "ru",
    ajax: {
        url: routeSearch,
        dataType: 'json',
        delay: 250,
        type: "POST",
        data: function (params) {
            return {
                myQuery: params.term,
                page: params.page,
                _token: $("meta[name='csrf-token']").attr('content')
            };
        },
        processResults: function (data, params) {
            params.page = params.page || 1;
            return {
                results: data,
                pagination: {
                    more: (params.page * 10) < data.total_count
                }
            };
        },
        cache: true
    },
    escapeMarkup: function (markup) {
        return markup;
    },
    minimumInputLength: 0,
    templateResult: formatResult,
    templateSelection: resultSelection
});
