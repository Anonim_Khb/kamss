<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Models\MembershipApplication;
use Illuminate\Contracts\Validation\Validator;
use Redirect;
use Notifications;

class ImportExcelForUsersPost extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'contacts' => 'required|boolean',
            'bids' => 'required|boolean',
            'file' => 'required|max:5120'
        ];

        return $rules;
    }

    public function response(Array $errors)
    {
        return redirect()->back()->withInput();
    }


    public function formatErrors(Validator $validator)
    {
        foreach ($validator->errors()->all() as $error) {
            Notifications::danger($error, 'page');
        }

        return $validator->errors()->getMessages();
    }
}
