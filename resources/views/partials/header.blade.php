<header class="main-header">
    <a href="{{ route('panel-index') }}" class="logo">
        <span class="logo-mini">
            <img src="{{ config('site.logo_mini_color_1') }}" alt="logo">
        </span>
        <span class="logo-lg">
            <img src="{{ config('site.logo_mini_color_1') }}" alt="logo">
        </span>
    </a>

    <nav class="navbar navbar-static-top" role="navigation">
        <a onclick="sidebarSetting()" href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!--NOTIFICATIONS>>>>-->
                <li class="dropdown messages-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i id="headerBell" class="fa fa-bell fa-lg @if(!count($notifications) > 0) not-exists-notifications @endif"></i>
                        @if(count($notifications) > 0)
                            <span id="notificationsCount" class="label label-danger">{{ count($notifications) }}</span>
                        @endif
                    </a>
                    <ul id="notificationMenuList" class="dropdown-menu">
                        <li class="header">
                            <?php $notifications ?: $notifications = 0;?>
                            У Вас <strong id="notificationsMenuCount">{{ count($notifications) }}</strong>
                            {{ trans_choice('dashboard.header-notifications', $notifications) }}
                        </li>
                        <li>
                            <ul class="menu">
                                @if(count($notifications) > 0)
                                    @foreach($notifications as $notification)
                                        <li class="menu-nli">
                                            <a href="{{ $notification->link ? route($notification->link) : 'javascript:void(0);' }}">
                                                <div class="pull-left">
                                                    <i style="color:{{ $notification->color }}"
                                                       class="{{ $notification->icon }} fa-fw fa-2x"></i>
                                                </div>
                                                <p class="hn-text">{!! $notification->text !!}</p>
                                                <p>
                                                    {{ ta($notification->created_at) }}
                                                    <span class="pull-right hn-remove"
                                                          onclick="deleteNotifications($(this), '{{ $notification->id }}', event)">
                                                        Удалить <i class="fa fa-close"></i>
                                                    </span>
                                                </p>
                                            </a>
                                        </li>
                                    @endforeach
                                @endif
                                <div id="headerNotificationsNone"
                                     @if(count($notifications) > 0) style="display:none;" @endif>
                                    <i class="fa fa-bell-slash fa-3x"></i>
                                    <p>У Вас нет новых оповещений</p>
                                </div>
                            </ul>
                        </li>
                        <li class="footer" @if(count($notifications) > 0) class="not-active" @endif>
                            <a href="javascript:void(0);" onclick="deleteNotifications($(this), 'all', event)">
                                <strong>Удалить все</strong>
                            </a>
                        </li>
                    </ul>
                </li>
                <!--NOTIFICATIONS<<<<-->
                <li>
                    <a href="{{ route('logout') }}" class="header-logout">
                        <i class="fa fa-power-off fa-fw fa-lg"></i>
                        <span>{{ trans('dashboard.header.logout') }}</span>
                    </a>
                </li>
            </ul>
        </div>
    </nav>
</header>
