<!DOCTYPE html>

<html lang="ru">

<head>

    @include('basis.meta')

    <title>{{ \Title::render() }}</title>

    @include('simple.css')

    @yield('css')

    <link href='/css/simple.css' rel='stylesheet' type='text/css'>

</head>

<body class="simple-main">

@include('basis.notifications-top')

<div class="wrapper">

    <div class="content-wrapper">
        @yield('body')
    </div>

</div>

@include('simple.js')

@yield('js')

</body>

</html>
