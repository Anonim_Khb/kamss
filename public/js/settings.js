toastr_options();
$('.settings-checkbox input').on('click', function () {
    var fieldName = $(this).closest('.settings-checkbox').text();
    $.ajax({
        type: "POST",
        dataType: "text",
        url: routeNameChange,
        data: {config_name: $(this).attr("name"), _token: $("meta[name='csrf-token']").attr('content')},
        cache: false,
        success: function (data) {
            toastr[data == true ? "success" : "warning"](data == true ? 'Включена' : 'Выключена', fieldName);
        }
    });
});

$('#add_new_config').on('click', function () {
    if ($('#new_key').val() && $('#new_value').val() && $('#new_description').val()) {
        $.ajax({
            type: "POST",
            dataType: "text",
            url: routeNameAdd,
            data: {
                key: $('#new_key').val(),
                value: $('#new_value').val(),
                description: $('#new_description').val(),
                _token: $("meta[name='csrf-token']").attr('content')
            },
            cache: false,
            success: function (data) {
                if (data == 'ok') {
                    $('#new_key, #new_value, #new_description').val('');
                    toastr["success"]('Запись успешно создана');
                } else {
                    toastr["error"]('Возникла непредвиденная ошибка');
                }
            }
        });
    } else {
        toastr["error"]('Не заполнены необходимые поля');
    }
});
