<?php

use Illuminate\Database\Seeder;
use App\Models\FaqCategory;

class FaqCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            [
                'category' => 'Вход и регистрация'
            ],
            [
                'category' => 'Мои данные'
            ],
            [
                'category' => 'Контактные лица'
            ],
            [
                'category' => 'Шаблоны'
            ],
            [
                'category' => 'Отправка писем'
            ],
            [
                'category' => 'Запросы/проекты'
            ],
            [
                'category' => 'Заметки'
            ],
            [
                'category' => 'Конфиденциальность'
            ],
            [
                'category' => 'Помощь/Предложения'
            ],
        ];

        FaqCategory::insert($categories);
    }
}
