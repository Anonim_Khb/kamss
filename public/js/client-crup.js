function addNewCompany(url) {
    swal({
        title: "Добавление организации",
        html: '<strong style="font-size: .9em">Вы точно убедились, что организации с таким названием в базе не существует?</strong>' +
        '<hr style="margin: .39em 0">' +
        '<br><strong style="float: left">Форма собственности</strong>' +
        '<input id="swal-input1" class="swal2-input" placeholder="ООО, ЗАО, АО, ПАО, ИП" autofocus>' +
        '<br><strong style="float: left">Название</strong>' +
        '<input id="swal-input2" class="swal2-input" placeholder="Название организации">' +
        '<br><strong style="float: left">ИНН</strong>' +
        '<input id="swal-input3" class="swal2-input" placeholder="ИНН, если известен">',
        text: "Вы хотите изменить статус заметки",
        confirmButtonColor: successColor,
        confirmButtonText: "Создать",
        showCancelButton: true,
        cancelButtonColor: defaultColor,
        cancelButtonText: 'Отмена',
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                if ($('#swal-input2').val()) {
                    resolve([
                        $('#swal-input1').val(),
                        $('#swal-input2').val(),
                        $('#swal-input3').val()
                    ]);
                } else {
                    reject('Поле "Название" обязательно для заполнения');
                }
            });
        }
    }).then(function () {
        $.ajax({
            type: "POST",
            dataType: "text",
            url: url,
            data: {
                companyType: $('#swal-input1').val(),
                companyName: $('#swal-input2').val(),
                companyINN: $('#swal-input3').val(),
                _token: $("meta[name='csrf-token']").attr('content')
            },
            cache: false,
            success: function (data) {
                if (data == 'ok') {
                    swal(
                        'Организация добавлена!',
                        'Теперь выберите ее из списка',
                        'success'
                    );
                } else {
                    swal(
                        'Ошибка!',
                        'Возникла ошибка. Пожалуйста, попробуйте еще раз.',
                        'error'
                    );
                }
            }
        });
    }, function (dismiss) {
        // dismiss can be 'cancel', 'overlay', 'close', 'timer'
        if (dismiss === 'cancel') {
            //
        }
    });
}
