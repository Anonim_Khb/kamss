<?php

use Jenssegers\Date\Date;
use App\Models\Setting;
use App\Models\Notification;

if (!function_exists('mb_ucfirst')) {
    function mb_ucfirst($string, $encoding = 'UTF-8')
    {
        $strlen = mb_strlen($string, $encoding);
        $firstChar = mb_substr($string, 0, 1, $encoding);
        $then = mb_substr($string, 1, $strlen - 1, $encoding);
        return mb_strtoupper($firstChar, $encoding) . $then;
    }
}

if (!function_exists('ur')) {
    function ur($role)
    {
        $user = \Auth::user();

        if ($user && in_array($user->roles, config('roles.' . $role . ''))) {
            return true;
        } else {
            return false;
        }
    }
}

if (!function_exists('df')) {
    function df($date, $format = 'd M Y')
    {
        return Date::parse($date)->format($format);
    }
}

if (!function_exists('ta')) {
    function ta($date)
    {
        return Date::parse($date)->ago();
    }
}

if (!function_exists('srq')) {
    function srq($string)
    {
        return str_replace(["'", "\""], "", $string);
    }
}

if (!function_exists('conf_site')) {
    function conf_site()
    {
        return new Setting();
    }
}

if (!function_exists('notify_site')) {
    function notify_site()
    {
        return new Notification();
    }
}

if (!function_exists('fs_conv')) { //File size convert
    function fs_conv($bytes)
    {
        if ($bytes >= 1048576)
        {
            $bytes = number_format($bytes / 1048576, 2) . ' Мб';
        }
        elseif ($bytes >= 1024)
        {
            $bytes = number_format($bytes / 1024, 2) . ' Кб';
        }
        elseif ($bytes > 1)
        {
            $bytes = $bytes . ' Б';
        }

        return $bytes;
    }
}

if (!function_exists('ffcc')) { //Check exists folder or file | create folder
    function ffcc($path, $create = false)
    {
        $check = \File::exists($path);

        if($create && !$check) {
            \File::makeDirectory($path);

            return true;
        }

        return $check;
    }
}
