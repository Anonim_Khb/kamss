<?php

return [

    'prepend' => [
        'site' => config('site.name'),
        'panel' => 'Доска',
    ],

    'index-panel' => 'Главная',
    'login' => 'Вход',
    'requests-show-admin' => 'Заявки',
    'request-add-accepted-admin' => 'Одобрение запроса',
    'password-reset' => 'Сброс пароля',
    'password-reset-form' => 'Сброс пароля',
    'groups-index' => 'Группы',
    'group-create' => 'Создание группы',
    'group-update' => 'Редактирование группы',
    'templates-ready' => 'Готовые шаблоны',
    'templates-own' => 'Свои шаблоны',
    'templates-create' => 'Создание шаблона',
    'users-index' => 'Пользователи',

];
