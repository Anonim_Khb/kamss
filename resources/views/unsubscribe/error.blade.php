@extends('errors.main')

@section('body')
    <div class="unsubscribe-page text-center">
        <h3 class="unsubscribe-text text-danger">
            <i class="fa fa-remove fa-lg"></i>
            Ссылка, по которой Вы перешли более не активна
        </h3>
        <img src="{{ '/images/error/500.png' }}" alt="Unsubscribe">
    </div>
@endsection
