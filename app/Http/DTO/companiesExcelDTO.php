<?php

namespace App\Http\DTO;

class companiesExcelDTO
{
    /**
     * @var array
     */
    private $page = ['name' => 'Компании', 'number' => 0];
    /**
     * @var array
     */
    private $id = ['name' => 'ID', 'export' => 'A', 'import' => 0, 'width' => 6];
    /**
     * @var array
     */
    private $company = ['name' => 'Организация', 'export' => 'B', 'import' => 1, 'width' => 44];
    /**
     * @var array
     */
    private $type = ['name' => 'Тип', 'export' => 'C', 'import' => 2, 'width' => 10];
    /**
     * @var array
     */
    private $inn = ['name' => 'ИНН', 'export' => 'D', 'import' => 3, 'width' => 16];
    /**
     * @return string
     */
    public function getPage($key)
    {
        return $this->page[$key];
    }
    /**
     * @return string
     */
    public function getId($key = 'export')
    {
        return $this->id[$key];
    }
    /**
     * @return string
     */
    public function getCompany($key = 'export')
    {
        return $this->company[$key];
    }
    /**
     * @return string
     */
    public function getType($key = 'export')
    {
        return $this->type[$key];
    }
    /**
     * @return string
     */
    public function getInn($key = 'export')
    {
        return $this->inn[$key];
    }
}