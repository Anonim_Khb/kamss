@extends('main')

@section('css')
    <link href="/libs/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" type='text/css'>
    <link href="/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type='text/css'>
    <link href="/css/datatable.css" rel="stylesheet" type='text/css'>
@endsection

@section('body-title')
    {{ trans('templates.index.title-own') }}
    @include('partials.button-create', ['route' => route('create-own-template'), 'text' => trans('clients.index.create')])
@endsection

@section('body')

    <div class="users-index email-templates no-background">
        @include('partials._info-block-1', ['text' => trans('info.own-templates')])
        <a href="{{ route('choose-carcass-template-for-send') }}">Посмотреть каркасы писем</a>
        @if(!$owns->isEmpty())
            <div class="table-responsive">
                <table id="OwnTemplatesTable" class="table table-hover table-bordered text-center">
                    <thead>
                    <tr>
                        <th class="col-sm-1">Id</th>
                        <th>Название</th>
                        @if($setting_email)
                            <th class="for-width-6">Файл</th> @endif
                        <th class="for-width-6">{{ trans('templates.table.created') }}</th>
                        <th class="for-width-6">{{ trans('templates.table.updated') }}</th>
                        <th class="no-print action-dropdown-icon"><i class="fa fa-flash fa-lg"></i></th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th class="col-sm-1">Id</th>
                        <th>Название</th>
                        @if($setting_email)
                            <th class="for-width-6">Файл</th> @endif
                        <th>{{ trans('templates.table.created') }}</th>
                        <th>{{ trans('templates.table.updated') }}</th>
                        <th></th>
                    </tr>
                    </tfoot>
                    <tbody>
                    @foreach($owns as $own)
                        <tr>
                            <th>{{ $own->id }}</th>
                            <td>{{ $own->name }}</td>
                            @if($setting_email)
                                <td>
                                    @if($own->file)
                                        <i>{{ fs_conv($own->file->size) }}</i>
                                    @else
                                        <i class="fa fa-minus"></i>
                                    @endif
                                </td>
                            @endif
                            <td>{{ df($own->created_at) }}</td>
                            <td>{{ df($own->updated_at) }}</td>
                            <td class="no-print action-dropdown-btn" style="white-space: nowrap;">
                                <div class="dropdown">
                                    <span id="bidDd{{ $own->id }}" class="btn-for-actions" data-toggle="dropdown"
                                          aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-ellipsis-h fa-fw fa-lg"></i>
                                    </span>
                                    <ul class="list-inline dropdown-menu dropdown-menu-right animated jello"
                                        aria-labelledby="bidDd{{ $own->id }}">
                                        <li onclick="redirectToRoute('{{ route('choose-text-for-own-template', [$own->id]) }}')"
                                            class="dmb-success" data-toggle="tooltip">
                                            <i class="fa fa-check"></i>
                                        </li>
                                        <li onclick="redirectToRoute('{{ route('edit-own-template', ['id' => $own->id]) }}')"
                                            class="dmb-warning" data-toggle="tooltip"
                                            data-placement="top"
                                            title="Посмотреть/Изменить">
                                            <i class="fa fa-pencil"></i>
                                        </li>
                                        <li onclick="deleteOwnTemplate(this, '{{ $own->id }}', '{{ route('delete-template-ajax') }}')"
                                            class="dmb-danger" data-toggle="tooltip" data-placement="top"
                                            title="{{ trans('templates.table.delete') }}">
                                            <i class="fa fa-remove"></i>
                                        </li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @else
            <div class="no-results text-center">
                <i class="fa fa-file-code-o fa-3x"></i>
                <p>{{ trans('templates.index.no-own-templates') }}</p>
            </div>
        @endif
    </div>

@endsection

@section('js')
    <script src="/js/own-template-actions.js"></script>
    <script src="/libs/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="/js/datatable.js"></script>
    <script src="/libs/sweetalert2/sweetalert2.min.js"></script>
    <script>
        $(document).ready(function () {
            myDataTable('#OwnTemplatesTable', true, 25);
            formSending('.form-filter-activation');
        });
    </script>
@endsection