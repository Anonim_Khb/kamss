<?php

return [

    'menu' => 'Отправка',

    'title' => [
        'index' => 'Отправка',
    ],

    'index' => [
        'title' => 'Отправка писем'
    ],

    'table' => [
        'select-all--tooltip' => 'Выбрать все',
        'template-select' => 'Выберите шаблон',
        'template-placeholder' => 'Не выбрано',
    ],

    'notification' => [
        'send-success' => 'Письма успешно отправлены',
        'send-error' => 'Какие-то ошибки с выбором заказчиков и/или шаблонов',
    ],

];
