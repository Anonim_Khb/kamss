<div>
    <hr>
    <?php !isset($author_signature_status) ? $author_signature_status = $user->signature_status : '';?>
    @if($author_signature_status == \App\Models\User::SIGNATURE_ACTIVE)
        <?php !isset($author_signature) ? $author_signature = $user->signature : '';?>
        {!! html_entity_decode($author_signature) !!}
    @else
        С уважением,<br>
        {{ $author_treatment or $user->treatment }},<br>
        менеджер по продажам,<br>
        {{ $author_branch_name or $user->companyBranch->name }},<br>
        <?php !isset($author_cell_phone) ? $author_cell_phone = $user->cell_phone : ''; ?>
        <?php !isset($author_work_phone) ? $author_work_phone = $user->work_phone : ''; ?>
        Тел.:<a style="color: #526a8d">{{ $author_cell_phone }}</a>,
        <a style="color: #526a8d">{{ $author_work_phone }}</a><br>
        Email: <a href="mailto:{{ $author_email or $user->email }}"
                  style="color: #526a8d">{{ $author_email or $user->email }}</a>
    @endif
</div>
