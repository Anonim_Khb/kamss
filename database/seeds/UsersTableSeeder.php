<?php

use Illuminate\Database\Seeder;
use \App\Models\User;

class UsersTableSeeder extends Seeder
{

    public function run()
    {
        User::create([
            'name' => 'Мазуряк Владимир',
            'work_phone' => '8(4212)26-05-76',
            'cell_phone' => '8 909 809 4444',
            'treatment' => 'Владимир',
            'email' => 'mazuryak.va@kamss.net',
            'password' => '******',
            'branch' => 1,
            'roles' => User::ROLE_SUPERADMIN
        ]);
    }
}
