<?php

namespace App\Http\Middleware;

use Closure;
use Notifications;

class ModeratorMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (\Auth::check() && !in_array(auth()->user()->roles, config('roles.moderator'))){
            Notifications::error(trans('notification.permission-error'), 'top');

            abort(403);
        }

        return $next($request);
    }
}
