toastr_options();
function changeClientStatus(element, clientId, url) {
    swal({
        title: "Вы уверены?",
        text: "Вы хотите изменить статус заказчика",
        type: "question",
        confirmButtonColor: successColor,
        confirmButtonText: "Да",
        showCancelButton: true,
        cancelButtonColor: defaultColor,
        cancelButtonText: 'Отмена'
    }).then(function() {
        $.ajax({
            type: "POST",
            dataType: "text",
            url: url,
            data: {clientId: clientId, _token: $("meta[name='csrf-token']").attr('content')},
            cache: false,
            success: function (data) {
                if (data == 'active') {
                    toastr["success"]("Контакт активирован");
                    $(element).removeClass('dmb-danger').addClass('dmb-success');
                } else {
                    toastr["warning"]("Контакт деактивирован");
                    $(element).removeClass('dmb-success').addClass('dmb-danger');
                }
            }
        });
    }, function(dismiss) {
        // dismiss can be 'cancel', 'overlay', 'close', 'timer'
        if (dismiss === 'cancel') {
            //
        }
    });
}

function deleteClient(element, clientId, url) {
    swal({
        title: "Вы уверены?",
        text: "Вы хотите удалить заказчика",
        type: "question",
        confirmButtonColor: warningColor,
        confirmButtonText: "Да",
        showCancelButton: true,
        cancelButtonColor: defaultColor,
        cancelButtonText: 'Отмена'
    }).then(function() {
        $.ajax({
            type: "POST",
            dataType: "text",
            url: url,
            data: {clientId: clientId, _token: $("meta[name='csrf-token']").attr('content')},
            cache: false,
            success: function (data) {
                if (data == 'success') {
                    toastr["warning"]("Контакт был удален");
                    $(element).parents('tr').hide('slow');
                }
            }
        });
    }, function(dismiss) {
        // dismiss can be 'cancel', 'overlay', 'close', 'timer'
        if (dismiss === 'cancel') {
            //
        }
    });
}

function changeClientGroup(element, clientId, groupId) {
    $.ajax({
        type: "POST",
        dataType: "text",
        url: $('.clients-all-groups').data('route'),
        data: {clientId: clientId, groupId: groupId, _token: $("meta[name='csrf-token']").attr('content')},
        cache: false,
        success: function (data) {
            if (data == 'add') {
                toastr["success"]("Контакту добавлена группа");
                $(element).css('color', '#ffa613');
            } else {
                toastr["warning"]("У контакта удалена группа");
                $(element).css('color', 'rgba(155, 170, 182, 0.75)');
            }
        }
    });
}

function shareContact(clientName, clientId, route) {
    swal({
        title: "Отправка данных",
        html: "Вы собираетесь отправить данные контакта «<strong>"+ clientName +"</strong>».<br>Введите эл.адрес получателя.",
        input: 'email',
        type: "question",
        confirmButtonColor: warningColor,
        confirmButtonText: "Да",
        showCancelButton: true,
        cancelButtonColor: defaultColor,
        cancelButtonText: 'Отмена',
        preConfirm: function(email) {
            return new Promise(function(resolve) {
                resolve();
            })
        }
    }).then(function(email) {
        $.ajax({
            type: "POST",
            dataType: "text",
            url: route,
            data: {clientId: clientId, email: email, _token: $("meta[name='csrf-token']").attr('content')},
            cache: false,
            success: function (data) {
                if (data == 'ok') {
                    toastr["success"]("Данные контакта успешно отправлены");
                } else {
                    toastr["warning"]("При отправке возникла непредвиденная ошибка");
                }
            }
        });
    }, function(dismiss) {
        if (dismiss === 'cancel') {
            //
        }
    });
}

function redirectToRoute(route) {
    window.location.href = route;
}
