function removeImportErrorCorrection(errorId) {
    swal({
        title: "Удаление исправления",
        text: "Исправление будет удалено без возможности восстановления!",
        type: "question",
        confirmButtonColor: warningColor,
        confirmButtonText: "Удалить",
        showCancelButton: true,
        cancelButtonColor: defaultColor,
        cancelButtonText: 'Отмена'
    }).then(function () {
        $.ajax({
            type: "POST",
            dataType: "text",
            url: routeRemoveErrorCorrection,
            data: {
                errorId: errorId,
                _token: $("meta[name='csrf-token']").attr('content')
            },
            cache: false,
            success: function (data) {
                if (data) {
                    setTimeout(function () {
                        window.location.replace('/');
                    }, 3500);
                    swal({
                        title: 'Исправление удалено',
                        text: 'Исправление было удалено. Сейчас Вы будете перемещены на главную страницу',
                        type: "success",
                        timer: 3000
                    })
                }
            }
        });
    }, function (dismiss) {
        if (dismiss === 'cancel') {
            //
        }
    });
}
