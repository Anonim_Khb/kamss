@extends('errors.main')

@section('body')

    <div class="error-404 text-center">
        <img src="{{ '/images/error/404.png' }}" alt="Error">
        @include('errors.buttons')
    </div>

@endsection
