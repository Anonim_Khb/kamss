<?php

namespace App\Http\ViewComposers;

use App\Models\ImportError;
use Illuminate\Contracts\View\View;

class MainComposer
{
    public function compose(View $view)
    {
        $errors = ImportError::where('user_id', \Auth::id())->count();

        $view->with([
            'importErrorsCount' => $errors
        ]);
    }
}
