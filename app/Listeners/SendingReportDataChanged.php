<?php

namespace App\Listeners;

use App\Events\ReportDataChanged;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;

class SendingReportDataChanged
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ReportDataChanged  $event
     * @return void
     */
    public function handle(ReportDataChanged $event)
    {
        $data = [
            'id' => $event->user->id,
            'name' => $event->user->name,
            'email' => $event->user->email,
        ];

        Mail::queue(['html' => 'email.system.html.report-data-changed', 'text' => 'email.system.text.report-data-changed'],
            $data, function ($message) use ($data) {
                $message->to($data['email'])->subject('Изменение данных');
        });

    }
}
