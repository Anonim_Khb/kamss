@extends('main')

@section('css')
    <link href="/libs/bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet" type='text/css'>
    <link href="/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type='text/css'>
@endsection

@section('body-title')
    Изменение компании
@endsection

@section('body')
    <div class="company-change">
        <div class="change-info-block">
            <h5>Изменение компании <span class="label label-primary">«{{ $change->company->name }}»</span></h5>
            <h5>Предложил <span class="label label-info">«{{ $change->author->name }}»</span></h5>
        </div>
        <div class="register-box">
            <div class="login-box-body">
                @include('basis.notifications-page')
                <form id="companyChangeForm" action="{{ route('company-change-post', ['id' => $change->id]) }}"
                      method="POST">
                    {!! csrf_field() !!}
                    <div class="form-group has-feedback">
                        <label>Тип (форма собственности)</label>
                        <input type="text" class="form-control"
                               placeholder="Форма собственности"
                               value="{{ $change->company->type }}" readonly>
                        <input type="text" name="type" class="form-control"
                               placeholder="Форма собственности"
                               value="{{ old('type', $change->type) }}">
                    </div>
                    <div class="form-group has-feedback">
                        <label>ИНН</label>
                        <input type="text" class="form-control"
                               placeholder="ИНН, если известен"
                               value="{{ $change->company->inn }}" readonly>
                        <input type="text" name="inn" class="form-control"
                               placeholder="ИНН, если известен"
                               value="{{ old('inn', $change->inn) }}">
                    </div>
                    <div class="form-group has-feedback">
                        <label>Название</label>
                        <input type="text" class="form-control"
                               placeholder="ИНН, если известен"
                               value="{{ $change->company->name }}" readonly>
                        <input type="text" name="name" class="form-control"
                               placeholder="ИНН, если известен"
                               value="{{ old('name', $change->name) }}">
                    </div>
                    <div class="checkbox checkbox-success">
                        <input id="change_confirm" type="checkbox" class="styled" name="change_confirm"
                               @if(old('change_confirm')) checked @endif>
                        <label for="change_confirm"><strong>Принять изменения</strong></label>
                    </div>
                    <div class="pull-right">
                        <button id="companyChangeButton" type="button" onclick="companyChangeSave()" class="btn btn-danger btn-flat">
                            Отказать
                        </button>
                    </div>
                </form>
                <br><br>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="/libs/sweetalert2/sweetalert2.min.js"></script>
    <script src="/js/company-change.js"></script>
@endsection