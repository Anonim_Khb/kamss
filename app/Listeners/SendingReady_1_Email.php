<?php

namespace App\Listeners;

use App\Events\Ready_1_Email;
use App\Models\User;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;
use Jenssegers\Date\Date;

class SendingReady_1_Email
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SendEmail $event
     * @return void
     */
    public function handle(Ready_1_Email $event)
    {
        $author = User::where('id', auth()->user()->id)->with('companyBranch')->first();

        $data = [
            'author_treatment' => $author->treatment,
            'author_work_phone' => $author->work_phone ? $author->work_phone : null,
            'author_cell_phone' => $author->cell_phone ? $author->cell_phone : null,
            'author_email' => $author->email,
            'author_branch_region' => $author->companyBranch->region,
            'author_branch_name' => $author->companyBranch->name,
            'author_branch_city' => $author->companyBranch->city,
            'author_branch_address' => $author->companyBranch->address,
            'author_branch_postcode' => $author->companyBranch->postcode,
        ];

        foreach ($event->clients as $client) {
            if($client->email) {
                $data = array_except($data, ['client_treatment', 'client_email', 'client_token']);
                $data = array_add($data, 'client_treatment', $client->treatment ? $client->treatment : 'коллега');
                $data = array_add($data, 'client_email', $client->email);
                $data = array_add($data, 'client_token', $client->token);

                Mail::queue(['html' => 'email.ready.html.' . $event->template, 'text' => 'email.ready.text.' . $event->template],
                    $data, function ($message) use ($data) {
                        $message->from($data['author_email'], $data['author_treatment']);
                        $message->to($data['client_email'])->subject('Здравствуйте, ' . $data['client_treatment']);
                    });

                $client->last_send = Date::now();
                $client->save();
            }
        }
    }
}
