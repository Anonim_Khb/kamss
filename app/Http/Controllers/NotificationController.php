<?php

namespace App\Http\Controllers;

use App\Http\DTO\CreateNotificationDTO;
use App\Http\Requests\NotificationCreatePostRequest;
use App\Models\Notification;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Notifications;
use Jenssegers\Date\Date;

class NotificationController extends Controller
{
    public function __construct()
    {
        $this->middleware('inactive')->except('index');

        view()->share('active_link', 'notifications_admin');
    }

    public function index()
    {
        \Title::append('Оповещения');

        \Javascript::put([
            'routeSearch' => route('notifications-search-ajax')
        ]);

        return view('notifications.index');
    }

    public function searchUsersAjax(User $user)
    {
        $query = trim(e(request('myQuery')));

        if (request()->has('myQuery')) {
            $users = $user->where('name', 'LIKE', '%' . $query . '%')
                ->select('id', 'name as text');
        } else {
            $users = $user->select('id', 'name as text');
        }

        $users = $users->get();

        return \Response::json($users);
    }

    public function createNotificationPost(NotificationCreatePostRequest $request, Notification $notification, User $user)
    {
        $dto = new CreateNotificationDTO();
        $dto->setUserId($request->user_id);
        $dto->setText($request->text);
        $dto->setIcon($request->icon);
        $dto->setColor($request->color);
        $dto->setLink($request->link);

        $notification->createNotification($dto);

        Notifications::success('Оповещение успешно создано', 'top');

        return redirect()->route('notifications-index');
    }

    public function delete(Notification $notification)
    {
        if (request()->has('id')) {

            if (is_numeric(request('id'))) {
                $notification = $notification->where('id', request('id'))
                    ->where('user_id', \Auth::user()->id);

                if (!is_null($notification->first())) {
                    $notification->delete();
                    return 'one';
                } else {
                    return 'error';
                }
            } elseif (request('id') == 'all') {
                $notification->where('user_id', \Auth::user()->id)
                    ->delete();

                return 'all';
            }

        }
    }

    public function deleteAllOldNotifications(Notification $notification)
    {
        $notification->where('created_at', '<', Date::now()->subHours(config('site-notifications.lifetime')))
            ->delete();

        return 'ok';
    }
}
