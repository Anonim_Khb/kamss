<div class="row no-background ">
    <div class="col-xs-12 col-sm-6">
        <form class="form-inline" action="{{ $route }}"
              method="GET">
            <br>
            <div class="input-group input-group-sm border-bg-blue">
                <span class="input-group-addon bg-blue"
                      id="basic-addon1"><b>{{ trans('membership.index.table.status-input') }}</b></span>
                <select name="status" class="form-control form-filter-activation" aria-describedby="basic-addon1">
                    <option @if(request('status') == 'all') selected
                            @endif value="all">{{ trans('membership.index.table.status-all') }}</option>
                    <option @if(request('status') == \App\Models\MembershipApplication::STATUS_READ) selected
                            @endif value="{{ \App\Models\MembershipApplication::STATUS_READ }}">{{ trans('membership.index.table.status-read') }}</option>
                    <option @if(request('status') == null) selected
                            @endif value="{{ null }}">{{ trans('membership.index.table.status-unread') }}</option>
                </select>
            </div>
            <br>
            <br>
        </form>
    </div>
    <div class="col-xs-12 col-sm-6">
        <form class="form-inline pull-right" action="{{ route('membership-delete-admin') }}"
              method="POST">
            {{ csrf_field() }}
            <br>
            <div class="input-group input-group-sm border-bg-red">
                <span class="input-group-addon bg-red"
                      id="basic-addon2"><b>{{ trans('membership.index.table.delete.title') }}</b></span>
                <select name="remove_status" class="form-control form-filter-activation" aria-describedby="basic-addon2">
                    <option @if(request('remove_status') == null) selected @endif value="{{ null }}">{{ trans('membership.index.table.delete.not-selected') }}</option>
                    <option @if(request('remove_status') == 'all') selected
                            @endif value="all">{{ trans('membership.index.table.status-all') }}</option>
                    <option @if(request('remove_status') == \App\Models\MembershipApplication::STATUS_READ) selected
                            @endif value="{{ \App\Models\MembershipApplication::STATUS_READ }}">{{ trans('membership.index.table.status-read') }}</option>
                    <option @if(request('remove_status') == \App\Models\MembershipApplication::STATUS_UNREAD) selected
                            @endif value="{{ \App\Models\MembershipApplication::STATUS_UNREAD }}">{{ trans('membership.index.table.status-unread') }}</option>
                </select>
            </div>
        </form>
    </div>
</div>
