function changeAddRequestStatus(element, requestId, text, url) {
    if (confirm(text)) {
        $.ajax({
            type: "POST",
            dataType: "text",
            url: url,
            data: {requestId: requestId, _token: $("meta[name='csrf-token']").attr('content')},
            cache: false,
            success: function (data) {
                $(element).removeClass('btn-warning').addClass('btn-default').attr('disabled', 'disabled');
            }
        });
    }
}

function redirectForAddRequestAccepted(url) {
    window.location.replace(url);
}
