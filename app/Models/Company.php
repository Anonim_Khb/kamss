<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table = 'companies';

    protected $fillable = [
        'author_id', 'name', 'type', 'inn', 'status'
    ];

    const STATUS_UNCHECKED = 0;
    const STATUS_CHECKED = 1;
    const STATUS_DEFAULT = 2;

    const COMPANY_UNKNOWN_ID = 1;

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = e(str_replace(['\'', '"'], '', $value));
    }

    public function setTypeAttribute($value)
    {
        $this->attributes['type'] = e(str_replace(['\'', '"'], '', $value));
    }

    public function contacts()
    {
        return $this->hasMany('App\Models\Client', 'company', 'id');
    }

    public function groups()
    {
        return $this->hasMany('App\Models\Client', 'company', 'id');
    }

    public function author()
    {
        return $this->hasOne('App\Models\User', 'id', 'author_id');
    }

    public static function rulesEdit($name, $inn)
    {
        return [
            $name => 'required',
            $inn => 'numeric'
        ];
    }

    public static function rulesCreate($name, $inn)
    {
        return [
            $name => 'required|unique:companies,name',
            $inn => 'numeric'
        ];
    }

    public static function createCompany($author_id, $type, $name, $inn, $status = self::STATUS_UNCHECKED)
    {
        $save = self::create([
            'author_id' => $author_id,
            'type' => $type ? e($type) : null,
            'name' => e($name),
            'inn' => $inn ? e($inn) : null,
            'status' => $status
        ]);

        return $save;
    }
}
