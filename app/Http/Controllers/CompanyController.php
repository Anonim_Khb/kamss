<?php

namespace App\Http\Controllers;

use App\Http\Classes\ImportErrorCorrections;
use App\Http\Requests\CheckCompanyAndProposeChangePost;
use App\Http\Requests\CompanyCreateFixImportErrorRequest;
use App\Models\Client;
use App\Models\CompanyChange;
use App\Models\ImportError;
use App\Models\Notification;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Company;
use Notifications;
use Validator;

class CompanyController extends Controller
{
    public function __construct()
    {
        $this->middleware('inactive')->except('companiesIndex');
        $this->middleware('moderator')->only(['setCheckedStatusAjax', 'replaceCompanyAndRemove']);

        view()->share('active_link', 'company_index');
    }

    public function companySearchAjax(Company $company)
    {
        if (request()->has('myQuery')) {

            $query = trim(e(request('myQuery')));

            $companies = $company->where('name', 'LIKE', '%' . $query . '%')
                ->select('id', 'name as text', 'inn')->get();

            return \Response::json($companies);
        } else {
            $companies = $company->whereIn('id', [1, 2])
                ->select('id', 'name as text')->get();

            return \Response::json($companies);
        }
    }

    public function createCompany(Request $request)
    {
        $valid = Validator::make($request->all(), Company::rulesCreate('companyName', 'companyINN'));

        if ($valid->fails()) {
            return 'error';
        } else {
            $new = Company::createCompany(\Auth::id(), $request->companyType, $request->companyName,
                $request->companyINN);

            return $new ? 'ok' : 'error';
        }
    }

    public function companiesIndex(Company $company)
    {
        $companies = $this->searchCompanies($company);

        $companyChangesCount = CompanyChange::count();

        \Title::append('Компании');

        return view('companies.index')->with([
            'companies' => $companies,
            'companyChangesCount' => $companyChangesCount
        ]);
    }

    public function setCheckedStatusAjax(Company $company)
    {
        if (request()->has('companyId')) {
            $company = $company->where('id', request('companyId'))->first();

            $company->update(['status' => Company::STATUS_CHECKED]);

            return 'ok';
        }
    }

    protected function searchCompanies($company)
    {
        if (request()->has('status')) {
            $company = $company->where('status', request('status'));
        }

        $company = $company->with('contacts')->get();

        return $company;
    }

    public function getEditCompany($company_id, Company $company)
    {
        $company = $company->where('id', $company_id)->first();

        \Javascript::put([
            'routeSearch' => route('company-search-ajax'),
            'showCompanyId' => true
        ]);

        \Title::append('Редактирование компании');

        return view('companies.edit')->withCompany($company);
    }

    public function postEditCompany(
        CheckCompanyAndProposeChangePost $request,
        Company $company,
        CompanyChange $change,
        $company_id
    ) {
        if (ur(\App\Models\User::ROLE_MODERATOR)) {
            if ($request->has('replacement_confirm')) {
                $this->replaceCompanyAndRemove($company_id, $request->replacement_id);

                Notifications::success('Компания заменена на другую', 'top');
            } else {
                $company->where('id', $company_id)
                    ->update([
                        'type' => $request->has('type') ? e($request->type) : null,
                        'name' => e($request->name),
                        'inn' => $request->has('inn') ? e($request->inn) : null,
                        'status' => Company::STATUS_CHECKED
                    ]);

                Notifications::success('Данные успешно внесены', 'top');
            }
        } else { //For users
            $change->create([
                'author_id' => \Auth::user()->id,
                'company_id' => $company_id,
                'type' => $request->has('type') ? e($request->type) : null,
                'name' => e($request->name),
                'inn' => $request->has('inn') ? e($request->inn) : null
            ]);

            Notifications::success('Данные переданы на рассмотрение', 'top');
        }

        return redirect()->route('companies-index');
    }

    public function replaceCompanyAndRemove($removed_id, $actual_id)
    {
        Client::where('company', $removed_id)
            ->update(['company' => $actual_id]);

        Company::destroy($removed_id);
    }

    public function searchCompanyByClientId(Client $client, Company $company)
    {
        if (request()->has('clientId')) {
            $company_id = $client->where('id', request('clientId'))
                ->with([
                    'clientCompany' => function ($query) {
                        $query->select('id', 'name as text');
                    }
                ])->first();

            return $company_id->clientCompany;
        }
    }

    public function proposeChangesTable(CompanyChange $change)
    {
        $changes = $change->with(['author', 'company'])->get();

        \Title::append('Изменения компаний');

        return view('companies.changes_table')->withChanges($changes);
    }

    public function proposeChange(CompanyChange $change, $id)
    {
        $change = $change->where('id', $id)
            ->with(['author', 'company'])->first();

        \Title::append('Изменение компании');

        return view('companies.change_edit')->withChange($change);
    }

    public function proposeChangePost(
        CheckCompanyAndProposeChangePost $request,
        CompanyChange $change,
        Company $company,
        $id
    ) {
        $notification = new Notification();

        $change = $change->where('id', $id)->first();

        if ($request->change_confirm) {
            $company = $company->where('id', $change->company_id);

            $company->update([
                'name' => $request->name,
                'type' => $request->type,
                'inn' => $request->inn,
                'status' => Company::STATUS_CHECKED
            ]);
        }

        $notification->setSuccessProposeCompanyNotification($company->first()->name, $change->author_id,
            $request->change_confirm ? 'success' : 'cancel');

        $change->destroy($id);

        Notifications::success('Заявка обработана', 'top');

        return redirect()->route('company-changes');
    }

    public function companyFixImportError(ImportError $error, ImportErrorCorrections $corrections)
    {
        $company = $error->where('user_id', \Auth::id())->where('type', $error::TYPE_COMPANIES)->first();

        $corrections->showNotificationsErrors($company->errors);

        $firstCompany = (object)unserialize($company->array);

        $firstCompany->error_id = $company->id;

        \Title::append('Компания');
        return view('companies.edit-import-error')->withCompany($firstCompany);
    }

    public function companyFixImportErrorPost(
        CompanyCreateFixImportErrorRequest $request,
        ImportError $error,
        Company $company
    ) {
        $company::createCompany(\Auth::id(), $request->type, $request->name, $request->inn);

        $error::removeError(\Auth::id(), $request->error_id);

        return redirect()->route('impex-import-errors-index');
    }
}
