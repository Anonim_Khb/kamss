<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Models\Client;
use Illuminate\Contracts\Validation\Validator;
use Redirect;
use Notifications;

class ClientCreateOrUpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if($this->route('id')) {
            return Client::where('id', $this->route('id'))
                ->where('author_id', \Auth::id())->exists();
        } else {
            return true;
        }
    }

    public function forbiddenResponse()
    {
        return abort(403);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (is_null(\Route::current()->getParameter('id'))) {
            $rules = Client::createRules();
        } else {
            $rules = Client::editRules();
        }

        return $rules;
    }

    public function response(Array $errors)
    {
        return redirect()->back()->withInput();
    }


    public function formatErrors(Validator $validator)
    {
        foreach ($validator->errors()->all() as $error) {
            Notifications::danger($error, 'page');
        }

        return $validator->errors()->getMessages();
    }
}
