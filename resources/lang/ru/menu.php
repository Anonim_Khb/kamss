<?php

return [

    'left' => [
        'title' => 'Навигационное меню',
        'users' => 'Пользователи',
        'groups' => 'Управление группами',
        'clients' => 'Заказчики',
        'templates-ready' => 'Готовые шаблоны',
        'templates-own' => 'Свои шаблоны',
        'sending' => 'Отправки писем',
        'request-add' => 'Заявки на вступл.',
    ],

];
