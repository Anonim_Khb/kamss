<div class="form-group has-feedback">
    <label>Тип (форма собственности)</label>
    <input type="text" name="type" class="form-control"
           placeholder="Форма собственности"
           value="{{ old('type', $company->type) }}">
    <span class="fa fa-ship form-control-feedback"></span>
</div>
<div class="form-group has-feedback">
    <label>Название компании</label>
    <input type="text" name="name" class="form-control"
           placeholder="Название компании"
           value="{{ old('name', $company->name) }}" required>
    <span class="fa fa-building form-control-feedback"></span>
</div>
<div class="form-group has-feedback">
    <label>ИНН</label>
    <input type="text" name="inn" class="form-control"
           placeholder="ИНН, если известен"
           value="{{ old('inn', $company->inn) }}">
    <span class="fa fa-paperclip form-control-feedback"></span>
</div>
