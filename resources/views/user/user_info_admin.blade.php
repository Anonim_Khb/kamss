@extends('main')

@section('body-title')
    Информация о пользователе {{ $user->name }}
@endsection

@section('body')
    <div class="no-background dashboard-general">
        <br>
        @include('dashboard.user_data')
    </div>
@endsection
