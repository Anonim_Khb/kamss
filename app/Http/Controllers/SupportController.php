<?php

namespace App\Http\Controllers;

use App\Events\ReportAdminAbout;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Faq;
use App\Models\FaqCategory;
use App\Models\Support;
use Notifications;
use App\Http\Requests\SupportUsersRequest;

class SupportController extends Controller
{
    public function __construct()
    {
        $this->middleware('inactive')->except(['faq', 'requestSupportFromUsers', 'requestSupportFromUsersPost', 'faqSearchAjax']);

        view()->share('active_link', 'help_index');
    }

    public function faq(FaqCategory $categories, Faq $faq)
    {
        $faq = request()->has('query') ? $faq->find(e(request('query'))) : null;
        $categories = $categories->with('faqs')->get();

        \Title::append('ВиО');

        \Javascript::put([
            'routeSearch' => route('help-faq-search')
        ]);

        return view('help.faq', ['categories' => $categories, 'faq' => $faq]);
    }

    public function requestSupportFromUsers()
    {
        \Title::append('Техподдержка');

        return view('help.support_request_create');
    }

    public function requestSupportFromUsersPost(SupportUsersRequest $request, Support $support)
    {
        $support->create([
            'title' => e($request->title),
            'text' => e($request->text),
            'author_id' => \Auth::user()->id,
        ]);

        event(new ReportAdminAbout('Создано новое обращение в техподдержку'));

        Notifications::success('Ваш запрос успешно отправлен', 'top');

        return redirect()->route('help-faq');
    }

    public function adminIndex(Support $support)
    {
        if (!request()->has('status') || request('status') == Support::STATUS_UNREAD) {
            $support = $support->where('status', Support::STATUS_UNREAD);
        } elseif (request('status') == Support::STATUS_READ) {
            $support = $support->where('status', Support::STATUS_READ);
        }

        $supports = $support->with('author')->get();

        \Title::append('Помощь (админ)');

        view()->share('active_link', 'help_admin_index');

        return view('help.admin-index', ['supports' => $supports]);
    }

    public function adminCategories(FaqCategory $categories)
    {
        $categories = $categories->all();

        \Title::append('ВиО категории');

        view()->share('active_link', 'help_admin_index');

        return view('help.admin_categories_manage', ['categories' => $categories]);
    }

    public function createCategory()
    {
        \Title::append('Создание');

        view()->share('active_link', 'help_admin_index');

        return view('help.category_create_update');
    }

    public function editCategory($id, FaqCategory $categories)
    {
        $category = $categories->find($id);

        \Title::append('Редактирование');

        view()->share('active_link', 'help_admin_index');

        return view('help.category_create_update', ['category' => $category]);
    }

    public function createOrUpdateCategory($id = null)
    {
        $category = FaqCategory::findOrNew($id);
        $category->category = request('category');
        $category->save();

        Notifications::success('Категория успешно изменена/создана', 'top');

        return redirect()->route('help-categories-admin');
    }

    public function destroyCategory($id)
    {
        FaqCategory::find($id)->delete();

        Notifications::success('Категория успешно удалена', 'top');

        return redirect()->back();
    }

    public function faqSearchAjax(Faq $faq)
    {
        if (request()->has('myQuery')) {

            $query = trim(e(request('myQuery')));

            $faqs = $faq->where('question', 'LIKE', '%' . $query . '%')
                ->orWhere('answer', 'LIKE', '%' . $query . '%')
                ->select('id', 'question as text')
                ->get();

            return \Response::json($faqs);
        }
    }

    public function destroyFaq($id)
    {
        Faq::find($id)->delete();

        Notifications::success('ВиО успешно удален', 'top');

        return redirect()->back();
    }

    public function createFaq(FaqCategory $categories)
    {
        $categories = $categories->all();

        \Title::append('Создание');

        return view('help.faq_create_update', ['categories' => $categories]);
    }

    public function editFaq($id, Faq $faq, FaqCategory $categories)
    {
        $faq = $faq->find($id);
        $categories = $categories->all();

        \Title::append('Редактирование');

        return view('help.faq_create_update', ['faq' => $faq, 'categories' => $categories]);
    }

    public function createOrUpdateFaq($id = null)
    {
        $faq = Faq::findOrNew($id);
        $faq->question = request('question');
        $faq->answer = request('answer');
        $faq->category = request('category');
        $faq->img_link = request()->has('img_link') ? request('img_link') : null;
        $faq->save();

        Notifications::success('ВиО успешно изменен/создан', 'top');

        return redirect()->route('help-faq');
    }

    public function changeSupportStatusAjax(Support $support)
    {
        $support = $support->where('id', request('supportId'))->first();

        $newStatus = $support->status == Support::STATUS_READ ? Support::STATUS_UNREAD : Support::STATUS_READ;

        $support->update(['status' => $newStatus]);

        return $newStatus;
    }

    public function destroySupportStatusAjax(Support $support)
    {
        $support->where('id', request('supportId'))->delete();

        return 'success';
    }

    public function watchSupportAdmin(Support $support, $id)
    {
        $support = $support->where('id', $id)->with('author')->first();

        $support->update(['status' => Support::STATUS_READ]);

        \Title::append('Просмотр');

        view()->share('active_link', 'help_admin_index');

        return view('help.watch-support', ['support' => $support]);
    }
}
