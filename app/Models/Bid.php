<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Date\Date;

class Bid extends Model
{
    protected $table = 'bids';

    protected $fillable = [
        'author_id',
        'client_id',
        'engine',
        'region',
        'source',
        'sector',
        'object_deal',
        'category',
        'price',
        'currency',
        'payment_term',
        'first_contact',
        'last_contact',
        'next_contact',
        'next_action',
        'current_stage',
        'comment',
        'status',
    ];

    const STATUS_PRIVATE = 'private';
    const STATUS_PUBLIC = 'public';
    const STATUS_DELETED = 'deleted';

    const CURRENCY_DOLLAR = 'Доллар';
    const CURRENCY_RUBLE = 'Рубль';
    const CURRENCY_EURO = 'Евро';

    public function arraySource()
    {
        return [
            'Не установленно',
            'Рассылка писем',
            'Farpost/Avito',
            'Интернет',
            'Выставка',
            'Пресса',
            'Наружная реклама',
            'Рекомендация',
            'Повт. обращение',
        ];
    }

    public function arraySector()
    {
        return [
            'Другое',
            'Строительство',
            'Добыча ископаемых',
            'Транспорт',
            'Судоходство',
            'ЖКХ',
            'Посредники',
        ];
    }

    public function arrayCurrency()
    {
        return [
            'Доллар',
            'Рубль',
            'Евро',
        ];
    }

    public function arrayCategory()
    {
        return [
            'Запчасти',
            'Сервис',
            'ДГУ',
            'ДВС в сборе',
            'Морские',
            'ГПУ',
            'Гарантия',
        ];
    }

    public function client()
    {
        return $this->hasOne('App\Models\Client', 'id', 'client_id');
    }

    public function current_stages()
    {
        return $this->hasOne('App\Models\BidCurrentStage', 'id', 'current_stage');
    }

    public function setFirstContactAttribute($value)
    {
        if(!is_null($value)) {
            $this->attributes['first_contact'] = df(Date::parse($value), 'Y-m-d');
        }
    }
    public function getFirstContactAttribute($value)
    {
        if(!is_null($value)) {
            return df(Date::parse($value), 'd.m.Y');
        }
    }

    public function setLastContactAttribute($value)
    {
        if(!is_null($value)) {
            $this->attributes['last_contact'] = df(Date::parse($value), 'Y-m-d');
        }
    }
    public function getLastContactAttribute($value)
    {
        if(!is_null($value)) {
            return df(Date::parse($value), 'd.m.Y');
        }
    }

    public function setNextContactAttribute($value)
    {
        if(!is_null($value)) {
            $this->attributes['next_contact'] = df(Date::parse($value), 'Y-m-d');
        }
    }
    public function getNextContactAttribute($value)
    {
        if(!is_null($value)) {
            return df(Date::parse($value), 'd.m.Y');
        }
    }

    public function validateCreateOrUpdate($id, $data, $user_id)
    {
        if ($id) {
            $check = $this->where('id', $id)->where('author_id', $user_id)->exists();
            if (!$check) {
                return false;
            }
        }

        $v = \Validator::make($data, $id ? self::editRules() : self::createRules($user_id));

        if ($v->fails()) {
            return $v->errors()->all();
        }
    }

    public static function createRules()
    {
        $bid = new Bid();
        return [
            'client_id' => 'required|exists:clients,id',
            'source' => 'in:' . implode(',', $bid->arraySource()),
            'sector' => 'in:' . implode(',', $bid->arraySector()),
            'currency' => 'in:' . implode(',', $bid->arrayCurrency()),
            'category' => 'in:' . implode(',', $bid->arrayCategory()),
            'first_contact' => 'date|date_format:d.m.Y',
            'last_contact' => 'date|date_format:d.m.Y',
            'next_contact' => 'date|date_format:d.m.Y',
            'current_stage' => 'required|exists:bid_current_stages,id',
            'status' => 'in:on'
        ];
    }

    public static function editRules()
    {
        $bid = new Bid();
        return [
            'source' => 'in:' . implode(',', $bid->arraySource()),
            'sector' => 'in:' . implode(',', $bid->arraySector()),
            'currency' => 'in:' . implode(',', $bid->arrayCurrency()),
            'category' => 'in:' . implode(',', $bid->arrayCategory()),
            'first_contact' => 'date|date_format:d.m.Y',
            'last_contact' => 'date|date_format:d.m.Y',
            'next_contact' => 'date|date_format:d.m.Y',
            'current_stage' => 'required|exists:bid_current_stages,id',
            'status' => 'in:on'
        ];
    }

    public function createOrUpdate($id, $data, $user_id)
    {
        $data = (object)$data;

        $bid = self::findOrNew($id);

        if (is_null($id)) {
            $bid->client_id = $data->client_id;
            $bid->author_id = $user_id;
        }

        $bid->engine = e($data->engine);
        $bid->region = e($data->region);
        $bid->source = e($data->source);
        $bid->sector = e($data->sector);
        $bid->object_deal = e($data->object_deal);
        $bid->category = e($data->category);
        $bid->price = e(rtrim($data->price));
        $bid->currency = e($data->currency);
        $bid->payment_term = e($data->payment_term);
        $bid->first_contact = $data->first_contact;
        $bid->last_contact = $data->last_contact;
        $bid->next_contact = $data->next_contact;
        $bid->next_action = e($data->next_action);
        $bid->current_stage = e($data->current_stage);
        $bid->comment = e($data->comment);
        $bid->status = isset($data->status) ? self::STATUS_PRIVATE : self::STATUS_PUBLIC;
        $bid->save();

        return $bid->id;
    }
}
