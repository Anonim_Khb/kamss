<?php

return [

    'menu' => 'Заявки',

    'title' => [
        'prepend' => 'Админ',
        'index' => 'Заявки на добавление',
        'accepted' => 'Заявка одобрена',
    ],

    'index' => [
        'title' => 'Заявки на добавление',
        'table' => [
            'name' => 'Имя',
            'email' => 'Эл.адрес',
            'phone' => 'Телефон',
            'text' => 'Комментарий',
            'action' => 'Действия',
            'status' => 'Статус',
            'created' => 'Создано',
            'status-input' => 'Статус',
            'status-unread' => 'Ожидает',
            'status-read' => 'Рассмотренно',
            'status-all' => 'Все',
            'rejected-tooltip' => 'Отклонить заявку',
            'accepted-tooltip' => 'Одобрить заявку',
            'delete' => [
                'title' => 'Удаление',
                'not-selected' => 'Не выбрано',
            ],
        ],
    ],

    'accepted' => [
        'title' => 'Одобрение заявки для <b>:name</b>',
        'table' => [
            'role-title' => 'Выберите права',
            'role-none' => 'Не выбрано',
            'branch' => 'Филиал',
            'branch-none' => 'Не выбрано',
            'treatment' => 'Обращение',
            'btn-create' => 'Создать',
        ],
    ],

    'notification' => [
        'request-rejected' => 'Заявка отклонена',
        'user-created' => 'Новый пользователь успешно создан',
        'delete-success' => 'Запросы успешно удалены',
        'delete-error' => 'Ничего не выбрано для удаления',
    ],

];
