<?php

namespace App\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\PasswordReset' => [
            'App\Listeners\EmailSendPasswordResetLink',
        ],
        'App\Events\Ready_1_Email' => [
            'App\Listeners\SendingReady_1_Email',
        ],
        'App\Events\Own_1_Email' => [
            'App\Listeners\SendingOwn_1_Email',
        ],
        'App\Events\NewUserCreated' => [
            'App\Listeners\SendingEmailForNewUser',
        ],
        'App\Events\ReportDataChanged' => [
            'App\Listeners\SendingReportDataChanged',
        ],
        'App\Events\ReportPasswordChangedAdmin' => [
            'App\Listeners\SendingReportPasswordChangedAdmin',
        ],
        'App\Events\ReportAdminAbout' => [
            'App\Listeners\ReportAdminAboutSomeEvent',
        ],
    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher  $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);

        //
    }
}
