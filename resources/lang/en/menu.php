<?php

return [

    'left' => [
        'title' => 'Navigation menu',
        'users' => 'Users',
        'groups' => 'Groups control',
        'clients' => 'Clients',
        'templates-ready' => 'Ready templates',
        'templates-own' => 'Own templates',
        'sending' => 'Sending email',
        'request-add' => 'Add requests',
    ],

];
