<?php

use App\Models\User;

return [

    User::ROLE_SUPERADMIN => [User::ROLE_SUPERADMIN],
    User::ROLE_MODERATOR => [User::ROLE_SUPERADMIN, User::ROLE_MODERATOR],

];
