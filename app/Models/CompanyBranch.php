<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompanyBranch extends Model
{
    protected $table = 'company_branches';

    protected $fillable = [
        'name',
        'region',
        'city',
        'address',
        'postcode'
    ];
}
