<?php

use Illuminate\Database\Seeder;
use \App\Models\BidCurrentStage;

class BidCurrentStagesTableSeeder extends Seeder
{

    public function run()
    {
        $stages = [
            ['name' => 'Предварит.контакт', 'status' => BidCurrentStage::STATUS_ACTIVE],
            ['name' => 'Отправлено КП', 'status' => BidCurrentStage::STATUS_ACTIVE],
            ['name' => 'Ожидание товара', 'status' => BidCurrentStage::STATUS_ACTIVE],
            ['name' => 'Отгружено', 'status' => BidCurrentStage::STATUS_INACTIVE],
            ['name' => 'Передано др.фил.', 'status' => BidCurrentStage::STATUS_INACTIVE],
            ['name' => 'Завершено', 'status' => BidCurrentStage::STATUS_INACTIVE],
            ['name' => 'Отложено', 'status' => BidCurrentStage::STATUS_INACTIVE],
            ['name' => 'Отказ', 'status' => BidCurrentStage::STATUS_INACTIVE],
        ];

        BidCurrentStage::insert($stages);
    }
}
