@extends('simple.main')

@section('css')
    <link href='/css/media.css' rel='stylesheet' type='text/css'>
@endsection


@section('body')

    <div class="container welcome-page">

        @include('basis.notifications-page')

        <ul class="list-inline welcome-btns">
            <li>
                <a class="btn btn-success" href="{{ route('login') }}">
                    <i class="fa fa-sign-in"></i>
                    <span class="welcome-btn-text">Вход</span>
                </a>
            </li>
            <li>
                <a class="btn btn-info" data-toggle="modal" data-target="#membershipApplication">
                    <i class="fa fa-user-plus"></i>
                    <span class="welcome-btn-text">Регистрация</span>
                </a>
            </li>
        </ul>
    </div>

    @include('modal.request-add')

@endsection
