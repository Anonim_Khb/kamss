<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SendStatistic extends Model
{
    protected $table = 'send_statistics';

    protected $fillable = [
        'user_id', 'count'
    ];
}
