@extends('main')

@section('css')
    <link href="/libs/jasny-bootstrap/css/jasny-bootstrap.min.css" rel="stylesheet" type='text/css'>
@endsection

@section('body-title')

    Панель управления

@endsection

@section('body')

    <div class="no-background dashboard-general">
        <br>
        <!--EDIT PROFILE-->
        <div class="row">
            <div class="col-md-4 col-sm-6 col-xs-12">
                <a class="info-box" href="{{ route('user-edit') }}">
                    <span class="info-box-icon bg-red"><i class="fa fa-edit"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-number">Редактировать данные</span>
                        <span class="info-box-text">изменено <br>{{ ta($user->updated_at) }}</span>
                    </div>
                </a>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <a class="info-box" href="{{ route('user-edit') . '#signature_status' }}">
                    <span class="info-box-icon bg-yellow"><i class="fa fa-pencil"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-number">Изменить подпись</span>
                        <span class="info-box-text">используется <br>
                            {{ $user->signature_status == \App\Models\User::SIGNATURE_ACTIVE ? 'личная' : 'стандартная' }}
                        </span>
                    </div>
                </a>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <a class="info-box" href="{{ route('help-faq') }}">
                    <span class="info-box-icon bg-aqua"><i class="fa fa-support"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-number">Вопросы и ответы</span>
                        <span class="info-box-text">Найдено ответов: <span class="info-box-my-count">{{ $faqs }}</span></span>
                    </div>
                </a>
            </div>
        </div>

        <!--CREATES-->
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <a class="info-box" href="{{ route('clients-create-or-update-get') }}">
                    <span class="info-box-icon bg-purple"><i class="fa fa-user-plus"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-number">Добавить заказчика</span>
                    </div>
                </a>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <a class="info-box" href="{{ route('bid-create-or-update-get') }}">
                    <span class="info-box-icon bg-teal-gradient"><i class="fa fa-cart-plus"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-number">Создать запрос</span>
                    </div>
                </a>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <a class="info-box" href="{{ route('note-create-update-get') }}">
                    <span class="info-box-icon bg-orange-active"><i class="fa fa-plus-square-o"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-number">Создать заметку</span>
                    </div>
                </a>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <a class="info-box" href="{{ route('create-own-template') }}">
                    <span class="info-box-icon bg-olive-active"><i class="fa fa-file-text-o"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-number">Создать шаблон</span>
                    </div>
                </a>
            </div>
        </div>

        @include('dashboard.user_data')

    </div>

@endsection

@section('js')
    <script src="/libs/jasny-bootstrap/js/jasny-bootstrap.min.js"></script>
@endsection
