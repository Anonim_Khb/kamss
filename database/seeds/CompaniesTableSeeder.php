<?php

use Illuminate\Database\Seeder;
use \App\Models\Company;

class CompaniesTableSeeder extends Seeder
{

    public function run()
    {
        $companies = [
            ['author_id' => 1, 'name' => 'Неизвестно', 'status' => Company::STATUS_DEFAULT],
            ['author_id' => 1, 'name' => 'Частное лицо', 'status' => Company::STATUS_DEFAULT],
        ];

        Company::insert($companies);
    }
}
