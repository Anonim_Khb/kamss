function myDatepickerStandart(element) {
    $(element).datepicker({
        format: "dd.mm.yyyy",
        weekStart: 1,
        todayBtn: true,
        language: "ru",
        autoclose: true,
        todayHighlight: true
    });
}

