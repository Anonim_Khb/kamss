if ($('#defaultValue').val() && !$('input[name="error_id"]').val()) { //not change field if exists bid
    $(".select2-bid-client, .select2-bid-company").prop("disabled", true);
}

$(".select2-bid-company").prop("disabled", true);

function formatResult(myResult) {
    return $('<option>' + myResult.text + '</option>');
}

function resultSelection(selectData) {
    return selectData.text;
}

$('.select2-bid-client').select2({
    placeholder: "Начните вводить имя....",
    allowClear: false,
    language: "ru",
    multiple: true,
    maximumSelectionLength: 1,
    ajax: {
        url: routeSearchForClients,
        dataType: 'json',
        delay: 250,
        type: "POST",
        data: function (params) {
            return {
                myQuery: params.term,
                page: params.page,
                _token: $("meta[name='csrf-token']").attr('content')
            };
        },
        processResults: function (data, params) {
            params.page = params.page || 1;
            return {
                results: data,
                pagination: {
                    more: (params.page * 10) < data.total_count
                }
            };
        },
        cache: true
    },
    escapeMarkup: function (markup) {
        return markup;
    },
    minimumInputLength: 2,
    templateResult: formatResult,
    templateSelection: resultSelection
});

$('.select2-bid-company').select2({
    placeholder: "Заполнится автоматически....",
    allowClear: false,
    language: "ru",
    multiple: true,
    maximumSelectionLength: 1
});

function getCompanyByClientId(url, clientId) {
    $.ajax({
        type: "POST",
        dataType: "text",
        url: url,
        data: {clientId: clientId, _token: $("meta[name='csrf-token']").attr('content')},
        cache: false,
        success: function (data) {
            data = $.parseJSON(data);
            $('.select2-bid-company').append('<option value=' + data.id + ' selected>' + data.text + '</option>');
        }
    });
}

$('.select2-bid-client').on("select2:select", function () {
    getCompanyByClientId(routeSearchCompanyByClientId, $(this).val());
}).on("select2:unselect", function () {
    $(".select2-bid-company").val(null).trigger("change");
});
