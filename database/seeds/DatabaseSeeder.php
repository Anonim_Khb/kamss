<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(ClientsGroupsTableSeeder::class);
        $this->call(CompanyBranchesTableSeeder::class);
        $this->call(FaqCategoriesTableSeeder::class);
        $this->call(BidCurrentStagesTableSeeder::class);
        $this->call(CompaniesTableSeeder::class);
        $this->call(SettingsTableSeeder::class);
    }
}
