function tynyMceVerse1(element, heightRatio){
    tinymce.init({
        selector:element,
        height: $(window).height() / heightRatio,
        convert_urls: false,
        content_css: '/libs/tinymce/js/tinymce/skins/lightgray/content.min.css',
        plugins : 'code save textcolor advlist autolink link image lists charmap print preview table pagebreak spellchecker',
        toolbar: [
            'undo redo fontfamily | styleselect | alignleft aligncenter alignright | bullist numlist outdent indent | table | bold italic underline strikethrough | fontselect fontsizeselect | forecolor backcolor | subscript superscript | link image | removeformat | ',
            'code print preview save'
        ],
        fontsize_formats: "8pt 10pt 11pt 12pt 14pt 16pt 18pt 24pt 36pt",
        table_default_styles: {
            minWidth: '560px',
            maxWidth: '560px',
            width: '560px'
        },
        table_default_attributes: {
            border: '1px solid black'
        },
        language_url: '/libs/tinymce/js/tinymce/langs/ru_RU.js',
        menubar: false
    });
}
