<?php

Route::group(['middlewareGroups' => ['throttle', 'web']], function () {

    Route::get('/', 'HomeController@index')->name('index'); //Home//all
    //Route::get('/locale/{locale}', 'HomeController@setLocale')->name('set-locale'); //Change locale//all

    Route::get('/request/unsubscribe/{email}/{token}', 'ClientsController@unsubscribeRequest')->name('unsubscribe-index'); //Unsubscribe//all

    /*AUTH*/
    Route::group(['middleware' => ['auth']], function () {

        Route::get('/logout', 'Auth\AuthController@logout')->name('logout'); //Logout//all

        Route::group(['prefix' => 'panel', ], function () {

            Route::get('/', 'DashboardController@index')->name('panel-index'); //Panel general page//auth

            Route::get('/user/edit', 'UserController@edit')->name('user-edit'); //User edit and redirect to method//auth
            Route::post('/user/{id?}', 'UserController@createOrUpdate')->name('user-create-or-update'); //User create or update//auth

            Route::get('/clients', 'ClientsController@index')->name('clients-index'); //Clients list//auth
            Route::get('/clients/action/{id?}/{iec?}', 'ClientsController@createOrUpdate')->name('clients-create-or-update-get'); //Client edit and redirect to method//auth
            Route::post('/clients/{id?}', 'ClientsController@createOrUpdatePost')->name('clients-create-or-update-admin'); //Client create or update//auth

            Route::get('/templates/ready', 'TemplateController@readyTemplates')->name('templates-ready'); //All ready templates//auth
            Route::get('/templates/own', 'TemplateController@ownTemplates')->name('templates-own'); //All own templates//auth
            Route::get('/templates/create', 'TemplateController@createOwnTemplate')->name('create-own-template'); //Create own template with trumbowyg//auth
            Route::get('/templates/{id}/edit', 'TemplateController@editOwnTemplate')->name('edit-own-template'); //Edit own template with trumbowyg//auth
            Route::post('/templates/create-or-update/{id?}', 'TemplateController@createOrUpdateOwnTemplatePost')->name('create-own-template-post'); //Create own template with trumbowyg. Post//auth
            Route::get('/templates/show/{template}/{type}/{text?}', 'TemplateController@showEmailTemplate')->name('template-show'); //Show template//auth
            Route::get('/templates/choose/{template}/{type}/{text?}', 'TemplateController@chooseTemplate')->name('choose-template-for-send'); //Choose template//auth
            Route::get('/templates/choose-text/{text}', 'TemplateController@chooseTextForOwnTemplate')->name('choose-text-for-own-template'); //Choose template//auth
            Route::get('/templates/carcass', 'TemplateController@getCarcassTemplates')->name('choose-carcass-template-for-send'); //Choose template//auth

            Route::get('/sending', 'SendingEmailController@index')->name('sending-index-admin'); //Show page for select users to sending//auth
            Route::post('/sending/start', 'SendingEmailController@sendingEmail')->name('sending-start-admin'); //Sending process//auth
            Route::get('/sending/cancel', 'SendingEmailController@cancelSendingEmail')->name('sending-cancel'); //Sending cancel//auth

            Route::get('/help', function(){return redirect()->route('help-faq');})->name('help-index');
            Route::get('/help/faq', 'SupportController@faq')->name('help-faq'); //Help FAQ//auth
            Route::get('/help/support', 'SupportController@requestSupportFromUsers')->name('help-support-index'); //Add requests support from users//auth
            Route::post('/help/support', 'SupportController@requestSupportFromUsersPost')->name('help-support-post'); //Add requests support from users//auth

            Route::get('/bids', 'BidController@index')->name('bid-index'); //Bid index//auth
            Route::get('/bid/record/{id?}/{iec?}', 'BidController@createOrUpdateBidGet')->name('bid-create-or-update-get'); //Bid create//auth
            Route::post('/bid/create-or-update/{id?}', 'BidController@createOrUpdateBidPost')->name('bid-create-or-update'); //Bid create//auth

            Route::get('/notes', 'NoteController@index')->name('note-index'); //Bid index//auth
            Route::get('/notes/record/{id?}', 'NoteController@createOrUpdateNoteGet')->name('note-create-update-get'); //Bid index//auth
            Route::post('/notes/create-or-update/{id?}', 'NoteController@createOrUpdateNotePost')->name('note-create-update-post'); //Bid index//auth

            Route::get('/impex', 'ExcelControllerExport@index')->name('impex-index'); //Import & Export//all
            Route::post('/impex/export', 'ExcelControllerExport@exportClientsAndBids')->name('impex-export'); //Excel//auth
            Route::post('/impex/import', 'ExcelControllerImport@importClientsAndBids')->name('impex-import'); //Excel//auth
            Route::get('/impex/errors', 'ExcelControllerImport@errorsViewIndex')->name('impex-import-errors-index'); //Excel//auth
            Route::get('/impex/errors/correct', 'ExcelControllerImport@correctionImportError')->name('impex-import-errors-correct'); //Excel//auth

            Route::get('/companies/{status?}', 'CompanyController@companiesIndex')->name('companies-index'); //Companies index//auth
            Route::get('/company/{company_id}/edit', 'CompanyController@getEditCompany')->name('companies-edit'); //Companies index//auth
            Route::post('/company/{company_id}/edit', 'CompanyController@postEditCompany')->name('companies-edit'); //Companies index//auth

            Route::get('/file/{id}/download', 'FileController@downloadEmailFile')->name('file-download'); //Download file//auth

            /*Import errors*/
            Route::get('/company/create-ie', 'CompanyController@companyFixImportError')->name('company-create-import-error'); //Create company after fix import error//auth
            Route::post('/company/create-ie', 'CompanyController@companyFixImportErrorPost')->name('company-create-import-error-post'); //Create company after fix import error//auth

            /*MODERATOR*/
            Route::group(['middleware' => ['moderator']], function () {

                Route::get('/users', 'UserController@getUsersList')->name('users-list-admin'); //All users list//moderator
                Route::get('/user/create', 'UserController@create')->name('user-create-admin'); //User create and redirect to method//moderator
                Route::get('/user/{id}/edit', 'UserController@editForAdmin')->name('user-edit-admin'); //User edit for admin//moderator
                Route::get('/user/{id}/info', 'UserController@infoForAdmin')->name('user-info-admin'); //User info for admin//moderator

                Route::get('/requests', 'RequestController@showRequests')->name('requests-show-admin'); //Show all add requests//superadmin, moderator
                Route::get('/request/add/{id}/accepted', 'RequestController@membershipAccepted')->name('request-add-accepted-admin'); //Add request accepted form//moderator
                Route::post('/request/add/accepted', 'RequestController@membershipAcceptedPost')->name('membership-accepted-admin-post'); //Add request accepted post//moderator
                Route::post('/request/add/delete', 'RequestController@membershipDeletePost')->name('membership-delete-admin'); //Remove add requests//moderator

                Route::get('/groups', 'GroupsController@index')->name('group-index-admin'); //Show group list//auth
                Route::get('/groups/create', 'GroupsController@create')->name('group-create-admin'); //Create group//moderator
                Route::get('/groups/{id}/edit', 'GroupsController@edit')->name('group-edit-admin'); //Edit group//moderator
                Route::post('/groups/{id?}', 'GroupsController@createOrUpdate')->name('group-create-or-update-admin'); //Create or update group//moderator
                Route::delete('/groups/{id}/destroy', 'GroupsController@destroy')->name('group-destroy-admin'); //Remove group//moderator

                Route::get('/help/admin', 'SupportController@adminIndex')->name('help-index-admin'); //Help for moderator//moderator
                Route::get('/help/admin/categories', 'SupportController@adminCategories')->name('help-categories-admin'); //Categories management//moderator
                Route::get('/help/admin/category/{id}/edit', 'SupportController@editCategory')->name('help-category-edit-admin'); //Destroy category//moderator
                Route::get('/help/admin/category/create', 'SupportController@createCategory')->name('help-category-create-admin'); //Destroy category//moderator
                Route::post('/help/admin/category/{id?}', 'SupportController@createOrUpdateCategory')->name('help-category-create-or-update-admin'); //Create or update group//moderator
                Route::delete('/help/admin/category/{id}/destroy', 'SupportController@destroyCategory')->name('help-category-destroy-admin'); //Destroy category//moderator

                Route::get('/help/admin/faq/{id}/edit', 'SupportController@editFaq')->name('help-faq-edit-admin'); //Destroy category//moderator
                Route::get('/help/admin/faq/create', 'SupportController@createFaq')->name('help-faq-create-admin'); //Destroy category//moderator
                Route::post('/help/admin/faq/{id?}', 'SupportController@createOrUpdateFaq')->name('help-faq-create-or-update-admin'); //Create or update group//moderator
                Route::delete('/help/admin/faq/{id}/destroy', 'SupportController@destroyFaq')->name('help-faq-destroy-admin'); //Destroy category//moderator

                Route::get('/help/support/{id}/watch', 'SupportController@watchSupportAdmin')->name('help-watch-support-admin'); //Watch support request//moderator

                Route::get('/company/changes', 'CompanyController@proposeChangesTable')->name('company-changes'); //Company changes table//moderator
                Route::get('/company/{id}/changes', 'CompanyController@proposeChange')->name('company-change'); //Company changes table//moderator
                Route::post('/company/{id}/changes', 'CompanyController@proposeChangePost')->name('company-change-post'); //Company changes table//moderator

                Route::get('/notifications', 'NotificationController@index')->name('notifications-index'); //Notifications index//moderator
                Route::post('/notifications/create', 'NotificationController@createNotificationPost')->name('notification-create-post'); //Notifications create post//moderator
            });

            /*SUPERADMIN*/
            Route::group(['middleware' => ['superadmin']], function () {
                Route::get('/settings', 'SettingsController@index')->name('settings-index'); //Settings index//admin
            });
        });

    });

    /*GUEST*/
    Route::group(['middleware' => ['guest']], function () {

        Route::post('/request/add', 'RequestController@requestAdd')->name('request-add'); //Request to add//guest

        Route::get('/login', 'Auth\AuthController@login')->name('login'); //Login-Get//guest
        Route::post('/login', 'Auth\AuthController@loginPost')->name('login.post'); //Login-Post//guest

        Route::get('/password/reset', 'Auth\PasswordController@showSendEmailForResetForm')->name('password-reset.email'); //Show form for reset link//guest
        Route::post('/password/reset', 'Auth\PasswordController@resetPost')->name('password-reset.post'); //Post method for send link to email//guest
        Route::get('/password/reset/{email}/{token}', 'Auth\PasswordController@showResetForm')->name('password-reset-from-email'); //Show form for enter new password//guest
        Route::post('/password/email', 'Auth\PasswordController@sendResetLinkEmail')->name('password-email.post'); //Post method for password update//guest

    });


    /*AJAX*/
    Route::group(['middleware' => ['ajax']], function () {

        Route::group(['middleware' => ['auth']], function () {

            Route::post('/ajax/client-change-status', 'ClientsController@changeStatusAjax')->name('change-client-status-ajax');
            Route::post('/ajax/client-change-group', 'ClientsController@changeGroupAjax')->name('change-client-group-ajax');
            Route::post('/ajax/client-delete', 'ClientsController@destroyAjax')->name('delete-client-ajax');
            Route::post('/client/share/email', 'SendingEmailController@shareContactToEmail')->name('client-share-email'); //Client share to email//auth
            Route::post('/ajax/template-delete', 'TemplateController@destroyAjax')->name('delete-template-ajax');

            Route::post('/ajax/bid-delete', 'BidController@destroyBidAjax')->name('delete-bid-ajax');
            Route::post('/ajax/bid-change-status', 'BidController@changeStatusBidAjax')->name('change-status-bid-ajax');

            Route::post('/ajax/note-delete', 'NoteController@destroyNoteAjax')->name('delete-note-ajax');
            Route::post('/ajax/note-change-status', 'NoteController@changeStatusNoteAjax')->name('change-status-note-ajax');

            Route::post('/help/support/search', 'SupportController@faqSearchAjax')->name('help-faq-search'); //Add requests support from users//auth

            Route::post('/bid/client/search', 'BidController@bidClientNameSearchAjax')->name('bid-client-name-search'); //Search client name for search//auth

            Route::post('/company/search', 'CompanyController@companySearchAjax')->name('company-search-ajax'); //Search companies//auth
            Route::post('/company/create', 'CompanyController@createCompany')->name('company-create'); //Create companies//auth
            Route::post('/company/search-by-client', 'CompanyController@searchCompanyByClientId')->name('company-search-by-client-id-ajax'); //Create companies//auth

            Route::post('/file/remove', 'FileController@removeEmailFile')->name('file-email-remove'); //Remove email file//auth

            Route::post('/notifications/delete', 'NotificationController@delete')->name('notifications-delete-ajax'); //Notifications delete ajax//auth
            Route::post('/notifications/delete/all', 'NotificationController@deleteAllOldNotifications')->name('notifications-delete-all-ajax'); //Notifications delete all old ajax//auth

            Route::post('/config/sidebar/user', 'SettingsController@changeUserSidebarSetting')->name('config-user-sidebar'); //Notifications delete all old ajax//auth

            Route::post('/import/errors/remove/all', 'ExcelControllerImport@removeImportAllCorrections')->name('excel-import-remove-all-corrections'); //Excel import remove corrections//auth
            Route::post('/import/errors/remove', 'ExcelControllerImport@removeImportCorrection')->name('excel-import-remove-correction'); //Excel import remove corrections//auth

            Route::group(['middleware' => ['moderator']], function () {
                Route::post('/ajax/add-request/rejected', 'RequestController@addRequestReadStatusAjax')->name('add-request-rejected-ajax');

                Route::post('/ajax/user/status', 'UserController@changeUserStatusAjax')->name('user-change-status-ajax');
                Route::post('/ajax/user/destroy', 'UserController@destroyUserAjax')->name('user-destroy-ajax');

                Route::post('/ajax/support/status', 'SupportController@changeSupportStatusAjax')->name('support-change-status-ajax');
                Route::post('/ajax/support/destroy', 'SupportController@destroySupportStatusAjax')->name('support-destroy-ajax');

                Route::post('/company/set-checked', 'CompanyController@setCheckedStatusAjax')->name('company-set-checked-ajax'); //Set checked status//auth

                Route::post('/config/change-ajax', 'SettingsController@changeConfigAjax')->name('config-change-superadmin-ajax'); //Change config superadmin//auth
                Route::post('/config/add-ajax', 'SettingsController@addConfigAjax')->name('config-add-superadmin-ajax'); //Change config superadmin//auth

                Route::post('/notifications/search', 'NotificationController@searchUsersAjax')->name('notifications-search-ajax'); //Notifications search ajax//moderator
            });

        });

    });

});
