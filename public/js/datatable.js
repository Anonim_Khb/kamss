function myDataTable(tableId, searchInputs, paginateNumber, paginateDisable) {
    if(searchInputs) {
        $(tableId + ' tfoot th').each(function () {
            var title = $(this).text();
            $(this).html('<input type="text" placeholder="Искать ' + title + '" />');
        });
    } else {
        $('tfoot').hide();
    }

    var table = $(tableId).DataTable({
        "bPaginate": paginateDisable ? false : true,
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Все"]],
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.12/i18n/Russian.json"
        },
        "initComplete": function() {
            if(searchInputs) {
                table.columns().every(function () {
                    var that = this;

                    $('input', this.footer()).on('keyup change', function () {
                        if (that.search() != this.value) {
                            that.search(this.value).draw();
                        }
                    });
                });
            }
        },
        "pageLength": paginateNumber
    });
}
