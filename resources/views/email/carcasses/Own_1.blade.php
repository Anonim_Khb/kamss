<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>КАМСС-сервис</title>

    <style>
        * {
            font-family: sans-serif !important;
        }
        html,
        body {
            margin: 0 auto !important;
            padding: 0 !important;
            height: 100% !important;
            width: 100% !important;
        }

        * {
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
        }

        div[style*="margin: 16px 0"] {
            margin: 0 !important;
        }

        table,
        td {
            mso-table-lspace: 0pt !important;
            mso-table-rspace: 0pt !important;
        }

        table {
            border-spacing: 0 !important;
            border-collapse: collapse !important;
            table-layout: fixed !important;
            Margin: 0 auto !important;
        }

        table table table {
            table-layout: auto;
        }

        img {
            -ms-interpolation-mode: bicubic;
        }

        .yshortcuts a {
            border-bottom: none !important;
        }

        .mobile-link--footer a,
        a[x-apple-data-detectors] {
            color: inherit !important;
            text-decoration: underline !important;
        }
        .button-td,
        .button-a {
            transition: all 100ms ease-in;
        }

        .button-td:hover,
        .button-a:hover {
            background: #28b666 !important;
            border-color: #28b666 !important;
        }

        .body-image {
            padding-top: 2em;
        }
        .own-text {
            padding: 20px 20px;
            max-width: 560px !important;
        }
        .own-text table {
            display: block;
            width: 560px !important;
            word-break: break-all;
        }
        .own-text img {
            max-width: 560px !important;
            height: auto;
        }
    </style>
</head>
<body width="100%" bgcolor="#222222" style="Margin: 0;">
<div style="margin:0 auto; width: 100%; background: #222222;">
    <div style="max-width: 600px; margin: auto;">
        <table cellspacing="0" cellpadding="0" border="0" width="600" align="center">
            <tr>
                <td style="padding: 20px 0">
                    <table cellspacing="0" cellpadding="0" border="0" align="center" bgcolor="#ffffff" width="100%"
                           style="max-width: 600px;">
                        <tr>
                            <td style="background: #e8d654;">
                                <table cellspacing="0" cellpadding="0" border="0" width="100%" style="text-align: center">
                                    <tr>
                                        <td>
                                            <img style="height: 3.9em; padding-top: 1.3em;"
                                                 src="{{ asset('/images/logo/kamss_mini.png') }}" alt="KAMSS LOGO">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: 600; font-size: .85em; color: #444444;letter-spacing: .05em; padding-top: .5em">
                                            МИР КАЧЕСТВЕННОГО СЕРВИСА
                                        </td>
                                    </tr>
                                </table>
                                <p></p>
                                <p></p>
                            </td>
                        </tr>
                        <tr>
                            <td class="own-text">
                                @if(isset($text))
                                    {!! $text !!}
                                @else
                                    <p style="padding: 5em 0">ВАШ ТЕКСТ</p>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 0 20px 20px 20px;font-size: 14px; font-family: sans-serif;line-height:18px; color: #444444;">
                                @include('email.signature.type_1')
                            </td>
                        </tr>
                    </table>
                    <table cellspacing="0" cellpadding="0" border="0" align="center" width="100%"
                           style="max-width: 680px;">
                        <tr>
                            <td style="padding: 20px 10px;width: 100%;font-size: 12px; font-family: sans-serif; mso-height-rule: exactly; line-height:18px; text-align: center; color: #888888;">
                                @include('email.signature.address_1')
                                <br><br>
                                <?php !isset($client_email) ? $client_email = 'проверка' : ''; ?>
                                <?php !isset($client_token) ? $client_token = 'проверка' : ''; ?>
                                <unsubscribe>
                                    <a style="color:#888888; font-size: .85em" href="{{ route('unsubscribe-index', [$client_email, $client_token]) }}">Отписаться</a>
                                </unsubscribe>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</div>
</body>
</html>
