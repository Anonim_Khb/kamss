@extends('main')

@section('css')
    <link href="/libs/bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet" type='text/css'>
    <link href="/libs/select2/dist/css/select2.min.css" rel="stylesheet" type='text/css'>
@endsection

@section('body-title')
    Оповещения
    <a id="removeOldNotifications" onclick="removeOldNotifications('{{ route('notifications-delete-all-ajax') }}')" class="btn btn-danger pull-right">
        <i class="fa fa-remove" data-toggle="tooltip" data-placement="left" title="Удалить старые оповещения"></i>&nbsp;
    </a>
@endsection

@section('body')
    <div class="notifications-pages">
        <div class="register-box">
            <div class="login-box-body">
                @include('basis.notifications-page')
                <form action="{{ route('notification-create-post') }}"
                      method="POST">
                    {!! csrf_field() !!}
                    <div class="form-group has-feedback">
                        <label>Текст / HTML текст</label>
                        <textarea maxlength="500" rows="4" id="text" type="text" name="text" class="form-control"
                                  required placeholder="Текст / HTML текст оповещения">{{ old('text') }}</textarea>
                        <span class="fa fa-font form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <label>Font Awesome</label>
                        <input type="text" name="icon" class="form-control" required placeholder="fa fa-icon"
                               value="{{ old('icon', 'fa fa-bell') }}">
                        <span class="fa fa-fonticons form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <label>Цвет</label>
                        <input type="color" name="color" class="form-control" placeholder="#ff0000"
                               value="{{ old('color', \App\Models\Notification::ICON_COLOR_DEFAULT) }}">
                        <span class="fa fa-paint-brush form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <label>Ссылка</label>
                        <input type="text" name="link" class="form-control" placeholder="route( LINK )"
                               value="{{ old('link') }}">
                        <span class="fa fa-link form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <label>Получатели</label>
                        <select class="form-control select2-notifications-users" name="user_id[]" multiple="multiple">
                            <option selected="selected" value="{{ \App\Models\Notification::ID_FOR_ALL }}0">Отправить
                                всем
                            </option>
                        </select>
                        <span class="fa fa-users form-control-feedback"></span>
                    </div>
                    <button type="submit" class="btn btn-primary pull-right">
                        Создать
                    </button>
                    <br><br>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="/libs/select2/dist/js/select2.min.js"></script>
    <script src="/libs/select2/dist/js/i18n/ru.js"></script>
    <script src="/js/select2-notifications.js"></script>
    <script src="/js/notifications_index.js"></script>
@endsection
