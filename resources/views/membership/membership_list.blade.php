@extends('main')

@section('css')
    <link href="/libs/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" type='text/css'>
    <link href="/css/datatable.css" rel="stylesheet" type='text/css'>
@endsection

@section('body-title')

    {{ trans('membership.index.title') }}

@endsection

@section('body')

    <div class="users-index no-background">

        @include('partials._filter-add-request-1', ['route' => route('requests-show-admin')])

        @if(!$memberships->isEmpty())
            <div class="table-responsive">
                <table id="addRequestTable" class="table table-hover text-center">
                    <thead>
                    <tr>
                        <th class="col-sm-1">Id</th>
                        <th>{{ trans('membership.index.table.name') }}</th>
                        <th>{{ trans('membership.index.table.email') }}</th>
                        <th>{{ trans('membership.index.table.phone') }}</th>
                        <th>{{ trans('membership.index.table.text') }}</th>
                        <th>{{ trans('membership.index.table.action') }}</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th>Id</th>
                        <th>{{ trans('membership.index.table.name') }}</th>
                        <th>{{ trans('membership.index.table.email') }}</th>
                        <th>{{ trans('membership.index.table.phone') }}</th>
                        <th>{{ trans('membership.index.table.text') }}</th>
                        <th>{{ trans('membership.index.table.action') }}</th>
                    </tr>
                    </tfoot>
                    <tbody>
                    @foreach($memberships as $membership)
                        <tr>
                            <th>{{ $membership->id }}</th>
                            <td>{{ $membership->name }}</td>
                            <td><a href="mailto:{{ $membership->email }}">{{ $membership->email }}</a></td>
                            <td>{{ $membership->work_phone . ', ' . $membership->cell_phone }}</td>
                            <td>{{ $membership->text }}</td>
                            <td style="white-space: nowrap;">
                                <button type="submit"
                                        @if($membership->status == \App\Models\MembershipApplication::STATUS_READ) disabled
                                        @endif
                                        onclick="changeAddRequestStatus(this, '{{ $membership->id }}','Пометить прочситанным?', '{{ route('add-request-rejected-ajax') }}');"
                                        class="btn btn-xs @if($membership->status == \App\Models\MembershipApplication::STATUS_READ) btn-default @else btn-warning @endif"
                                        data-toggle="tooltip" data-placement="top"
                                        title="{{ trans('membership.index.table.rejected-tooltip') }}">
                                    <i class="fa fa-times"></i>
                                </button>
                                <button type="submit"
                                        onclick="redirectForAddRequestAccepted('{{ route('request-add-accepted-admin', $membership->id) }}');"
                                        class="btn btn-xs btn-success" data-toggle="tooltip" data-placement="top"
                                        title="{{ trans('membership.index.table.accepted-tooltip') }}">
                                    <i class="fa fa-check"></i>
                                </button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

        @else
            <div class="no-results text-center">
                <i class="fa fa-bell-slash fa-3x"></i>
                <p>{{ trans('group.search.no-results') }}</p>
            </div>
        @endif
    </div>

@endsection

@section('js')
    <script src="/js/request-actions.js"></script>
    <script src="/libs/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="/js/datatable.js"></script>
    <script>
        $(document).ready(function () {
            myDataTable('#addRequestTable', true, 25);
            formSending('.form-filter-activation');
        });
    </script>
@endsection
