function removeOldNotifications(url) {
    console.log(url);
    $.ajax({
        type: "POST",
        dataType: "text",
        url: url,
        data: {
            _token: $("meta[name='csrf-token']").attr('content')
        },
        cache: false,
        success: function (data) {
            if (data == 'ok') {
                toastr_options();
                toastr["success"]('Оповещения успешно удалены');
            }
        }
    });
}
