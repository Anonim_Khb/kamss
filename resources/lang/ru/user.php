<?php

return [
    'title' => [
        'create' => 'Создание пользователя',
        'update' => 'Редактирование пользователя <strong>:name</strong>',
    ],

    'form' => [
        'name' => 'Имя',
        'name-placeholder' => 'Имя',
        'treatment' => 'Обращение',
        'treatment-placeholder' => 'Обращение',
        'work_phone' => 'Рабочий телефон',
        'work_phone-placeholder' => 'Рабочий телефон',
        'cell_phone' => 'Сотовый телефон',
        'cell_phone-placeholder' => 'Сотовый телефон',
        'email' => 'Эл.адрес',
        'email-placeholder' => 'Эл.адрес',
        'branch' => 'Филиал',
        'branch-none' => 'Филиал не выбран',
        'role' => 'Роль',
        'role-none' => 'Роль не выбрана',
        'comment' => 'Комментарий',
        'status' => 'Статус',
        'created' => 'Создано',
        'updated' => 'Изменено',
        'actions' => 'Действия',
        'password' => 'Пароль',
        'password-placeholder' => 'Пароль',
        'report_info' => 'Сообщить о смене данных',
        'report_password' => 'Сообщить новый пароль',
        'send_welcome' => 'Отправить приветственное письмо',
        'btn-create' => 'Создать',
        'btn-update' => 'Редактировать',
    ],

];
