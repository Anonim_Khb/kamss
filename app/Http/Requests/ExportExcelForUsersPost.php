<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Models\MembershipApplication;
use Illuminate\Contracts\Validation\Validator;
use Redirect;
use Notifications;

class ExportExcelForUsersPost extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'clients_status' => 'required|in:all,active',
            'clients_type' => 'required|in:created,updated',
            'clients_created' => 'required|in:all,limit',
            'clients_day_limit' => 'numeric|min:1',
            'bids_status' => 'required|in:all,active',
            'bids_type' => 'required|in:created,updated',
            'bids_created' => 'required|in:all,limit',
            'bids_day_limit' => 'numeric|min:1',
            'send_email' => 'in:on',
            'email_address' => 'required_if:send_email,on',
            'email_text' => 'max:500',
        ];

        return $rules;
    }

    public function response(Array $errors)
    {
        return redirect()->back()->withInput();
    }


    public function formatErrors(Validator $validator)
    {
        foreach ($validator->errors()->all() as $error) {
            Notifications::danger($error, 'page');
        }

        return $validator->errors()->getMessages();
    }
}
