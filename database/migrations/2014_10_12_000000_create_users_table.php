<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\User;

class CreateUsersTable extends Migration
{
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('work_phone');
            $table->string('cell_phone');
            $table->string('treatment');
            $table->string('email')->unique();
            $table->string('password');
            $table->text('signature')->nullable();
            $table->string('signature_status')->default(User::SIGNATURE_INACTIVE);
            $table->integer('branch');
            $table->string('status')->default(User::STATUS_ACTIVE);
            $table->string('token')->nullable();
            $table->string('roles')->default(User::ROLE_MANAGER);
            $table->timestamp('last_restore')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('users');
    }
}
