<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\User;

class CreateSendStatisticsTable extends Migration
{
    public function up()
    {
        Schema::create('send_statistics', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('count');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('send_statistics');
    }
}
