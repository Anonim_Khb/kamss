<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UsersTemplate extends Model
{
    protected $table = 'users_templates';

    protected $fillable = [
        'user_id', 'name', 'text', 'file_id'
    ];
    
    const STATUS_OWN = 'own';
    const STATUS_READY = 'ready';

    public function setNameAttribute($value)
    {
//        $this->attributes['name'] = mb_ucfirst(mb_strtolower($value));
        $this->attributes['name'] = $value;
    }

    public function file()
    {
        return $this->hasOne('App\Models\UsersFile', 'id', 'file_id');
    }
}
