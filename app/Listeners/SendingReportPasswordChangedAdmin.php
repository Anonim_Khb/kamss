<?php

namespace App\Listeners;

use App\Events\ReportPasswordChangedAdmin;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;

class SendingReportPasswordChangedAdmin
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ReportPasswordChangedAdmin  $event
     * @return void
     */
    public function handle(ReportPasswordChangedAdmin $event)
    {
        $data = [
            'name' => $event->user->name,
            'email' => $event->user->email,
            'password' => $event->password,
        ];

        Mail::queue(['html' => 'email.system.html.report-password-changed-admin', 'text' => 'email.system.text.report-password-changed-admin'],
            $data, function ($message) use ($data) {
                $message->to($data['email'])->subject('Ваш пароль изменен');
            });

    }
}
