@extends('main')

@section('css')
    <link href="/libs/bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet" type='text/css'>
    <link href="/libs/select2/dist/css/select2.min.css" rel="stylesheet" type='text/css'>
    <link href="/libs/bootstrap-datepicker/css/bootstrap-datepicker3.standalone.min.css" rel="stylesheet"
          type='text/css'>
    <link href="/libs/jasny-bootstrap/css/jasny-bootstrap.min.css" rel="stylesheet" type='text/css'>
    <link href="/css/bid.css" rel="stylesheet" type='text/css'>
@endsection

@section('body-title')
    {{ isset($bid) ? 'Редактирование запроса' : 'Создание запроса' }}
@endsection

@section('body')
    <div class="bids-pages no-background">
        @include('basis.notifications-page')
        <form id="createBid"
              action="{{ route('bid-create-or-update', ['id' => isset($bid) ? $bid->id : null]) }}"
              method="POST">
            {!! csrf_field() !!}
            <div id="bidClientInfo" class="row">
                <div class="row">
                    <div class="form-group has-feedback col-sm-6 col-xs-12">
                        <label>
                            Контакт
                            @if(!isset($bid))
                                <a target="_blank" href="{{ route('clients-create-or-update-get') }}"
                                   data-toggle="tooltip" data-placement="right" title="Создать новый контакт">
                                    <i class="fa fa-plus fa-lg fa-fw"></i>
                                </a>
                            @endif
                        </label>
                        <select class="form-control select2-bid-client" name="client_id">
                            @if(isset($bid) && !isset($bid->error_id))
                                <option id="defaultValue" selected="selected"
                                        value="{{ $bid->client->id }}">{{ $bid->client->name }}</option>
                            @elseif(isset($bid->error_id) && !is_null($bid->client_id))
                                <option id="defaultValue" selected="selected"
                                        value="{{ $bid->client_id }}">{{ $bid->client_name }}</option>
                            @endif
                        </select>
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                        <div class="tip-block tip-block-under">
                            Начните вводить ФИО или название организации Контакта из <a
                                    href="{{ route('clients-index') }}">списка
                                контактов</a>. Если Контакт пока отсутствует в списке, то внесите его нажав на <i
                                    class="fa fa-plus fa-lg"></i>
                        </div>
                    </div>
                    <div class="form-group has-feedback col-sm-6 col-xs-12">
                        <label>Организация <i style="font-weight: 500"></i></label>
                        <select class="form-control select2-bid-company" name="company_id">
                            @if(isset($bid) && !isset($bid->error_id))
                                <option id="defaultValue" selected>{{ $bid->client->clientCompany->name }}</option>
                            @elseif(isset($bid->error_id) && !is_null($bid->client_id))
                                <option id="defaultValue" selected="selected">{{ $bid->client_company }}</option>
                            @endif
                        </select>
                        <span class="fa fa-building form-control-feedback"></span>
                        <div class="tip-block tip-block-under">
                            Начните вводить название организации
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group has-feedback col-sm-6 col-xs-12">
                        <label for="sector">Сектор</label>
                        <select id="sector" name="sector" class="form-control">
                            @foreach($sectors as $sector)
                                <option value="{{ $sector }}"
                                        @if(old('sector', isset($bid) ? $bid->sector : '') == $sector) selected @endif>
                                    {{ $sector }}</option>
                            @endforeach
                        </select>
                        <div class="tip-block tip-block-under">
                            Выберите из списка сектор, наиболее подходящий для данного запроса
                        </div>
                    </div>
                    <div class="form-group has-feedback col-sm-6 col-xs-12">
                        <label for="source">Источник</label>
                        <select id="source" name="source" class="form-control">
                            @foreach($sources as $source)
                                <option value="{{ $source }}"
                                        @if(old('source', isset($bid) ? $bid->source : '') == $source) selected @endif>
                                    {{ $source }}</option>
                            @endforeach
                        </select>
                        <div class="tip-block tip-block-under">
                            Выберите из списка источник, из которого заказчик узнал о Вас
                        </div>
                    </div>
                </div>
            </div>

            <div id="bidEngineInfo" class="row">
                <div class="row">
                    <div class="form-group has-feedback col-sm-6 col-xs-12">
                        <label for="engine">Двигатель</label>
                        <input type="text" name="engine" class="form-control"
                               placeholder="Номер или название ДВС"
                               value="{{ old('engine', isset($bid) ? $bid->engine : '') }}">
                        <span class="fa fa-cog form-control-feedback"></span>
                        <div class="tip-block tip-block-under">
                            Введите серийный номер ДВС и/или модель двигателя
                        </div>
                    </div>
                    <div class="form-group has-feedback col-sm-6 col-xs-12">
                        <label for="region">Регион</label>
                        <input type="text" name="region" class="form-control"
                               placeholder="Регион"
                               value="{{ old('region', isset($bid) ? $bid->region : '') }}">
                        <span class="fa fa-map-marker form-control-feedback"></span>
                        <div class="tip-block tip-block-under">
                            Введите регион, из которого обращается заказчик (в некоторых случаях регион, где находится
                            техника)
                        </div>
                    </div>
                </div>
            </div>

            <div id="bidCategoryInfo" class="row">
                <div class="row">
                    <div class="form-group has-feedback col-sm-6 col-xs-12">
                        <label for="category">Категория</label>
                        <select id="category" name="category" class="form-control">
                            @foreach($categories as $category)
                                <option value="{{ $category }}"
                                        @if(old('category', isset($bid) ? $bid->category : '') == $category) selected @endif>
                                    {{ $category }}</option>
                            @endforeach
                        </select>
                        <div class="tip-block tip-block-under">
                            Категория, к которой относится данный заказ/заявка (именно текущая заявка, а не группа, к
                            которой Вы отнесли заказчика)
                        </div>
                    </div>
                    <div class="form-group has-feedback col-sm-6 col-xs-12">
                        <label for="object_deal">Объект сделки</label>
                        <textarea rows="3" id="object_deal" type="text" name="object_deal" class="form-control"
                                  placeholder="Объект сделки или обращения (например: форсунки - 12 шт.)">{{ old('object_deal', isset($bid) ? $bid->object_deal : '') }}</textarea>
                        <span class="fa fa-wrench form-control-feedback"></span>
                        <div class="tip-block tip-block-under">
                            Укажите конечную цель (предмет) обращения, например: форсунка 3652514PX - 5 шт, фильтр
                            WF5421 - 15 шт.
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-sm-6 col-xs-12">
                        <label for="price">Сумма сделки</label>
                        <div class="input-group">
                            <input id="price" type="text" name="price" class="form-control"
                                   placeholder="Сумма сделки"
                                   value="{{ old('price', isset($bid) ? $bid->price : '') }}">
                            <span class="input-group-btn">
                                <select id="currency" name="currency" class="btn">
                                @foreach($currencies as $currency)
                                        <option value="{{ $currency }}"
                                                @if(old('currency', isset($bid) ? $bid->currency : '') == $currency) selected @endif>
                                    {{ $currency }}</option>
                                    @endforeach
                            </select>
                            </span>
                        </div>
                        <div class="tip-block tip-block-under">
                            Сумма заказа/услуги, которую Вы можете корректировать с процессе. Обратите внимание, что Вы
                            можете выбирать валюту
                        </div>
                    </div>
                    <div class="form-group has-feedback col-sm-6 col-xs-12">
                        <label for="payment_term">Условия оплаты</label>
                        <input id="payment_term" type="text" name="payment_term" class="form-control"
                               placeholder="Условия оплаты/передачи товаров/услуг"
                               value="{{ old('payment_term', isset($bid) ? $bid->payment_term : '') }}">
                        <span class="fa fa-money form-control-feedback"></span>
                        <div class="tip-block tip-block-under">
                            Условия оплаты. Например: в коммерческом предложении Вы указали условие оплаты 50/50,
                            укажите это здесь и Вам будет проще следить за этой информацией
                        </div>
                    </div>
                </div>
            </div>

            <div id="bidDatesInfo" class="row">
                <div class="row">
                    <div class="form-group has-feedback col-sm-6 col-xs-12">
                        <label for="first_contact">Дата обращения</label>
                        <input id="first_contact" data-mask="99.99.9999" type="text" name="first_contact"
                               class="form-control" placeholder="Дата первого контакта"
                               value="{{ old('first_contact', isset($bid) ? $bid->first_contact : df(Jenssegers\Date\Date::now(), 'd.m.Y')) }}">
                        <span class="fa fa-calendar-plus-o form-control-feedback"></span>
                        <div class="tip-block tip-block-under">
                            Дата, когда к Вам впервые обратился заказчик (относится к текущему проекту/заявке)
                        </div>
                    </div>
                    <div class="form-group has-feedback col-sm-6 col-xs-12">
                        <label for="last_contact">Посл. контакт</label>
                        <input id="last_contact" data-mask="99.99.9999" type="text" name="last_contact"
                               class="form-control"
                               placeholder="Дата последнего контакта"
                               value="{{ old('last_contact', isset($bid) ? $bid->last_contact : df(Jenssegers\Date\Date::now(), 'd.m.Y')) }}">
                        <span class="fa fa-calendar-o form-control-feedback"></span>
                        <div class="tip-block tip-block-under">
                            Дата, когда Вы в последний раз связывались с заказчиком по поводу этого проекта/заявки
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group has-feedback col-sm-6 col-xs-12">
                        <label for="next_contact">Следующий контакт</label>
                        <input id="next_contact" data-mask="99.99.9999" type="text" name="next_contact"
                               class="form-control"
                               placeholder="Дата следующего контакта"
                               value="{{ old('next_contact', isset($bid) ? $bid->next_contact : '') }}">
                        <span class="fa fa-calendar-check-o form-control-feedback"></span>
                        <div class="tip-block tip-block-under">
                            Дата, которую Вы назначаете для следующего контакта. Например: здесь Вы можете указать дату,
                            когда Вам нужно позвонить заказчику, а в комментариях указать цель
                        </div>
                    </div>
                    <div class="form-group has-feedback col-sm-6 col-xs-12">
                        <label for="current_stage">Текущий этап</label>
                        <select id="current_stage" name="current_stage" class="form-control">
                            @foreach($currentStages as $currentStage)
                                <option value="{{ $currentStage->id }}"
                                        @if(old('current_stage', isset($bid) ? $bid->current_stage : '') == $currentStage->id) selected @endif>
                                    {{ $currentStage->name }}</option>
                            @endforeach
                        </select>
                        <div class="tip-block tip-block-under">
                            Стадия, в которой сейчас находится Ваш проект/заявка. Вам будет удобно отслеживать все свои
                            начинания
                        </div>
                    </div>
                </div>
            </div>

            <div id="bidNotesInfo" class="row">
                <div class="row">
                    <div class="form-group has-feedback col-sm-6 col-xs-12">
                        <label for="next_action">Следующее действие</label>
                        <textarea rows="4" id="next_action" type="text" name="next_action" class="form-control"
                                  placeholder="Укажите следующее действие, например: напомнить заказчику о договоре">{{ old('next_action', isset($bid) ? $bid->next_action : '') }}</textarea>
                        <span class="fa fa-plug form-control-feedback"></span>
                        <div class="tip-block tip-block-under">
                            Укажите следующее действие
                        </div>
                    </div>
                    <div class="form-group has-feedback col-sm-6 col-xs-12">
                        <label for="comment">Комментарий</label>
                        <textarea rows="4" id="comment" type="text" name="comment" class="form-control"
                                  placeholder="Укажите всю необходимую информацию">{{ old('comment', isset($bid) ? $bid->comment : '') }}</textarea>
                        <span class="fa fa-comment form-control-feedback"></span>
                        <div class="tip-block tip-block-under">
                            Поле для Ваших комментариев, наблюдений, заметок и т.д.
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <p class="checkbox checkbox-warning col-sm-6 col-xs-12">
                    <input id="signature_status" type="checkbox" name="status" class="styled" checked>
                    <label for="signature_status">Приватная запись</label>
                    <div class="tip-block tip-block-under">
                        При включении этого флажка Ваша запись, за исключением поля "Двигатель", не будет видна
                        другим пользователям при поисковом запросе
                    </div>
                </p>
                @if(isset($bid->error_id))
                    <input type="number" name="error_id" value="{{ $bid->error_id }}" hidden>
                @endif
                <div class="col-sm-6 col-xs-12">
                    <button type="submit" class="btn btn-success pull-right">
                        {{ isset($bid) ? 'Сохранить' : 'Создать' }}
                        <i class="fa fa-check"></i>
                    </button>
                </div>
            </div>
            <br><br>
        </form>
    </div>
    <div id="tipsBlocks">Показать подсказки</div>
    @if(isset($bid->error_id))
        @include('impex._btn-remove-correction', ['error_id' => $bid->error_id])
    @endif
@endsection

@section('js')
    <script src="/libs/select2/dist/js/select2.min.js"></script>
    <script src="/libs/select2/dist/js/i18n/ru.js"></script>
    <script src="/js/select2-bid.js"></script>
    <script src="/libs/jasny-bootstrap/js/jasny-bootstrap.min.js"></script>
    <script src="/libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
    <script src="/libs/bootstrap-datepicker/locales/bootstrap-datepicker.ru.min.js"></script>
    <script src="/js/datepicker_1.js"></script>
    <script>
        myDatepickerStandart('#first_contact');
        myDatepickerStandart('#last_contact');
        myDatepickerStandart('#next_contact');
    </script>
@endsection
