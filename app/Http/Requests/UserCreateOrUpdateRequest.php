<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Contracts\Validation\Validator;
use Notifications;
use App\Http\Requests\Request;

class UserCreateOrUpdateRequest extends Request
{
    public function authorize()
    {
        if($this->route('id')) {
            if (ur(\App\Models\User::ROLE_MODERATOR)) {
                $check = true;
            } else {
                $check = User::where('id', $this->route('id'))
                    ->where('id', \Auth::id())->exists();
            }

            if ($this->route('id') == 1 && \Auth::id() != 1) {
                $check = false;
            }

            return $check;
        } else {
            return true;
        }
    }

    public function forbiddenResponse()
    {
        return abort(403);
    }

    public function rules()
    {
        $rules = [
            'name' => 'required|regex:/^([а-яА-ЯЁёa-zA-Z0-9 ]+)$/u',
            'treatment' => 'required|regex:/^([а-яА-ЯЁёa-zA-Z0-9 ]+)$/u',
            'cell_phone' => 'required',
            'work_phone' => 'required',
            'email' => 'required|email',
            'roles' => 'required',
            'branch' => 'required|numeric',
            'signature_status' => 'in:on',
            'report_info' => 'in:on',
            'report_password' => 'in:on',
            'send_welcome' => 'in:on',
        ];

        if (\Route::current()->getParameter('id')) {
            $rules = array_add($rules, 'password', 'required_if:report_password,on|min:6|max:30');
        } else {
            $rules = array_add($rules, 'password', 'required');
        }

        return $rules;
    }


    public function response(Array $errors)
    {
        return redirect()->back()->withInput();
    }


    public function formatErrors(Validator $validator)
    {
        foreach ($validator->errors()->all() as $error) {
            Notifications::danger($error, 'page');
        }

        return $validator->errors()->getMessages();
    }


}
