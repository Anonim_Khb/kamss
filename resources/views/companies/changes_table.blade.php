@extends('main')

@section('css')
    <link href="/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type='text/css'>
    <link href="/libs/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" type='text/css'>
    <link href="/css/datatable.css" rel="stylesheet" type='text/css'>
@endsection

@section('body-title')
    Изменения компаний
@endsection

@section('body')

    <div class="no-background">
        @if(!$changes->isEmpty())
            <form class="no-background form-inline" action="{{ route('companies-index')}}"
                  method="GET">
            </form>
            <div class="table-responsive">
                <table id="companyChangesTable" class="table table-hover table-bordered text-center">
                    <thead>
                    <tr>
                        <th class="col-sm-1">Id</th>
                        <th>Компания</th>
                        <th>Автор</th>
                        <th>Создано</th>
                        <th class="for-width-6">Действие</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th class="col-sm-1">Id</th>
                        <th>Компания</th>
                        <th>Автор</th>
                        <th>Создано</th>
                        <th class="for-width-6">Действие</th>
                    </tr>
                    </tfoot>
                    <tbody>
                    @foreach($changes as $change)
                        <tr>
                            <td>{{ $change->id }}</td>
                            <td>{{ $change->company->name }}</td>
                            <td>{{ $change->author->name }}</td>
                            <td class="for-width-8">{{ ta($change->created_at) }}</td>
                            <td>
                                <a class="btn btn-xs bg-orange-active" data-toggle="tooltip"
                                   href="{{ route('company-change', $change->id) }}" data-placement="top"
                                   title="Просмотр">
                                    <i class="fa fa-eye"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @else
            <div class="no-results text-center">
                <i class="fa fa-exchange fa-3x"></i>
                <p>Изменений пока не предложено</p>
            </div>
        @endif
    </div>

@endsection

@section('js')
    <script src="/libs/sweetalert2/sweetalert2.min.js"></script>
    <script src="/libs/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="/js/datatable.js"></script>
    <script src="/js/company-actions.js"></script>
    <script>
        $(document).ready(function () {
            myDataTable('#companyChangesTable', true, 25);
            formSending('.form-filter-activation');
        });
    </script>
@endsection
