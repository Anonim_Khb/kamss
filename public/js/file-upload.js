var extension = $('#file_upload').data('extension');
var excel = ['xls', 'xlsx', 'xml', 'xlsb', 'xlr', 'xlsm', 'sxc', 'ods', 'odb'];
var email = ['xls', 'xlsx', 'xml', 'xlsb', 'xlr', 'xlsm', 'sxc', 'ods', 'odb',
    '7z', '7zip', 'zip', 'rar', 'zipx', 'tar.bz2', 'tar.gz',
    'pdf', 'doc', 'docx', 'docm', 'txt', 'odm', 'odf',
    'jpg', 'jpeg', 'png', 'gif'];

$('#file_upload').on('click', function () {
    $('#input_upload_file').click();
});
$('#input_upload_file').on('change', function () {
    validateFileExtension($(this).val().split('.').pop().toLowerCase(), extension);
});

function validateFileExtension(fileExt, extension) {
    if ($.inArray(fileExt, window[extension]) !== -1) {
        $('#file_upload').css({
            'background': '#d9ffed',
            'box-shadow': '0 0 0 2px #60B88D, 0 0 0 5px #90CDAF, 0 0 0 9px #BFE3D1'
        }).children('i, #choose_file').css('color', '#1f765e');
        $('#submitFormWithFileButton').prop('disabled', false);
        $('#choose_file').text($('#input_upload_file').val().split('\\').pop());
    } else {
        $('#file_upload').css({
            'background': '#ffe6e6',
            'box-shadow': '0 0 0 2px #b8625c,0 0 0 5px #e28f85, 0 0 0 9px #e3b1ae'
        }).children('i, #choose_file').css('color', '#b8625c');
        $('#submitFormWithFileButton').prop('disabled', true);
        $('#choose_file').html('<strong>Недопустимый формат файла</strong>');
    }
}

function removeFile(fileId) {
    swal({
        title: "Удаление файла",
        text: "Удаление является необратимым. Вы действительно хотите удалить файл?",
        type: "warning",
        confirmButtonColor: warningColor,
        confirmButtonText: "Удалить",
        showCancelButton: true,
        cancelButtonColor: defaultColor,
        cancelButtonText: 'Отмена'
    }).then(function () {
        $.ajax({
            type: "POST",
            dataType: "text",
            url: routeRemove,
            data: {
                fileId: fileId,
                _token: $("meta[name='csrf-token']").attr('content')
            },
            cache: false,
            success: function (data) {
                if(data == 'remove') {
                    $('.ot-file-exists').slideUp();
                    $('#selectNewFile').slideDown();
                }
            }
        });
    }, function (dismiss) {
        if (dismiss === 'cancel') {
            //
        }
    });
}
