<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Support\Facades\Auth;

class CheckDisabledUsers
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()) {

            $user = Auth::user();

            if ($user->id !== 1 && $user->status == User::STATUS_INACTIVE) {
                if ($request->ajax() || $request->wantsJson()) {
                    return response('Access denied.', 444);
                } else {
                    return abort(444);
                }
            }
        }

        return $next($request);
    }
}
