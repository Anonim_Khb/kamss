<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>КАМСС-сервис</title>

    <style>
        * {
            font-family: sans-serif !important;
        }
    </style>
    <!-- insert web font reference, eg: <link href='https://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'> -->
    <style type="text/css">
        html,
        body {
            margin: 0 auto !important;
            padding: 0 !important;
            height: 100% !important;
            width: 100% !important;
        }

        * {
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
        }

        div[style*="margin: 16px 0"] {
            margin: 0 !important;
        }

        table,
        td {
            mso-table-lspace: 0pt !important;
            mso-table-rspace: 0pt !important;
        }

        table {
            border-spacing: 0 !important;
            border-collapse: collapse !important;
            table-layout: fixed !important;
            Margin: 0 auto !important;
        }

        table table table {
            table-layout: auto;
        }

        img {
            -ms-interpolation-mode: bicubic;
        }

        .yshortcuts a {
            border-bottom: none !important;
        }

        .mobile-link--footer a,
        a[x-apple-data-detectors] {
            color: inherit !important;
            text-decoration: underline !important;
        }
        .button-td,
        .button-a {
            transition: all 100ms ease-in;
        }

        .button-td:hover,
        .button-a:hover {
            background: #28b666 !important;
            border-color: #28b666 !important;
        }

        .body-image {
            padding-top: 2em;
        }
    </style>
</head>
<body width="100%" bgcolor="#222222" style="Margin: 0;">
<div style="margin:0 auto; width: 100%; background: #222222;">
    <div style="max-width: 600px; margin: auto;">
        <table cellspacing="0" cellpadding="0" border="0" width="600" align="center">
            <tr>
                <td style="padding: 20px 0; text-align: center">
                    <table cellspacing="0" cellpadding="0" border="0" align="center" bgcolor="#ffffff" width="100%"
                           style="max-width: 600px;">
                        <tr>
                            <td style="background: #e8d654;">
                                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                    <tr>
                                        <td>
                                            <img style="height: 3.9em; padding-top: 1.3em;"
                                                 src="{{ asset('/images/logo/kamss_mini_silver.png') }}" alt="KAMSS LOGO">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: 600; font-size: .85em; color: #444444;letter-spacing: .05em; padding-top: .5em">
                                            МИР КАЧЕСТВЕННОГО СЕРВИСА
                                        </td>
                                    </tr>
                                </table>
                                <p></p>
                                <p></p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                    <tr>
                                        <td style="padding: 40px 40px 10px 40px; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;">
                                            <b><h3 style="margin-top: 0">
                                                    Здравствуйте, {{ $client_treatment or 'ИМЯ ЗАКАЗЧИКА' }}!</h3></b>
                                            <p style="text-align: left;text-indent: 20px;">
                                                Я предлагаю Вам перестать терять свои деньги из-за простоя техники в частых ремонтах. Сделать это просто и вот каким образом.
                                            </p>
                                            <p style="text-align: left;text-indent: 20px;">
                                                Вы знали, что одна некачественная деталь двигателя может привести к его поломке, требующей дорогостоящего ремонта многих его систем?
                                            </p>
                                            <p style="text-align: left;text-indent: 20px;">
                                                Меня зовут {{ $author_treatment or $user->treatment }}, я менеджер по продажам
                                                компании <a href="http://kamss.ru/"
                                                   style="color: #526a8d">{{ $author_branch_name or $user->companyBranch->name }}</a> - единственного официального представителя
                                                компании Cummins в России.
                                            </p>
                                            <p style="text-align: left;text-indent: 20px;">
                                                Покупая у нас запчасти Вы получаете <strong>официальную гарантию Cummins</strong> - гарантию того, что все запчасти прошли строжайшую проверку качества, ряд технических испытаний и сооветсвуют всем стандартам.
                                                <br>
                                                Заказывая у нас ремонт или диагностику Вы получаете <strong>гарантию на выполненную работу</strong>, обученными профессионалами, в распоряжении которых новейшее оборудование и инструменты, аналогов которых нет в России.
                                            </p>
                                            <p style="text-align: left;text-indent: 20px;">
                                                Хотите быть уверенным в надежности своей техники? Хотите использовать ее на 100%? <strong>Просто напишите мне!</strong>
                                            </p>
                                            <p>
                                            <div style="font-size: 1em; font-weight:600;">
                                                <?php !isset($author_cell_phone) ? $author_cell_phone = $user->cell_phone : ''; ?>
                                                <span>&#9990; <a
                                                            href="tel:{{ str_replace([' ','(',')','-'],'',$author_cell_phone) }}"
                                                            style="color: #526a8d">{{ $author_cell_phone }}</a></span>
                                                <?php !isset($author_work_phone) ? $author_work_phone = $user->work_phone : ''; ?>
                                                <span style="padding: 0 .75em;">&#9990; <a
                                                            href="tel:{{ str_replace([' ','(',')','-'],'',$author_work_phone) }}"
                                                            style="color: #526a8d">{{ $author_work_phone }}</a></span>
                                                <span>&#9993; <a
                                                            href="mailto:{{ $author_email or $user->email }}"
                                                            style="color: #526a8d">{{ $author_email or $user->email }}</a></span>
                                            </div>
                                            </p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table cellspacing="0" cellpadding="0" border="0" align="center"
                                                   style="Margin: auto;">
                                                <tr>
                                                    <td style="border-radius: 3px; background: #222222; text-align: center;"
                                                        class="button-td">
                                                        <a href="mailto:{{ $author_email or $user->email }}"
                                                           style="background: #32974d; border: 15px solid #32974d; font-family: sans-serif; font-size: 13px; line-height: 1.1; text-align: center; text-decoration: none; display: block; border-radius: 3px; font-weight: bold;"
                                                           class="button-a">
                                                            <span style="padding: 0 .75em; color:#ffffff">Связаться с нами</span>
                                                        </a>
                                                    </td>
                                                </tr>
                                            </table>
                                            <br>
                                            <br>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="#ffffff" align="center" height="100%" valign="top" width="100%"
                                style="padding-bottom: 40px">
                                <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%"
                                       style="max-width:560px;">
                                    <tr>
                                        <td align="center" valign="top" width="50%">
                                            <table cellspacing="0" cellpadding="0" border="0" width="100%"
                                                   style="font-size: 14px;text-align: left;">
                                                <tr>
                                                    <td style="text-align: center; padding: 0 10px;">
                                                        <img src="{{ asset('/images/email/cummins_2.png') }}" width="200"
                                                             alt="Cummins"
                                                             style="border: 0;width: 100%;max-width: 200px;"
                                                             class="center-on-narrow">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: center;font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555; padding: 10px 10px 0;"
                                                        class="stack-column-center">
                                                        {{ $author_branch_name or $user->companyBranch->name }} - официальный
                                                        представитель компании Cummins. Только оригинальные запчасти
                                                        Cummins с завода изготовителя. Мы предоставляем гарантию на всю
                                                        продаваемую нами продукцию.
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td align="center" valign="top" width="50%">
                                            <table cellspacing="0" cellpadding="0" border="0" width="100%"
                                                   style="font-size: 14px;text-align: left;">
                                                <tr>
                                                    <td style="text-align: center; padding: 0 10px;">
                                                        <img src="{{ asset('/images/email/fleetguard_2.png') }}"
                                                             width="200"
                                                             alt="Fleetguard"
                                                             style="border: 0;width: 100%;max-width: 200px;"
                                                             class="center-on-narrow">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: center;font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555; padding: 10px 10px 0;"
                                                        class="stack-column-center">
                                                        Продукция Fleetguard – это изготовленные с использованием
                                                        новейших технологий воздушные, топливные и гидравлические
                                                        фильтры, фильтры систем охлаждения. Рекомендованно Cummins.
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td align="center" valign="top" width="50%">
                                            <table cellspacing="0" cellpadding="0" border="0" width="100%"
                                                   style="font-size: 14px;text-align: left;">
                                                <tr>
                                                    <td style="text-align: center; padding: 0 10px;">
                                                        <img src="{{ asset('/images/email/valvoline_2.png') }}"
                                                             width="200"
                                                             alt="Valvoline oil"
                                                             style="border: 0;width: 100%;max-width: 200px"
                                                             class="center-on-narrow body-image">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: center;font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555; padding: 10px 10px 0;"
                                                        class="stack-column-center">
                                                        Рекомендованное заводом Cummins масло Valvoline. По
                                                        характеристикам, пройдя все испытание, как специалистами, так и
                                                        потребителями, в самых различных климатических условиях,
                                                        показало высокие результаты.
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td align="center" valign="top" width="50%">
                                            <table cellspacing="0" cellpadding="0" border="0" width="100%"
                                                   style="font-size: 14px;text-align: left;">
                                                <tr>
                                                    <td style="text-align: center; padding: 0 10px;">
                                                        <img src="{{ asset('/images/email/total_2.png') }}" width="200"
                                                             alt="Total oil"
                                                             style="border: 0;width: 100%;max-width: 200px;"
                                                             class="center-on-narrow body-image">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: center;font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555; padding: 10px 10px 0;"
                                                        class="stack-column-center">
                                                        Моторные и трансмисионные масла различной вязкости и назначения
                                                        фирмы Total. Многочисленные победы продуктов TOTAL в самых
                                                        разных номинациях.
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td align="center" valign="top" width="50%">
                                            <table cellspacing="0" cellpadding="0" border="0" width="100%"
                                                   style="font-size: 14px;text-align: left;">
                                                <tr>
                                                    <td style="text-align: center; padding: 0 10px;">
                                                        <img src="{{ asset('/images/email/service_2.png') }}" width="200"
                                                             alt="Service"
                                                             style="border: 0;width: 100%;max-width: 200px;"
                                                             class="center-on-narrow body-image">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: center;font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555; padding: 10px 10px 0;"
                                                        class="stack-column-center">
                                                        {{ $author_branch_name or $user->companyBranch->name }} предоставляет
                                                        качественный и надежный сервис по диагностике и ремонту любых
                                                        двигателей Cummins. Выездные бригады компании на территорию
                                                        заказчика сразу на месте выполнят весь необходимый спектр работ.
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td align="center" valign="top" width="50%">
                                            <table cellspacing="0" cellpadding="0" border="0" width="100%"
                                                   style="font-size: 14px;text-align: left;">
                                                <tr>
                                                    <td style="text-align: center; padding: 0 10px;">
                                                        <img src="{{ asset('/images/email/engine_2.png') }}" width="200"
                                                             alt="Engine"
                                                             style="border: 0;width: 100%;max-width: 200px;"
                                                             class="center-on-narrow body-image">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: center;font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555; padding: 10px 10px 0;"
                                                        class="stack-column-center">
                                                        Дизельные двигатели Cummins отвечают самым современным
                                                        требованиям в области технической, эксплуатационной и
                                                        экологической безопасности. Помимо этого технические разработки
                                                        компании являются своеобразной «точкой отсчета» для
                                                        моторостроительных компаний всего мира.
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>

                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table cellspacing="0" cellpadding="0" border="0" align="center"
                                       style="Margin: auto;">
                                    <tr>
                                        <td style="border-radius: 3px; background: #222222; text-align: center;"
                                            class="button-td">
                                            <a href="mailto:{{ $author_email or $user->email }}"
                                               style="background: #32974d; border: 15px solid #32974d; font-family: sans-serif; font-size: 13px; line-height: 1.1; text-align: center; text-decoration: none; display: block; border-radius: 3px; font-weight: bold;"
                                               class="button-a">
                                               <span style="padding: 0 .75em; color:#ffffff">Связаться с нами</span>
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                                <br>
                            </td>
                        </tr>
                    </table>
                    <table cellspacing="0" cellpadding="0" border="0" align="center" width="100%"
                           style="max-width: 680px;">
                        <tr>
                            <td style="padding: 40px 10px;width: 100%;font-size: 12px; font-family: sans-serif; mso-height-rule: exactly; line-height:18px; text-align: center; color: #888888;">
                                @include('email.signature.address_1')
                                <br><br>
                                <?php !isset($client_email) ? $client_email = 'проверка' : ''; ?>
                                <?php !isset($client_token) ? $client_token = 'проверка' : ''; ?>
                                <unsubscribe>
                                    <a style="color:#888888; font-size: .85em" href="{{ route('unsubscribe-index', [$client_email, $client_token]) }}">Отписаться</a>
                                </unsubscribe>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</div>
</body>
</html>
