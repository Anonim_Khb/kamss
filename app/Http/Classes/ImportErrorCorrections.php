<?php

namespace App\Http\Classes;

class ImportErrorCorrections
{
    public function showNotificationsErrors($errors)
    {
        foreach ((object) unserialize($errors) as $error) {
            \Notifications::danger($error, 'page');
        }
    }
}