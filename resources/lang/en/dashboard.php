<?php

return [

    'title' => [
        'prepend' => 'Dashboard',
        'index' => 'General',
    ],

    'header' => [
        'logout' => 'Log out'
    ],

    'left-menu' => [
        'title' => 'Navigation menu',
        'administrators' => 'Administrators',
    ],

];
