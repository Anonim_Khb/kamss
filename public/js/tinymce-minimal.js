tinymce.init({
    selector:'#tinymceMinimal',
    height: '200px',
    convert_urls: false,
    content_css: '/libs/tinymce/js/tinymce/skins/lightgray/content.min.css',
    plugins : 'textcolor advlist autolink link lists charmap pagebreak spellchecker',
    toolbar: [
        'undo redo fontfamily | styleselect | alignleft aligncenter alignright | bullist numlist outdent indent | bold italic underline strikethrough | fontselect fontsizeselect | forecolor backcolor | subscript superscript | link | removeformat | '
    ],
    fontsize_formats: "8pt 10pt 11pt 12pt 14pt 16pt 18pt 24pt 36pt",
    language_url: '/libs/tinymce/js/tinymce/langs/ru_RU.js',
    menubar: false
});
