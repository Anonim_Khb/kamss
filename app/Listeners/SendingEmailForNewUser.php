<?php

namespace App\Listeners;

use App\Events\NewUserCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;

class SendingEmailForNewUser
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewUserCreated  $event
     * @return void
     */
    public function handle(NewUserCreated $event)
    {
        $data = [
            'name' => $event->user->name,
            'email' => $event->user->email,
            'role' => $event->user->role,
            'password' => $event->password,
        ];

        Mail::queue(['html' => 'email.system.html.new-user', 'text' => 'email.system.text.new-user'],
            $data, function ($message) use ($data) {
                $message->to($data['email'])->subject('Добро пожаловать, ' . $data['name']);
            });
    }
}
