<?php

return [

    'welcome' => [
        'text-1' => 'Private resource',
        'membership-btn' => 'Make a request',
        'modal_ma' => [
            'title' => 'Make a request',
            'text' => 'If you think that kind of life, and/or you are invited, then please apply by filling in all the fields. If your application is approved, you will get a notification to your email.',
            'name_placeholder' => 'Enter your name',
            'work_phone-placeholder' => 'Enter work phone',
            'cell_phone-placeholder' => 'Enter your cellphone',
            'email_placeholder' => 'Enter your email',
            'text_placeholder' => 'Leave a comment if required',
            'btn-cancel' => 'Cancel',
            'btn-submit' => 'Send',
        ],
        'success-notification' => 'Your request successfully send',
        'link-login' => 'Login',
        'link-admin-panel' => 'Management',
    ],

    'errors' => [
        'btn-back' => 'Back',
        'btn-home' => 'Home',
    ]

];
