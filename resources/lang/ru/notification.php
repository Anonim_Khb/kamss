<?php

return [

    'permission-error' => 'Ошибка прав доступа',
    'only-for-guest' => 'Только для незарегистрированных пользователей',
    'request-add-send-success' => 'Ваш запрос успешно отправлен',
    'login-success' => 'Вы успешно вошли',
    'login-error' => 'Вы неправильно ввели логин и/или пароль',
    'request-rejected' => 'Заявка отклонена',
    'requests-user-created' => 'Новый пользователь успешно создан',
    'request-add-delete-success'=> 'Запросы успешно удалены',
    'request-add-delete-error' => 'Ничего не выбрано для удаления',
    'password-reset-email-error' => '{1} С предыдущего запроса прошло менее :minutes минуты. Пожалуйста, повторите попытку позже.|[2,Inf] С предыдущего запроса прошло менее :minutes минут. Пожалуйста, повторите попытку позже.',
    'password-reset-email-success' => 'Письмо успешно отправленно. Пожалуйста, проверьте ваш ящик эл.почты',
    'password-reset-form-error' => 'Ссылка по которой вы перешли более не действительна',
    'password-reset-form-success' => 'Пароль успешно изменен',
    'password-reset-form-unknown-error' => 'Произошла неизвестная ошибка. Пожалуйста, повторите попытку позже.',
    'group-create-or-update' => 'Группа успешно обновлена/создана',
    'group-destroy' => 'Группа спешно удалена',
    'user-update-create-success' => 'Пользователь успешно создан/отредактирован',
    'own-template-create-or-update' => 'Шаблон успешно создан/изменен',
];
