<?php

return [

    'permission-error' => 'Permissions error',
    'only-for-guest' => 'Only for guest',
    'request-add-send-success' => 'Your request successfully send',
    'login-success' => 'You have successfully logged',
    'login-error' => 'You have entered wrong username and/or password',
    'request-rejected' => 'The request is rejected',
    'requests-user-created' => 'New user successfully created',
    'request-add-delete-success' => 'Requests successfully removed',
    'request-add-delete-error' => 'Nothing for removed',
    'password-reset-email-error' => '{1} it took less than :minutes minute with previous request, please try again later.|[2,Inf] it took less than :minutes minutes with previous request, please try again later.',
    'password-reset-email-success' => 'A letter has been sent successfully. Please check your mailbox.',
    'password-reset-form-error' => 'The link you followed is invalid',
    'password-reset-form-success' => 'Password successfully changed',
    'password-reset-form-unknown-error' => 'An error has occurred. Please try again later.',
    'group-create-or-update' => 'Group successfully created/updated',
    'group-destroy' => 'Group successfully removed',
    'user-update-create-success' => 'User successfully created/updated',
    'own-template-create-or-update' => 'Template successfully created/updated',
];
