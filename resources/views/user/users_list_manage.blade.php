@extends('main')

@section('css')
    <link href="/libs/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" type='text/css'>
    <link href="/css/datatable.css" rel="stylesheet" type='text/css'>
@endsection

@section('body-title')
    {{ trans('clients.index.title') }}
    @include('partials.button-create', ['route' => route('user-create-admin'), 'text' => trans('clients.index.create')])
@endsection

@section('body')
    <div class="users-index no-background">
        <div class="table-responsive">
            <table id="usersTable" class="table table-hover table-bordered text-center">
                <thead>
                <tr>
                    <th class="col-sm-1">Id</th>
                    <th>{{ trans('user.form.name') }}</th>
                    <th>{{ trans('user.form.email') }}</th>
                    <th>{{ trans('user.form.branch') }}</th>
                    <th>{{ trans('user.form.role') }}</th>
                    <th>{{ trans('user.form.created') }}</th>
                    <th>{{ trans('user.form.updated') }}</th>
                    <th>{{ trans('user.form.actions') }}</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th class="col-sm-1">Id</th>
                    <th>{{ trans('user.form.name') }}</th>
                    <th>{{ trans('user.form.email') }}</th>
                    <th>{{ trans('user.form.branch') }}</th>
                    <th>{{ trans('user.form.role') }}</th>
                    <th>{{ trans('user.form.created') }}</th>
                    <th>{{ trans('user.form.updated') }}</th>
                    <th>{{ trans('user.form.actions') }}</th>
                </tr>
                </tfoot>
                <tbody>
                @foreach($users as $user)
                    <tr>
                        <th>{{ $user->id }}</th>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <th>{{ $user->companyBranch->city }}</th>
                        <th>{{ $user->roles }}</th>
                        <th>{{ df($user->created_at) }}</th>
                        <th>{{ df($user->updated_at) }}</th>
                        <td style="white-space: nowrap;">
                            <a class="btn btn-xs btn-info" data-toggle="tooltip"
                               href="{{ route('user-info-admin', $user->id) }}" data-placement="top"
                               title="Информация">
                                <i class="fa fa-eye"></i>
                            </a>
                            <a onclick="changeUserStatus(this, '{{ $user->id }}','Сменить статус?', '{{ route('user-change-status-ajax') }}');"
                                    class="btn btn-xs @if($user->status == \App\Models\User::STATUS_ACTIVE) btn-success @else btn-danger @endif"
                                    data-toggle="tooltip" data-placement="top"
                                    title="{{ trans('clients.index.table.change-status-tooltip') }}">
                                <i class="fa fa-smile-o"></i>
                            </a>
                            <a class="btn btn-xs btn-warning" data-toggle="tooltip"
                                    href="{{ route('user-edit-admin', $user->id) }}" data-placement="top"
                                    title="{{ trans('group.index.table.update-tooltip') }}">
                                <i class="fa fa-pencil"></i>
                            </a>
                            <a onclick="deleteUser(this, '{{ $user->id }}','Удалить пользователя?', '{{ route('user-destroy-ajax') }}')"
                                    class="btn btn-xs btn-danger" data-toggle="tooltip" data-placement="top"
                                    title="{{ trans('group.index.table.delete-tooltip') }}">
                                <i class="fa fa-power-off"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('js')
    <script src="/js/user-actions.js"></script>
    <script src="/libs/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="/js/datatable.js"></script>
    <script>
        $(document).ready(function () {
            myDataTable('#usersTable', true, 25);
            formSending('.form-filter-activation');
        });
    </script>
@endsection