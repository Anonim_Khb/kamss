<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\User;

class CreateSupportsTable extends Migration
{
    public function up()
    {
        Schema::create('supports', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('author_id');
            $table->string('title');
            $table->string('text');
            $table->string('status')->default(\App\Models\Support::STATUS_UNREAD);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('supports');
    }
}
