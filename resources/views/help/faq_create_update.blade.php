@extends('main')

@section('css')
    <script src='//cdn.tinymce.com/4/tinymce.min.js'></script>
@endsection

@section('body-title')

    {{ isset($faq) ? 'Редактирование ВиО' : 'Создание ВиО' }}

@endsection

@section('body')

    <div class="users-create">
        <div class="register-box">

            @include('basis.notifications-page')

            <div class="login-box-body">
                <form action="{{ route('help-faq-create-or-update-admin', ['id' => isset($faq) ? $faq->id : null]) }}"
                      method="POST">
                    {!! csrf_field() !!}
                    <div class="form-group has-feedback">
                        <label for="question">Вопрос</label>
                        <textarea id="question" type="text" name="question" class="form-control"
                                  placeholder="Вопрос"
                                  required>@if(isset($faq)) {{ old('question', $faq->question) }} @endif</textarea>
                        <span class="glyphicon glyphicon-question-sign form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <label for="answer">Ответ</label>
                        <textarea id="answer" name="answer" placeholder="Ответ">
                            @if(isset($faq)) {{ old('answer', $faq->answer) }} @endif</textarea>
                        <span class="glyphicon glyphicon-font form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="text" name="img_link" class="form-control" placeholder="Путь к картинке"
                               @if(isset($faq)) value="{{ old('img_link', $faq->img_link) }}" @endif>
                        <span class="fa fa-image form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <label for="category">Категория</label>
                        <select id="category" name="category" class="form-control" required>
                            <option value="{{ null }}">Выберите категорию</option>
                            @foreach($categories as $category)
                                <option value="{{ $category->id }}" class="text-uppercase"
                                @if(isset($faq)) {{ $faq->category == $category->id ? 'selected' : '' }} @endif>{{ $category->category }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="row">
                        <div class="col-xs-4 pull-right">
                            <button type="submit"
                                    class="btn btn-primary btn-flat">
                                {{ isset($faq) ? 'Изменить' : 'Создать' }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script src="/js/tinymce-verse-1.js"></script>
    <script>
        tynyMceVerse1('#answer', '3.5');
    </script>
@endsection
