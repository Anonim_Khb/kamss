<form class="no-background form-inline" action="{{ $route }}"
      method="GET">
    <br>
    <div class="input-group input-group-sm border-bg-blue">
        <span class="input-group-addon bg-aqua-active"
              id="basic-addon1"><b>{{ trans('clients.index.table.sort-group') }}</b></span>
        <select name="group" class="form-control form-filter-activation" aria-describedby="basic-addon1">
            <option @if(request('group') == null) selected
                    @endif value="{{ null }}">{{ trans('clients.index.table.show-all') }}</option>
            @foreach($groups as $group)
                <option @if(request('group') == $group->id) selected
                        @endif value="{{ $group->id }}">{{ $group->name }}</option>
            @endforeach
            <option @if(request('group') == 'without') selected
                    @endif value="{{ 'without' }}">{{ trans('clients.index.table.show-without-group') }}</option>
        </select>
    </div>
    &nbsp;&nbsp;&nbsp;&nbsp;
    <div class="input-group input-group-sm border-bg-blue">
        <span class="input-group-addon bg-blue" id="sizing-addon2">
            <b>{{ trans('clients.index.table.sort-status') }}</b>
        </span>
        <select name="filter" class="form-control form-filter-activation" aria-describedby="sizing-addon2">
            <option @if(request('filter') == null) selected
                    @endif value="{{ null }}">{{ trans('clients.index.table.show-all') }}</option>
            <option @if(request('filter') == \App\Models\Client::STATUS_ACTIVE) selected
                    @endif value="{{ \App\Models\Client::STATUS_ACTIVE }}">{{ trans('clients.index.table.show-active') }}</option>
            <option @if(request('filter') == \App\Models\Client::STATUS_INACTIVE) selected
                    @endif value="{{ \App\Models\Client::STATUS_INACTIVE }}">{{ trans('clients.index.table.show-inactive') }}</option>
        </select>
    </div>
    <div class="func-btns-prie">
        @include('partials._select_all_contacts_button')
        @include('partials._print_button')
    </div>
    <br><br>
</form>
