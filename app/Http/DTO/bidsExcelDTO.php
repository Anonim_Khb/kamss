<?php

namespace App\Http\DTO;

class bidsExcelDTO
{
    /**
     * @var array
     */
    private $page = ['name' => 'Запросы', 'number' => 2];
    /**
     * @var array
     */
    private $id = ['name' => 'ID', 'export' => 'A', 'import' => 0, 'width' => 6];
    /**
     * @var array
     */
    private $contact = ['name' => 'Контакт', 'export' => 'B', 'import' => 1, 'width' => 30];
    /**
     * @var array
     */
    private $company = ['name' => 'Организация', 'export' => 'C', 'import' => 2, 'width' => 26];
    /**
     * @var array
     */
    private $source = ['name' => 'Источник', 'export' => 'D', 'import' => 3, 'width' => 20];
    /**
     * @var array
     */
    private $sector = ['name' => 'Сектор', 'export' => 'E', 'import' => 4, 'width' => 20];
    /**
     * @var array
     */
    private $region = ['name' => 'Регион', 'export' => 'F', 'import' => 5, 'width' => 20];
    /**
     * @var array
     */
    private $objectDeal = ['name' => 'Объект сделки', 'export' => 'G', 'import' => 6, 'width' => 28];
    /**
     * @var array
     */
    private $price = ['name' => 'Сумма', 'export' => 'H', 'import' => 7, 'width' => 16];
    /**
     * @var array
     */
    private $currency = ['name' => 'Валюта', 'export' => 'I', 'import' => 8, 'width' => 10];
    /**
     * @var array
     */
    private $paymentTerm = ['name' => 'Усл.оплаты', 'export' => 'J', 'import' => 9, 'width' => 16];
    /**
     * @var array
     */
    private $currentStages = ['name' => 'Стадия', 'export' => 'K', 'import' => 10, 'width' => 22];
    /**
     * @var array
     */
    private $category = ['name' => 'Категория', 'export' => 'L', 'import' => 11, 'width' => 18];
    /**
     * @var array
     */
    private $engine = ['name' => 'Двигатель', 'export' => 'M', 'import' => 12, 'width' => 18];
    /**
     * @var array
     */
    private $firstContact = ['name' => 'Дата обращ.', 'export' => 'N', 'import' => 13, 'width' => 10];
    /**
     * @var array
     */
    private $lastContact = ['name' => 'Посл.конт.', 'export' => 'O', 'import' => 14, 'width' => 10];
    /**
     * @var array
     */
    private $nextContact = ['name' => 'След.конт.', 'export' => 'P', 'import' => 15, 'width' => 10];
    /**
     * @var array
     */
    private $nextAction = ['name' => 'След.действие', 'export' => 'Q', 'import' => 16, 'width' => 26];
    /**
     * @var array
     */
    private $comment = ['name' => 'Комментарий', 'export' => 'R', 'import' => 17, 'width' => 44];

    public function getPage($key)
    {
        return $this->page[$key];
    }
    /**
     * @return string
     */
    public function getId($key = 'export')
    {
        return $this->id[$key];
    }
    /**
     * @return string
     */
    public function getContact($key = 'export')
    {
        return $this->contact[$key];
    }
    /**
     * @return string
     */
    public function getCompany($key = 'export')
    {
        return $this->company[$key];
    }
    /**
     * @return string
     */
    public function getSource($key = 'export')
    {
        return $this->source[$key];
    }
    /**
     * @return string
     */
    public function getSector($key = 'export')
    {
        return $this->sector[$key];
    }
    /**
     * @return string
     */
    public function getRegion($key = 'export')
    {
        return $this->region[$key];
    }
    /**
     * @return string
     */
    public function getObjectDeal($key = 'export')
    {
        return $this->objectDeal[$key];
    }
    /**
     * @return string
     */
    public function getPrice($key = 'export')
    {
        return $this->price[$key];
    }
    /**
     * @return string
     */
    public function getCurrency($key = 'export')
    {
        return $this->currency[$key];
    }
    /**
     * @return string
     */
    public function getPaymentTerm($key = 'export')
    {
        return $this->paymentTerm[$key];
    }
    /**
     * @return string
     */
    public function getCurrentStages($key = 'export')
    {
        return $this->currentStages[$key];
    }
    /**
     * @return string
     */
    public function getCategory($key = 'export')
    {
        return $this->category[$key];
    }
    /**
     * @return string
     */
    public function getEngine($key = 'export')
    {
        return $this->engine[$key];
    }
    /**
     * @return string
     */
    public function getFirstContact($key = 'export')
    {
        return $this->firstContact[$key];
    }
    /**
     * @return string
     */
    public function getLastContact($key = 'export')
    {
        return $this->lastContact[$key];
    }
    /**
     * @return string
     */
    public function getNextContact($key = 'export')
    {
        return $this->nextContact[$key];
    }
    /**
     * @return string
     */
    public function getNextAction($key = 'export')
    {
        return $this->nextAction[$key];
    }
    /**
     * @return string
     */
    public function getComment($key = 'export')
    {
        return $this->comment[$key];
    }
}