@extends('auth.main')

@section('css')
    <link href="/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type='text/css'>
    <link href="/css/impex.css" rel="stylesheet" type='text/css'>
    <link href='/css/media.css' rel='stylesheet' type='text/css'>
@endsection

@section('body')
    <div class="no-background import-errors">
        <div class="ie-body">
            <div class="ie-body-el ie-el-companies">
                <i class="fa fa-building fa-fw"></i>
                <span class="e-body-el-title">Компании</span>
                <span class="badge @if($companies) bg-red-gradient @else bg-green-gradient @endif">{{ trans_choice('dashboard.import-errors-page', $companies, [$companies]) }}</span>
            </div>
            <div class="ie-body-el ie-el-contacts">
                <i class="fa fa-address-book fa-fw"></i>
                <span class="e-body-el-title">Контакты</span>
                <span class="badge @if($clients) bg-red-gradient @else bg-green-gradient @endif">{{ trans_choice('dashboard.import-errors-page', $clients, [$clients]) }}</span>
            </div>
            <div class="ie-body-el ie-el-bids">
                <i class="fa fa-shopping-cart fa-fw"></i>
                <span class="e-body-el-title">Запросы</span>
                <span class="badge @if($bids) bg-red-gradient @else bg-green-gradient @endif">{{ trans_choice('dashboard.import-errors-page', $bids, [$bids]) }}</span>
            </div>
        </div>
        <div class="ie-button">
            <a class="btn btn-lg bg-green-active" href="{{ route('impex-import-errors-correct') }}">
                Исправить
            </a>
            <span class="errors-count-info">Осталось {{ trans_choice('dashboard.import-errors-page', ($companies + $clients + $bids), [($companies + $clients + $bids)]) }}</span>
        </div>
        <div class="remove-corrections" onclick="removeCorrections()">
            Отказаться от исправлений
            <i class="fa fa-remove fa-fw fa-lg"></i>
        </div>
    </div>
@endsection

@section('js')
    <script src="/libs/sweetalert2/sweetalert2.min.js"></script>
    <script src="/js/impex.js"></script>
    <script>var routeForRemoveCorrections = '{{ route('excel-import-remove-all-corrections') }}'</script>
@endsection
