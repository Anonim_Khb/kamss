@extends('main')

@section('css')
    <link href="/libs/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" type='text/css'>
    <link href="/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type='text/css'>
    <link href="/css/datatable.css" rel="stylesheet" type='text/css'>
@endsection

@section('body-title')

    Заметки

    @include('partials.button-create', ['route' => route('note-create-update-get'), 'text' => 'Создать'])

@endsection

@section('body')

    <div class="users-index no-background">
        <form class="no-background form-inline" action="{{ route('note-index')}}"
              method="GET">
            <br>
            <div class="input-group input-group-sm border-bg-blue">
                <span class="input-group-addon bg-blue" id="sizing-addon2">
                    <b>Статус:</b>
                </span>
                <select name="status" class="form-control form-filter-activation" aria-describedby="sizing-addon2">
                    <option @if(request('status') == 'all') selected
                            @endif value="{{ 'all' }}">Все
                    </option>
                    <option @if(request('status') == null) selected
                            @endif value="{{ null }}">Активные
                    </option>
                    <option @if(request('status') == \App\Models\Note::STATUS_INACTIVE) selected
                            @endif value="{{ \App\Models\Note::STATUS_INACTIVE }}">Неактивные
                    </option>
                </select>
            </div>
            <br>
        </form>
        @if(!$notes->isEmpty())
            <div class="table-responsive">
                <table id="NoteTable" class="table table-hover table-bordered table-responsive text-center">
                    <thead>
                    <tr>
                        <th class="col-sm-1">Id</th>
                        <th>Текст</th>
                        <th class="col-sm-2">Создано</th>
                        <th class="no-print action-dropdown-icon"><i class="fa fa-flash fa-lg"></i></th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th class="col-sm-1">Id</th>
                        <th>Текст</th>
                        <th class="col-sm-2">Создано</th>
                        <th></th>
                    </tr>
                    </tfoot>
                    <tbody>
                    @foreach($notes as $note)
                        <tr>
                            <td>{{ $note->id }}</td>
                            <td>
                                <div class="text-left">{{ $note->text }}</div>
                            </td>
                            <td>{{ ta($note->created_at) }}</td>
                            <td class="no-print action-dropdown-btn" style="white-space: nowrap;">
                                <div class="dropdown">
                                    <span id="bidDd{{ $note->id }}" class="btn-for-actions" data-toggle="dropdown"
                                          aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-ellipsis-h fa-fw fa-lg"></i>
                                    </span>
                                    <ul class="list-inline dropdown-menu dropdown-menu-right animated jello"
                                        aria-labelledby="bidDd{{ $note->id }}">
                                        @if($note->status == \App\Models\Note::STATUS_ACTIVE)
                                            <li class="dmb-success" data-toggle="tooltip" data-placement="top"
                                                title="Активная заметка | Изменить"
                                                onclick="changeNoteStatus(this, '{{ $note->id }}', '{{ route('change-status-note-ajax') }}')">
                                                <i class="fa fa-volume-up"></i>
                                            </li>
                                        @else
                                            <li class="dmb-warning" data-toggle="tooltip" data-placement="top"
                                                title="Неактивная заметка | Изменить"
                                                onclick="changeNoteStatus(this, '{{ $note->id }}', '{{ route('change-status-note-ajax') }}')">
                                                <i class="fa fa-volume-up"></i>
                                            </li>
                                        @endif
                                        <li onclick="redirectToRoute('{{ route('note-create-update-get', $note->id) }}')"
                                            class="dmb-warning" data-toggle="tooltip" data-placement="top"
                                            title="Просмотр | Редактирование">
                                            <i class="fa fa-pencil"></i>
                                        </li>
                                        <li type="submit"
                                            onclick="deleteNote(this, '{{ $note->id }}', '{{ route('delete-note-ajax') }}')"
                                            class="dmb-danger" data-toggle="tooltip" data-placement="top"
                                            title="{{ trans('group.index.table.delete-tooltip') }}">
                                            <i class="fa fa-power-off"></i>
                                        </li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @else
            <div class="no-results text-center">
                <i class="fa fa-file-code-o fa-3x"></i>
                <p>Записей пока нет</p>
            </div>
        @endif
    </div>

@endsection

@section('js')
    <script src="/libs/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="/js/datatable.js"></script>
    <script src="/js/note-actions.js"></script>
    <script src="/libs/sweetalert2/sweetalert2.min.js"></script>
    <script>
        $(document).ready(function () {
            myDataTable('#NoteTable', true, 25);
            formSending('.form-filter-activation');
        });
    </script>
@endsection
