<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\User;

class CreateFaqsTable extends Migration
{
    public function up()
    {
        Schema::create('faqs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('question');
            $table->text('answer');
            $table->string('img_link')->nullable();
            $table->integer('category');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('faqs');
    }
}
