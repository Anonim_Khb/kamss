<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\BidCreateOrUpdateRequest;
use App\Http\Requests\NoteRequest;
use App\Models\Note;
use Notifications;

class NoteController extends Controller
{
    public function __construct()
    {
        $this->middleware('inactive')->except('index');

        view()->share('active_link', 'note_index');
    }

    public function index(Note $note)
    {
        $notes = $note->where('author_id', \Auth::user()->id);

        if (request()->has('current_stage')) {
            $notes = $notes->where('current_stage', e(request('current_stage')));
        }

        if (request()->has('next_contact')) {
            $notes = $notes->where('next_contact', e(request('next_contact')));
        }

        if (request()->has('status')) {
            if (request('status') == Note::STATUS_INACTIVE) {
                $notes = $notes->where('status', Note::STATUS_INACTIVE);
            }
        } else {
            $notes = $notes->where('status', Note::STATUS_ACTIVE);
        }

        $notes = $notes->get();

        \Title::append('Заметки');

        return view('notes.index', ['notes' => $notes]);
    }

    public function createOrUpdateNoteGet(Note $note, $id = null)
    {
        if ($id) {
            $thisNote = $note->where('id', $id)->where('author_id', \Auth::user()->id)->first();
            if (!$thisNote) abort(403);
            \Title::append('Редактирование');
        } else {
            $thisNote = null;
            \Title::append('Создание');
        }


        return view('notes.create_or_update', [
            'note' => $thisNote,
        ]);
    }

    public function createOrUpdateNotePost(NoteRequest $request, $id = null)
    {
        $note = Note::findOrNew($id);
        $note->author_id = \Auth::id();
        $note->text = e($request->text);
        $note->save();

        Notifications::success('Данные успешно внесены', 'top');

        return redirect()->route('note-index');
    }

    public function destroyNoteAjax(Note $note)
    {
        $note->where('author_id', \Auth::user()->id)->where('id', e(request('noteId')))->delete();

        return 'success';
    }

    public function changeStatusNoteAjax(Note $note)
    {
        $note = $note->where('author_id', \Auth::user()->id)->where('id', e(request('noteId')))->first();

        $newStatus = $note->status == Note::STATUS_ACTIVE ? Note::STATUS_INACTIVE : Note::STATUS_ACTIVE;

        $note->update(['status' => $newStatus]);

        return $newStatus;
    }
}
