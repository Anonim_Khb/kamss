@extends('auth.main')

@section('body')

    <div class="container">
        <div class="row">
            <div class="login-box">
                @include('partials.logo-on-page')

                @include('basis.notifications-page')

                <div class="login-box-body">
                    <p class="login-box-msg">{{ trans('password.form.password-email-title') }}</p>
                    <form role="form" method="POST" action="{{ route('password-email.post') }}">
                        {!! csrf_field() !!}
                        <div class="form-group has-feedback">
                            <input type="email" name="email" class="form-control" placeholder="{{ trans('password.form.password-email-placeholder') }}"
                                   value="{{ old('email') }}">
                            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                        </div>
                        @if(conf_site()->getOne(\App\Models\Setting::CAPTCHA_PASS_RESET))
                            @include('partials.recaptcha')
                        @endif
                        <br>
                        <div class="text-right">
                            <button type="submit" class="btn btn-primary btn-flat">
                                {{ trans('password.button.password-email-submit') }}
                            </button>
                        </div>
                        <div class="form-description">
                            {{ trans('password.form.password-email-description') }}
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
