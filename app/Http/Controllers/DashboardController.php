<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Bid;
use App\Models\BidCurrentStage;
use App\Models\Client;
use App\Models\Faq;
use App\Models\Note;
use App\Models\SendStatistic;
use App\Models\UsersTemplate;
use Notifications;
use Jenssegers\Date\Date;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('inactive')->except('index');
    }

    public function index(Client $client, Bid $bid, Note $note, SendStatistic $statistic, Faq $faq)
    {
        $user = \Auth::user();

        $faqs = $faq->count();

        $clients_all = $client->where('author_id', $user->id)
            ->where('status', '!=', Client::STATUS_DELETED)->count();
        $clients_active = $client->where('author_id', $user->id)->where('status', Client::STATUS_ACTIVE)->count();
        $clients_inactive = $client->where('author_id', $user->id)->where('status', Client::STATUS_INACTIVE)->count();

        $bids_all = $bid->where('author_id', $user->id)->count();
        $bids_active = $bid->where('author_id', $user->id)->whereHas('current_stages', function ($query) {
            $query->where('status', BidCurrentStage::STATUS_ACTIVE);
        })->count();
        $bids_today = $bid->where('author_id', $user->id)->where('next_contact', df(Date::now(), 'Y-m-d'))->whereHas('current_stages', function ($query) {
            $query->where('status', BidCurrentStage::STATUS_ACTIVE);
        })->count();

        $note_all = $note->where('author_id', $user->id)->count();
        $note_active = $note->where('author_id', $user->id)->where('status', Note::STATUS_ACTIVE)->count();
        $note_inactive = $note->where('author_id', $user->id)->where('status', Note::STATUS_INACTIVE)->count();

        $client_last_create = $client->where('author_id', $user->id)->where('status', '!=', Client::STATUS_DELETED)->max('created_at');
        $client_last_send = $client->where('author_id', $user->id)->where('status', '!=', Client::STATUS_DELETED)->max('last_send');
        $bid_last_create = $bid->where('author_id', $user->id)->max('created_at');
        $note_last_create = $note->where('author_id', $user->id)->max('created_at');

        $statistic_day = $statistic->where('user_id', $user->id)->where('created_at', '>', Date::now()->subDay())->sum('count');
        $statistic_month = $statistic->where('user_id', $user->id)->where('created_at', '>', Date::now()->subMonth())->sum('count');
        $statistic_year = $statistic->where('user_id', $user->id)->where('created_at', '>', Date::now()->subYear())->sum('count');
        $statistic_all = $statistic->where('user_id', $user->id)->sum('count');

        \Title::append(trans('title.index-panel'));

        view()->share('active_link', 'dashboard_index');
        
        return view('dashboard.index', [
            'user' => $user,
            'faqs' => $faqs,
            'clients_all' => $clients_all,
            'clients_active' => $clients_active,
            'clients_inactive' => $clients_inactive,
            'bids_all' => $bids_all,
            'bids_active' => $bids_active,
            'bids_today' => $bids_today,
            'note_all' => $note_all,
            'note_active' => $note_active,
            'note_inactive' => $note_inactive,
            'client_last_create' => $client_last_create,
            'client_last_send' => $client_last_send,
            'bid_last_create' => $bid_last_create,
            'note_last_create' => $note_last_create,
            'statistic_day' => $statistic_day,
            'statistic_month' => $statistic_month,
            'statistic_year' => $statistic_year,
            'statistic_all' => $statistic_all,
        ]);
    }

}
