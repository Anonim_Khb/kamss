<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Notifications;

class Client extends Model
{
    protected $table = 'clients';

    protected $fillable = [
        'author_id',
        'name',
        'email',
        'phone',
        'treatment',
        'company',
        'position',
        'status',
        'last_send'
    ];

    const STATUS_ACTIVE = 'active';
    const STATUS_INACTIVE = 'inactive';
    const STATUS_DELETED = 'deleted';

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = mb_convert_case($value, MB_CASE_TITLE, "UTF-8");
    }

    public function setTreatmentAttribute($value)
    {
        $this->attributes['treatment'] = mb_convert_case($value, MB_CASE_TITLE, "UTF-8");
    }

    public function groups()
    {
        return $this->belongsToMany('App\Models\ClientsGroup', 'clients_groups_lists', 'client_id', 'group_id');
    }

    public function clientCompany()
    {
        return $this->belongsTo('App\Models\Company', 'company', 'id');
    }

    public function author()
    {
        return $this->hasOne('App\Models\User', 'id', 'author_id');
    }

    public function createOrUpdate($id, $data, $user_id)
    {
        $data = (object)$data;
        $client = self::findOrNew($id);
        $client->author_id = $user_id;
        $client->name = srq($data->name);
        $client->email = $data->email;
        $client->phone = srq($data->phone);
        $client->treatment = srq($data->treatment);
        if (is_null($id) || conf_site()->getOne(\App\Models\Setting::CHANGE_CLIENT_COMPANY)) {
            $client->company = $data->company_id;
        }
        $client->position = srq($data->position);
        $client->token = str_random();
        $client->save();

        if(!isset($data->company_name)) {
            $client->groups()->sync(isset($data->groups) ? $data->groups : []);
        }

        return $client->id;
    }

    public function validateCreateOrUpdate($id, $data, $user_id)
    {
        if ($id) {
            $check = $this->where('id', $id)->where('author_id', $user_id)->exists();
            if (!$check) {
                return false;
            }
        }

        $v = \Validator::make($data, $id ? self::editRules() : self::createRules($user_id));

        if ($v->fails()) {
            return $v->errors()->all();
        }
    }

    public static function createRules($user_id)
    {
        return [
            'name' => 'required',
            'email' => 'email|unique:clients,email,NULL,id,author_id,' . $user_id,
            'company_id' => 'required|exists:companies,id'
        ];
    }

    public static function editRules()
    {
        return [
            'name' => 'required',
            'email' => 'email',
            'company_id' => 'required|exists:companies,id'
        ];
    }
}
