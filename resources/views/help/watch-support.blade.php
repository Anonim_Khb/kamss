@extends('main')

@section('body-title')

    Просмотр обращения <strong>№{{ $support->id }}</strong>

@endsection

@section('body')

    <div class="users-create">
        <div class="register-box">
            <div class="login-box-body">
                <div class="form-group has-feedback">
                    <label for="author">Автор</label>
                    <textarea id="author" rows="2" type="text" class="form-control" disabled>{{ $support->author->name }}</textarea>
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <label for="title">Название</label>
                    <textarea id="title" rows="3" type="text" class="form-control" disabled>{{ $support->title }}</textarea>
                    <span class="glyphicon glyphicon-tag form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <label for="text">Текст обращения</label>
                    <textarea id="text" rows="10" type="text" class="form-control" disabled>{{ $support->text }}</textarea>
                    <span class="glyphicon glyphicon-font form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <label for="created">Дата создания</label>
                    <textarea id="created" rows="1" type="text" class="form-control" disabled>{{ $support->created_at }}</textarea>
                    <span class="glyphicon glyphicon-time form-control-feedback"></span>
                </div>
                <a class="btn btn-primary" href="{{ route('help-index-admin') }}">
                    <i class="fa fa-chevron-left"></i>&nbsp;
                    Назад
                </a>
            </div>
        </div>
    </div>

@endsection
