function changeSupportStatus(element, supportId, text, url) {
    if (confirm(text)) {
        $.ajax({
            type: "POST",
            dataType: "text",
            url: url,
            data: {supportId: supportId, _token: $("meta[name='csrf-token']").attr('content')},
            cache: false,
            success: function (data) {
                if (data == 'read') {
                    $(element).removeClass('btn-warning').addClass('btn-success');
                } else {
                    $(element).removeClass('btn-success').addClass('btn-warning');
                }
            }
        });
    }
}

function deleteSupport(element, supportId, text, url) {
    if (confirm(text)) {
        $.ajax({
            type: "POST",
            dataType: "text",
            url: url,
            data: {supportId: supportId, _token: $("meta[name='csrf-token']").attr('content')},
            cache: false,
            success: function (data) {
                if (data == 'success') {
                    $(element).parents('tr').hide('slow');
                }
            }
        });
    }
}
