<form class="no-background form-inline" action="{{ route('templates-ready') }}"
      method="GET">
    <br>
    <div class="input-group input-group-sm border-bg-blue">
        <span class="input-group-addon bg-blue" id="sizing-addon2">
            <b>{{ trans('templates.filter.templates') }}</b>
        </span>
        <select name="template" class="form-control form-filter-activation" aria-describedby="sizing-addon2">
            <option @if(request('template') == null) selected
                    @endif value="{{ null }}">{{ trans('templates.filter.all') }}</option>
            <option @if(request('template') == \App\Models\UsersTemplate::STATUS_OWN) selected
                    @endif value="{{ \App\Models\UsersTemplate::STATUS_OWN }}">{{ trans('templates.filter.own') }}</option>
            <option @if(request('template') == \App\Models\UsersTemplate::STATUS_READY) selected
                        @endif value="{{ \App\Models\UsersTemplate::STATUS_READY }}">{{ trans('templates.filter.ready') }}</option>
        </select>
    </div>
    <br>
    <br>
</form>