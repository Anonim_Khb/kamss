var status_text = false;

$('#change_confirm').on('click', function () {
    status_text = $('#change_confirm:checked').length > 0;
    if(status_text) {
        $('#companyChangeButton').removeClass('btn-danger').addClass('btn-success').text('Принять');
    } else {
        $('#companyChangeButton').removeClass('btn-success').addClass('btn-danger').text('Отказать');
    }
});

function companyChangeSave() {
    swal({
        title: "Вы уверены?",
        text: status_text ? 'Вы уверены, что хотите принять изменения?' : 'Оставить без изменений?',
        type: status_text ? "question" : "warning",
        confirmButtonColor: status_text ? successColor : warningColor,
        confirmButtonText: "Да",
        showCancelButton: true,
        cancelButtonColor: defaultColor,
        cancelButtonText: 'Отмена'
    }).then(function() {
        $('#companyChangeForm').submit();
    }, function(dismiss) {
        if (dismiss === 'cancel') {
            //
        }
    });
}
