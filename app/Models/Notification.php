<?php

namespace App\Models;

use App\Http\DTO\CreateNotificationDTO;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Date\Date;

class Notification extends Model
{
    protected $table = 'notifications';

    protected $fillable = [
        'user_id', 'status', 'text', 'icon', 'color', 'link'
    ];

    const STATUS_ACTIVE = 1;

    const ID_FOR_ALL = 0;

    const ICON_DEFAULT = 'fa fa-bell';
    const ICON_COLOR_DEFAULT = '#3c8dbc';

    public function getNotifications()
    {
        $notifications = $this->where('user_id', \Auth::user()->id)
            ->where('created_at', '>', Date::now()->subHours(config('site-notifications.lifetime')))
            ->orderBy('id', 'desc')->get();

        return $notifications;
    }

    public function createNotification(CreateNotificationDTO $dto)
    {
        if (is_array($dto->getUserId())) {
            if (in_array(Notification::ID_FOR_ALL, $dto->getUserId())) {
                $users = User::pluck('id');
                foreach ($users as $user) {
                    $dto->setUserId($user);
                    self::createEntry($dto);
                }
            } else {
                foreach ($dto->getUserId() as $user) {
                    $dto->setUserId($user);
                    self::createEntry($dto);
                }
            }
        } else {
            if ($dto->getUserId() == Notification::ID_FOR_ALL) {
                $users = User::pluck('id');
                foreach ($users as $user) {
                    $dto->setUserId($user);
                    self::createEntry($dto);
                }
            } else {
                self::createEntry($dto);
            }
        }
    }

    protected function createEntry(CreateNotificationDTO $dto)
    {
        $this->create([
            'user_id' => $dto->getUserId(),
            'text' => $dto->getText(),
            'icon' => $dto->getIcon(),
            'color' => $dto->getColor(),
            'link' => $dto->getLink()
        ]);
    }

    public function setNewUserNotification($user_id, $audience)
    {
        $dto = new CreateNotificationDTO();
        $dto->setUserId($audience);
        $dto->setText('Зарегистрировался новый пользователь - <strong>«' . User::where('id', $user_id)->first()->name . '»</strong>');
        $dto->setIcon('fa fa-user');
        $dto->setColor('#CBC854');

        self::createNotification($dto);
    }

    public function setEditUserNotification($audience)
    {
        $dto = new CreateNotificationDTO();
        $dto->setUserId($audience);
        $dto->setText('Ваши данные были отредактированы модератором');
        $dto->setIcon('fa fa-user');
        $dto->setColor('#CBC854');
        $dto->setLink('user-edit');

        self::createNotification($dto);
    }

    public function setClientUnsubscribeNotification($client_name, $audience)
    {
        $dto = new CreateNotificationDTO();
        $dto->setUserId($audience);
        $dto->setText('Ваш контакт - <strong>«' . $client_name . '»</strong> отписался от расыылки писем');
        $dto->setIcon('fa fa-user-times');
        $dto->setColor('#CB6266');
        $dto->setLink('clients-index');

        self::createNotification($dto);
    }

    public function setSuccessProposeCompanyNotification($company_name, $audience, $status)
    {
        $dto = new CreateNotificationDTO();
        $dto->setUserId($audience);
        if($status == 'success') {
            $dto->setText('Ваши правки данных компании <strong>«' . $company_name . '»</strong> успешно приняты');
        } else {
            $dto->setText('Ваши правки данных компании <strong>«' . $company_name . '»</strong> были отклонены');
        }
        $dto->setIcon('fa fa-building');
        $dto->setColor($status == 'success' ? '#34A148' : '#BB4444');
        $dto->setLink('companies-index');

        self::createNotification($dto);
    }

    public function setUnsuccessSendExcelReportNotification($audience, $recipient_email)
    {
        $dto = new CreateNotificationDTO();
        $dto->setUserId($audience);
        $dto->setText('Отчет, посланный на эл.адрес <strong>' . $recipient_email . '</strong> не был отправлен. Попробуйте еще раз.');
        $dto->setIcon('fa fa-envelope-o');
        $dto->setColor('#ff4f13');

        self::createNotification($dto);
    }

    public function setUnsuccessSendClientEmailBecouseFileNotification($audience, $email_name)
    {
        $dto = new CreateNotificationDTO();
        $dto->setUserId($audience);
        $dto->setText('Ваши письма <strong>«' . $email_name . '»</strong> не были отправлены из-за ошибки в прикрепленном файле. Попробуйте удалить файл из письма и прикрепить его заново.');
        $dto->setIcon('fa fa-envelope-o');
        $dto->setColor('#ff4f13');
        $dto->setLink('templates-own');

        self::createNotification($dto);
    }

    public function setSuccessSendClientEmailsNotification($audience, $client_count)
    {
        $dto = new CreateNotificationDTO();
        $dto->setUserId($audience);
        $dto->setText('Ваше письмо было успешно отправлено <strong>' . $client_count . '</strong> контактам.');
        $dto->setIcon('fa fa-envelope-o');
        $dto->setColor('#00a65a');
        $dto->setLink('clients-index');

        self::createNotification($dto);
    }
}
