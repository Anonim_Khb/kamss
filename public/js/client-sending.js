$(document).ready(function () {
    var templateCheck = $('input[name="t_name"]').val();
    var clientsTableID = '#clientsTable';
    myDataTable(clientsTableID, true, 25, templateCheck != '' ? true : false); //Include Datatable + parameters
    formSending('.form-filter-activation'); //Submit form for click

    $('input[type="checkbox"]').on('change', function(){
        if ($('input[name="client[]"]').filter(':checked').length > 0) {
            if(templateCheck != ''){
                $('#sendEmailsButton').prop('disabled', false);
            }
        } else {
            $('#sendEmailsButton').prop('disabled', true);
        }
    });
    
    $('#sendEmailsButton').on('click', function () {
        /*For clear filters/search fields*/
        var clientsTable = $(clientsTableID).DataTable();
        clientsTable.search('').columns().search('').draw();
        swal({
            title: "Отправить письма?",
            html: "Вы собираетесь отправить письма <strong>" + $('input[name="client[]"]').filter(':checked').length + "</strong> контактам.",
            type: "question",
            confirmButtonColor: successColor,
            confirmButtonText: "Да",
            showCancelButton: true,
            cancelButtonColor: defaultColor,
            cancelButtonText: 'Отмена'
        }).then(function() {
            $(this).find("button[type='submit']").prop('disabled', true);
            $('#sendEmailsButton').prop('disabled', true);
            $('.loading-process').show();
            $('#sendEmailsForClients').submit();
        }, function(dismiss) {
            if (dismiss === 'cancel') {
                swal(
                    'Отменено!',
                    'Отправка была отменена',
                    'warning'
                );
            }
        });
    });

    /*Select all contacts*/
    $('#seletAllContactsForSendEmails').on('click', function () {
        $('input[name="client[]"]').prop('checked', true);

        if ($('input[name="client[]"]').filter(':checked').length > 0) {
            $('#sendEmailsButton').prop('disabled', false);
        }
    });
});

function cancelSendingEmail(route) {
    swal({
        title: "Отменить отправку?",
        text: "Вы уверены, что хотите отменить отправку письма?",
        type: "question",
        confirmButtonColor: warningColor,
        confirmButtonText: "Да",
        showCancelButton: true,
        cancelButtonColor: defaultColor,
        cancelButtonText: 'Отмена'
    }).then(function() {
        window.location.href = route;
    }, function(dismiss) {
        //
    });
}
