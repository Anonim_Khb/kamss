<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\User;

class CreateCompanyBranchesTable extends Migration
{
    public function up()
    {
        Schema::create('company_branches', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('region');
            $table->string('city');
            $table->string('address');
            $table->string('postcode');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('company_branches');
    }
}
