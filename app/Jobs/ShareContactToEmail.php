<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ShareContactToEmail extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $email;
    protected $contact;
    protected $author;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($email, $contact, $author)
    {
        $this->email = $email;
        $this->contact = $contact;
        $this->author = $author;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data = [
            'author_name' => $this->author->name,
            'author_email' => $this->author->email,
            'recipient_email' => $this->email,
            'contact' => $this->contact
        ];

        \Mail::send(['html' => 'email.system.html.share-contact'],
            $data, function ($message) use ($data) {
                $message->from($data['author_email'], $data['author_name']);
                $message->to($data['recipient_email'])->subject($data['author_name'] . ' поделился контактом');
            });
    }
}
