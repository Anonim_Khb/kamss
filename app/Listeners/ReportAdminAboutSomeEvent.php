<?php

namespace App\Listeners;

use App\Events\ReportAdminAbout;
use App\Models\User;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;

class ReportAdminAboutSomeEvent
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewUserCreated $event
     * @return void
     */
    public function handle(ReportAdminAbout $event)
    {

        $moderators = User::whereIn('roles', config('roles.' . \App\Models\User::ROLE_MODERATOR . ''))
            ->select('email')->get();

        $data = [
            'text' => $event->text
        ];

        foreach ($moderators as $moderator) {
            $data = array_add($data, 'moderator_email', $moderator->email);

            Mail::queue(['html' => 'email.system.html.report-admin-about', 'text' => 'email.system.text.report-admin-about'],
                $data, function ($message) use ($data) {
                    $message->to($data['moderator_email'])->subject('Новое событие');
                });
        }
    }
}
