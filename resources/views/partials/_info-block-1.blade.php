<br>
<div class="page-info-block callout callout-{{ $style or 'info' }} alert alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <h4>{{ $title or 'ВНИМАНИЕ!' }}</h4>
    <p>{!! $text !!}</p>
</div>
