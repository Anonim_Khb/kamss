<?php

namespace App\Http\ViewComposers;

use Illuminate\Contracts\View\View;

class HeaderComposer
{
    public function compose(View $view)
    {
        $view->with([
            'notifications' => notify_site()->getNotifications()
        ]);
    }
}
