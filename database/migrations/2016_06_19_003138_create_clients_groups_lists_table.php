<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsGroupsListsTable extends Migration
{
    public function up()
    {
        Schema::create('clients_groups_lists', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('client_id')->unsigned();
            $table->integer('group_id')->unsigned();
            $table->timestamps();

            $table->foreign('client_id')->references('id')->on('clients')->onDelete('cascade');
            $table->foreign('group_id')->references('id')->on('clients_groups')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('clients_groups_lists');
    }
}
