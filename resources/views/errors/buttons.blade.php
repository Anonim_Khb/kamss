<div class="error-btn text-center">
    <ul class="list-inline">
        <li>
            <a class="btn btn-lg" href="{{ URL::previous() }}">
                <i class="fa fa-arrow-circle-left fa-lg"></i>&nbsp;&nbsp;
                {{ trans('simple.errors.btn-back') }}
            </a>
        </li>
        <li>
            <a class="btn btn-lg" href="{{ route('index') }}">
                {{ trans('simple.errors.btn-home') }}&nbsp;&nbsp;
                <i class="fa fa-arrow-circle-right fa-lg"></i>
            </a>
        </li>
    </ul>
</div>
