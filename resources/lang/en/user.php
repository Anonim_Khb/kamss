<?php

return [

    'title' => [
        'create' => 'User create',
        'update' => 'Edit user <strong>:name</strong>',
    ],

    'form' => [
        'name' => 'Name',
        'name-placeholder' => 'Name',
        'treatment' => 'Treatment',
        'treatment-placeholder' => 'Treatment',
        'work_phone' => 'Work phone',
        'work_phone-placeholder' => 'Work phone',
        'cell_phone' => 'Cellphone',
        'cell_phone-placeholder' => 'Cellphone',
        'email' => 'Email',
        'email-placeholder' => 'Email',
        'branch' => 'Branch',
        'branch-none' => 'Branch not selected',
        'role' => 'Role',
        'role-none' => 'Role not selected',
        'comment' => 'Comment',
        'status' => 'Status',
        'created' => 'Created',
        'updated' => 'Updated',
        'actions' => 'Actions',
        'password' => 'Password',
        'password-placeholder' => 'Password',
        'report_info' => 'Report about the change of data',
        'report_password' => 'Announce new password',
        'send_welcome' => 'Send welcome email',
        'btn-create' => 'Create',
        'btn-update' => 'Update',
    ],

];
