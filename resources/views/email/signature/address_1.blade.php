Филиал {{ $author_branch_name or $user->companyBranch->name }} {{ $author_branch_city or $user->companyBranch->city }}
<br><span class="mobile-link--footer">{{ $author_branch_postcode or $user->companyBranch->postcode }}, РФ,
    {{ $author_branch_region or $user->companyBranch->region }}
    , {{ $author_branch_city or $user->companyBranch->city }}
    , {{ $author_branch_address or $user->companyBranch->address }}</span>
