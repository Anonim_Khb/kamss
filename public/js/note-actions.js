toastr_options();
function changeNoteStatus(element, noteId, url) {
    swal({
        title: "Вы уверены?????",
        text: "Вы хотите изменить статус заметки",
        type: "question",
        confirmButtonColor: successColor,
        confirmButtonText: "Да",
        showCancelButton: true,
        cancelButtonColor: defaultColor,
        cancelButtonText: 'Отмена'
    }).then(function() {
        $.ajax({
            type: "POST",
            dataType: "text",
            url: url,
            data: {noteId: noteId, _token: $("meta[name='csrf-token']").attr('content')},
            cache: false,
            success: function (data) {
                if (data == 'active') {
                    toastr["success"]("Заметка активирована");
                    $(element).removeClass('dmb-warning').addClass('dmb-success');
                } else {
                    toastr["warning"]("Заметка деактивирована");
                    $(element).removeClass('dmb-success').addClass('dmb-warning');
                }
            }
        });
    }, function(dismiss) {
        // dismiss can be 'cancel', 'overlay', 'close', 'timer'
        if (dismiss === 'cancel') {
            //
        }
    });
}

function deleteNote(element, noteId, url) {
    swal({
        title: "Вы уверены?",
        text: "Вы хотите удалить заметку",
        type: "question",
        confirmButtonColor: warningColor,
        confirmButtonText: "Да",
        showCancelButton: true,
        cancelButtonColor: defaultColor,
        cancelButtonText: 'Отмена'
    }).then(function() {
        $.ajax({
            type: "POST",
            dataType: "text",
            url: url,
            data: {noteId: noteId, _token: $("meta[name='csrf-token']").attr('content')},
            cache: false,
            success: function (data) {
                if (data == 'success') {
                    toastr["warning"]("Заметка удалена");
                    $(element).parents('tr').hide('slow');
                }
            }
        });
    }, function(dismiss) {
        // dismiss can be 'cancel', 'overlay', 'close', 'timer'
        if (dismiss === 'cancel') {
            //
        }
    });
}

function redirectToRoute(route) {
    window.location.href = route;
}
