@extends('main')

@section('css')
    <link href='/css/faq.css' rel='stylesheet' type='text/css'>
    <link href="/libs/select2/dist/css/select2.min.css" rel="stylesheet" type='text/css'>
@endsection

@section('body-title')

    Помощь

    @include('partials.button-create', ['route' => route('help-support-index'), 'text' => 'Техподдержка'])

@endsection

@section('body')

    <div class="faq-index no-background">
        <br>
        <form id="searchFaq" action="{{ route('help-faq') }}" method="GET">
            <div class="form-group has-feedback">
                <select class="form-control select2-faq" name="query"></select>
            </div>
        </form>
        <br>
        @if(!is_null($faq))
            <div class="find-faq">
                <div class="find-faq-answer">{!! $faq->question !!}</div>
                <div class="find-faq-question">{!! $faq->answer !!}</div>
                @if(!is_null($faq->img_link))
                    <img src="{{ asset($faq->img_link) }}" alt="helper">
                @endif
            </div>
        @endif
        <div class="panel-group faq-category" id="accordion" role="tablist" aria-multiselectable="false">
            @foreach($categories as $faq)
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="heading{{ $faq->id }}">
                        <h4 class="panel-title faq-category-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion"
                               href="#collapse{{ $faq->id }}" aria-expanded="false"
                               aria-controls="collapse{{ $faq->id }}">
                                {{ $faq->category }}
                            </a>
                        </h4>
                    </div>
                    <div id="collapse{{ $faq->id }}" class="panel-collapse collapse" role="tabpanel"
                         aria-labelledby="heading{{ $faq->id }}">
                        <div class="panel-body">
                            @foreach($faq->faqs as $faq)
                                <div class="faq-list">
                                    <div class="faq-question">
                                        {!! $faq->question !!}
                                        @if(ur(\App\Models\User::ROLE_MODERATOR))
                                            <span class="pull-right">
                                                <form class="inline"
                                                      action="{{ route('help-faq-edit-admin', $faq->id) }}"
                                                      method="GET">
                                                    <button type="submit" class="btn btn-xs btn-warning"
                                                            data-toggle="tooltip"
                                                            data-placement="top"
                                                            title="{{ trans('group.index.table.update-tooltip') }}"><i
                                                                class="fa fa-pencil"></i>
                                                    </button>
                                                </form>
                                                <form class="inline"
                                                      action="{{ route('help-faq-destroy-admin', ['id' => $faq->id]) }}"
                                                      method="POST">
                                                    {{ csrf_field() }}
                                                    {{ method_field('DELETE') }}
                                                    <button type="submit"
                                                            onclick="return buttonConfirmation(event, 'Delete?')"
                                                            class="btn btn-xs btn-danger" data-toggle="tooltip"
                                                            data-placement="top"
                                                            title="{{ trans('group.index.table.delete-tooltip') }}"><i
                                                                class="fa fa-power-off"></i>
                                                    </button>
                                                </form>
                                            </span>
                                        @endif
                                    </div>
                                    <blockquote class="faq-answer">
                                        {!! $faq->answer !!}
                                        @if(!is_null($faq->img_link))
                                            <img src="{{ asset($faq->img_link) }}" alt="helper">
                                        @endif
                                    </blockquote>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        @if(ur(\App\Models\User::ROLE_MODERATOR))
            @include('partials.button-create', ['route' => route('help-faq-create-admin'), 'text' => 'Создать ВиО'])
        @endif
    </div>

@endsection

@section('js')
    <script src="/js/faq.js"></script>
    <script src="/libs/select2/dist/js/select2.min.js"></script>
    <script src="/libs/select2/dist/js/i18n/ru.js"></script>
    <script src="/js/select2-faq.js"></script>
@endsection
