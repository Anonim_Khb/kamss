<?php

namespace App\Jobs;

use App\Http\DTO\clientsExcelDTO;
use App\Http\DTO\companiesExcelDTO;
use App\Jobs\Job;
use App\Models\Client;
use App\Models\Company;
use App\Models\ImportError;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ClientsImportProcessing extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $clients;
    protected $user_id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($clients, $user_id)
    {
        $this->clients = $clients;
        $this->user_id = $user_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(clientsExcelDTO $dto, ImportError $error, Client $clientModel)
    {
        foreach ($this->clients as $client) {
            $array = [
                'id' => isset($client[$dto->getId('import')]) ? $client[$dto->getId('import')] : null,
                'name' => isset($client[$dto->getName('import')]) ? $client[$dto->getName('import')] : null,
                'email' => isset($client[$dto->getEmail('import')]) ? $client[$dto->getEmail('import')] : null,
                'phone' => isset($client[$dto->getPhone('import')]) ? rtrim($client[$dto->getPhone('import')]) : null,
                'treatment' => isset($client[$dto->getTreatment('import')]) ? $client[$dto->getTreatment('import')] : null,
                'company_id' => isset($client[$dto->getCompany('import')]) ? Company::where('name', $client[$dto->getCompany('import')])->value('id') : null,
                'company_name' => isset($client[$dto->getCompany('import')]) ? $client[$dto->getCompany('import')] : null,
                'position' => isset($client[$dto->getPosition('import')]) ? $client[$dto->getPosition('import')] : null,
            ];

            $errors = $clientModel->validateCreateOrUpdate($array['id'], $array, $this->user_id);

            if (!is_null($errors)) {
                $error::createError($this->user_id, $error::TYPE_CLIENTS, $array, $errors);
            } else {
                $clientModel->createOrUpdate($array['id'], $array, $this->user_id);
            }
        }
    }
}
