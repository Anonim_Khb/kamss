<?php

return [

    'menu' => 'Templates',

    'title' => [
        'index' => 'Templates',
    ],

    'index' => [
        'title-ready' => 'Ready templates',
        'title-own' => 'Own templates',
        'show' => 'Show',
        'choose' => 'Choose',
        'no-own-templates' => 'You don\'t have your own templates',
    ],

    'filter' => [
        'templates' => 'Templates',
        'all' => 'All',
        'ready' => 'Ready',
        'own' => 'Own',
        'title-ready' => 'Ready templates',
        'title-own' => 'Your templates',
    ],

    'create' => [
        'title' => 'Template create',
        'save-btn' => 'Save',
        'name' => 'Template name',
        'name-placeholder' => 'Enter template name',
        'text-placeholder' => 'Enter the main text and stylize it as you need',
    ],
    
    'table' => [
        'name' => 'Name',
        'created' => 'Created',
        'updated' => 'Updated',
        'actions' => 'Actions',
        'choose' => 'Choose',
        'watch' => 'Watch',
        'edit' => 'Edit',
        'delete' => 'Delete',
    ],

];
