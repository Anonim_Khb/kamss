<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use Notifications;
use Auth;
use Illuminate\Http\JsonResponse;

class AuthController extends Controller
{
    public function logout()
    {
        Auth::guard('web')->logout();

        return redirect()->guest(route('index'));
    }

    public function login()
    {
        \Title::append(trans('title.login'));

        return view('auth.login');
    }

    public function loginPost(LoginRequest $request)
    {
        if (!Auth::attempt($request->only(['email', 'password']), $request->has('remember'))) {
            if ($request->ajax() || $request->wantsJson()) {
                return new JsonResponse(['Not authorized'], 403);
            }

            Notifications::error(trans('notification.login-error'), 'top');

            return redirect()->route('login')->withInput();
        }

        Notifications::success(trans('notification.login-success'), 'top');

        return redirect()->route('panel-index');
    }

}
