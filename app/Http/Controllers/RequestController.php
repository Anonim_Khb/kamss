<?php

namespace App\Http\Controllers;

use App\Events\NewUserCreated;
use App\Events\ReportAdminAbout;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\CompanyBranch;
use App\Models\MembershipApplication;
use App\Models\User;
use Notifications;
use App\Http\Requests\AddToServiceRequest;
use App\Http\Requests\MembershipAcceptedRequest;

class RequestController extends Controller
{
    public function __construct()
    {
        $this->middleware('inactive');

        view()->share('active_link', 'request_add_index');
    }

    public function requestAdd(AddToServiceRequest $request, MembershipApplication $membership)
    {
        $membership->create([
            'name' => e($request->name),
            'work_phone' => e($request->work_phone),
            'cell_phone' => e($request->cell_phone),
            'email' => e($request->email),
            'text' => e($request->text),
        ]);

        event(new ReportAdminAbout('Получена новая заявка на вступление'));

        Notifications::success(trans('notification.request-add-send-success'), 'top');

        return redirect()->route('index');
    }


    public function showRequests()
    {
        $memberships = $this->searchMemberships();

        \Title::append(trans('title.requests-show-admin'));

        return view('membership.membership_list', ['memberships' => $memberships]);
    }

    public function searchMemberships()
    {
        $memberships = new MembershipApplication();

        if (request()->has('status') && request('status') == 'read') {
            $memberships = $memberships->where('status', MembershipApplication::STATUS_READ);
        } elseif (!request()->has('status')) {
            $memberships = $memberships->where('status', MembershipApplication::STATUS_UNREAD);
        }

        return $memberships->orderBy('id', 'desc')->paginate(25);
    }

    public function addRequestReadStatusAjax()
    {
        MembershipApplication::where('id', request('requestId'))->update([
            'status' => MembershipApplication::STATUS_READ
        ]);

        return 'SUCCESS';
    }

    public function membershipAccepted($id, MembershipApplication $membership, User $roles, CompanyBranch $branch)
    {
        $membership = $membership->find($id);
        $roles = $roles->getRoles();
        $branches = $branch->orderBy('city', 'asc')->get();

        \Title::append(trans('title.request-add-accepted-admin'));

        return view('user.user_accepted', ['membership' => $membership, 'roles' => $roles, 'branches' => $branches]);
    }

    public function membershipAcceptedPost(MembershipAcceptedRequest $request, MembershipApplication $membership)
    {
        $membership = $membership->find($request->id);

        $password = str_random(6);

        $user = new UserController();
        $user = $user->createFromRequest($password, $request);

        event(new NewUserCreated($user, $password));

        notify_site()->setNewUserNotification($user->id, notify_site()::ID_FOR_ALL);

        $membership->delete();

        Notifications::success(trans('notification.requests-user-created'), 'top');

        return redirect()->route('requests-show-admin');
    }

    public function membershipDeletePost(MembershipApplication $memberships)
    {
        if (request()->has('remove_status')) {
            if (request('remove_status') != 'all') {
                $memberships->where('status', request('remove_status'))->delete();
            } else {
                $memberships->truncate();
            }
            Notifications::success(trans('membership.notification.delete-success'), 'top');
        } else {
            Notifications::danger(trans('membership.notification.delete-error'), 'top');
        }

        return redirect()->back();
    }

    public function requestSupportFromUsers()
    {
        abort(404);
    }
}
