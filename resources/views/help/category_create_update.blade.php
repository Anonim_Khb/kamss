@extends('main')

@section('body-title')

    {{ isset($category) ? 'Редактирование категории' : 'Создание категории' }}

@endsection

@section('body')

    <div class="users-create">
        <div class="register-box">

            @include('basis.notifications-page')

            <div class="login-box-body">
                <form action="{{ route('help-category-create-or-update-admin', ['id' => isset($category) ? $category->id : null]) }}"
                      method="POST">
                    {!! csrf_field() !!}
                    <div class="form-group has-feedback">
                        <input type="text" name="category" class="form-control"
                               placeholder="Категория"
                               @if(isset($category)) value="{{ old('category', $category->category) }}" @endif required>
                        <span class="glyphicon glyphicon-pencil form-control-feedback"></span>
                    </div>
                    <div class="row">
                        <div class="col-xs-4 pull-right">
                            <button type="submit"
                                    class="btn btn-primary btn-flat">
                                    {{ isset($category) ? 'Изменить' : 'Создать' }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
