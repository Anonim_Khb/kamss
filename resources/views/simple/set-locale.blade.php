<ul class="list-inline">
    <li>
        <a href="{{ route('set-locale', 'ru') }}">
            <img src="{{ asset('/images/flags/ru.png') }}" alt="ru">
        </a>
    <li>
        <a href="{{ route('set-locale', 'en') }}">
            <img src="{{ asset('/images/flags/en.png') }}" alt="en">
        </a>
</ul>