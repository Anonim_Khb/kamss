@extends('main')

@section('css')
    <link href="/libs/bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet" type='text/css'>
    <link href="/libs/select2/dist/css/select2.min.css" rel="stylesheet" type='text/css'>
    <link href="/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type='text/css'>
@endsection

@section('body-title')

    Редактирование компании <strong>«{{ $company->name }}»</strong>

@endsection

@section('body')

    <div class="users-create">
        <div class="register-box">
            <div class="login-box-body">
                @include('basis.notifications-page')
                <form action="{{ route('companies-edit', ['id' => $company->id]) }}"
                      method="POST">
                    {!! csrf_field() !!}

                    @include('companies._fields-simple')

                    @if(ur(\App\Models\User::ROLE_MODERATOR))
                        <div class="form-group has-feedback">
                            <label for="client">Автор</label>
                            <input type="text" name="author" class="form-control"
                                   placeholder="Автор"
                                   value="{{ $company->author->name }}" readonly>
                            <span class="fa fa-user form-control-feedback"></span>
                        </div>
                        <div class="form-group has-feedback">
                            <label>Замена</label>
                            <select class="form-control select2-company" name="replacement_id"></select>
                            <span class="fa fa-refresh form-control-feedback"></span>
                        </div>
                        <div class="checkbox checkbox-danger">
                            <input id="replacement_confirm" type="checkbox" class="styled" name="replacement_confirm"
                                   @if(old('replacement_confirm')) checked @endif>
                            <label for="replacement_confirm"><strong>Удаление с заменой</strong></label>
                        </div>
                    @else
                        <div class="callout callout-info">
                            <p>
                                При внесении изменений помните, что Вы лишь предлагаете правки, поэтому увидеть изменения
                                в названии и/или реквизитах компании Вы сможете <strong>только после проверки данных модератором</strong>.
                            </p>
                        </div>
                    @endif
                    <div class="pull-right">
                        <button type="submit" class="btn btn-primary btn-flat">
                            Сохранить
                        </button>
                    </div>
                </form>
                <br><br>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="/libs/sweetalert2/sweetalert2.min.js"></script>
    <script src="/libs/select2/dist/js/select2.min.js"></script>
    <script src="/libs/select2/dist/js/i18n/ru.js"></script>
    <script src="/js/select2-for-companies.js"></script>
@endsection