@extends('main')

@section('css')
    <link href="/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type='text/css'>
@endsection

@section('body-title')
    Редактирование компании <strong>«{{ $company->name }}»</strong>
@endsection

@section('body')

    <div class="users-create">
        <div class="register-box">
            <div class="login-box-body">
                <strong class="text-red">Вы не исправили все или одну из этих ошибок:</strong>
                @include('basis.notifications-page')
                <form action="{{ route('company-create-import-error-post') }}"
                      method="POST">
                    {!! csrf_field() !!}

                    @include('companies._fields-simple')

                    <input type="number" name="error_id" value="{{ $company->error_id }}" hidden>
                    <div class="callout callout-info">
                        <p>
                            При внесении изменений помните, что Вы лишь предлагаете правки, поэтому увидеть изменения
                            в названии и/или реквизитах компании Вы сможете <strong>только после проверки данных модератором</strong>.
                        </p>
                    </div>
                    <div class="pull-right">
                        <button type="submit" class="btn btn-primary btn-flat">
                            Сохранить
                        </button>
                    </div>
                </form>
                <br><br>
            </div>
        </div>
    </div>
    @include('impex._btn-remove-correction', ['error_id' => $company->error_id])
@endsection

@section('js')
    <script src="/libs/sweetalert2/sweetalert2.min.js"></script>
    <script src="/js/excel-import-errors.js"></script>
@endsection