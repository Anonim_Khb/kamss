<?php

namespace App\Http\Middleware;

use Closure;
use Notifications;
use App\Models\User;

class SuperAdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (\Auth::check() && auth()->user()->roles != User::ROLE_SUPERADMIN){
            Notifications::error(trans('notification.permission-error'), 'top');

            abort(403);
        }

        return $next($request);
    }
}
