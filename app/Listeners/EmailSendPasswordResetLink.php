<?php

namespace App\Listeners;

use App\Events\PasswordReset;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;

class EmailSendPasswordResetLink
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PasswordReset  $event
     * @return void
     */
    public function handle(PasswordReset $event)
    {
        $data = [
            'name' => $event->user->name,
            'email' => $event->user->email,
            'token' => $event->user->token,
        ];

        Mail::queue(['html' => 'email.system.html.password-reset-link', 'text' => 'email.system.text.password-reset-link'],
            $data, function ($message) use ($data) {
                $message->to($data['email'])->subject('Смена пароля');
            });
    }
}
