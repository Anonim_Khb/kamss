@extends('main')

@section('css')
    <link href="/libs/bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet" type='text/css'>
    <script src='//cdn.tinymce.com/4/tinymce.min.js'></script>
@endsection

@section('body-title')
    {!! isset($user) ? trans('user.title.update', ['name' => $user->name]) : trans('user.title.create') !!}
@endsection

@section('body')
    <div class="no-background">
        @include('basis.notifications-page')
        @include('partials._info-block-1', ['text' => trans('info.user-create-or-update'), 'title' => 'ВНИМАНИЕ!', 'style' => 'warning'])
    </div>

    <div class="register-box">
        <form action="{{ route('user-create-or-update', ['id' => isset($user) ? $user->id : null ]) }}"
              method="POST">
            {{ csrf_field() }}
            <div class="form-group has-feedback">
                <label for="name">{{ trans('user.form.name') }}</label>
                <input id="name" type="text" name="name" class="form-control"
                       placeholder="{{ trans('user.form.name-placeholder') }}"
                       value="{{ old('name', isset($user) ? $user->name : '') }}" required>
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
                <span class="tip-block tip-block-under">
                    Ваше полное имя (не видно получателям писем)
                </span>
            </div>
            <div class="form-group has-feedback">
                <label for="treatment">{{ trans('user.form.treatment') }}</label>
                <input id="treatment" type="text" name="treatment" class="form-control"
                       placeholder="{{ trans('user.form.treatment-placeholder') }}"
                       value="{{ old('treatment', isset($user) ? $user->treatment : '') }}" required>
                <span class="glyphicon glyphicon-volume-up form-control-feedback"></span>
                <span class="tip-block tip-block-under">
                    Будет служить для Вашего представления получателю. Например: Здравствуйте, меня зовут <strong>Василий Иванов</strong>
                </span>
            </div>
            <div class="form-group has-feedback">
                <label for="cell_phone">{{ trans('user.form.cell_phone') }}</label>
                <input id="cell_phone" type="text" name="cell_phone" class="form-control"
                       placeholder="{{ trans('user.form.cell_phone-placeholder') }}"
                       value="{{ old('cell_phone', isset($user) ? $user->cell_phone : '') }}" required>
                <span class="glyphicon glyphicon-phone form-control-feedback"></span>
                <span class="tip-block tip-block-under">
                    Указывайте действующий номер. <strong>ВНИМАНИЕ! Указывайте только один номер</strong>
                </span>
            </div>
            <div class="form-group has-feedback">
                <label for="work_phone">{{ trans('user.form.work_phone') }}</label>
                <input id="work_phone" type="text" name="work_phone" class="form-control"
                       placeholder="{{ trans('user.form.work_phone-placeholder') }}"
                       value="{{ old('work_phone', isset($user) ? $user->work_phone : '') }}" required>
                <span class="glyphicon glyphicon-phone-alt form-control-feedback"></span>
                <span class="tip-block tip-block-under">
                    Указывайте свой рабочий (городской) номер телефона. Для реализации возможности звонка по ссылке прямо из письма, лучше не указывать добавочный номер
                </span>
            </div>
            <div class="form-group has-feedback">
                <label for="email">{{ trans('user.form.email') }}</label>
                <input id="email" type="email" name="email" class="form-control"
                       @if(!ur(\App\Models\User::ROLE_MODERATOR)) readonly @endif
                       placeholder="{{ trans('user.form.email-placeholder') }}"
                       value="{{ old('email', isset($user) ? $user->email : '') }}" required>
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                <span class="tip-block tip-block-under">
                    Для изменения данных этого поля <a href="{{ route('help-support-index') }}">создайте запрос</a>
                </span>
            </div>
            <div class="form-group has-feedback">
                <label for="branch">{{ trans('user.form.branch') }}</label>
                <select id="branch" name="branch" class="form-control" required
                        @if(!ur(\App\Models\User::ROLE_MODERATOR)) readonly @endif>
                    <option value="{{ null }}">{{ trans('user.form.branch-none') }}</option>
                    @foreach($branches as $branch)
                        <option value="{{ $branch->id }}" class="text-uppercase"
                        @if(isset($user)) {{ $user->branch == $branch->id ? 'selected' : '' }} @endif>
                            {{ $branch->city }}</option>
                    @endforeach
                </select>
                <span class="tip-block tip-block-under">
                    Для изменения данных этого поля <a href="{{ route('help-support-index') }}">создайте запрос</a>
                </span>
            </div>
            <div class="form-group has-feedback">
                <label for="roles">{{ trans('user.form.role') }}</label>
                <select id="roles" name="roles" class="form-control" required
                        @if(!ur(\App\Models\User::ROLE_MODERATOR)) readonly @endif>
                    <option value="{{ null }}">{{ trans('user.form.role-none') }}</option>
                    @foreach($roles as $role)
                        <option value="{{ $role }}" class="text-uppercase"
                        @if(isset($user)) {{ $user->roles == $role ? 'selected' : '' }} @endif>{{ $role }}</option>
                    @endforeach
                </select>
                <span class="tip-block tip-block-under">
                    Для изменения данных этого поля <a href="{{ route('help-support-index') }}">создайте запрос</a>
                </span>
            </div>
            @if(isset($user))
                <div>
                    <label for="tinymceMinimal">Личная подпись</label>
                    <textarea style="border: .1em solid #6c7692" name="signature"
                              id="tinymceMinimal">{!! old('signature', $user->signature) !!}</textarea>
                    <span class="tip-block tip-block-under">
                        Создайте свою собственную подпись к своим письмам
                    </span>
                </div>
                <p class="checkbox checkbox-circle checkbox-warning">
                    <input id="signature_status" type="checkbox" class="styled" name="signature_status"
                            {{ $user->signature_status == \App\Models\User::SIGNATURE_ACTIVE ? 'checked' : '' }}>
                    <label for="signature_status"><strong>Использовать свою подпись</strong></label>
                <div class="tip-block tip-block-under">
                    При включении этого флажка в Ваши письма будет вставляться подпись созданная Вами (поле выше).
                    При выключенном флажке будет добавляться стандартная подпись, игнорируя имеющуюся у Вас.
                </div>
                </p>
            @endif
            @if(ur(\App\Models\User::ROLE_MODERATOR))
                <div class="form-group has-feedback">
                    <label for="password">{{ trans('user.form.password') }}</label>
                    <input id="password" type="text" name="password" class="form-control"
                           placeholder="{{ trans('user.form.password-placeholder') }}"
                           value="{{ old('password') }}" @if(!isset($user)) required @endif>
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                @if(isset($user))
                    <p class="checkbox checkbox-primary">
                        <input id="report_info" type="checkbox" class="styled" name="report_info" checked>
                        <label for="report_info"><strong>{{ trans('user.form.report_info') }}</strong></label>
                    </p>
                    <p class="checkbox checkbox-warning">
                        <input id="report_password" type="checkbox" class="styled" name="report_password">
                        <label for="report_password"><strong>{{ trans('user.form.report_password') }}</strong></label>
                    </p>
                    <p class="checkbox checkbox-info">
                        <input id="notification_edit" type="checkbox" class="styled" name="notification_edit" checked>
                        <label for="notification_edit"><strong>Оповещение о редактировании данных</strong></label>
                    </p>
                @else
                    <p class="checkbox checkbox-success">
                        <input id="send_welcome" type="checkbox" class="styled" name="send_welcome" checked>
                        <label for="send_welcome"><strong>{{ trans('user.form.send_welcome') }}</strong></label>
                    </p>
                    <p class="checkbox checkbox-info">
                        <input id="notification_new_user" type="checkbox" class="styled" name="notification_new_user"
                               checked>
                        <label for="notification_new_user"><strong>Оповещение о новом пользователе</strong></label>
                    </p>
                @endif
            @endif
            <button type="submit" class="btn btn-success">
                {{ isset($user) ? 'Сохранить' : 'Создать' }}
                <i class="fa fa-check"></i>
            </button>
            <br>
            <br>
        </form>
    </div>
    </div>

    <div id="tipsBlocks">Показать подсказки</div>
@endsection

@section('js')
    <script src="/js/tinymce-minimal.js"></script>
@endsection
