<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Client;

class CreateClientsTable extends Migration
{
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('author_id');
            $table->string('name');
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->string('treatment')->nullable();
            $table->integer('company')->default(\App\Models\Company::COMPANY_UNKNOWN_ID);
            $table->string('position')->nullable();
            $table->string('status')->default(Client::STATUS_ACTIVE);
            $table->string('token')->nullable();
            $table->timestamp('last_send')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('clients');
    }
}
