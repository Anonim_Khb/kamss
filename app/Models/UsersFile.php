<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UsersFile extends Model
{
    protected $table = 'users_files';

    protected $fillable = [
        'user_id', 'origin_name', 'unique_name', 'size'
    ];

    const MAX_SIZE = 5120; //kilobytes
}
