<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Jenssegers\Date\Date;

class SendExcelReportAndRemoveFile extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $email;
    protected $text;
    protected $origin_name;
    protected $full_path;
    protected $author;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($email, $text, $origin_name, $full_path, $author)
    {
        $this->email = $email;
        $this->text = $text;
        $this->origin_name = $origin_name;
        $this->full_path = $full_path;
        $this->author = $author;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data = [
            'author_name' => $this->author->name,
            'author_email' => $this->author->email,
            'recipient_email' => $this->email,
            'letter_text' => $this->text,
            'file_origin_name' => $this->origin_name,
            'file_full_path' => $this->full_path
        ];

        if (ffcc($data['file_full_path'])) {
            \Mail::send(['html' => 'email.system.html.excel-report'],
                $data, function ($message) use ($data) {
                    $message->from($data['author_email'], $data['author_name']);
                    $message->to($data['recipient_email'])->subject('Отчет от ' . $data['author_name'] . ' (' . df(Date::now(), 'd.m.Y') . ')');
                    $message->attach($data['file_full_path'], ['as' => $data['file_origin_name']]);
                });

            \File::delete($this->full_path);
        } else {
            notify_site()->setUnsuccessSendExcelReportNotification($this->author->id, $data['recipient_email']);
        }
    }
}
