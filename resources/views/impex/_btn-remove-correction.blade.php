<span id="removeImportErrorCorrection" onclick="removeImportErrorCorrection('{{ $error_id }}')">
    Отказаться от исправления
    <i class="fa fa-remove fa-fw fa-lg"></i>
</span>
<script>var routeRemoveErrorCorrection = '{{ route('excel-import-remove-correction') }}'</script>