<?php

namespace App\Http\Controllers;

use App\Http\Classes\ImportErrorCorrections;
use App\Http\Controllers\Controller;
use App\Http\Requests\BidCreateOrUpdateRequest;
use App\Models\ImportError;
use Illuminate\Http\Request;
use App\Models\Bid;
use App\Models\BidCurrentStage;
use App\Models\Client;
use Notifications;
use Jenssegers\Date\Date;

class BidController extends Controller
{
    public function __construct()
    {
        $this->middleware('inactive')->except('index');

        view()->share('active_link', 'bid_index');
    }

    public function index(Bid $bid)
    {
        $bids = $bid->where('author_id', \Auth::user()->id)
            ->where('status', '!=', Bid::STATUS_DELETED);

        if (request('current_stage') == BidCurrentStage::STATUS_INACTIVE) {
            $bids = $bids->whereHas('current_stages', function ($query) {
                $query->where('status', BidCurrentStage::STATUS_INACTIVE);
            });
        } elseif (request('current_stage') == 'all') {
            $bids = $bids->with('current_stages');
        } else {
            $bids = $bids->whereHas('current_stages', function ($query) {
                $query->where('status', BidCurrentStage::STATUS_ACTIVE);
            });
        }

        if (request('next_contact') == 'today') {
            $bids = $bids->where('next_contact', df(Date::now(), 'Y-m-d'));
        } elseif (request('next_contact') == 'week') {
            $bids = $bids->whereBetween('next_contact',
                [df(Date::now(), 'Y-m-d'), df(Date::now()->addWeek(), 'Y-m-d')]);
        }

        if (request()->has('created')) {
            $bids = $bids->where('created_at', '>', Date::now()->subDays(request('created')));
        }

        $bids = $bids->with(['client.clientCompany'])->get();

        \Title::append('Запросы');

        return view('bids.index', ['bids' => $bids]);
    }

    public function createOrUpdateBidGet(
        Bid $bid,
        BidCurrentStage $currentStage,
        ImportError $error,
        ImportErrorCorrections $corrections,
        Client $client,
        $id = null,
        $iec = null
    ) {
        if ($iec) { //Import Error Client
            $thisError = $error->where('user_id', \Auth::id())->where('type', $error::TYPE_BIDS)->first();
            $corrections->showNotificationsErrors($thisError->errors);
            $thisBid = (object)unserialize($thisError->array);
            $thisBid->error_id = $thisError->id;
            $thisBidClient = $client->where('id', $thisBid->client_id)->first();
            $thisBid->client_name = $thisBidClient ? $thisBidClient->name : null;
            $thisBid->client_company = $thisBidClient ? $thisBidClient->clientCompany->name : null;
        } else {
            if ($id) {
                $thisBid = $bid->where('id', $id)->where('author_id', \Auth::user()->id)
                    ->whereHas('client', function ($query) {
                        $query->where('status', '!=', Client::STATUS_DELETED);
                    })->first();

                if (!$thisBid) {
                    abort(403);
                }
            } else {
                $thisBid = null;
            }
        }

        \Title::append('Запрос');

        $sources = $bid->arraySource();
        $categories = $bid->arrayCategory();
        $currencies = $bid->arrayCurrency();
        $currentStages = $currentStage->all();
        $sectors = $bid->arraySector();

        \Javascript::put([
            'routeSearchForClients' => route('bid-client-name-search'),
            'routeSearchForCompanies' => route('company-search-ajax'),
            'routeSearchCompanyByClientId' => route('company-search-by-client-id-ajax')
        ]);

        return view('bids.bid_create_or_update', [
            'bid' => $thisBid,
            'sources' => $sources,
            'categories' => $categories,
            'currencies' => $currencies,
            'currentStages' => $currentStages,
            'sectors' => $sectors,
        ]);
    }

    public function createOrUpdateBidPost(Request $request, Bid $bid, ImportError $error, $id = null)
    {
        $check = $bid->validateCreateOrUpdate($id, $request->all(), \Auth::id());

        if (!is_null($check)) {
            foreach ($check as $error) {
                Notifications::danger($error, 'page');
            }
            return redirect()->back()->withInput();
        }

        $bid = $bid->createOrUpdate($id, $request->all(), \Auth::id());

        !isset($request->error_id) ?: $error::removeError(\Auth::id(), $request->error_id);

        if (!is_numeric($bid)) {
            Notifications::error('Непредвиденная ошибка. Попробуйте еще раз.', 'top');
            return redirect()->back()->withInput();
        }

        Notifications::success('Данные успешно внесены', 'top');
        return redirect()->route('bid-index');
    }

    public function bidClientNameSearchAjax(Client $client)
    {
        if (request()->has('myQuery')) {

            $query = trim(e(request('myQuery')));

            $clients = $client->where('author_id', \Auth::user()->id)
                ->where('status', '!=', Client::STATUS_DELETED)
                ->where('name', 'LIKE', '%' . $query . '%')
                ->select('id', 'name as text')
                ->get();

            return \Response::json($clients);
        }
    }

    public function destroyBidAjax(Bid $bid)
    {
        $bid = $bid->where('author_id', \Auth::user()->id)->where('id', e(request('bidId')))->first();

        $bid->update(['status' => Bid::STATUS_DELETED]);

        return 'success';
    }

    public function changeStatusBidAjax(Bid $bid)
    {
        $bid = $bid->where('author_id', \Auth::user()->id)->where('id', e(request('bidId')))->first();

        $newStatus = $bid->status == Bid::STATUS_PRIVATE ? Bid::STATUS_PUBLIC : Bid::STATUS_PRIVATE;

        $bid->update(['status' => $newStatus]);

        return $newStatus;
    }
}
