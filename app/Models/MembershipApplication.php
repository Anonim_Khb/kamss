<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MembershipApplication extends Model
{
    protected $table = 'membership_applications';

    protected $fillable = [
        'name','work_phone', 'cell_phone','email','text','status'
    ];
    
    const STATUS_READ = 'read';
    const STATUS_UNREAD = 'unread';

    const TEXT_LIMIT = 100;
}
