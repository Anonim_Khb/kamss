<?php

use Illuminate\Database\Seeder;
use \App\Models\ClientsGroup;

class ClientsGroupsTableSeeder extends Seeder
{

    public function run()
    {
        $groups = [
            ['name' => 'Директора', 'description' => 'Директора и высшее руководство', 'icon' => 'fa fa-diamond fa-fw'],
            ['name' => 'Коммерция', 'description' => 'Коммерческие отделы, пожразделения по закупу', 'icon' => 'fa fa-suitcase fa-fw'],
            ['name' => 'Технические', 'description' => 'Технические специалисты, гл.механики, начальники участков', 'icon' => 'fa fa-cog fa-fw'],
            ['name' => 'Остальные', 'description' => 'Записи не вошедшие ни в одну группу', 'icon' => 'fa fa-th-large fa-fw'],
            ['name' => 'Времянка', 'description' => 'Для новых/временных записей', 'icon' => 'fa fa-hourglass-half fa-fw'],
        ];

        ClientsGroup::insert($groups);
    }
}
