$('input[name=clients_created]').on('change', function () {
    $('#cc_radio2').is(':checked') ? $('#ib_cc').slideDown() : $('#ib_cc').slideUp();
});
$('input[name=bids_created]').on('change', function () {
    $('#bc_radio2').is(':checked') ? $('#ib_bc').slideDown() : $('#ib_bc').slideUp();
});

$('.impex-export-block :input[type=radio]').on('change', function () {
    $('.ib-radio-filter').is(':checked') ? $('#ib_export_alert').slideDown() : $('#ib_export_alert').slideUp();
});
$('.impex-import-block :input[type=radio]').on('change', function () {
    $('.ibimp-radio-filter').is(':checked') ? $('#ib_import_alert').slideDown() : $('#ib_import_alert').slideUp();
});

/*Send email*/
$('#checkbox_send_email').on('change', function () {
    if ($('#checkbox_send_email').is(':checked')) {
        $('.ib-sub-btns').html('<i class="fa fa-envelope fa-fw"></i> Отправить');
        $('#sendEmailAdressAndText').slideDown();
        $('input[name=email_address]').prop('required', true);
    } else {
        $('.ib-sub-btns').html('<i class="fa fa-download fa-fw"></i> Скачать');
        $('#sendEmailAdressAndText').slideUp();
        $('input[name=email_address]').prop('required', false);
    }
});

/*Import errors page, remove corrections*/
function removeCorrections() {
    swal({
        title: "Отказ от исправлений",
        text: "Будут удалены все записи не прошедшие проверку, без возможности восстановления!",
        type: "warning",
        confirmButtonColor: errorColor,
        confirmButtonText: "Удалить",
        showCancelButton: true,
        cancelButtonColor: defaultColor,
        cancelButtonText: 'Отмена'
    }).then(function () {
        $.ajax({
            type: "POST",
            dataType: "text",
            url: routeForRemoveCorrections,
            data: {
                _token: $("meta[name='csrf-token']").attr('content')
            },
            cache: false,
            success: function (data) {
                if (data) {
                    setTimeout(function () {
                        window.location.replace('/');
                    }, 4500);
                    swal({
                        title: 'Исправления удалены',
                        text: 'Все исправления были удалены. Сейчас Вы будете перемещены на главную страницу',
                        type: "success",
                        timer: 4000
                    })
                }
            }
        });
    }, function (dismiss) {
        if (dismiss === 'cancel') {
            //
        }
    });
}
