<!--CLIENTS-->
<div class="row">
    <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-blue-active">
            <div class="inner">
                <h3><i class="fa fa-user-plus"></i></h3>
                <p>Добавить заказчика</p>
            </div>
            <div class="icon">
                <i class="fa fa-user-plus"></i>
            </div>
            <a href="{{ route('clients-create-or-update-get') }}" class="small-box-footer">
                Создать <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-aqua-active">
            <div class="inner">
                <h3>{{ $clients_all }}</h3>
                <p>{{ trans_choice('dashboard.clients-all', $clients_all) }}</p>
            </div>
            <div class="icon">
                <i class="fa fa-users"></i>
            </div>
            <a href="{{ route('clients-index') }}" class="small-box-footer">
                Посмотреть <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-green-active">
            <div class="inner">
                <h3>{{ $clients_active }}</h3>
                <p>{{ trans_choice('dashboard.clients-active', $clients_active) }}</p>
            </div>
            <div class="icon">
                <i class="fa fa-user"></i>
            </div>
            <a href="{{ route('clients-index') . '?filter=' . \App\Models\Client::STATUS_ACTIVE }}" class="small-box-footer">
                Посмотреть <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-yellow">
            <div class="inner">
                <h3>{{ $clients_inactive }}</h3>
                <p>{{ trans_choice('dashboard.clients-inactive', $clients_inactive) }}</p>
            </div>
            <div class="icon">
                <i class="fa fa-user-times"></i>
            </div>
            <a href="{{ route('clients-index') . '?filter=' . \App\Models\Client::STATUS_INACTIVE }}" class="small-box-footer">
                Посмотреть <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>
</div>

<!--BIDS-->
<div class="row">
    <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-blue">
            <div class="inner">
                <h3><i class="fa fa-cart-plus"></i></h3>
                <p>Новый запрос</p>
            </div>
            <div class="icon">
                <i class="fa fa-cart-plus"></i>
            </div>
            <a href="{{ route('bid-create-or-update-get') }}" class="small-box-footer">
                Создать <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-aqua">
            <div class="inner">
                <h3>{{ $bids_all }}</h3>
                <p>{{ trans_choice('dashboard.bids-all', $bids_all) }}</p>
            </div>
            <div class="icon">
                <i class="fa fa-shopping-cart"></i>
            </div>
            <a href="{{ route('bid-index') }}?current_stage=all" class="small-box-footer">
                Посмотреть <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-green">
            <div class="inner">
                <h3>{{ $bids_active }}</h3>
                <p>{{ trans_choice('dashboard.bids-active', $bids_active) }}</p>
            </div>
            <div class="icon">
                <i class="fa fa-cart-arrow-down"></i>
            </div>
            <a href="{{ route('bid-index') }}" class="small-box-footer">
                Посмотреть <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-red">
            <div class="inner">
                <h3>{{ $bids_today }}</h3>
                <p>{{ trans_choice('dashboard.bids-for-today', $bids_today) }}</p>
            </div>
            <div class="icon">
                <i class="fa fa-opencart"></i>
            </div>
            <a href="{{ route('bid-index') . '?next_contact=today' }}" class="small-box-footer">
                Посмотреть <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>
</div>

<!--NOTES-->
<div class="row">
    <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-blue-gradient">
            <div class="inner">
                <h3><i class="fa fa-plus-square-o"></i></h3>
                <p>Новая заметка</p>
            </div>
            <div class="icon">
                <i class="fa fa-plus-square-o"></i>
            </div>
            <a href="{{ route('note-create-update-get') }}" class="small-box-footer">
                Создать <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-aqua-gradient">
            <div class="inner">
                <h3>{{ $note_all }}</h3>
                <p>{{ trans_choice('dashboard.note-all', $note_all) }}</p>
            </div>
            <div class="icon">
                <i class="fa fa-square-o"></i>
            </div>
            <a href="{{ route('note-index') . '?status=all' }}" class="small-box-footer">
                Посмотреть <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-green-gradient">
            <div class="inner">
                <h3>{{ $note_active }}</h3>
                <p>{{ trans_choice('dashboard.note-active', $note_active) }}</p>
            </div>
            <div class="icon">
                <i class="fa fa-caret-square-o-down"></i>
            </div>
            <a href="{{ route('note-index') }}" class="small-box-footer">
                Посмотреть <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-yellow-gradient">
            <div class="inner">
                <h3>{{ $note_inactive }}</h3>
                <p>{{ trans_choice('dashboard.note-inactive', $note_inactive) }}</p>
            </div>
            <div class="icon">
                <i class="fa fa-caret-square-o-up"></i>
            </div>
            <a href="{{ route('note-index') . '?status=' . \App\Models\Note::STATUS_INACTIVE }}" class="small-box-footer">
                Посмотреть <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>
</div>

<!--EMAIL STATISTICS-->
<div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
        <a class="info-box">
            <span class="info-box-icon bg-yellow-active"><i class="fa fa-hourglass-1"></i></span>
            <div class="info-box-content">
                <span class="info-box-number">Отправлено сегодня</span>
                <?php $statistic_day ?: $statistic_day = 0;?>
                <span class="info-box-text"><span class="info-box-my-count">{{ $statistic_day }}</span>
                    {{ trans_choice('dashboard.send-email-statistics', $statistic_day) }}</span>
            </div>
        </a>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <a class="info-box">
            <span class="info-box-icon bg-fuchsia"><i class="fa fa-bar-chart-o"></i></span>
            <div class="info-box-content">
                <span class="info-box-number">Отправлено за месяц</span>
                <?php $statistic_month ?: $statistic_month = 0;?>
                <span class="info-box-text"><span class="info-box-my-count">{{ $statistic_month }}</span>
                    {{ trans_choice('dashboard.send-email-statistics', $statistic_month) }}</span>
            </div>
        </a>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <a class="info-box">
            <span class="info-box-icon bg-light-blue"><i class="fa fa-line-chart"></i></span>
            <div class="info-box-content">
                <span class="info-box-number">Отправлено за год</span>
                <?php $statistic_year ?: $statistic_year = 0;?>
                <span class="info-box-text"><span class="info-box-my-count">{{ $statistic_year  }}</span>
                    {{ trans_choice('dashboard.send-email-statistics', $statistic_year) }}</span>
            </div>
        </a>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <a class="info-box">
            <span class="info-box-icon bg-lime-active"><i class="fa fa-hourglass"></i></span>
            <div class="info-box-content">
                <span class="info-box-number">Отправлено всего</span>
                <?php $statistic_all ?: $statistic_all = 0;?>
                <span class="info-box-text"><span class="info-box-my-count">{{ $statistic_all }}</span>
                    {{ trans_choice('dashboard.send-email-statistics', $statistic_all) }}</span>
            </div>
        </a>
    </div>
</div>

<!--TIME AGO-->
<div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
        <a class="info-box">
            <span class="info-box-icon bg-maroon-active"><i class="fa fa-clock-o"></i></span>
            <div class="info-box-content">
                <span class="info-box-number">Добавление заказчика</span>
                <span class="info-box-text">{{ is_null($client_last_create) ? 'Заказчиков нет' : ta($client_last_create) }}</span>
            </div>
        </a>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <a class="info-box">
            <span class="info-box-icon bg-olive-active"><i class="fa fa-clock-o"></i></span>
            <div class="info-box-content">
                <span class="info-box-number">Создание запроса</span>
                <span class="info-box-text">{{ is_null($bid_last_create) ? 'Запросов нет' : ta($bid_last_create) }}</span>
            </div>
        </a>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <a class="info-box">
            <span class="info-box-icon bg-navy"><i class="fa fa-clock-o"></i></span>
            <div class="info-box-content">
                <span class="info-box-number">Создание заметки</span>
                <span class="info-box-text">{{ is_null($note_last_create) ? 'Заметок нет' : ta($note_last_create) }}</span>
            </div>
        </a>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <a class="info-box">
            <span class="info-box-icon bg-teal-active"><i class="fa fa-clock-o"></i></span>
            <div class="info-box-content">
                <span class="info-box-number">Отправка письма</span>
                <span class="info-box-text">{{ is_null($client_last_send) ? 'Отправок не было' : ta($client_last_send) }}</span>
            </div>
        </a>
    </div>
</div>
