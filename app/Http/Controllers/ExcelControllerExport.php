<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\DTO\bidsExcelDTO;
use App\Http\DTO\clientsExcelDTO;
use App\Http\Requests\ExportExcelForUsersPost;
use App\Jobs\SendExcelReportAndRemoveFile;
use App\Models\Bid;
use App\Models\BidCurrentStage;
use App\Models\Client;
use App\Models\Company;
use App\Http\DTO\companiesExcelDTO;
use Notifications;
use Jenssegers\Date\Date;
use PHPExcel;
use PHPExcel_IOFactory;
use PHPExcel_Worksheet_PageSetup;
use File;

class ExcelControllerExport extends Controller
{
    private $headingColor = 'B4B4B4';
    private $greenColor = 'C4EFC0';
    private $redColor = 'F0C8C5';
    private $warningColor = 'F0D8C5';
    private $infoColor = '00C0EF';
    private $kamssColor = 'C5D2D8';

    private $companiesDTO;
    private $clientsDTO;
    private $bidsDTO;

    const COLUMNS_START = 4; // columns name
    const COLUMN_START_DATA = 4 + 1; // under columns name row

    const PAGE_TECHNICAL_NAME = 'Temp';
    const PAGE_TECHNICAL_NUMBER = 3;

    public function __construct()
    {
        $this->middleware('inactive');

        view()->share('active_link', 'impex_index');

        $this->companiesDTO = new companiesExcelDTO();
        $this->clientsDTO = new clientsExcelDTO();
        $this->bidsDTO = new bidsExcelDTO();
    }

    public function index()
    {
        \Title::append('Импорт/Экспорт');

        return view('impex.index');
    }

    public function exportClientsAndBids(ExportExcelForUsersPost $request)
    {
        $objPHPExcel = new PHPExcel();
        /*Create companies sheet*/
        $objPHPExcel->setActiveSheetIndex($this->companiesDTO->getPage('number'));
        $this->createCompaniesSheet($objPHPExcel);
        /*Create clients sheet*/
        $objPHPExcel->createSheet();
        $objPHPExcel->setActiveSheetIndex($this->clientsDTO->getPage('number'));
        $this->createClientsSheet($objPHPExcel);
        /*Create bids sheet*/
        $objPHPExcel->createSheet();
        $objPHPExcel->setActiveSheetIndex($this->bidsDTO->getPage('number'));
        $this->createBidsSheet($objPHPExcel);
        /*Create technical on bids sheet*/
        $objPHPExcel->setActiveSheetIndex($this->bidsDTO->getPage('number'));
        $this->createTechnicalSheet($objPHPExcel);
        /*Technical*/
        $this->tableOutput($objPHPExcel);

        return redirect()->back();
    }

    protected function createClientsSheet($objPHPExcel)
    {
        $client = Client::where('author_id', auth()->user()->id)
            ->where('status', '!=', Client::STATUS_DELETED);

        if (request('clients_status') == 'active') $client = $client->where('status', 'active');

        if (request('clients_created') == 'limit') {
            if (request()->has('clients_day_limit')) {
                $client = $client->where(request('clients_type') . '_at', '>', Date::now()->subDays(request('clients_day_limit')));
            }
        }

        $clients = $client->with('clientCompany')->get();

        $activeSheet = $objPHPExcel->getActiveSheet();
        $activeSheet->setTitle($this->clientsDTO->getPage('name'));
        /*Font sheet and title*/
        $this->sheetSettings($activeSheet);
        /*Header and Footer text (visible for print)*/
        $this->headerSheetInfo($activeSheet);
        /*Columns width*/
        $activeSheet->getColumnDimension($this->clientsDTO->getId())->setWidth($this->clientsDTO->getId('width'));
        $activeSheet->getColumnDimension($this->clientsDTO->getName())->setWidth($this->clientsDTO->getName('width'));
        $activeSheet->getColumnDimension($this->clientsDTO->getCompany())->setWidth($this->clientsDTO->getCompany('width'));
        $activeSheet->getColumnDimension($this->clientsDTO->getEmail())->setWidth($this->clientsDTO->getEmail('width'));
        $activeSheet->getColumnDimension($this->clientsDTO->getTreatment())->setWidth($this->clientsDTO->getTreatment('width'));
        $activeSheet->getColumnDimension($this->clientsDTO->getPhone())->setWidth($this->clientsDTO->getPhone('width'));
        $activeSheet->getColumnDimension($this->clientsDTO->getPosition())->setWidth($this->clientsDTO->getPosition('width'));
        /*Columns name*/
        $activeSheet->getRowDimension(self::COLUMNS_START)->setRowHeight(26);
        $activeSheet->setCellValue($this->clientsDTO->getId() . self::COLUMNS_START, $this->clientsDTO->getId('name'));
        $activeSheet->setCellValue($this->clientsDTO->getName() . self::COLUMNS_START, $this->clientsDTO->getName('name'));
        $activeSheet->setCellValue($this->clientsDTO->getCompany() . self::COLUMNS_START, $this->clientsDTO->getCompany('name'));
        $activeSheet->setCellValue($this->clientsDTO->getEmail() . self::COLUMNS_START, $this->clientsDTO->getEmail('name'));
        $activeSheet->setCellValue($this->clientsDTO->getTreatment() . self::COLUMNS_START, $this->clientsDTO->getTreatment('name'));
        $activeSheet->setCellValue($this->clientsDTO->getPhone() . self::COLUMNS_START, $this->clientsDTO->getPhone('name'));
        $activeSheet->setCellValue($this->clientsDTO->getPosition() . self::COLUMNS_START, $this->clientsDTO->getPosition('name'));
        /*Foreach array*/
        $i = 0;
        foreach ($clients as $client) {
            $nextRow = self::COLUMN_START_DATA + $i;
            $activeSheet->setCellValue($this->clientsDTO->getId() . $nextRow, $client->id);
            $activeSheet->setCellValue($this->clientsDTO->getName() . $nextRow, $client->name);
            $activeSheet->setCellValue($this->clientsDTO->getCompany() . $nextRow, $client->clientCompany->name);
            $activeSheet->setCellValue($this->clientsDTO->getEmail() . $nextRow, $client->email);
            $activeSheet->setCellValue($this->clientsDTO->getTreatment() . $nextRow, $client->treatment);
            $activeSheet->setCellValue($this->clientsDTO->getPhone() . $nextRow, $client->phone);
            $activeSheet->setCellValue($this->clientsDTO->getPosition() . $nextRow, $client->position);
            if ($client->status == 'inactive') {
                $activeSheet->getStyle('A' . $nextRow . ':' . $activeSheet->getHighestColumn() . $nextRow)
                    ->applyFromArray($this->styleRowBackground($this->redColor));
            }
            $i++;
        }
        /*Styles*/
        $activeSheet->getStyle('A' . self::COLUMNS_START . ':' . $activeSheet->getHighestColumn() . $activeSheet->getHighestRow())
            ->applyFromArray($this->styleBorders());
        $activeSheet->getStyle('A1:D1')
            ->applyFromArray($this->styleSheetHeader());
        $activeSheet->getStyle('A2:D3')
            ->applyFromArray($this->styleWeightSizeText(true, 9));
        $activeSheet->getStyle('A' . self::COLUMNS_START . ':A' . $activeSheet->getHighestRow())
            ->applyFromArray($this->styleBackground($this->kamssColor));
        $activeSheet->getStyle('A' . self::COLUMNS_START . ':' . $activeSheet->getHighestColumn() . self::COLUMNS_START)
            ->applyFromArray($this->styleBackground($this->kamssColor));
        $activeSheet->getStyle('A' . self::COLUMNS_START . ':' . $activeSheet->getHighestColumn() . $activeSheet->getHighestRow())
            ->applyFromArray($this->styleAlignmentSheetContent());
        $activeSheet->getStyle('A' . self::COLUMNS_START . ':' . $activeSheet->getHighestColumn() . self::COLUMNS_START)
            ->applyFromArray($this->styleSheetContentHeader(true));
        $activeSheet->getStyle('A1' . ':' . $activeSheet->getHighestColumn() . self::COLUMNS_START)
            ->getAlignment()->setWrapText(true);
        /*Named range*/
        $this->namedRange($objPHPExcel, 'clients', $this->clientsDTO->getPage('number'), $this->clientsDTO->getName() . self::COLUMN_START_DATA, $activeSheet->getHighestRow());
        /*Protection*/
        $this->cellProtected($activeSheet, 'B' . self::COLUMN_START_DATA, $activeSheet->getHighestColumn() . ($activeSheet->getHighestRow() + 500));
        /*Validation*/
        for ($i = self::COLUMN_START_DATA; $i < $activeSheet->getHighestRow(); $i++) {
            $this->dropDownValidation($this->clientsDTO->getPage('number'), $objPHPExcel, $this->clientsDTO->getCompany() . $i, '=companies');
        }
    }

    protected function createBidsSheet($objPHPExcel)
    {
        $bids = Bid::where('author_id', auth()->user()->id)
            ->where('status', '!=', Bid::STATUS_DELETED);

        if (request('bids_status') == 'active') {
            $bids = $bids->whereHas('current_stages', function ($query) {
                $query->where('status', BidCurrentStage::STATUS_ACTIVE);
            });
        } else {
            $bids = $bids->with('current_stages');
        }

        if (request('bids_created') == 'limit') {
            if (request()->has('bids_day_limit')) {
                $bids = $bids->where(request('bids_type') . '_at', '>', Date::now()->subDays(request('bids_day_limit')));
            }
        }

        $bids = $bids->with(['client.clientCompany'])->get();

        $activeSheet = $objPHPExcel->getActiveSheet();
        $activeSheet->setTitle($this->bidsDTO->getPage('name'));
        /*Font sheet and title*/
        $this->sheetSettings($activeSheet);
        /*Header and Footer text (visible for print)*/
        $this->headerSheetInfo($activeSheet);
        /*Columns width*/
        $activeSheet->getColumnDimension($this->bidsDTO->getId())->setWidth($this->bidsDTO->getId('width'));
        $activeSheet->getColumnDimension($this->bidsDTO->getContact())->setWidth($this->bidsDTO->getContact('width'));
        $activeSheet->getColumnDimension($this->bidsDTO->getCompany())->setWidth($this->bidsDTO->getCompany('width'));
        $activeSheet->getColumnDimension($this->bidsDTO->getSource())->setWidth($this->bidsDTO->getSource('width'));
        $activeSheet->getColumnDimension($this->bidsDTO->getSector())->setWidth($this->bidsDTO->getSector('width'));
        $activeSheet->getColumnDimension($this->bidsDTO->getRegion())->setWidth($this->bidsDTO->getRegion('width'));
        $activeSheet->getColumnDimension($this->bidsDTO->getObjectDeal())->setWidth($this->bidsDTO->getObjectDeal('width'));
        $activeSheet->getColumnDimension($this->bidsDTO->getPrice())->setWidth($this->bidsDTO->getPrice('width'));
        $activeSheet->getColumnDimension($this->bidsDTO->getCurrency())->setWidth($this->bidsDTO->getCurrency('width'));
        $activeSheet->getColumnDimension($this->bidsDTO->getPaymentTerm())->setWidth($this->bidsDTO->getPaymentTerm('width'));
        $activeSheet->getColumnDimension($this->bidsDTO->getCurrentStages())->setWidth($this->bidsDTO->getCurrentStages('width'));
        $activeSheet->getColumnDimension($this->bidsDTO->getCategory())->setWidth($this->bidsDTO->getCategory('width'));
        $activeSheet->getColumnDimension($this->bidsDTO->getEngine())->setWidth($this->bidsDTO->getEngine('width'));
        $activeSheet->getColumnDimension($this->bidsDTO->getFirstContact())->setWidth($this->bidsDTO->getFirstContact('width'));
        $activeSheet->getColumnDimension($this->bidsDTO->getLastContact())->setWidth($this->bidsDTO->getLastContact('width'));
        $activeSheet->getColumnDimension($this->bidsDTO->getNextContact())->setWidth($this->bidsDTO->getNextContact('width'));
        $activeSheet->getColumnDimension($this->bidsDTO->getNextAction())->setWidth($this->bidsDTO->getNextAction('width'));
        $activeSheet->getColumnDimension($this->bidsDTO->getComment())->setWidth($this->bidsDTO->getComment('width'));
        /*Columns name*/
        $activeSheet->getRowDimension(self::COLUMNS_START)->setRowHeight(26);
        $activeSheet->setCellValue($this->bidsDTO->getId() . self::COLUMNS_START, $this->bidsDTO->getId('name'));
        $activeSheet->setCellValue($this->bidsDTO->getContact() . self::COLUMNS_START, $this->bidsDTO->getContact('name'));
        $activeSheet->setCellValue($this->bidsDTO->getCompany() . self::COLUMNS_START, $this->bidsDTO->getCompany('name'));
        $activeSheet->setCellValue($this->bidsDTO->getSource() . self::COLUMNS_START, $this->bidsDTO->getSource('name'));
        $activeSheet->setCellValue($this->bidsDTO->getSector() . self::COLUMNS_START, $this->bidsDTO->getSector('name'));
        $activeSheet->setCellValue($this->bidsDTO->getRegion() . self::COLUMNS_START, $this->bidsDTO->getRegion('name'));
        $activeSheet->setCellValue($this->bidsDTO->getObjectDeal() . self::COLUMNS_START, $this->bidsDTO->getObjectDeal('name'));
        $activeSheet->setCellValue($this->bidsDTO->getPrice() . self::COLUMNS_START, $this->bidsDTO->getPrice('name'));
        $activeSheet->setCellValue($this->bidsDTO->getCurrency() . self::COLUMNS_START, $this->bidsDTO->getCurrency('name'));
        $activeSheet->setCellValue($this->bidsDTO->getPaymentTerm() . self::COLUMNS_START, $this->bidsDTO->getPaymentTerm('name'));
        $activeSheet->setCellValue($this->bidsDTO->getCurrentStages() . self::COLUMNS_START, $this->bidsDTO->getCurrentStages('name'));
        $activeSheet->setCellValue($this->bidsDTO->getCategory() . self::COLUMNS_START, $this->bidsDTO->getCategory('name'));
        $activeSheet->setCellValue($this->bidsDTO->getEngine() . self::COLUMNS_START, $this->bidsDTO->getEngine('name'));
        $activeSheet->setCellValue($this->bidsDTO->getFirstContact() . self::COLUMNS_START, $this->bidsDTO->getFirstContact('name'));
        $activeSheet->setCellValue($this->bidsDTO->getLastContact() . self::COLUMNS_START, $this->bidsDTO->getLastContact('name'));
        $activeSheet->setCellValue($this->bidsDTO->getNextContact() . self::COLUMNS_START, $this->bidsDTO->getNextContact('name'));
        $activeSheet->setCellValue($this->bidsDTO->getNextAction() . self::COLUMNS_START, $this->bidsDTO->getNextAction('name'));
        $activeSheet->setCellValue($this->bidsDTO->getComment() . self::COLUMNS_START, $this->bidsDTO->getComment('name'));
        /*Foreach array*/
        $i = 0;
        foreach ($bids as $bid) {
            $nextRow = self::COLUMN_START_DATA + $i;
            $activeSheet->setCellValue($this->bidsDTO->getId() . $nextRow, $bid->id);
            $activeSheet->setCellValue($this->bidsDTO->getContact() . $nextRow, $bid->client->name);
            $activeSheet->setCellValue($this->bidsDTO->getCompany() . $nextRow, $bid->client->clientCompany->name);
            $activeSheet->setCellValue($this->bidsDTO->getSource() . $nextRow, $bid->source);
            $activeSheet->setCellValue($this->bidsDTO->getSector() . $nextRow, $bid->sector);
            $activeSheet->setCellValue($this->bidsDTO->getRegion() . $nextRow, $bid->region);
            $activeSheet->setCellValue($this->bidsDTO->getObjectDeal() . $nextRow, $bid->object_deal);
            $activeSheet->setCellValue($this->bidsDTO->getPrice() . $nextRow, $bid->price);
            $activeSheet->setCellValue($this->bidsDTO->getCurrency() . $nextRow, $bid->currency);
            $activeSheet->setCellValue($this->bidsDTO->getPaymentTerm() . $nextRow, $bid->payment_term);
            $activeSheet->setCellValue($this->bidsDTO->getCurrentStages() . $nextRow, $bid->current_stages->name);
            $activeSheet->setCellValue($this->bidsDTO->getCategory() . $nextRow, $bid->category);
            $activeSheet->setCellValue($this->bidsDTO->getEngine() . $nextRow, $bid->engine);
            $activeSheet->setCellValue($this->bidsDTO->getFirstContact() . $nextRow, $bid->first_contact);
            $activeSheet->setCellValue($this->bidsDTO->getLastContact() . $nextRow, $bid->last_contact);
            $activeSheet->setCellValue($this->bidsDTO->getNextContact() . $nextRow, $bid->next_contact);
            $activeSheet->setCellValue($this->bidsDTO->getNextAction() . $nextRow, $bid->next_action);
            $activeSheet->setCellValue($this->bidsDTO->getComment() . $nextRow, $bid->comment);
            if ($bid->current_stages->status == BidCurrentStage::STATUS_INACTIVE) {
                $activeSheet->getStyle('A' . $nextRow . ':' . $activeSheet->getHighestColumn() . $nextRow)
                    ->applyFromArray($this->styleRowBackground($this->warningColor));
            }
            $i++;
        }
        /*Styles*/
        $activeSheet->getStyle('A' . self::COLUMNS_START . ':' . $activeSheet->getHighestColumn() . $activeSheet->getHighestRow())
            ->applyFromArray($this->styleBorders());
        $activeSheet->getStyle('A1:D1')
            ->applyFromArray($this->styleSheetHeader());
        $activeSheet->getStyle('A2:D3')
            ->applyFromArray($this->styleWeightSizeText(true, 9));
        $activeSheet->getStyle('A' . self::COLUMNS_START . ':A' . $activeSheet->getHighestRow())
            ->applyFromArray($this->styleBackground($this->kamssColor));
        $activeSheet->getStyle('A' . self::COLUMNS_START . ':' . $activeSheet->getHighestColumn() . self::COLUMNS_START)
            ->applyFromArray($this->styleBackground($this->kamssColor));
        $activeSheet->getStyle('A' . self::COLUMNS_START . ':' . $activeSheet->getHighestColumn() . $activeSheet->getHighestRow())
            ->applyFromArray($this->styleAlignmentSheetContent());
        $activeSheet->getStyle('A' . self::COLUMNS_START . ':' . $activeSheet->getHighestColumn() . self::COLUMNS_START)
            ->applyFromArray($this->styleSheetContentHeader(true));
        $activeSheet->getStyle('A1' . ':' . $activeSheet->getHighestColumn() . self::COLUMNS_START)
            ->getAlignment()->setWrapText(true);
        /*Protection*/
        $this->cellProtected($activeSheet, 'D' . self::COLUMN_START_DATA, $activeSheet->getHighestColumn() . $activeSheet->getHighestRow());
        $this->cellProtected($activeSheet, 'B' . ($activeSheet->getHighestRow() + 1), $activeSheet->getHighestColumn() . ($activeSheet->getHighestRow() + 500));
        /*Validation*/
        for ($i = self::COLUMN_START_DATA; $i < $activeSheet->getHighestRow(); $i++) {
            $this->dropDownValidation($this->bidsDTO->getPage('number'), $objPHPExcel, $this->bidsDTO->getCompany() . $i, '=companies');
            $this->dropDownValidation($this->bidsDTO->getPage('number'), $objPHPExcel, $this->bidsDTO->getSource() . $i, '=sources');
            $this->dropDownValidation($this->bidsDTO->getPage('number'), $objPHPExcel, $this->bidsDTO->getSector() . $i, '=sectors');
            $this->dropDownValidation($this->bidsDTO->getPage('number'), $objPHPExcel, $this->bidsDTO->getCurrency() . $i, '=currencies');
            $this->dropDownValidation($this->bidsDTO->getPage('number'), $objPHPExcel, $this->bidsDTO->getCategory() . $i, '=categories');
            $this->dropDownValidation($this->bidsDTO->getPage('number'), $objPHPExcel, $this->bidsDTO->getCurrentStages() . $i, '=currentStages');
            $this->dropDownValidation($this->bidsDTO->getPage('number'), $objPHPExcel, $this->bidsDTO->getContact() . $i, '=clients');
        }
    }

    protected function createTechnicalSheet($objPHPExcel)
    {
        $bid = new Bid();
        $sources = $bid->arraySource();
        $sectors = $bid->arraySector();
        $currencies = $bid->arrayCurrency();
        $categories = $bid->arrayCategory();
        $currentStages = BidCurrentStage::pluck('name')->toArray();

        $activeSheet = $objPHPExcel->getActiveSheet();
        /*Set values*/
        $this->setListValues($activeSheet, $sources, 'AA');
        $this->setListValues($activeSheet, $sectors, 'AB');
        $this->setListValues($activeSheet, $currencies, 'AC');
        $this->setListValues($activeSheet, $categories, 'AD');
        $this->setListValues($activeSheet, $currentStages, 'AE');
        /*Named ranges*/
        $this->namedRange($objPHPExcel, 'sources', $this->bidsDTO->getPage('number'), 'AA1', count($sources));
        $this->namedRange($objPHPExcel, 'sectors', $this->bidsDTO->getPage('number'), 'AB1', count($sectors));
        $this->namedRange($objPHPExcel, 'currencies', $this->bidsDTO->getPage('number'), 'AC1', count($currencies));
        $this->namedRange($objPHPExcel, 'categories', $this->bidsDTO->getPage('number'), 'AD1', count($categories));
        $this->namedRange($objPHPExcel, 'currentStages', $this->bidsDTO->getPage('number'), 'AE1', count($currentStages));
        /*Protected*/
        $activeSheet->getProtection()->setSheet(true);
    }

    protected function namedRange($objPHPExcel, $name, $sheet, $column, $count)
    {
        $objPHPExcel->addNamedRange(
            new \PHPExcel_NamedRange(
                $name,
                $objPHPExcel->setActiveSheetIndex($sheet),
                $column . ':' . $column . $count
            )
        );
    }

    protected function dropDownValidation($sheet, $objPHPExcel, $cell, $formula)
    {
        $objPHPExcel->setActiveSheetIndex($sheet);
        $objValidation = $objPHPExcel->getSheet($sheet)->getCell($cell)->getDataValidation();
        $objValidation->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
        $objValidation->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_STOP);
        $objValidation->setAllowBlank(false);
        $objValidation->setShowInputMessage(true);
        $objValidation->setShowErrorMessage(true);
        $objValidation->setShowDropDown(true);
        $objValidation->setErrorTitle('Ошибка ввода');
        $objValidation->setError('Выбирайте значения из списка');
        $objValidation->setFormula1($formula);
    }

    protected function setListValues($activeSheet, $array, $column)
    {
        $i = 1;
        foreach ($array as $item) {
            $activeSheet->setCellValue($column . $i, $item);
            $i++;
        }
    }

    protected function sheetSettings($activeSheet)
    {
        $activeSheet->getDefaultStyle()->getFont()->setName('Arial');
        $activeSheet->getDefaultStyle()->getFont()->setSize(11);
        $activeSheet->getPageSetup()
            ->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
        $activeSheet->getPageSetup()
            ->SetPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
        $activeSheet->getPageMargins()->setTop(1);
        $activeSheet->getPageMargins()->setRight(.75);
        $activeSheet->getPageMargins()->setLeft(.75);
        $activeSheet->getPageMargins()->setBottom(1);
    }

    protected function headerSheetInfo($activeSheet)
    {
        /*Header and Footer text (visible for print)*/
        $activeSheet->getHeaderFooter()
            ->setOddHeader('&C КАМСС-сервис');
        $activeSheet->getHeaderFooter()
            ->setOddFooter('&L&B' . $activeSheet->getTitle() . '&RСтраница &P из &N');
        /*Header of sheet*/
        $activeSheet->mergeCells('A1:D1');
        $activeSheet->getRowDimension('1')->setRowHeight(44);
        $activeSheet->setCellValue('A1', 'КАМСС-сервис');
        /*Under header lines*/
        $activeSheet->mergeCells('A2:D2');
        $activeSheet->setCellValue('A2', 'Автор: ' . auth()->user()->name);
        $activeSheet->mergeCells('A3:D3');
        $activeSheet->setCellValue('A3', 'Дата составления: ' . df(Date::now(), 'd F Y'));
    }

    protected function setCellValue($sheet, $cell, $value)
    {
        $sheet->cell($cell, function ($cell) use ($value) {
            $cell->setValue($value);
        });
    }

    protected function tableOutput($objPHPExcel)
    {
        $name = 'Отчет на ' . df(Date::now(), 'd F Y') . '.xlsx';
        if (request()->has('send_email')) {
            $v = \Validator::make(request()->all(), [
                'email_address' => 'required|email'
            ]);
            if ($v->fails())
            {
                foreach ($v->errors()->all() as $error) {
                    Notifications::danger($error, 'page');
                }
                return redirect()->back()->withInput();
            }

            $random = str_random(20);
            $full_path = config('site.files_excel_report_path') . '/' . $random . '.xlsx';
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

            ffcc(config('site.files_excel_report_path'), true);
            $objWriter->save($full_path);

            $job = (new SendExcelReportAndRemoveFile(request('email_address'), e(request('email_text')), $name, $full_path, \Auth::user()));
            dispatch($job);

            Notifications::success('Отчет успешно отправлен', 'top');
            return redirect()->route('impex-index');
        } else {
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename=' . $name);
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            $objWriter->save('php://output');
        }

        exit();
    }

    protected function styleRowBackground($color)
    {
        return [
            'fill' => [
                'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                'color' => [
                    'rgb' => $color
                ]
            ]
        ];
    }

    protected function cellProtected($activeSheet, $from, $to)
    {
        $activeSheet->getProtection()->setSheet(true);
        $activeSheet->getStyle($from . ':' . $to)
            ->getProtection()
            ->setLocked(\PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
    }

    protected function styleBorders()
    {
        return [
            'borders' => [
                'outline' => [
                    'style' => \PHPExcel_Style_Border::BORDER_THICK
                ],
                'allborders' => [
                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                ]
            ]
        ];
    }

    protected function styleSheetHeader()
    {
        return [
            'borders' => [
                'outline' => [
                    'style' => \PHPExcel_Style_Border::BORDER_THICK
                ]
            ],
            'font' => [
                'font' => 'Times New Roman',
                'bold' => true,
                'size' => 42
            ],
            'alignment' => [
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ],
            'fill' => [
                'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                'color' => [
                    'rgb' => 'E9DB58'
                ]
            ]
        ];
    }

    protected function styleWeightSizeText($bold, $size)
    {
        return [
            'font' => [
                'bold' => $bold,
                'size' => $size
            ]
        ];
    }

    protected function styleBackground($color)
    {
        return [
            'fill' => [
                'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                'color' => [
                    'rgb' => $color
                ]
            ]
        ];
    }

    protected function styleAlignmentSheetContent()
    {
        return [
            'alignment' => [
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ]
        ];
    }

    protected function styleSheetContentHeader($bold)
    {
        return [
            'alignment' => [
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ],
            'font' => [
                'bold' => $bold
            ]
        ];
    }

    protected function createCompaniesSheet($objPHPExcel)
    {
        $companies = Company::get();
        $activeSheet = $objPHPExcel->getActiveSheet();
        $activeSheet->setTitle($this->companiesDTO->getPage('name'));
        /*Font sheet and title*/
        $this->sheetSettings($activeSheet);
        /*Header and Footer text (visible for print)*/
        $this->headerSheetInfo($activeSheet);
        /*Columns width*/
        $activeSheet->getColumnDimension($this->companiesDTO->getId())->setWidth($this->companiesDTO->getId('width'));
        $activeSheet->getColumnDimension($this->companiesDTO->getCompany())->setWidth($this->companiesDTO->getCompany('width'));
        $activeSheet->getColumnDimension($this->companiesDTO->getType())->setWidth($this->companiesDTO->getType('width'));
        $activeSheet->getColumnDimension($this->companiesDTO->getInn())->setWidth($this->companiesDTO->getInn('width'));
        /*Columns name*/
        $activeSheet->getRowDimension(self::COLUMNS_START)->setRowHeight(26);
        $activeSheet->setCellValue($this->companiesDTO->getId() . self::COLUMNS_START, $this->companiesDTO->getId('name'));
        $activeSheet->setCellValue($this->companiesDTO->getCompany() . self::COLUMNS_START, $this->companiesDTO->getCompany('name'));
        $activeSheet->setCellValue($this->companiesDTO->getType() . self::COLUMNS_START, $this->companiesDTO->getType('name'));
        $activeSheet->setCellValue($this->companiesDTO->getInn() . self::COLUMNS_START, $this->companiesDTO->getInn('name'));
        /*Foreach array*/
        $i = 0;
        foreach ($companies as $company) {
            $nextRow = self::COLUMN_START_DATA + $i;
            $activeSheet->setCellValue($this->companiesDTO->getId() . $nextRow, $company->id);
            $activeSheet->setCellValue($this->companiesDTO->getCompany() . $nextRow, $company->name);
            $activeSheet->setCellValue($this->companiesDTO->getType() . $nextRow, $company->type);
            $activeSheet->setCellValue($this->companiesDTO->getInn() . $nextRow, $company->inn);
            $i++;
        }
        /*Styles*/
        $activeSheet->getStyle('A' . self::COLUMNS_START . ':' . $activeSheet->getHighestColumn() . $activeSheet->getHighestRow())
            ->applyFromArray($this->styleBorders());
        $activeSheet->getStyle('A1:D1')
            ->applyFromArray($this->styleSheetHeader());
        $activeSheet->getStyle('A2:D3')
            ->applyFromArray($this->styleWeightSizeText(true, 9));
        $activeSheet->getStyle('A' . self::COLUMNS_START . ':A' . $activeSheet->getHighestRow())
            ->applyFromArray($this->styleBackground($this->kamssColor));
        $activeSheet->getStyle('A' . self::COLUMNS_START . ':' . $activeSheet->getHighestColumn() . self::COLUMNS_START)
            ->applyFromArray($this->styleBackground($this->kamssColor));
        $activeSheet->getStyle('A' . self::COLUMNS_START . ':' . $activeSheet->getHighestColumn() . $activeSheet->getHighestRow())
            ->applyFromArray($this->styleAlignmentSheetContent());
        $activeSheet->getStyle('A' . self::COLUMNS_START . ':' . $activeSheet->getHighestColumn() . self::COLUMNS_START)
            ->applyFromArray($this->styleSheetContentHeader(true));
        $activeSheet->getStyle('A1' . ':' . $activeSheet->getHighestColumn() . self::COLUMNS_START)
            ->getAlignment()->setWrapText(true);
        /*Named range*/
        $this->namedRange($objPHPExcel, 'companies', $this->companiesDTO->getPage('number'), 'B' . self::COLUMN_START_DATA, $activeSheet->getHighestRow());
        /*Protection*/
        $this->cellProtected($activeSheet, 'B' . ($activeSheet->getHighestRow() + 1), 'D' . ($activeSheet->getHighestRow() + 500));
    }
}
