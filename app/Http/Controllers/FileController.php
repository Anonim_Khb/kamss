<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\UsersFile;
use App\Models\UsersTemplate;
use Notifications;
use Validator;
use File;

class FileController extends Controller
{
    public function __construct()
    {
        $this->middleware('inactive');

        view()->share('active_link', 'file_index');
    }

    public function fileValidation($file, $type)
    {
        if ($type == 'email') {
            $type = self::emailRules();
        } elseif ($type == 'excel') {
            $type = self::excelRules();
        }
        $validator = Validator::make(
            array('file_type' => $file->getClientOriginalExtension()),
            array('file_type' => 'required|in:' . $type)
        );
        if ($validator->fails()) {
            foreach ($validator->errors()->all() as $error) {
                Notifications::danger($error, 'page');
            }

            return 'error';
        }
    }

    public function fileSave($file)
    {
        $random = str_random(20);
        $userFile = new UsersFile();
        $userFile->user_id = \Auth::user()->id;
        $userFile->origin_name = $file->getClientOriginalName();
        $userFile->unique_name = $random . '.' . $file->getClientOriginalExtension();
        $userFile->size = $file->getSize();
        $userFile->save();

        ffcc(config('site.files_email_path'), true);
        $file->move(config('site.files_email_path'), $random . '.' . $file->getClientOriginalExtension());

        return $userFile->id;
    }

    protected function emailRules()
    {
        return 'xls,xlsx,xml,xlsb,xlr,xlsm,sxc,ods,odb,7z,7zip,zip,rar,zipx,tar.bz2,tar.gz,pdf,doc,docx,docm,txt,odm,odf,jpg,jpeg,png,gif';
    }

    protected function excelRules()
    {
        return 'xls,xlsx,xml,xlsb,xlr,xlsm,sxc,ods,odb';
    }

    public function downloadEmailFile($id, UsersFile $file)
    {
        $file = $file->where('id', $id)->first();

        $file->user_id == \Auth::id() ?: abort(403);

        ffcc(config('site.files_email_path') . '/' . $file->unique_name) ?: abort(404);

        return response()->download(config('site.files_email_path') . '/' . $file->unique_name, $file->origin_name);
    }

    public function removeEmailFile(UsersTemplate $template)
    {
        if (request()->has('fileId') && is_numeric(request('fileId'))) {

            self::removeFile(request('fileId'));

            $template->where('file_id', request('fileId'))
                ->where('user_id', \Auth::user()->id)
                ->update([
                    'file_id' => null
                ]);

            return 'remove';
        }
    }

    public function removeFile($file_id)
    {
        $file = new UsersFile();

        $file = $file->where('id', $file_id)
            ->where('user_id', \Auth::user()->id)->first();

        File::delete(config('site.files_email_path') . '/' . $file->unique_name);

        $file->delete();
    }
}
