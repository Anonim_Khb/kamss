<div id="file_upload" data-extension="{{ $extension }}" class="text-center">
    <i class="fa fa-file-excel-o fa-4x"></i>
    <p id="choose_file"><strong>Нажмите, чтобы выбрать файл</strong></p>
</div>
<input id="input_upload_file" type="file" name="file">
