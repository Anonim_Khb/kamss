<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Models\Company;
use Illuminate\Contracts\Validation\Validator;
use Redirect;
use Notifications;

class CheckCompanyAndProposeChangePost extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = Company::rulesEdit('name', 'inn');

        if(ur(\App\Models\User::ROLE_MODERATOR)) {
            $rules = array_add($rules, 'replacement_confirm', 'required_with:replacement_id');
            $rules = array_add($rules, 'replacement_id', 'required_with:replacement_confirm');
        }

        return $rules;
    }

    public function response(Array $errors)
    {
        return redirect()->back()->withInput();
    }


    public function formatErrors(Validator $validator)
    {
        foreach ($validator->errors()->all() as $error) {
            Notifications::danger($error, 'page');
        }

        return $validator->errors()->getMessages();
    }
}
