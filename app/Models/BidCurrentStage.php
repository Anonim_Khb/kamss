<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BidCurrentStage extends Model
{
    protected $table = 'bid_current_stages';

    protected $fillable = [
        'name', 'status',
    ];

    const STATUS_ACTIVE = 'active';
    const STATUS_INACTIVE = 'inactive';
}
