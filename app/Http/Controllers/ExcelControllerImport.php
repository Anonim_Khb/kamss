<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\DTO\bidsExcelDTO;
use App\Http\DTO\clientsExcelDTO;
use App\Http\DTO\companiesExcelDTO;
use App\Http\Requests\ImportExcelForUsersPost;
use App\Jobs\BidsImportProcessing;
use App\Jobs\ClientsImportProcessing;
use App\Jobs\CompaniesImportProcessing;
use App\Models\ImportError;
use Notifications;
use PHPExcel_IOFactory;
use File;
use Validator;

class ExcelControllerImport extends Controller
{
    protected $companiesDTO;
    protected $clientsDTO;
    protected $bidsDTO;
    protected $user;

    public function __construct()
    {
        $this->middleware('inactive');

        view()->share('active_link', 'impex_index');

        $this->companiesDTO = new companiesExcelDTO();
        $this->clientsDTO = new clientsExcelDTO();
        $this->bidsDTO = new bidsExcelDTO();
        $this->user = \Auth::user();
    }

    public function importClientsAndBids(ImportExcelForUsersPost $request)
    {
        $excelFile = $request->file('file');

        /*Validation*/
        $validator = Validator::make(
            array('file_type' => $excelFile->getClientOriginalExtension()),
            array('file_type' => 'required|in:xls,xlsx,xml,xlsb,xlr,xlsm,sxc,ods,odb')
        );
        if ($validator->fails()) {
            foreach ($validator->errors()->all() as $error) {
                Notifications::danger($error, 'page');
            }
            return redirect()->back();
        }

        $objReader = PHPExcel_IOFactory::createReader('Excel2007');
        $objPHPExcel = $objReader->load($excelFile);
        $objPHPExcel->setActiveSheetIndex(0);

        /*Get sheets with names "getTitle"*/
        foreach ($objPHPExcel->getAllSheets() as $sheet) {
            $rowData[$sheet->getTitle()] = $sheet->toArray();
        }

        /*Remove empty strings*/
        $rowData[$this->clientsDTO->getPage('name')] = $this->removeEmptyStrings($rowData[$this->clientsDTO->getPage('name')]);
        $rowData[$this->bidsDTO->getPage('name')] = $this->removeEmptyStrings($rowData[$this->bidsDTO->getPage('name')]);
        $rowData[$this->companiesDTO->getPage('name')] = $this->removeEmptyStrings($rowData[$this->companiesDTO->getPage('name')]);

        /*Remove technical header*/
        $rowData[$this->clientsDTO->getPage('name')] = $this->removeTechnicalHeader($rowData[$this->clientsDTO->getPage('name')]);
        $rowData[$this->bidsDTO->getPage('name')] = $this->removeTechnicalHeader($rowData[$this->bidsDTO->getPage('name')]);
        $rowData[$this->companiesDTO->getPage('name')] = $this->removeTechnicalHeader($rowData[$this->companiesDTO->getPage('name')]);

        /*Processing data*/
        self::processingCompanies($rowData[$this->companiesDTO->getPage('name')]);
        self::processingClients($rowData[$this->clientsDTO->getPage('name')], $request->contacts);
        self::processingBids($rowData[$this->bidsDTO->getPage('name')], $request->bids);

        Notifications::success('Данные загружены, ожидайте их обработку', 'success');
        return redirect()->route('index');
    //        dd(rtrim($rowData['Контакты'][129][5])); // cut .0
    //        dd(array_filter($rowData['Контакты']['666']) ? 'ARRAY' : 'EMPTY'); //if array string not empty
    }

    protected function removeEmptyStrings($sheet)
    {
        $sheet = array_map('array_filter', $sheet);
        $sheet = array_filter($sheet);

        return $sheet;
    }

    protected function removeTechnicalHeader($sheet)
    {
        for ($i = 0; $i < ExcelControllerExport::COLUMNS_START; $i++) {
            unset($sheet[$i]);
        }

        return $sheet;
    }

    protected function processingCompanies($companies)
    {
        foreach ($companies as $company) {
            if (!isset($company[0])) {
                $newCompanies[] = $company;
            }
        }

        if (isset($newCompanies) && count($newCompanies) > 0) {
            $chunkNewCompanies = array_chunk($newCompanies, 50);

            foreach ($chunkNewCompanies as $item) {
                dispatch(new CompaniesImportProcessing($item, $this->user->id));
            }
        }
    }

    protected function processingClients($clients, $changes)
    {
        if (count($clients) > 0) {
            if (!$changes) {
                foreach ($clients as $client) {
                    if (!isset($client[0])) {
                        $userClients[] = $client;
                    }
                }
            } else {
                $userClients = $clients;
            }
        }

        if (isset($userClients) && count($userClients) > 0) {
            $chunkClients = array_chunk($userClients, 50);

            foreach ($chunkClients as $item) {
                dispatch(new ClientsImportProcessing($item, $this->user->id));
            }
        }
    }

    protected function processingBids($bids, $changes)
    {
        if (count($bids) > 0) {
            if (!$changes) {
                foreach ($bids as $bid) {
                    if (!isset($bid[0])) {
                        $userBids[] = $bid;
                    }
                }
            } else {
                $userBids = $bids;
            }
        }

        if (isset($userBids) && count($userBids) > 0) {
            $chunkBids = array_chunk($userBids, 50);

            foreach ($chunkBids as $item) {
                dispatch(new BidsImportProcessing($item, $this->user->id));
            }
        }
    }


    public function errorsViewIndex(ImportError $error)
    {
        $companies = $error->where('user_id', \Auth::id())->where('type', $error::TYPE_COMPANIES)->count();
        $clients = $error->where('user_id', \Auth::id())->where('type', $error::TYPE_CLIENTS)->count();
        $bids = $error->where('user_id', \Auth::id())->where('type', $error::TYPE_BIDS)->count();

        if ($companies || $clients || $bids) {
            \Title::append('Ошибки импорта');
            return view('impex.errors-index', [
                'companies' => $companies,
                'clients' => $clients,
                'bids' => $bids
            ]);
        } else {
            Notifications::success('У Вас больше нет исправлений', 'error');
            return redirect()->route('index');
        }
    }

    public function removeImportAllCorrections(ImportError $error)
    {
        $error->where('user_id', \Auth::id())->delete();

        return 'ok';
    }

    public function removeImportCorrection(ImportError $error)
    {
        $error::removeError(\Auth::id(), request('errorId'));

        return 'ok';
    }

    /*If push button "Correct" on import errors page*/
    public function correctionImportError(ImportError $error)
    {
        $company = $error->where('user_id', \Auth::id())->where('type', $error::TYPE_COMPANIES)->exists();
        $client = $error->where('user_id', \Auth::id())->where('type', $error::TYPE_CLIENTS)->exists();
        $bid = $error->where('user_id', \Auth::id())->where('type', $error::TYPE_BIDS)->exists();

        if ($company) {
            return redirect()->route('company-create-import-error');
        }
        if ($client) {
            return redirect()->route('clients-create-or-update-get', ['id' => '-', 'iec' => true]);
        }
        if ($bid) {
            return redirect()->route('bid-create-or-update-get', ['id' => '-', 'iec' => true]);
        }
    }
}
