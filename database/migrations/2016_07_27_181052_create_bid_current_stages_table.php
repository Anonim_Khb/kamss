<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBidCurrentStagesTable extends Migration
{
    public function up()
    {
        Schema::create('bid_current_stages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('status');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('bid_current_stages');
    }
}
