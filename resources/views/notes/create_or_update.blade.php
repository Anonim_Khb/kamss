@extends('main')

@section('body-title')

    {{ isset($note) ? 'Редактирование заметки' : 'Создание заметки' }}

@endsection

@section('body')
    <div class="bids-pages">
        <div class="register-box">
            <div class="login-box-body">
                @include('basis.notifications-page')
                <form id="createBid"
                      action="{{ route('note-create-update-post', ['id' => isset($note) ? $note->id : null]) }}"
                      method="POST">
                    {!! csrf_field() !!}
                    <div class="form-group has-feedback">
                        <label for="text">Заметка</label>
                        <textarea maxlength="140" rows="4" id="text" type="text" name="text" class="form-control"
                                  required
                                  placeholder="Текст заметки (максимальная длина: 140 символов)">{{ old('text', isset($note) ? $note->text : '') }}</textarea>
                        <span class="fa fa-font form-control-feedback"></span>
                        <div class="tip-block tip-block-under">
                            Поле для Ваших комментариев, наблюдений, заметок и т.д. (максимальная длина: 140 символов)
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary pull-right">
                        {{ isset($note) ? 'Сохранить' : 'Создать' }}
                        <i class="fa fa-check"></i>
                    </button>
                    <br><br>
                </form>
            </div>
        </div>
    </div>
@endsection
