<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Models\Company;
use App\Models\UsersFile;
use App\Models\UsersTemplate;
use Illuminate\Contracts\Validation\Validator;
use Redirect;
use Notifications;

class CompanyCreateFixImportErrorRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = Company::rulesCreate('name', 'inn');
        $rules = array_add($rules, 'error_id', 'required|exists:import_errors,id,user_id,' . \Auth::id());
        return $rules;
    }

    public function response(Array $errors)
    {
        return redirect()->back()->withInput();
    }


    public function formatErrors(Validator $validator)
    {
        foreach ($validator->errors()->all() as $error) {
            Notifications::danger($error, 'page');
        }

        return $validator->errors()->getMessages();
    }
}
