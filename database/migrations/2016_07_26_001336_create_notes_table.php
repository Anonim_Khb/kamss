<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\User;

class CreateNotesTable extends Migration
{
    public function up()
    {
        Schema::create('notes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('author_id');
            $table->string('text');
            $table->string('status')->default(\App\Models\Note::STATUS_ACTIVE);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('notes');
    }
}
