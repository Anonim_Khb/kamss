<?php

namespace App\Listeners;

use App\Events\Own_1_Email;
use App\Models\User;
use App\Models\UsersFile;
use App\Models\UsersTemplate;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;
use Jenssegers\Date\Date;

class SendingOwn_1_Email
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SendEmail $event
     * @return void
     */
    public function handle(Own_1_Email $event)
    {
        $author = User::where('id', auth()->user()->id)->with('companyBranch')->first();
        $own = UsersTemplate::where('id', $event->text)->with('file')->first();

        if ($own->file && !ffcc(config('site.files_email_path') . '/' . $own->file->unique_name)) {
            notify_site()->setUnsuccessSendClientEmailBecouseFileNotification($author->id, $own->name);
        } else {

            $data = [
                'author_treatment' => $author->treatment,
                'author_work_phone' => $author->work_phone ? $author->work_phone : null,
                'author_cell_phone' => $author->cell_phone ? $author->cell_phone : null,
                'author_email' => $author->email,
                'author_branch_region' => $author->companyBranch->region,
                'author_branch_name' => $author->companyBranch->name,
                'author_branch_city' => $author->companyBranch->city,
                'author_branch_address' => $author->companyBranch->address,
                'author_branch_postcode' => $author->companyBranch->postcode,
                'author_signature' => $author->signature,
                'author_signature_status' => $author->signature_status,
                'text' => html_entity_decode($own->text),
                'own_name' => $own->name,
                'own_file' => $own->file,
                'setting_files' => conf_site()->getOne(\App\Models\Setting::FILES_WITH_EMAILS)
            ];

            foreach ($event->clients as $client) {
                if ($client->email) {
                    $data = array_except($data, ['client_email', 'client_token']);
                    $data = array_add($data, 'client_email', $client->email);
                    $data = array_add($data, 'client_token', $client->token);

                    Mail::queue(['html' => 'email.carcasses.' . $event->template],
                        $data, function ($message) use ($data) {
                            $message->from($data['author_email'], $data['author_treatment']);
                            $message->to($data['client_email'])->subject($data['own_name']);
                            if ($data['own_file'] && $data['setting_files']) {
                                $message->attach(config('site.files_email_path') . '/' . $data['own_file']->unique_name, ['as' => $data['own_file']->origin_name]);
                            }
                        });

                    $client->last_send = Date::now();
                    $client->save();
                }
            }
        }
    }
}
