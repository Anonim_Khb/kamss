function changeUserStatus(element, userId, text, url) {
    if (confirm(text)) {
        $.ajax({
            type: "POST",
            dataType: "text",
            url: url,
            data: {userId: userId, _token: $("meta[name='csrf-token']").attr('content')},
            cache: false,
            success: function (data) {
                if (data == 'active') {
                    $(element).removeClass('btn-danger').addClass('btn-success');
                } else {
                    $(element).removeClass('btn-success').addClass('btn-danger');
                }
            }
        });
    }
}

function deleteUser(element, userId, text, url) {
    if (confirm(text)) {
        $.ajax({
            type: "POST",
            dataType: "text",
            url: url,
            data: {userId: userId, _token: $("meta[name='csrf-token']").attr('content')},
            cache: false,
            success: function (data) {
                if (data == 'success') {
                    $(element).parents('tr').hide('slow');
                }
            }
        });
    }
}
