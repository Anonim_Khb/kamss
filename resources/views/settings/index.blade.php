@extends('main')

@section('css')
    <link href="/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type='text/css'>
    <link href="/libs/bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet" type='text/css'>
@endsection

@section('body-title')
    Настройки
@endsection

@section('body')
    <div class="settings-page no-background">
        @foreach($configs as $config)
            <div class="checkbox checkbox-primary settings-checkbox">
                <input id="{{ $config->id }}" type="checkbox" class="styled" name="{{ $config->key }}" {{ !$config->value ?: 'checked'  }}>
                <label for="{{ $config->id }}" class="checkbox-text">{{ $config->description }}</label>
            </div>
        @endforeach

        <div class="form-inline new-entry">
            <h4>
                <strong>Добавить новую запись</strong>
            </h4>
            <div class="input-group has-feedback">
                <input id="new_key" type="text" name="key" class="form-control"
                       placeholder="Введите ключ">
                <span class="fa fa-key form-control-feedback"></span>
            </div>
            <div class="input-group has-feedback">
                <input id="new_value" type="text" name="value" class="form-control"
                       placeholder="Введите значение">
                <span class="fa fa-pencil form-control-feedback"></span>
            </div>
            <div class="input-group has-feedback">
                <input id="new_description" type="text" name="description" class="form-control"
                       placeholder="Введите описание">
                <span class="fa fa-headphones form-control-feedback"></span>
            </div>
            <button id="add_new_config" type="button" class="btn bg-green"><i class="fa fa-save fa-fw"></i></button>
        </div>
    </div>
@endsection

@section('js')
    <script src="/libs/sweetalert2/sweetalert2.min.js"></script>
    <script src="/js/settings.js"></script>
@endsection
