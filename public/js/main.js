var successColor = '#5cb85c';
var errorColor = '#c9302c';
var warningColor = '#ec971f';
var primaryColor = '#428bca';
var infoColor = '#31b0d5';
var defaultColor = '#B9B9B9';

/*Spinner*/
function spinlineStart() {
    $('header').spinline('start', {
        height: '.2em',
        color: '#FFF404',
        initialWidth: '10%',
        frequency: 500,
        step: 10,
        position: 'top'
    });
}
function spinlineFinish() {
    $('header').spinline('finish');
}

spinlineStart(); //Spinner start
$(window).load(function () { //spinner stop
    spinlineFinish();
});

function buttonConfirmation(e, text) {
    if (confirm(text)) {
        return true;
    }
    e.preventDefault();
    return false;
}

function formSending(element) {
    $(element).on('change', function () {
        $(this).closest('form').trigger('submit');
    });
}

$(function () {
    /*Popover*/
    $('[data-toggle="popover"]').popover();

    /*Top notification auto hide*/
    if (!$('.notification-top').is(':empty')) {
        setTimeout(function () {
            $('.notification-top').slideUp('slow');
        }, 4444);
    }

    /*Show and hide tips*/
    $('#tipsBlocks').on('click', function () {
        toastr_options();
        if ($('.tip-block').is(':visible')) {
            $(this).text('Показать подсказки');
            $('#tipsBlocks').css('background', successColor);
            toastr["warning"]('Подсказки скрыты');
        } else {
            $(this).text('Скрыть подсказки');
            $('#tipsBlocks').css('background', warningColor);
            toastr["success"]('Подсказки активированы');
        }
        $('.tip-block').toggle();
    });

    /*Print button*/
    $('.table-print-button').on('click', function () {
        $('.for-print').print();
    });

    /*Notification dropdown of header*/
    $('#notificationMenuList').on('click', function (e) { //Not close dropdown by click
        e.stopPropagation();
    });
});

function toastr_options(position) {
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": true,
        "positionClass": position ? position : "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
}

/*Notifications*/
function deleteNotifications(element, id, event) {
    event.preventDefault();
    $.ajax({
        type: "POST",
        dataType: "text",
        url: '/notifications/delete',
        data: {id: id, _token: $("meta[name='csrf-token']").attr('content')},
        cache: false,
        success: function (data) {
            if (data == 'one') {
                element.closest('a').slideUp();
                afterDeleteNotifications(data, $('#notificationsCount').text());
            } else {
                if (data == 'all') {
                    afterDeleteNotifications(data);
                } else {
                    toastr["error"]('Возникли непредвиденные проблемы');
                }
            }
        }
    });
}
function afterDeleteNotifications(type, n_count) {
    if(type == 'one' && n_count > 1) {
        $('#notificationsCount, #notificationsMenuCount').text(n_count - 1);
    } else {
        $('#headerNotificationsNone').show();
        $('#notificationsCount, .menu-nli').hide();
        $('#notificationsMenuCount').text(0);
        $('#headerBell').addClass('not-exists-notifications');
    }
}

function sidebarSetting() {
    if ($(window).width() > 767) {
        $.ajax({
            type: "POST",
            dataType: "text",
            url: '/config/sidebar/user',
            data: {_token: $("meta[name='csrf-token']").attr('content')},
            cache: false
        });
    }
}

$('.action-dropdown-btn .dropdown-menu li, .animated-jqe').on('mouseenter hover', function () {
    animateDropdownElement($(this));
});
function animateDropdownElement(element) {
    var animateClass = 'rubberBand';
    element.addClass('animated ' + animateClass);
    setTimeout(function () {
        element.removeClass('animated ' + animateClass);
    }, 1100);
}
