<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClientsGroup extends Model
{
    protected $table = 'clients_groups';

    protected $fillable = [
        'name', 'description', 'icon'
    ];

    public function users()
    {
        return $this->belongsToMany('App\Models\Client', 'clients_groups_lists', 'group_id', 'client_id');
    }
}
