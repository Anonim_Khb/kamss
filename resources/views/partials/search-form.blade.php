<form class="no-background form-inline" action="{{ $route }}"
      method="GET">
    <br>
    <div class="form-group">
        <label class="form-control-static"><span><b>
                {{ trans('clients.index.table.sort-status') }}
            </b></span></label>
        <select name="filter" class="form-control">
            <option @if(request('filter') == null) selected
                    @endif value="{{ null }}">{{ trans('clients.index.table.show-all') }}</option>
            <option @if(request('filter') == \App\Models\Client::STATUS_ACTIVE) selected
                    @endif value="{{ \App\Models\Client::STATUS_ACTIVE }}">{{ trans('clients.index.table.show-active') }}</option>
            <option @if(request('filter') == \App\Models\Client::STATUS_INACTIVE) selected
                    @endif value="{{ \App\Models\Client::STATUS_INACTIVE }}">{{ trans('clients.index.table.show-inactive') }}</option>
        </select>
        &nbsp;&nbsp;&nbsp;&nbsp;
    </div>
    <div class="form-group">
        <label class="form-control-static"><span>
                            <b>{{ trans('clients.index.table.sort-group') }}</b>
                        </span>
        </label>
        <select name="group" class="form-control">
            <option @if(request('group') == null) selected
                    @endif value="{{ null }}">{{ trans('clients.index.table.show-all') }}</option>
            @foreach($groups as $group)
                <option @if(request('group') == $group->id) selected
                        @endif value="{{ $group->id }}">{{ $group->name }}</option>
            @endforeach
            <option @if(request('group') == 'without') selected
                    @endif value="{{ 'without' }}">{{ trans('clients.index.table.show-without-group') }}</option>
        </select>
        &nbsp;&nbsp;&nbsp;&nbsp;
    </div>
    <div class="form-group">
        <label class="form-control-static"><span>
                            <b>{{ trans('clients.index.table.sort-keywords') }}</b>
                        </span>
        </label>

        <input type="text" name="keywords" class="form-control" value="{{ request('keywords') }}"
               placeholder="{{ trans('clients.index.table.sort-keywords-placeholder') }}">
        &nbsp;&nbsp;&nbsp;&nbsp;
    </div>
    <div class="form-group">
        <label class="form-control-static"><span>
                <b>{{ trans('clients.index.table.sort-date-title') }}</b>
            </span>
        </label>

        <select name="at_first" class="form-control">
            <option @if(request('at_first') == null) selected
                    @endif value="{{ null }}">{{ trans('clients.index.table.sort-date-new') }}</option>
            <option @if(request('at_first') == 'old') selected
                    @endif value="old">{{ trans('clients.index.table.sort-date-old') }}</option>
        </select>
        &nbsp;&nbsp;&nbsp;&nbsp;
    </div>

    <div class="form-group">
        <label class="form-control-static"><span>
                <b>{{ trans('clients.index.table.display-title') }}</b>
            </span>
        </label>
        <select name="display" class="form-control">
            <option @if(request('display') == null) selected @endif value="{{ null }}">25</option>
            <option @if(request('display') == 50) selected @endif value="50">50</option>
            <option @if(request('display') == 100) selected @endif value="100">100</option>
            <option @if(request('display') == 'all') selected @endif value="all">
                {{ trans('clients.index.table.display-all') }}
            </option>
        </select>
        &nbsp;&nbsp;&nbsp;&nbsp;
    </div>
    <div class="form-group">
        <label class="form-control-static"><span>
                <b>{{ trans('clients.index.table.last-send-title') }}</b>
            </span>
        </label>
        <select name="last_send_filter" class="form-control">
            <option @if(request('last_send_filter') == null) selected @endif value="{{ null }}">
                {{ trans('clients.index.table.last-send-not-more') }}
            </option>
            <option @if(request('last_send_filter') == 'more') selected @endif value="more">
                {{ trans('clients.index.table.last-send-more') }}
            </option>
        </select>
        <input type="number" name="days" class="form-control" value="{{ request('days') }}"
               placeholder="{{ trans('clients.index.table.last-send-placeholder') }}">
    </div>
    <div class="input-group input-group-sm border-bg-blue">
        <span class="input-group-addon bg-blue" id="sizing-addon3">
            <b>{{ trans('clients.index.table.last-send-title') }}</b>
        </span>
        <div class="input-group-btn input-group-sm">
            <select name="last_send_filter" class="form-control">
                <option @if(request('last_send_filter') == null) selected @endif value="{{ null }}">
                    {{ trans('clients.index.table.last-send-not-more') }}
                </option>
                <option @if(request('last_send_filter') == 'more') selected @endif value="more">
                    {{ trans('clients.index.table.last-send-more') }}
                </option>
            </select>
            <input type="number" name="days" class="form-control" value="{{ request('days') }}"
                   aria-describedby="sizing-addon3" placeholder="{{ trans('clients.index.table.last-send-placeholder') }}">
        </div>
    </div>
    &nbsp;&nbsp;&nbsp;&nbsp;
    @if(isset($group_manage))
        <br>
        <div class="form-group">
            <label class="form-control-static"><span>
                <b>{{ trans('clients.index.table.group-member-title') }}</b>
            </span>
            </label>
            <select name="group_member" class="form-control">
                <option @if(request('group_member') == null) selected @endif value="{{ null }}">
                    {{ trans('clients.index.table.group-member-no') }}
                </option>
                <option @if(request('group_member') == 'yes') selected @endif value="yes">
                    {{ trans('clients.index.table.group-member-yes') }}
                </option>
            </select>
        </div>
    @endif
    <button type="submit" class="btn btn-primary glyphicon glyphicon-search"></button>
    <br>
    <br>
</form>
