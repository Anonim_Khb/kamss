<?php

namespace App\Jobs;

use App\Http\DTO\bidsExcelDTO;
use App\Http\DTO\clientsExcelDTO;
use App\Http\DTO\companiesExcelDTO;
use App\Jobs\Job;
use App\Models\Bid;
use App\Models\BidCurrentStage;
use App\Models\Client;
use App\Models\Company;
use App\Models\ImportError;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class BidsImportProcessing extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $bids;
    protected $user_id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($bids, $user_id)
    {
        $this->bids = $bids;
        $this->user_id = $user_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(bidsExcelDTO $dto, ImportError $error, Bid $bidModel)
    {
        foreach ($this->bids as $bid) {
            $array = [
                'id' => isset($bid[$dto->getId('import')]) ? $bid[$dto->getId('import')] : null,
                'client_id' => isset($bid[$dto->getContact('import')]) ? Client::where('name', $bid[$dto->getContact('import')])->value('id') : null,
                'first_contact' => isset($bid[$dto->getFirstContact('import')]) ? $bid[$dto->getFirstContact('import')] : null,
                'engine' => isset($bid[$dto->getEngine('import')]) ? $bid[$dto->getEngine('import')] : null,
                'region' => isset($bid[$dto->getRegion('import')]) ? $bid[$dto->getRegion('import')] : null,
                'source' => isset($bid[$dto->getSource('import')]) ? $bid[$dto->getSource('import')] : null,
                'sector' => isset($bid[$dto->getSector('import')]) ? $bid[$dto->getSector('import')] : null,
                'object_deal' => isset($bid[$dto->getObjectDeal('import')]) ? $bid[$dto->getObjectDeal('import')] : null,
                'category' => isset($bid[$dto->getCategory('import')]) ? $bid[$dto->getCategory('import')] : null,
                'price' => isset($bid[$dto->getPrice('import')]) ? $bid[$dto->getPrice('import')] : null,
                'currency' => isset($bid[$dto->getCurrency('import')]) ? $bid[$dto->getCurrency('import')] : null,
                'payment_term' => isset($bid[$dto->getPaymentTerm('import')]) ? $bid[$dto->getPaymentTerm('import')] : null,
                'last_contact' => isset($bid[$dto->getLastContact('import')]) ? $bid[$dto->getLastContact('import')] : null,
                'next_contact' => isset($bid[$dto->getNextContact('import')]) ? $bid[$dto->getNextContact('import')] : null,
                'next_action' => isset($bid[$dto->getNextAction('import')]) ? $bid[$dto->getNextAction('import')] : null,
                'current_stage' => isset($bid[$dto->getCurrentStages('import')]) ? BidCurrentStage::where('name', $bid[$dto->getCurrentStages('import')])->value('id') : null,
                'comment' => isset($bid[$dto->getComment('import')]) ? $bid[$dto->getComment('import')] : null,
            ];

            $errors = $bidModel->validateCreateOrUpdate($array['id'], $array, $this->user_id);

            if (!is_null($errors)) {
                $error::createError($this->user_id, $error::TYPE_BIDS, $array, $errors);
            } else {
                $bidModel->createOrUpdate($array['id'], $array, $this->user_id);
            }
        }
    }
}
