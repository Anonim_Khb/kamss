<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ImportError extends Model
{
    protected $table = 'import_errors';

    protected $fillable = [
        'user_id', 'type', 'array', 'errors'
    ];

    const TYPE_COMPANIES = 'company';
    const TYPE_CLIENTS = 'client';
    const TYPE_BIDS = 'bid';

    public static function createError($user_id, $type, $data, $errors)
    {
        self::create([
            'user_id' => $user_id,
            'type' => $type,
            'array' => serialize($data),
            'errors' => serialize($errors)
        ]);
    }

    public static function removeError($user_id, $error_id)
    {
        self::where('user_id', $user_id)
            ->where('id', $error_id)->delete();
    }
}
