@extends('main')

@section('css')
    <link href="/libs/bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet" type='text/css'>
    <link href="/libs/select2/dist/css/select2.min.css" rel="stylesheet" type='text/css'>
    <link href="/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type='text/css'>
@endsection

@section('body-title')

    {{ !is_null($client) ? 'Редактирование контакта' : 'Создание нового контакта' }}

@endsection

@section('body')

    <div class="users-create">
        <div class="register-box">

            @include('basis.notifications-page')

            <div class="login-box-body">
                <form action="{{ route('clients-create-or-update-admin', ['id' => !is_null($client) ? $client->id : null]) }}"
                      method="POST">
                    {!! csrf_field() !!}
                    <div class="form-group has-feedback">
                        <label for="client">ФИО</label>
                        <input type="text" name="name" class="form-control text-capitalize"
                               placeholder="Введите ФИО контакта"
                               value="{{ old('name', !is_null($client) ? $client->name : '') }}" required>
                        <span class="fa fa-user form-control-feedback"></span>
                        <span class="tip-block tip-block-under">
                            Введите ФИО Контакта
                        </span>
                    </div>
                    <div class="form-group has-feedback">
                        <label for="client">Эл.адрес</label>
                        <input type="email" name="email" class="form-control"
                               placeholder="Эл.адрес Контакта"
                               value="{{ old('email', !is_null($client) ? $client->email : '') }}">
                        <span class="fa fa-envelope form-control-feedback"></span>
                        <span class="tip-block tip-block-under">
                            Введите эл.адрес Контакта
                        </span>
                    </div>
                    <div class="form-group has-feedback">
                        <label for="client">Телефон</label>
                        <input type="text" name="phone" class="form-control"
                               placeholder="Номер телефона Контакта"
                               value="{{ old('phone', !is_null($client) ? $client->phone : '') }}">
                        <span class="fa fa-phone form-control-feedback"></span>
                        <span class="tip-block tip-block-under">
                            Укажите номер телефона Контакта
                        </span>
                    </div>
                    <div class="form-group has-feedback">
                        <label for="client">Обращение</label>
                        <input type="text" name="treatment" class="form-control text-capitalize"
                               placeholder="Обращение к Контакту"
                               value="{{ old('treatment', !is_null($client) ? $client->treatment : '') }}">
                        <span class="fa fa-volume-up form-control-feedback"></span>
                        <span class="tip-block tip-block-under">
                            Введите обращение к Контакту. <strong>ВНИМАНИЕ!</strong> При отправке готовых шаблонов, обращение к Контакту в письме будет идти согласно данного поля
                        </span>
                    </div>
                    <div class="form-group has-feedback">
                        <label>Организация</label>
                        <select class="form-control select2-company" name="company_id" required>
                            @if(!is_null($client) && !isset($client->error_id))
                                <option id="defaultValue" selected="selected" value="{{ $client->clientCompany->id }}">
                                    {{ $client->clientCompany->name }}</option>
                            @elseif(isset($client->company_id))
                                <option id="defaultValue" selected="selected" value="{{ $client->company_id }}">
                                    {{ $client->company_name }}</option>
                            @endif
                        </select>
                        <span class="fa fa-building form-control-feedback"></span>
                        <a id="plusButtonToCreateNewCompany" class="btn-add-right" href="javascript:void(0);"
                           onclick="addNewCompany('{{ route('company-create') }}')"
                           data-toggle="tooltip" data-placement="left" title="Добавить компанию">
                            @if(conf_site()->getOne(\App\Models\Setting::CHANGE_CLIENT_COMPANY))
                                <i class="fa fa-plus fa-lg"></i>
                            @endif
                        </a>
                        <div class="tip-block tip-block-under">
                            Начните вводить название организации Контакта. Если организация отсутствует в списке, то
                            внесите ее нажав на <i
                                    class="fa fa-plus fa-lg"></i>
                        </div>
                    </div>


                    <div class="form-group has-feedback">
                        <label for="client">Должность</label>
                        <input type="text" name="position" class="form-control"
                               placeholder="Введите должность Контакта"
                               value="{{ old('position', !is_null($client) ? $client->position : '') }}">
                        <span class="fa fa-graduation-cap form-control-feedback"></span>
                        <span class="tip-block tip-block-under">
                            Укажите должность вносимого контакта
                        </span>
                    </div>
                    @if(!$groups->isEmpty())
                        <label for="client">Группы</label>
                        @foreach($groups as $group)
                            <div class="checkbox checkbox-success">
                                <input id="{{ $group->id }}" type="checkbox" class="styled" name="groups[]"
                                       value="{{ $group->id }}"
                                       @if(!is_null($client) && isset($client->groups))
                                       @foreach($client->groups as $client_group)
                                       @if($client_group->id == $group->id)
                                       checked
                                        @endif
                                        @endforeach
                                        @endif
                                >
                                <label for="{{ $group->id }}"><b>{{ $group->name }}</b>&nbsp;<i
                                            class="{{ $group->icon }}"></i></label>
                            </div>
                        @endforeach
                        <span class="tip-block tip-block-under">
                            Выберите те группы, к которым относится данный Контакт
                        </span>
                    @endif
                    <div class="client-crup-bottom">
                        @if(!!is_null($client))
                            <div class="checkbox checkbox-primary">
                                <input id="return" type="checkbox" class="styled" name="return">
                                <label for="return"><strong>Вернуться</strong></label>
                                <span class="tip-block tip-block-under">
                                    <br>Если галочка установлена, то после создания нового Контакта Вы вернетесь на эту страницу
                                </span>
                            </div>
                        @else
                            <div class="client-date-clock">
                                <a href="javascript:void(0);" onclick="$('.client-date-inputs').toggle('slow')">
                                    <i class="fa fa-clock-o fa-2x"></i>
                                </a>
                                <div class="client-date-inputs">
                                    <div class="form-group form-group-sm has-feedback">
                                        <label>Дата создания</label>
                                        <input disabled type="text" class="form-control"
                                               value="{{ !is_null($client) ? $client->created_at : '' }}">
                                        <span class="fa fa-clock-o form-control-feedback"></span>
                                        <span class="tip-block tip-block-under">
                                    Дата создания карточки Контакта
                                </span>
                                    </div>
                                    <div class="form-group form-group-sm has-feedback">
                                        <label>Дата изменения</label>
                                        <input disabled type="text" class="form-control"
                                               value="{{ !is_null($client) ? $client->updated_at : '' }}">
                                        <span class="fa fa-clock-o form-control-feedback"></span>
                                        <span class="tip-block tip-block-under">
                                    Дата предыдущего редактирования карточки Контакта
                                </span>
                                    </div>
                                </div>
                            </div>
                        @endif
                        @if(isset($client->error_id))
                            <input type="number" name="error_id" value="{{ $client->error_id }}" hidden>
                        @endif
                        <div class="pull-right">
                            <button type="submit" class="btn btn-primary btn-flat">
                                Сохранить
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="tipsBlocks">Показать подсказки</div>
    @if(isset($client->error_id))
        @include('impex._btn-remove-correction', ['error_id' => $client->error_id])
    @endif
@endsection

@section('js')
    <script src="/libs/sweetalert2/sweetalert2.min.js"></script>
    <script src="/libs/select2/dist/js/select2.min.js"></script>
    <script src="/libs/select2/dist/js/i18n/ru.js"></script>
    <script src="/js/select2-for-companies.js"></script>
    <script src="/js/client-crup.js"></script>
    <script src="/js/excel-import-errors.js"></script>
@endsection