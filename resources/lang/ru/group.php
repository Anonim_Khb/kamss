<?php

return [

    'menu' => 'Управление группами',

    'title' => [
        'prepend' => 'Админ',
        'index' => 'Группы',
        'create' => 'Создание группы',
        'update' => 'Редактирование группы',
        'add-clients' => 'Все заказчики',
    ],

    'index' => [
        'title' => 'Управление группами',
        'create' => 'Создать',
        'table' => [
            'name' => 'Название',
            'description' => 'Описание',
            'icon' => 'Эмблема',
            'actions' => 'Действия',
            'update-tooltip' => 'Редактировать',
            'delete-tooltip' => 'Удалить',
            'add-clients-tooltip' => 'Добавить в группу',
        ],
    ],

    'create' => [
        'title' => 'Создание группы',
        'btn' => 'Создать',
        'placeholder-name' => 'Введите название группы',
        'placeholder-description' => 'Введите описание группы',
        'placeholder-icon' => 'Код эмблемы (fa fa-user fa-fw)',
    ],

    'update' => [
        'title' => 'Редактирование группы',
        'btn' => 'Редактировать',
    ],

    'search' => [
        'no-results' => 'Нет результатов'
    ],

    'notification' => [
        'group-create-or-update' => 'Группа успешно обновлена/создана',
        'group-destroy' => 'Группа спешно удалена',
    ],

    'add-clients' => [
        'title' => 'Все заказчики группы <b>:group</b><i class=":icon fa-lg"></i>',
        'add-in-group-tooltip' => 'Добавить в группу',
    ],

];
