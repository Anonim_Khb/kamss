<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use Notifications;

class SettingsController extends Controller
{
    public function __construct()
    {
        $this->middleware('inactive');

        view()->share('active_link', 'settings_index');
    }

    public function index()
    {
        $configs = conf_site()->getMany();

        \Javascript::put([
            'routeNameChange' => route('config-change-superadmin-ajax'),
            'routeNameAdd' => route('config-add-superadmin-ajax')
        ]);

        \Title::append('Настройки');

        return view('settings.index')->withConfigs($configs);
    }

    public function changeConfigAjax(Setting $setting)
    {
        $setting = $setting->where('key', e(request('config_name')));

        $newValue = $setting->value('value') == true ? 0 : 1;

        $setting->update(['value' => $newValue]);

        return $newValue;
    }

    public function addConfigAjax(Setting $setting)
    {
        $setting->create([
            'key' => request('key'),
            'value' => request('value'),
            'description' => request('description')
        ]);

        return 'ok';
    }

    public function changeUserSidebarSetting(Setting $setting)
    {
        $setting = $setting->firstOrNew([
            'user_id' => \Auth::id(),
            'key' => 'sidebar'
        ]);

        if(!is_null($setting->value)) {
            $setting->value = $setting->value == true ? 0 : 1;
        } else {
            $setting->value = 0;
        }

        $setting->save();

        return $setting->value;
    }
}
