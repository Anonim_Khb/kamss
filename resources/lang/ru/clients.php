<?php

return [

    'menu' => 'Заказчики',

    'title' => [
        'prepend' => 'Админ',
        'index' => 'Заказчики',
        'create' => 'Создание заказчика',
        'update' => 'Обновление заказчика',
    ],

    'index' => [
        'title' => 'Список заказчиков',
        'create' => 'Создать',
        'table' => [
            'count' => 'Всего найдено',
            'name' => 'Имя',
            'email' => 'Эл.адрес',
            'phone' => 'Телефон',
            'last-send' => 'Посл.отправка',
            'company' => 'Компания',
            'position' => 'Должность',
            'actions' => 'Действия',
            'status-tooltip' => 'Статус',
            'update-tooltip' => 'Редактировать',
            'delete-tooltip' => 'Удалить',
            'sort-status' => 'Статус: ',
            'sort-group' => 'Группа: ',
            'sort-keywords' => 'Ключевое слово: ',
            'sort-keywords-placeholder' => 'Ключевое слово',
            'show-all' => 'Все',
            'show-active' => 'Активные',
            'show-inactive' => 'Не активные',
            'show-without-group' => 'Без группы',
            'deactivate-tooltip' => 'Деактивировать',
            'activate-tooltip' => 'Активировать',
            'sort-date-title' => 'Сначала',
            'sort-date-new' => 'Новые',
            'sort-date-old' => 'Старые',
            'display-title' => 'Показывать по',
            'display-all' => 'Все',
            'last-send-title' => 'Отправлено',
            'last-send-not-more' => 'Не позднее',
            'last-send-more' => 'Позднее',
            'last-send-placeholder' => 'Дней',
            'group-member-title' => 'Эта группа',
            'group-member-no' => 'Не в группе',
            'group-member-yes' => 'В группе',
            'client-groups' => 'Группы',
            'change-status-tooltip' => 'Сменить статус',
            'watch-tooltip' => 'Просмотр',
            'send' => 'Отправить',
            'created' => 'Создан',
            'updated' => 'Изменен',
            'treatment' => 'Обращение',
            'status' => 'Статус',
            'template' => 'Шаблон',
            'send-btn' => 'Отправить',
        ],
    ],

    'create' => [
        'title' => 'Создание заказчика',
        'btn' => 'Создать',
        'placeholder-name' => 'Введите имя заказчика',
        'placeholder-email' => 'Введите эл.адрес заказчика',
        'placeholder-treatment' => 'Введите обращение к заказчику',
        'placeholder-company' => 'Введите компанию заказчика',
        'placeholder-position' => 'Введите должность заказчика',
        'placeholder-phone' => 'Введите телефон заказчика',
        'return-back' => 'Вернуться на страницу',
    ],

    'update' => [
        'title' => 'Редактирование заказчика',
        'btn' => 'Редактировать',
    ],

    'search' => [
        'no-results' => 'Нет результатов'
    ],

    'form' => [
        'login-title' => 'Войдите для начала работы',
        'login-email-placeholder' => 'Эл.адрес',
        'login-password-placeholder' => 'пароль',
        'login-remember' => 'Запомнить меня',
    ],

    'notification' => [
        'client-create-or-update' => 'Заказчик успешно создан/отредактирован',
        'client-destroy' => 'Заказчик успешно удален',
        'client-change-status' => 'Статус заказчика успешно изменен',
    ],

    'modal' => [
        'btn-close' => 'Закрыть',
    ],

    'clients_create_per-day' => '{0} контактов|{1} контакт|[2,4] контакта|[5,Inf] контактов',

];
