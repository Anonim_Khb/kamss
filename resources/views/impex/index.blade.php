@extends('main')

@section('css')
    <link href="/libs/bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet" type='text/css'>
    <link href="/css/impex.css" rel="stylesheet" type='text/css'>
@endsection

@section('body')

    <div class="no-background impex-index">
        <br>
        @include('basis.notifications-page')
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active text-bold"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Экспорт</a></li>
                <li class="text-bold"><a href="#tab_2" data-toggle="tab" aria-expanded="false">Импорт</a></li>
            </ul>
            <div class="tab-content">
                <!--EXPORT-->
                <div class="tab-pane impex-export-block active" id="tab_1">
                    <div class="tip-block tip-block-under">
                        <h5><strong>Экспорт данных позволит выгрузить на отдельные листы excel документа списки Ваших
                                контактов, запросов и компаний</strong></h5>
                    </div>
                    <form action="{{ route('impex-export') }}" method="POST">
                        {!! csrf_field() !!}
                        <div class="impex-block">
                            <div class="ib-title">Контакты</div>
                            <div class="ib-radio">
                                <div class="radio radio-success radio-inline">
                                    <input type="radio" id="cs_radio1" value="all" name="clients_status" checked>
                                    <label for="cs_radio1">Все контакты</label>
                                </div>
                                <div class="radio radio-warning radio-inline">
                                    <input type="radio" id="cs_radio2" value="active" name="clients_status"
                                           class="ib-radio-filter">
                                    <label for="cs_radio2">Только активные</label>
                                </div>
                                <div class="tip-block tip-block-under">
                                    Устанавливая флажок на отметку "Только активные" Вы исключаете из отчета контакты,
                                    которые самостоятельно отписались от рассылок писем или Вы самостотельно изменили
                                    их статус.
                                </div>
                            </div>
                            <div class="ib-radio">
                                <div class="radio radio-success radio-inline">
                                    <input type="radio" id="cc_radio1" value="all" name="clients_created" checked>
                                    <label for="cc_radio1">За все время</label>
                                </div>
                                <div class="radio radio-warning radio-inline">
                                    <input type="radio" id="cc_radio2" value="limit" name="clients_created"
                                           class="ib-radio-filter">
                                    <label for="cc_radio2">Указать период</label>
                                </div>
                                <div id="ib_cc">
                                    <select name="clients_type">
                                        <option value="created" selected>Созданные</option>
                                        <option value="updated">Измененные</option>
                                    </select>
                                    <span>&nbsp;за последние&nbsp;</span>
                                    <input type="number" name="clients_day_limit" class="ib-num-input" placeholder="№№">
                                    <span>&nbsp;дней</span>
                                </div>
                                <div class="tip-block tip-block-under">
                                    Установленный флажок на отметке "За все время" выведет контакты, созданные Вами за
                                    весь период пользования сервисом. Флажок "Указать период" позволит выводить контакты
                                    созданные/измененные за последние N дней.
                                </div>
                            </div>
                        </div>

                        <div class="impex-block">
                            <div class="ib-title">Запросы</div>
                            <div class="ib-radio">
                                <div class="radio radio-success radio-inline">
                                    <input type="radio" id="bs_radio1" value="all" name="bids_status" checked>
                                    <label for="bs_radio1">Все запросы</label>
                                </div>
                                <div class="radio radio-warning radio-inline">
                                    <input type="radio" id="bs_radio2" value="active" name="bids_status"
                                           class="ib-radio-filter">
                                    <label for="bs_radio2">Только активные</label>
                                </div>
                                <div class="tip-block tip-block-under">
                                    Устанавливая флажок на отметку "Только активные" Вы исключаете из отчета запросы,
                                    статус которых не закрыт, т.е. находящиеся в работе.
                                </div>
                            </div>
                            <div class="ib-radio">
                                <div class="radio radio-success radio-inline">
                                    <input type="radio" id="bc_radio1" value="all" name="bids_created" checked>
                                    <label for="bc_radio1">За все время</label>
                                </div>
                                <div class="radio radio-warning radio-inline">
                                    <input type="radio" id="bc_radio2" value="limit" name="bids_created"
                                           class="ib-radio-filter">
                                    <label for="bc_radio2">Указать период</label>
                                </div>
                                <div id="ib_bc">
                                    <div>
                                        <select name="bids_type">
                                            <option value="created" selected>Созданные</option>
                                            <option value="updated">Измененные</option>
                                        </select>
                                        <span>&nbsp;за последние&nbsp;</span>
                                        <input type="number" name="bids_day_limit" id="exampleInputName2"
                                               placeholder="№№" class="ib-num-input">
                                        <span>&nbsp;дней</span>
                                    </div>
                                </div>
                                <div class="tip-block tip-block-under">
                                    Установленный флажок на отметке "За все время" выведет запросы, созданные Вами за
                                    весь период пользования сервисом. Флажок "Указать период" позволит выводить запросы
                                    созданные/измененные за последние N дней.
                                </div>
                            </div>
                        </div>
                        <div id="ib_export_alert">
                            <div class="callout callout-warning">
                                <strong>Таблицы, полученные путем выставления фильтров (отбор по активности, датам
                                    создания и т.д.)
                                    не должны участвовать в импорте данных.</strong>
                            </div>
                        </div>
                        <div id="sendEmailAll">
                            <div id="sendEmailCheckbox" class="checkbox checkbox-circle checkbox-primary">
                                <input id="checkbox_send_email" name="send_email" class="styled" type="checkbox">
                                <label for="checkbox_send_email">
                                    <strong class="text-blue">Отправить на почту <i class="fa fa-send-o fa-fw fa-lg"></i></strong>
                                </label>
                            </div>
                            <div class="tip-block tip-block-under">
                                Установленный флажок на отметке "Отправить на почту" позволяет Вам отправлять отчет прямиком
                                на эл.почту.
                            </div>
                            <div id="sendEmailAdressAndText">
                                <div class="form-group col-xs-12">
                                    <label>Эл.адрес</label>
                                    <input type="email" name="email_address" class="form-control"
                                           placeholder="Введите Эл.адрес получателя" value="{{ old('email_address') }}">
                                </div>
                                <div class="form-group col-xs-12">
                                    <label for="next_action">Текст сообщения</label>
                                    <textarea rows="3" maxlength="500" type="text" name="email_text" class="form-control"
                                              placeholder="Введите текст сообщение, если это требуется">{{ old('email_text') }}</textarea>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary ib-sub-btns">
                            <i class="fa fa-download fa-fw"></i> Скачать
                        </button>
                    </form>
                </div>
                <!--IMPORT-->
                <div class="tab-pane impex-import-block" id="tab_2">
                    <div class="tip-block tip-block-under">
                        <h5>
                            <strong>Им     позволит Вам загружать данные на сервер прямо из таблиц</strong>
                        </h5>
                    </div>
                    <p class="text-danger">
                        <strong>Внимание! </strong>
                        Должны использоваться только таблицы, полученные при экспорте данных <strong>без
                            фильтров</strong>.
                        В противном случае Ваши данные могут быть затерты, изменения не учтены.
                    </p>
                    <form action="{{ route('impex-import') }}" method="POST" enctype="multipart/form-data">
                    {!! csrf_field() !!}
                    @include('partials._file-upload', ['extension' => 'excel']) <!--Submit button must be '#submitFormWithFileButton'-->
                        <div class="impex-block">
                            <div class="ib-title">Контакты</div>
                            <div class="ib-radio">
                                <div class="radio radio-success radio-inline">
                                    <input type="radio" id="icnt_radio1" value="0" name="contacts" checked>
                                    <label for="icnt_radio1">Нет изменений</label>
                                </div>
                                <div class="radio radio-warning radio-inline">
                                    <input type="radio" id="icnt_radio2" value="1" name="contacts"
                                           class="ibimp-radio-filter">
                                    <label for="icnt_radio2">Изменены старые контакты</label>
                                </div>
                                <div class="tip-block tip-block-under">
                                    В таблице, полученной при экспорте данных, на странице "Контакты" Вы внесли
                                    изменения
                                    в уже существовавщие записи (не путать с добавлением новых контактов).
                                </div>
                            </div>
                            <div class="ib-title">Запросы</div>
                            <div class="ib-radio">
                                <div class="radio radio-success radio-inline">
                                    <input type="radio" id="ibb_radio1" value="0" name="bids" checked>
                                    <label for="ibb_radio1">Нет изменений</label>
                                </div>
                                <div class="radio radio-warning radio-inline">
                                    <input type="radio" id="ibb_radio2" value="1" name="bids"
                                           class="ibimp-radio-filter">
                                    <label for="ibb_radio2">Изменены старые запросы</label>
                                </div>
                                <div class="tip-block tip-block-under">
                                    В таблице, полученной при экспорте данных, на странице "Запросы" Вы внесли изменения
                                    в уже существовавщие записи (не путать с добавлением новых запросов).
                                </div>
                            </div>
                        </div>
                        <div id="ib_import_alert">
                            <div class="callout callout-warning">
                                <strong>Прикрепляемая Вами таблица должна быть получена при экспорте данных. При этом,
                                    чтобы избежать недочетов и расхождений, экспорт надо производить без фильтрации
                                    результатов по типу и/или срокам.
                                    <br>
                                    Данные на сервере (сайте) будут заменены данными из таблиц, в которых, как Вы
                                    указываете, были сделаны изменения уже существующих записей.
                                </strong>
                            </div>
                        </div>
                        <br>
                        <button id="submitFormWithFileButton" type="submit" class="btn btn-primary" disabled>
                            Загрузить
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div id="tipsBlocks">Показать подсказки</div>
@endsection

@section('js')
    <script src="/js/impex.js"></script>
    <script src="/js/file-upload.js"></script>
@endsection
